---
authors: 
    - Lasse Marburg
date: 2024-07-11
hide:
  - navigation
---

Getting Startet with adaptor:ex
===============================

Learn in a few steps how to install adaptor:ex and create and start a first level.

Installation
------------

Install the software [NodeJS](https://nodejs.org) to install adaptor:ex with the NodeJS Package Manager NPM.

> See the [Install](./install.md) chapter if you want to install adaptor:ex with Docker or from source code instead.

Once you have installed NodeJS, open your [console/terminal](./install.md#command-line-terminal) and run the installation from the command line using npm

=== "macOS & Linux"
    ```console
    sudo npm install -g adaptorex --unsafe-perm
    ```
=== "Windows"
    ```console
    npm install -g adaptorex --unsafe-perm
    ```

If the installation was successful, start adaptor:ex by typing the following command in the console:

```console
adaptorex
```

If everything worked, you should see something like the following in your command line:

![The console output of a fresh installation](./assets/getting_startet_console.png)

Keep the console open as long as you are working with adaptor:ex. As soon as you close the console adaptor:ex will be closed too.

First steps
---------------

Enter the following URL, the web address of your adaptor:ex setup, in your web browser: [http://localhost:8081](http://localhost:8081)

![The adaptor:ex home page](./assets/no_games.png)

We recommend to use a current version of Google Chrome or Firefox as browser.

> The URL may differ depending on the settings you use to run adaptor:ex.
>
> If you have installed adaptor:ex on a server or another machine you have to replace the URL with the IP or domain name of your server.

### Create a game

Start by creating a new game project. Click on 'New Game' and give your first game a name.

![Create a new game](./assets/new_game.png)

### Add a level
When you click on the newly created game project, you will be send to the game overview. It shows all existing levels.Each game can consist of several levels that can be linked together in various ways.

Create your first level with 'Add new'.

> Games and levels get their own url path, which is used to access them in the web browser. Because of that, the name must not contain spaces or special characters.

Give the level a name and click on `Add new Level`.

![Add a level](./assets/new_level.png)

### Edit a level

Open the level by clicking on its name. This will open the level editor.

Start (*START*) and end point (*QUIT*) of your level already exist.

![The level editor](./assets/empty_level.png)

Find the control action [Log Message](./basics/actions/control/log.md) in the left sidebar, listing all possible [actions](./basics/editor.md#action). Drag and drop it on the center of the page.

A new [State](./basics/editor.md#state) element is created. It contains a [Log Message](./basics/actions/control/log.md) action (*log_1*).

![Add an action](./assets/log_action.png)

Click on the new action to edit it. Write a short text into the `message` input field and select *info* in the `level` dropdown.

![Edit an action](./assets/edit_log_action.png)

To make your level easier to understand, it is a good idea to give all states comprehensive names.

> Also state names must not contain spaces, because they can be used in [variables](./basics/variables.md).

Click the title bar of your log message state and rename it.

![Rename States](./assets/rename_state.png)

Now edit the [Next](./basics/actions/control/next.md) action *next_1* in the *START* state as well: First click on it. Then select the new state (here *HelloWorld*) from the `next state` dropdown to connect it to the *START* state.

![connect 2 states](./assets/connect_log_action.png)

### Start a level

Switch from edit mode to [live mode](./basics/live.md) by clicking on the play button in the upper right corner

![Live Mode Toggle](./assets/live_toggle.png)

The left sidebar containing the actions will close and a menu opens at the right sidebar.

Click on `Create Session` in the right sidebar and then on `Start Session` to create a [Session](./basics/live.md#session) of your level.

![Start a session](./assets/start_session.png)

The newly started session is selected in the right sidebar.

![Select a session](./assets/select_session.png)

The *HelloWorld* state is already highlighted.

When we create a new session from a level, the *START* state is automatically triggered. The *next_1* action has continued to our *HelloWorld* state with the log message right after.

Open the log console by clicking on the cursor icon in the upper right section of the screen.

![Show Log Console](./assets/console_toggle.png)

The latest log message should be 'Hello World!

By clicking on the play icon ![Play Icon](./assets/play_icon.png) of the *HelloWorld* state, you can manually retrigger it. Watch the log console and see if the message appears!

### Automate a level

Switch back to edit mode by clicking the Live Toggle button in the top right corner of the menu bar again.

Find the [Timeout](./basics/actions/time/timeout.md) action in the left sidebar and drag it over the *START* state.

Dragging the action over the state will highlight the state and indicate that the action is being added to that state instead of creating a new state.

> When we add an action to a state that is waiting for something, for example a timer that expires, it automatically replaces any [Next](./basics/actions/control/next.md) action.

Edit the timeout. Enter a small number of seconds in the `timeout` field and select the *HelloWorld* state as `next state`.

![Create a timeout](./assets/timeout.png)

Add a timeout action to the *HelloWorld* state as well. Edit the timeout and this time select *Quit* as `next state`.

If you now switch to live mode and start a new session the states will be triggered in fixed time intervals.

![Multiple sessions at the same time](./assets/automated_session.png)

### End level sessions

Once the *QUIT* state is reached, the session is terminated and disappears from the right sidebar.

Your first session will not end automatically. To end it you can close it in the right sidebar (x) or trigger the *QUIT* state if you have selected the session.

> Each session is always based on the latest version of the level. However, if the state a session is in changes, it must be retriggered before changes take hold.

Next steps
----------------

In the [Glossary](./basics/editor.md) you can find out more about the different basic modules, plugins and actions of adaptor:ex.

[Tutorials](./tutorials/arduino-serial/index.md) guide you step by step through example projects you can implement with adaptor:ex.

For example, learn how to use the Telegram plugin to create a messenger adventure: [Storytelling with Telegram and adaptor eX](./tutorials/telegram-basics/index.md)

Or  you can start to control an Arduino with adaptor:ex: [Arduino Serial](./tutorials/arduino-serial/index.md)