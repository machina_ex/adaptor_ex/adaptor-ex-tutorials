Get Item
==========

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Creates a new or overwrites an existing reference to an item.

The item can then be used within the level under the new reference name.

A list of several items can also be referenced with **Get Item**.

Learn more about [Item References](../../variables.md#item-container-und-level-arguments).

Settings
--------

### reference ame

The name of the reference by which the item can then be addressed within the level.

!!! info
    **reference name** cannot contain spaces.

### Collection

The collection that contains the item that is to be referenced.

### find query

Formulate a database search in the style of a [MongoDB find query](https://docs.mongodb.com/manual/reference/method/db.collection.find/) to load the item.

**find query** uses JSON syntax to determine which item to get.

Find the item named "Hans Hövel":

`{name: "Hans Hövel"}`

Find an item whose `score` variable is greater than 30:

`{score: {$gt:30}}`

Find an item whose `score` variable is greater than the value in the local variable `minimum`:

`{score: {$gt:[[minimum]]}}`

!!! note
    You can omit the outer curly brackets in **find query** if you wish.

Alternatively, you can also specify the `name` property of the item you want to have.

`Ada` is therefore the same as `{name: "Ada"}`

If you specify an empty query for **find query**, meaning only the curly brackets `{}`, all items in the **[collection](#collection)** will be referenced.

### multiple items

*optional*

If **multiple items** is selected, it is possible to reference more than 1 item.

All items that match the specified [find query](#find-query) will then be referenced by the given [name](#name).

If **multiple items** is not selected, only 1 item will be loaded, even if the specified query matches several items.

For details, see the chapter [Variables, Data, References](../../variables.md#referencing-multiple-items).

### sort

*optional*

Specify the order in which the items found with the specified **[find query](#find-query)** are to be referenced.

If you only want to reference a single item (**[multiple items](#multiple-items)** is deactivated), you can still use **sort** to specify  precisely which of the items should be referenced. Only the upmost item is then used.

Under **by**, select the property by which the items are to be sorted.

Use **direction** to specify whether the items should be sorted in ascending (`ascending`) or descending (`descending`) order.

In this example, the `locations` items are sorted according to the size of the value specified for `height`:

!!! action inline end "Get Item"
    ``` json
    {
        "reference": "Location",
        "collection": "locations",
        "query": "{}",
        "multiple": true,
        "sort": [
            {
                "by": "height",
                "direction": 1
            }
        ]
    }
    ```
![Screenshot](./assets/sort_example.png){ style=width:60%; float:left }

!!! note
    The **sort** option has the same function as the `$sort` key within a query. You can find out more about this in the chapter on [Variables](../../variables.md#sort-and-narrow-down-item-references).

### limit

*optional*

Restricts the number of items that are found via the **[find query](#find-query)**. Fewer, but not more items than specified for **limit** will then be referenced.

For example, if you set **limit** to `3`, a maximum of 3 items will be referenced.

**limit** has no effect if [multiple items](#multiple-items) is deactivated.

!!! note
    The **limit** option has the same function as the `$limit` key within a query. You can find out more about this in the chapter on [Variables](../../variables.md#sort-and-narrow-down-item-references).

### skip

*optional*

Skips the first **n** items found via the **[find query](#find-query)**.

For example, if you set **skip** to `2`, only the 3rd item and possibly all subsequent items will be referenced.

If the total number of items found is less than the **skip** value, no item will be referenced.

!!! note
    The **skip** option has the same function as the `$skip` key within a query. You can find out more about this in the chapter on [Variables](../../variables.md#sort-and-narrow-down-item-references).

example
-------

Get an item from the "locations" collection and make it accessible as local item reference "Location".

![locations collection Beispiel](./assets/load_collection_example.png)

Search criterion for the reference is that the `name` variable is "laboratory".

!!! action inline end "Get Item"
    ``` json
    {
        "reference": "Location",
        "collection": "locations",
        "query": "name: \"laboratory\"",
        "multiple": true
    }
    ```
![Loading an item into the level](./assets/load_example.png){ style=width:60%; float:left }

You can then for example use the item inside the [Switch](../logic/switch.md) action.

!!! action inline end "Switch"
    ``` json
    {
        "if": [
            {
            "value": "[[Location.condition]]",
            "equals": [
                "locked"
            ],
            "next": "NoEntry"
            }
        ]
    }
    ```
![Use the new reference inside the switch action](./assets/load_switch_example.png){ style=width:60%; float:left }

Since the `condition` parameter in the found item is `unlocked`, the switch condition will not match and it will go on with *EnterLocation*.