Add Item
=========

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Erstellt ein neues Data Item in einer deiner Collections.

Mehr über [Items](../../variables.md#collections-und-data-items) erfahren.

Settings
--------

### Collection

Wähle die Collection aus, in der das neue Data Item erstellt werden soll.

Unter `Game -> settings -> COLLECTIONS` kannst du deine collections einsehen und neue collections erstellen. 

### variables

Füge dem Item Variablen hinzu, die es von Beginn an hat.

Jedes Item, dass du erstellst, benötigt von Beginn an eine `name` Variable.

Um weitere Variablen hinzuzufügen nutze den `variables -> Settings` Button. Gib eine neue Variable im Eingabefeld ein und drücke auf `Add`.

![Eine Variable zu einem neuen Item hinzufügen](./assets/create_add_variable.png)

Du kannst für jede zusätzliche Variable zwischen den Datentypen *string*, *number*, *boolean* und *array* auswählen.

*string* - Ein text oder eine andere Variable

Nutze eckige Klammern (`[[` und `]]`) um den Wert einer anderen Variable zuzuweisen.

Nutze geschwungene Klammern (`{` und `}`) um ein JS Object zuzuweisen.

*number* - Ein Zahlenwert. Nutze `.` um eine Fließkommazahl anzugeben.

*boolean* - 1 der 0, true oder false.

*array* - Eine liste von Werten

### reference

Ein Name, der Innerhalb des Levels auf das Data Item verweist. So kannst du das Item in folgenden actions mit diesem Referenz Namen adressieren.

Beispiel
--------

Erstelle ein neues Data Item in der `Collection` "locations" mit den Anfangswerten `name` "laboratory" und `condition` "locked", dass im Level als "Lab" `reference` verfügbar sein wird.

![beispiel ein neues Item erstellen](./assets/create_example.png)

Anschließend kannst du das Item z.B. in der [Switch](../logic/switch.md) action verwenden und überprüfen ob die `condition` Variable "locked" oder "open" ist

![Das Item in der switch action verwenden](./assets/create_switch_example.png)
