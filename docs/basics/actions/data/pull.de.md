Remove from list
================

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Entfernt einen oder mehrere Einträge aus einer Liste (einem Array).

Settings
--------

### remove values

Gib hier die Werte an die aus der Liste entfernt werden sollen.

Der Wert in der Liste muss genau mit dem hier angegebenen Wert übereinstimmen. Auch der Datentyp muss identisch sein.

Füge weitere Werte mit `Add Value` hinzu.

Wähle `string` um einen Text oder eine andere Variable und `number` um eine Zahl anzugeben.

Nutze eckige Klammern (`[[` und `]]`) um den Wert einer anderen Variable zuzuweisen.

### from list

Die Array Variable, aus der die Einträge entfernt werden sollen.

Du kannst eine lokale Variable, ein Level Attribute oder Argument oder eine Variable in einem Item angeben.

Du kannst die Variable per Hand formulieren oder aus der VARIABLES TOOLBAR herüberziehen.

Die Variable kann mit oder ohne Umfassende eckige Klammern `[[` und `]]` geschrieben werden.

`Player.friends` ist hier das selbe wie `[[Player.friends]]`

Es muss sich bei der Variable um ein *array* Handeln.

### multiple items

Wenn **multiple items** angewählt ist, ist es möglich Einträge aus Listen in mehr als einem Item zu entfernen.

Es werden alle variablen von Items angepasst, auf die die angegebene Referenz verweist.

Ist **multiple items** nicht angewählt werden immer nur variablen in einem Item angepasst auch wenn die angegebene Referenz auf mehrere Items verweist.

Details dazu findest du im Kapitel [Variablen, Daten, Referenzen](../../variables.md#mehrere-items-referenzieren).

Beispiel
--------

Entferne den Eintrag "Monica Mc Lain" aus der Liste `friends` im Level Argument `Player`

![Beispiel einen Eintrag aus einer Liste zu entfernen](./assets/pull_example.png)