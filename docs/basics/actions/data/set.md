Set Variables
============

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Changes the value of a variable.

If the variable does not exist, it will be created.

Learn more about [Variables](../../variables.md). 

Settings
--------

### Set variables

Enter the variable you want to change here. You can specify a local variable, a level attribute or argument, or a variable in an item.

You can formulate the variable manually or drag it over from the VARIABLES TOOLBAR.

The variable can be written with or without surrounding square brackets `[[` and `]]`.

`Player.favorite_color` is the same as `[[Player.favorite_color]]`.

You cannot specify an item without specifying a variable in the item.

For example, the level argument `Player` cannot be overridden with a plain `string` or `number` value.

### To value

The value to which the variable should be changed.

Choose to specify `string` text or another variable and `number` to specify a number.

Use square brackets (`[[` and `]]`) to assign the value of another variable.

Use curly brackets (`{` and `}`) to assign a JS object.

### multiple items

If **multiple items** is selected, it is possible to change or add variables in more than one item.

All variables of items the specified reference points towards will be adjusted.

If **multiple items** is not selected, only variables in exactly one item are adjusted, even if the specified reference points towards several items.

Details can be found in the chapter [Variables, Data, References](../../variables.md#referencing-multiple-items).

Example
--------

Change the variable `favorite_color` (`Player.favorite_color`) in the level argument `Player` to `purple`

![Eine Variable ändern mit set](./assets/set_example.png)
