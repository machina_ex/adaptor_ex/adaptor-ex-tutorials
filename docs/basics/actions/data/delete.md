Delete Item or variable
=======================

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Remove an item or variable.

## Settings

### Variable

Enter the variable or item that you want to remove here. You can specify a local variable, a level attribute or argument, a variable in an item, or an item.

You can formulate the variable or item manually or drag it over from the VARIABLES TOOLBAR.

The variable or item can be written with or without surrounding square brackets `[[` and `]]`.

`Player.inventory` is the same as here `[[Player.inventory]]`

### multiple items

If **multiple items** is selected, it is possible to delete more than 1 item or to remove a variable in more than one item.

All items or variables of items the specified reference points to, will be deleted.

If **multiple items** is not selected, only 1 item will be deleted or a variable in this one item will be removed, even if the specified reference points towards several items.

Details can be found in the chapter [Variables, Data, References](../../variables.md#referencing-multiple-items).

## Example

Remove the entire data item referenced by the level argument `Player`.

![Ein Item löschen](./assets/delete_item_example.png)

Remove the "inventory" variable in the level argument `Player`

![Eine Variable entfernen](./assets/delete_variable_example.png)
