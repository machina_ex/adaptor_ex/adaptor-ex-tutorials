Get Item
==========

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Erstellt eine neue oder überschreibt eine existierende Referenz auf ein Item.

Das Item kann anschließend mit dem neuen Referenz Namen innerhalb des Levels verwendet werden.

Mit **Get Item** kann auch eine Liste von mehreren Items referenziert werden.

Mehr über [Item Referenzen](../../variables.md#item-container-und-level-arguments) erfahren.

Settings
--------

### reference name

Der name der Referenz, unter der das Item innerhalb des Levels anschließend adressierbar ist.

!!! info
    **reference name** darf keine Leerzeichen enthalten.

### Collection

Die Collection, in der das zu referenzierende Item enthalten ist.

### find query

Formuliere eine Datenbank Suche im Stile einer [MongoDB find query](https://docs.mongodb.com/manual/reference/method/db.collection.find/) um das Item zu laden.

**find query** verwendet JSON syntax um festzustellen welches Item geladen werden soll.

Finde das Item mit dem Namen "Hans Hövel":

`{name: "Hans Hövel"}`

Finde ein Item, dessen `score` Variable größer ist als 30:

`{score: {$gt:30}}`

Finde ein Item, dessen `score` Variable größer ist als der Wert in der lokalen Variable `minimum`:

`{score: {$gt:[[minimum]]}}`

!!! note
    Die äußeren geschweiften Klammern in **find query** kannst du weglassen, wenn du möchtest.

Alternativ kannst du auch die `name` Eigenschaft des Items angeben, dass du haben möchtest.

`Ada` ist also das selbe wie `{name: "Ada"}`

Gibst du für **find query** eine leere Query, also nur die geschweiften Klammern `{}` an, werden alle Items der **[Collection](#collection)** referenziert

### multiple items

*optional*

Wenn **multiple items** angewählt ist, ist es möglich mehr als 1 Item zu referenzieren.

Es werden dann alle Items unter [name](#name) geladen, auf die die angegebene [find query](#find-query) verweist.

Ist **multiple items** nicht angewählt wird immer nur 1 Item unter [name](#name) geladen, auch wenn die angegebene Referenz auf mehrere Items verweist.

Details dazu findest du im Kapitel [Variablen, Daten, Referenzen](../../variables.md#mehrere-items-referenzieren).

### sort

*optional*

Lege fest, in welcher Reihenfolge die Items, die mit der angegebenen **[find query](#find-query)** gefunden werden, referenziert werden sollen.

Willst du nur ein einzelnes Item referenzieren (**[multiple items](#multiple-items)** ist deaktiviert) kannst du über **sort** genauer bestimmen welches der gefundenen Items referenziert werden soll. Es wird dann nur das oberste Item referenziert.

Wähle unter **by** aus, nach welcher Eigenschaft die Items sortiert werden sollen.

Mit **direction** legst du fest, ob die Items in aufsteigender (`ascending`) oder absteigender (`descending`) Reihenfolge sortiert werden sollen.

In diesem Beispiel werden die `locations` Items nach der Größe des Wertes, der für `height` angegeben ist sortiert:

!!! action inline end "Get Item"
    ``` json
    {
        "reference": "Location",
        "collection": "locations",
        "query": "{}",
        "multiple": true,
        "sort": [
            {
                "by": "height",
                "direction": 1
            }
        ]
    }
    ```
![Screenshot](./assets/sort_example.png){ style=width:60%; float:left }

!!! note
    Die **sort** Option hat die selbe Funktion wie der `$sort` key innerhalb einer query. Im Kapitel über [Variablen](../../variables.de.md#item-referenzen-sortieren-und-eingrenzen) findest du mehr dazu heraus.

### limit

*optional*

Beschränkt die Anzahl der Items, die über die **[find query](#find-query)** gefunden werden. Es werden dann ggf. weniger, aber nicht mehr Items als für **limit** angegeben referenziert.

Setzt du z.B. **limit** auf `3`, werden höchstens 3 Items referenziert werden.

**limit** hat keinen Effekt, wenn [multiple items](#multiple-items) deaktiviert ist.

!!! note
    Die **limit** Option hat die selbe Funktion wie der `$limit` key innerhalb einer query. Im Kapitel über [Variablen](../../variables.de.md#item-referenzen-sortieren-und-eingrenzen) findest du mehr dazu heraus.

### skip

*optional*

Überspringt die ersten **n** Items die über die **[find query](#find-query)** gefunden wurden.

Setzt du z.B. **skip** auf `2`, wird erst das 3. Item und ggf. alle darauffolgenden referenziert werden.

Ist die Anzahl der gesamt Items, die gefunden wurden kleiner als der **skip** Wert, wird kein Item referenziert.

!!! note
    Die **skip** Option hat die selbe Funktion wie der `$skip` key innerhalb einer query. Im Kapitel über [Variablen](../../variables.de.md#item-referenzen-sortieren-und-eingrenzen) findest du mehr dazu heraus.

Beispiel
--------

Lade ein Item aus der "locations" Collection in die lokale Item Referenz "Location". 

![locations collection Beispiel](./assets/load_collection_example.png)

Suchkriterium für die Referenz ist, dass die `name` Variable "laboratory" ist.


!!! action inline end "Get Item"
    ``` json
    {
        "reference": "Location",
        "collection": "locations",
        "query": "name: \"laboratory\"",
        "multiple": true
    }
    ```
![Ein Item als Referenz ins Level laden](./assets/load_example.png){ style=width:60%; float:left }

Anschließend kannst du das Item z.B. in der [Switch](../logic/switch.md) action verwenden.

!!! action inline end "Switch"
    ``` json
    {
        "if": [
            {
            "value": "[[Location.condition]]",
            "equals": [
                "locked"
            ],
            "next": "NoEntry"
            }
        ]
    }
    ```
![Die Referenz in der switch action verwenden](./assets/load_switch_example.png){ style=width:60%; float:left }

Since the `condition` parameter in the found item is `unlocked`, the switch condition will not apply and the *EnterLocation* state will be next.