Increase number
===============

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Erhöht oder verringert eine Nummern Variable um einen bestimmten Wert.


Settings
--------

### increase variable

Gib hier die variable an, die du erhöhen willst. Du kannst eine lokale Variable, ein Level Attribute oder Argument oder eine Variable in einem Item angeben.

Du kannst die Variable per Hand formulieren oder aus der VARIABLES TOOLBAR herüberziehen.

Die Variable kann mit oder ohne Umfassende eckige Klammern `[[` und `]]` geschrieben werden.

`Player.score` ist hier das selbe wie `[[Player.score]]`

Es muss sich bei der Variable, wenn sie bereits existiert, um einen *number* oder *integer* Wert Handeln.

### by value

Der Wert, um den die variable erhöht wird.

Stelle ein `-` voran um den Wert stattdessen zu verringern.

Erhöhe die Variable um 6:  
`6`  

Verringere die Variable um 22,5:  
`-22.5`

Nutze eckige Klammern (`[[` und `]]`) um die Variable um den Wert einer anderen Nummern Variable zu erhöhen. 

### multiple items

Wenn **multiple items** angewählt ist, ist es möglich Werte in mehr als einem Item zu erhöhen oder zu verringern.

Es werden alle variablen von Items angepasst, auf die die angegebene Referenz verweist.

Ist **multiple items** nicht angewählt werden immer nur variablen in einem Item angepasst auch wenn die angegebene Referenz auf mehrere Items verweist.

Details dazu findest du im Kapitel [Variablen, Daten, Referenzen](../../variables.md#mehrere-items-referenzieren).

Beispiel
--------

Erhöhe die Variable `score` im Level Argument `Player` um 3

![Einen Variablen Wert erhöhen](./assets/increase_example.png)