Set Lights
==========

**Plugin**: [usb-dmx-pro](../../plugins/usb-dmx-pro.md) | **Mode**: [Run](../../editor.md#run-actions)

Change the DMX values of your DMX-USB-PRO Box.

You can assign a new value to each channel.

In the tutorial [Light control with adaptor eX and the ENTTEC DMXUSB Pro Box](../../../tutorials/lightcontrol-dmx-usb-pro/index.md) you can find out more about the **Set Lights** action.