On MQTT Message
===============

**Plugin**: [MQTT](../../plugins/mqtt.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Abonniere ([subscribe](https://www.hivemq.com/blog/mqtt-essentials-part2-publish-subscribe/)) ein MQTT Topic und reagiere auf eingehende Nachrichten auf diesem Topic.

Settings
--------

### topic

Das [MQTT Topic](https://www.hivemq.com/blog/mqtt-essentials-part-5-mqtt-topics-best-practices/) das abonniert wird um auf eingehende Nachrichten zu reagieren.

Nutze wildcard topics (`+` und `#`) um auf Nachrichten in verschiedenen topics reagieren zu können.

Wenn du ein wildcard topic definierst, kannst du in der if condition mit [topic contains](#topic-contains) festlegen auf welches topic sich die jeweilige Bedingung bezieht.

### if

Überprüfe eingehende Nachrichten auf dem abonnierten **topic** und löse ggf. den `next state` aus.

Im Kapitel [Bedingungen abfragen mit conditions](../../conditions.md) findest du genauer heraus, wie du die eingehende Nachricht abfragen kannst.

Der `value` der Bedingung ist dabei jeweils die eingehende Nachricht.

#### field

Nutze `field` um einen bestimmten Wert innerhalb einer JSON Message abzufragen. Du kannst Punkt Notation verwenden um tieferliegende Werte in der JSON Message anzugeben.

Um in der folgenden JSON Message den "humidity" Wert abzufragen

```json
{
    "sensor":{
        "temperature":20,
        "humidity":120
    }
}
```

Kannst du unter `field`

`sensor.humidity`

angeben.

![Screenshot On MQTT Message action mit field option](./assets/on_message_field_example.png)

#### topic contains

Wenn du ein Wildcard [topic](#topic) angegeben hast, kannst du mit **topic contains** filtern auf welches Topic in dieser condition reagiert werden soll.

Wenn das Topic der Message den **topic contains** Wert enthält, wird die Bedingung abgefragt. **topic contains** filtert dabei immer das vollständige Topic.

Du kannst [Regular Expression](https://regexr.com/) Syntax verwenden um den Filter genauer festzulegen.

Zum Beispiel kannst du so abfragen ob das Topic einen Abschnitt nur am Ende enthält. Eine Message mit Topic `garden/apple-tree/sensor/ph-value` würde etwa mit dem folgenden **topic contains** abgefangen:

`ph-value$`

![Screenshot On MQTT Message action mit topic contains option](./assets/on_message_topic_contains_example.png)

### else

Lege den `next state` fest, der ausgelöst wird, wenn keine der `if` Bedingungen auf die eingegangene Nachricht zugetroffen hat oder wenn du keine if Bedingung angegeben hast.

Wenn du keine if Bedingung definiert hast, wird bei jeder Nachricht, die auf dem angegebenen **topic** oder einem der Topics die der wildcard entsprechen empfangen wird empfangen wird, in den `next state` gewechselt.

### keep listening and queue

*optional*

Schalte den **On MQTT Message** listener Stumm anstatt ihn ganz zu schließen, wenn der State, der **On MQTT Message** enthält, verlassen wird.

Es wird dann zwar nicht weiter auf eingehende MQTT Nachrichten reagiert, die messages werden aber gespeichert. 

Wird der State, der diese **On MQTT Message** Action enthält erneut aufgerufen, werden zunächst die gespeicherten Nachrichten überprüft, bevor der listener beginnt auf weitere Nachrichten zu hören.

!!! info
    Im Kapitel über [Schleifen](../../loops.md) findest du zusätzliche Infos zu [keep listening and queue](../../loops.md#keep-listening-and-queue)

Mit **keep listening and queue** kannst du sicherstellen, dass **On MQTT Message** keine Nachrichten verpasst, während der listener nicht aktiv ist, sollte es möglich sein, dass er später ein weiteres mal aktiviert wird.

Wird **On MQTT Message** mit gespeicherten Nachrichten neu aktiviert, werden die messages beginnend mit der ältesten angewandt. Wird aufgrund einer gespeicherten Nachricht in den `Next State` gewechselt, bleiben ggf. weitere, jüngere Nachrichten gespeichert.

Mit **enabled** kannst du das Verhalten bei Bedarf aktivieren oder deaktivieren.

Die option **max queue length** legt eine Grenze für die Anzahl der Nachrichten fest, die gespeichert werden, wenn **On MQTT Message** stumm geschaltet ist. Wird die Grenze überschritten, werden alte Nachrichten vom Anfang der Liste entfernt.

Setze **max queue length** auf 1 um immer nur die zuletzt eingegangene Nachricht auf dem **[Topic](#topic)** zu speichern.

Ist **max queue length** nicht ausgewählt, ist die Anzahl der MQTT Nachrichten, die gespeichert werden nicht begrenzt.

Deaktiviere **max queue length** über die *Settings* von **keep listening and queue**

!!! Warning
    Eine sehr große Anzahl gespeicherter Nachrichten kann die Performance von adaptor:ex beeinträchtigen.

Im [Live Modus :material-play-circle-outline:{ .is-live }](../../live.md) wird die Gesamt Anzahl der eingereihten Nachrichten, auf die bisher nicht reagiert wurde, an der **On MQTT Message** Action angezeigt :material-clock-outline:{ .is-live }

Action Daten
------------

Du kannst auf die zuletzt Eingegangene MQTT Message über die **On MQTT Message** action data variable zugreifen. Nutze etwa:

`[[state.MyState.onMQTTMessage_1.message]]`

Um auf die zuletzt eingegangenen Message der obersten **On MQTT Message** action im State *MyState* zuzugreifen.

### match

Die Message oder der Teil der Message, die eine [if](#if) Bedingung in der **On MQTT Message** action erfüllt hat.

Beispiel: 

`part of the message`

### message

Gesamte zuletzt eingegangene Message auf dem Topic, dass die **On MQTT Message** action abonniert hat.

Beispiel:

`This is not the only part of the message`

### topic

Genaues Topic als einzelner string, der zuletzt eingegangenen Message, auf dem Topic, dass die **On MQTT Message** action abonniert hat.

Beispiel:

`some/topic`

### topic_levels

Eine Liste aller einzelnen Topic levels, der zuletzt eingegangenen Message, auf dem Topic, dass die **On MQTT Message** action abonniert hat.

Beispiel:

`["some", "topic"]`

### topic_contains

Der Teil des Topic, der mit [topic contains](#topic-contains) gematcht wurde.

Beispiel:

`partof/topic`

### retain

Gibt an, ob die Nachricht **retained** wurde, also vom Broker durch die subscription nachträglich gesendet wurde.

Boolean Wert (`true`/`false`)

### qos

Gibt den [Quality of Service](https://www.hivemq.com/blog/mqtt-essentials-part-6-mqtt-quality-of-service-levels/) Level der Message an.

Kann Ganzzahlwert `0`, `1` oder `2` sein.

### dup

Wenn **dup** `true` ist, handelt es sich um ein Duplikat.

### length

Die Zeichen Anzahl der Message

Beispiel
--------

Sieh dir das Beispiel im [MQTT Plugin](../../plugins/mqtt.de.md#beispiel) Eintrag an um zu erfahren wie du die Action einsetzen kannst.