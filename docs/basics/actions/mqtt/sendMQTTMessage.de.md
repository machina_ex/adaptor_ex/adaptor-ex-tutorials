Send MQTT Message
=================

**Plugin**: [MQTT](../../plugins/mqtt.md) | **Mode**: [Run](../../editor.md#run-actions)

Versende ([publish](https://www.hivemq.com/blog/mqtt-essentials-part2-publish-subscribe/)) MQTT Nachrichten auf einem bestimmten Topic.

Settings
--------

### topic

Das MQTT Topic auf dem die Nachricht gesendet wird.

> MQTT send Operationen erlauben keine wildcard topics und das topic sollte nicht mit einem `/` beginnen.

### message

Die Nachricht, die gesendet wird.

Nutze JSON Notation nutzen um komplexere Daten zu senden.

Beispiel
--------

Sieh dir das Beispiel im [MQTT Plugin](../../plugins/mqtt.de.md#beispiel) Eintrag an.