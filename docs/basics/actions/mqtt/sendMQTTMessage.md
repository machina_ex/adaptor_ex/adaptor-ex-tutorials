Send MQTT Message
=================

**Plugin**: [MQTT](../../plugins/mqtt.md) | **Mode**: [Run](../../editor.md#run-actions)

Send ([publish](https://www.hivemq.com/blog/mqtt-essentials-part2-publish-subscribe/)) MQTT messages on a specific topic.

Settings
--------

### topic

The MQTT topic on which the message is sent.

> MQTT send operations do not allow wildcard topics and the topic should not start with a `/` character.

### message

The message that will be sent.

Use JSON notation to send more complex data.

Example
--------

Take a look at the example in the [MQTT Plugin](../../plugins/mqtt.md#example) chapter.