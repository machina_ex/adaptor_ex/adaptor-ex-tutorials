On MQTT Message
===============

**Plugin**: [MQTT](../../plugins/mqtt.md) | **Mode**: [Listen](../../editor.md#listen-actions)

[Subscribe](https://www.hivemq.com/blog/mqtt-essentials-part2-publish-subscribe/) to an MQTT topic and respond to incoming messages on that topic.

Settings
--------

### topic

The [MQTT Topic](https://www.hivemq.com/blog/mqtt-essentials-part-5-mqtt-topics-best-practices/) that is subscribed to in order to respond to incoming messages.

Use wildcard topics (`+` and `#`) to respond to messages in multiple (sub-)topics.

If you define a wildcard topic, the if condition allows you to specify with [topic contains](#topic-contains) to which exact topic the respective condition refers.

### if

Check incoming messages on the subscribed **topic** and trigger the `next state` if the condition matches.

In the [Conditions](../../conditions.md) chapter you can find out in more detail how to query incoming messages.

The `value` of the condition is the incoming message.

#### field

Use `field` to retrieve a specific value within a JSON message. You can use dot notation to specify deeper values in the JSON message.

To query the "humidity" value in the following JSON message

```json
{
    "sensor":{
        "temperature":20,
        "humidity":120
    }
}
```

use the `field` with

`sensor.humidity`

![Screenshot On MQTT Message action with field option](./assets/on_message_field_example.png)

#### topic contains

If you have specified a wildcard [topic](#topic), you can use **topic contains** to filter which topic should be responded to in this condition.

If the topic of the message contains the **topic contains** value, the condition will be reviewed. **topic contains** always filters the complete topic.

You can use [Regular Expressions](https://regexr.com/) to specify the filter more precisely.

For example you can query if the topic contains a topic section only at the end. A message with topic `garden/apple-tree/sensor/ph-value` would be caught with the following **topic contains**:

`ph-value$`

![Screenshot On MQTT Message action with topic contains option](./assets/on_message_topic_contains_example.png)

### else

Set the `next state` that will be triggered if none of the `if` conditions matched the received message or if you did not specify any if conditions.

If you did not specify an if condition, the system will switch to the `next state` for any message received on the specified **topic** or one of the topics corresponding to the wildcard.

### keep listening and queue

*optional*

Mute the **On MQTT Message** listener instead of closing it completely when the state containing **On MQTT Message** is exited.

The listener will then no longer respond to incoming MQTT messages, but the messages will be stored.

If the state containing the **On MQTT Message** action is called again, the stored messages are checked first before the listener starts listening for further messages.

!!! info
    In the chapter about [loops](../../loops.md) you will find additional information about [keep listening and queue](../../loops.md#keep-listening-and-queue)

With **keep listening and queue** you can ensure that **On MQTT Message** does not miss any messages while the listener is not active, should it be possible for it to be activated again later.

If **On MQTT Message** is reactivated with stored messages, the messages are applied starting with the oldest. If you switch to the `Next State` due to a stored message, other, more recent messages remain stored.

With **enabled** you can activate or deactivate the behavior as required.

The **max queue length** option sets a limit for the number of messages that are stored when **On MQTT Message** is muted. If the limit is exceeded, old messages are removed from the top of the list.

Set **max queue length** to 1 to always save only the latest message received on the **[Topic](#topic)**.

If **max queue length** is not selected, the number of MQTT messages that are stored is not limited.

Deactivate **max queue length** via the *Settings* of **keep listening and queue**

!!! Warning
    A very large number of stored messages can impair the performance of adaptor:ex.

In [live mode :material-play-circle-outline:{ .is-live }](../../live.md), the total number of queued messages that have not yet been responded to is displayed at the **On MQTT Message** action :material-clock-outline:{ .is-live }

Action data
------------

You can access the last received MQTT message via the **On MQTT Message** action data variable. Use for example:

`[[state.MyState.onMQTTMessage_1.message]]`

To access the most recently received message of the top **On MQTT Message** action in the state *MyState*.

### match

The message or the part of the message that has fulfilled an [if](#if) condition in the **On MQTT Message** action.

Example:

`part of the message`

### message

Total last received message on the topic that subscribed to the **On MQTT Message** action.

Example:

`This is not the only part of the message`.

### topic

Exact topic as a single string, of the last received message, on the topic that subscribed to the **On MQTT Message** action.

Example:

`some/topic`

### topic_levels

A list of all the individual topic levels of the most recently received message on the topic to which the **On MQTT Message** action is subscribed.

Example:

`["some", "topic"]`

### topic_contains

The part of the topic that was matched with [topic contains](#topic-contains).

Example:

`partof/topic`

### retain

Indicates whether the message has been **retained**, i.e. subsequently sent by the broker through the subscription.

Boolean Value (`true`/`false`)

### qos

Indicates the [Quality of Service](https://www.hivemq.com/blog/mqtt-essentials-part-6-mqtt-quality-of-service-levels/) level of the message.

Can be an integer value `0`, `1` or `2`.

### dup

If `true`, the Message is a duplicate.

### length

The message length.

Example
--------

Take a look at the example in the [MQTT Plugin](../../plugins/mqtt.md#example) chapter.