Send SMS
========

**Plugin**: [Twilio](../../plugins/twilio.md) | **Mode**: [Run](../../editor.md#run-actions)

Send an SMS from one of your Twilio Phone Items.

Settings
--------

### Twilio Phone

The Twilio plugin phone item that sends the SMS.

### to

Data item with `phone_number` variable to which the SMS should be sent.

### text

SMS text to be sent. Messages longer than 160 characters will be billed by Twilio as 2 or more messages.