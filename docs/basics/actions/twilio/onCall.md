Incoming Call
=============

**Plugin**: [Twilio](../../plugins/twilio.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Wait and respond to an incoming call on one of your Twilio phone items.

Respond to the incoming call in one of three ways:

1. say text and play audio files

Use Twilio's speech generation to have texts read aloud (`Say Text`) or play your own audio files (`Play Audio`).

2 Forward to Twilio Studio Flow

Start a [Studio Flow](https://www.twilio.com/de/docs/studio) previously created in Twilio and forward the call data to the Studio Flow.

Reject Call

Reject the call or indicate that the line is busy.

Settings
--------

### Twilio Phone

The Twilio Plugin phone Item that responds to the call.

### From

Data item with `phone_number` variable whose call is waited for and responded to.

### priority

Specify whether this **Incoming Call** action is prioritised or subordinated when multiple **Incoming Calls**s with the same subscribers (same items in `Twilio Phone` and `From`) are waiting for a call.

This can be important, for example, if **Incoming Call** is used in separate [Paths](../../paths.md) or [Sessions](../../live.md#session).

If several **Incoming Call** actions, with the same subscribers, are waiting for a call, the response of the **Incoming Call** listener with higher `priority` is used.

If several **Incoming Call** actions with the same participants and the same `priority` are waiting for a call, the reaction of the **Incoming Call** listener triggered last is used.

### next state

*optional*

Change to 'next state' when the response to the call is finished and a 'Say Text and Play Audiofiles' has not been interrupted.
### SayPlay

*option: Say Text and Play Audiofiles*

A series of text to speech outputs or audio files that are played within the call.

Add the audio files you want to use here to your `files` folder in your game. See the glossary entry [Files](../../files.md).

Text to Speech Texts specified in `say` are interpreted by the voice and in the language specified in the settings of the Twilio phone item.

You can also customise the voice and language within the **Incoming Call** action.

Select an alternative language or voice via the 'Settings' within the 'sayplay'.

Screenshot of selecting an alternative voice and language](./assets/call_voice.png)

### failed

*option: Say Text and Play Audiofiles*

Select a `next state` to switch to if the call is prematurely interrupted.

### flow

*option: Forward to Twilio Studio Flow*

Select the [Twilio Studio Flow](https://www.twilio.com/de/docs/studio) to be started with the incoming call.

The call data, phone number of the phone item and data item, are forwarded to the flow.

A transferred flow is treated as an "Incoming Call" in Twilio Studio.

If the call is ended or interrupted in the Studio Flow, the `next state` is triggered, if specified.

### reject

*option: Reject Call*

Specify how the call is to be rejected.

**reject** corresponds to rejecting the call by hanging up.

**busy** corresponds to rejecting the call through a busy line.

Example
--------

Drag a new **Incoming Call** action from the TOOLBAR to the STAGE.

Select the option *Say Text and Play Audiofiles*.

Select the first option in the top dropdown](./assets/callin_select.png)

Select the 'Twilio Phone' that is to respond to calls from the data item under 'from'.

In `SayPlay` select an audio file from your [Files](../../files.md).

Add another `SayPlay` action with `Add sayplay`.

Select `Say Text` for this `SayPlay` action.

![Screenshot of an incoming call action with audio file and text to speech](./assets/callin_example.png)

When this action is executed, "Veronica" waits to be called by the phone number of "Player".

As soon as "Player" calls, the call is accepted by Veronica. Immediately afterwards, the audio file "jingle.mp3" is played. Afterwards the text in `say` is read out by the Twilio voice service. `[[Player.name]]` is replaced beforehand by the variable "name" in "Player".

When the text is finished, the call is ended and the `next state` *NewJob* is triggered.

If the call is interrupted beforehand, the `next state` *Retry* is triggered.