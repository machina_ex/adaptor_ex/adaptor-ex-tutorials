Send SMS
========

**Plugin**: [Twilio](../../plugins/twilio.md) | **Mode**: [Run](../../editor.md#run-actions)

Versende eine SMS von einem deiner Twilio Phone Items.

Settings
--------

### Twilio Phone

Das Twilio Plugin phone Item, das die SMS verschickt.

### to

Data Item mit `phone_number` variable, an das die SMS verschickt werden soll.

### text

SMS Text der versendet wird. Nachrichten die länger als 160 Zeichen lang sind, werden von Twilio als 2 oder Mehr Nachrichten abgerechnet.