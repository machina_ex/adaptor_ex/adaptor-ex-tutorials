Outgoing Call
=============

**Plugin**: [Twilio](../../plugins/twilio.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Start a phone call from one of your Twilio phone items.

You can use Twilio's voice generation to have texts read aloud (`Say Text`) or play your own audio files (`Play Audio`).

Several `Say Text` and `Play Audiofile` can be strung together in a call.

When all texts have been read out and audio files have been played, the call is ended. If specified, ending the call switches to `next state`.

If the call was ended early because the connection was interrupted or hung up early, the call changes to `next state` in `failed`, if specified.

Settings
--------

### Twilio Phone

The Twilio plugin phone item that makes the call.

### To

Data item with `phone_number` variable that is called.

### SayPlay

A series of text to speech outputs or audio files that are played within the call.

Add the audio files you want to use here to your `files` folder in your game. See the glossary entry about [Files](../../files.md).

Text to Speech Texts specified in `say` are interpreted by the voice and in the language specified in the settings of the Twilio phone item.

You can also customise the voice and language within the **Outgoing Call** action.

Select an alternative language or voice via the `Settings` within the `sayplay`.

![Screenshot of selecting an alternative voice and language](./assets/call_voice.png)

### next state

### optional

The state to switch to as soon as the lowest `sayplay` action is completed.

### failed

Select a `next state` to switch to if the call is interrupted prematurely.

###

Example
--------

Drag an `Outgoing Call` action onto the STAGE.

Select the 'Twilio Phone' that the data item should call under 'to'.

In `SayPlay` select an audio file from your [Files](../../files.md).

Add another `SayPlay` action with `Add sayplay`.

Select `Say Text` for this `SayPlay` action.

![Screenshot of an outgoing call action with audio file and text to speech](./assets/call_example.png)

If this action is executed, the phone number of "Player" is called by "Veronica". As soon as "Player" answers the call, the audio file "jingle.mp3" is played. Afterwards, the text in `say` is read out by the Twilio voice service. `[[Player.name]]` is replaced by the variable "name" in "Player" beforehand.

When the text is finished, the `next state` *NewJob* is triggered.

If the call is interrupted beforehand, the `next state` *Retry* is triggered.