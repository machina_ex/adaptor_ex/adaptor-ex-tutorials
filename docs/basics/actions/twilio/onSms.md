Incoming SMS
============

**Plugin**: [Twilio](../../plugins/twilio.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Respond to incoming SMS messages sent to one of your Twilio Phone Items.

**Incoming SMS** checks the incoming SMS text with an if... else condition. See the glossary chapter [Querying Conditions](../../conditions.md) for more information.

Settings
--------

### Twilio Phone

The Twilio plugin phone item that responds to the incoming message.

### From

Data item with `phone_number` variable whose SMS is waited for and responded to.

### priority

Specify whether this **Incoming SMS** action is prioritised or subordinated if several **Incoming SMS**s with the same subscribers (same items in `Twilio Phone` and `From`) are waiting for incoming SMS.

This can be important, for example, if **Incoming SMS** is used in separate [Paths](../../paths.md) or [Sessions](../../live.md#session).

If several **Incoming SMS** actions, with the same subscribers, are waiting for incoming SMS, the reaction of the **Incoming SMS** listener with higher `priority` is used.

If several **Incoming SMS** actions with the same subscribers and the same `priority` are waiting for incoming SMS, the reaction of the **Incoming SMS** listener triggered last is used.

### if

*optional*

[React](#reactions) to incoming messages if the [conditions](../../conditions.md) specified for this apply.

### else

*optional*

[React](#reactions) to incoming messages if none of the specified [if](#if) conditions are met.

### Timeout Messages

*optional*

 Send messages to the data item specified under `from` after a certain time has elapsed.

 **Timeout messages** are only sent as long as no messages have been received that have been responded to with [if](#if) or [else](#else).

`timeout` specifies how many seconds elapse before the next SMS is sent.

`respond` contains the SMS messages that will be sent after `timeout` has expired.

The first `timeout` specifies the time that elapses before the first SMS message is sent. The second `timeout` indicates the time that elapses before the second SMS message is sent, and so on.

Reactions
----------

You can react to incoming SMS messages in two ways

### next state

Change to the next state if the incoming SMS text matches the condition.

### respond

React with an SMS response to the incoming SMS if the incoming SMS text matches the condition.

If you specify more than one **respond** text, the next text will be replied to each time the corresponding condition is met.

If you specify both `next state` and `respond`, `next state` is triggered as soon as the last `respond` text is replied to.

Example
--------

First drag a [Send SMS](sendSms.md) action onto the STAGE and formulate a question to be sent to "Player".

![Screenshot sms out with question text](./assets/smsin_example_0.png)

Now drag an **Incoming SMS** onto the STAGE and rename the state so that it directly follows the one you just created (here *WeatherOrNot*).

![Screenshot Incoming SMS action with Yes No query and 2 else answers](./assets/smsin_example_1.png)
![Screenshot Incoming SMS action with Yes No query and 2 else reply](./assets/smsin_example_2.png)

Incoming SMS that contain "yes" in the text, regardless of upper or lower case, lead to a direct reply and the system switches to the *Rainjacket* state.

Incoming SMS that contain "no" in the text lead to a direct response and the system switches to the *Weatherproof* state.

The first incoming SMS that contains neither "no" nor "yes" in the text is answered with "If you think I am insufficiently fully automated, ...". Another SMS that contains neither "no" nor "yes" is answered with "Simple question, simple answer..." and the system switches to the `next state` "AskAgain".