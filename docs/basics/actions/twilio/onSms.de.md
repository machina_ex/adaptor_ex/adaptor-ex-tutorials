Incoming SMS
============

**Plugin**: [Twilio](../../plugins/twilio.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Reagiere auf eingehende SMS die an einen deiner Twilio Phone Items geschickt werden.

**Incoming SMS** überprüft den eingehenden SMS Text mit einer if... else Bedingung. Im Glossar Kapitel [Bedingungen abfragen mit Conditions](../../conditions.md) findest du mehr darüber heraus.

Settings
--------

### Twilio Phone

Das Twilio Plugin phone Item, das auf die eingehende Nachricht reagiert.

### From

Data Item mit `phone_number` variable, auf dessen SMS gewartet und reagiert wird.

### priority

Lege fest, ob diese **Incoming SMS** action vor- oder nachrangig behandelt wird, wenn mehrere **Incoming SMS**s mit gleichen Teilnehmern (selbe Items in `Twilio Phone` und `From`) auf eingehende SMS warten.

Dies kann z.B. wichtig sein, wenn **Incoming SMS** in getrennten [Pfaden](../../paths.md) oder [Sessions](../../live.md#session) verwendet wird.

Warten mehrere **Incoming SMS** actions, mit gleichen Teilnehmern auf eingehende SMS, wird die Reaktion des **Incoming SMS** listener mit höherer `priority` verwendet.

Warten mehrere **Incoming SMS** actions, mit gleichen Teilnehmern und gleicher `priority` auf eingehende SMS, wird die Reaktion des zuletzt ausgelösten **Incoming SMS** listener verwendet.

### if

*optional*

[Reagiere](#reaktionen) auf eingehende Nachrichten, wenn die dazu angegebenen [Bedingungen](../../conditions.md) zutreffen.

### else

*optional*

[Reagiere](#reaktionen) auf eingehende Nachrichten, wenn keine der angegebenen [if](#if) Bedingungen erfüllt ist.

### Timeout Messages

*optional*

 Sende nach Ablauf einer Bestimmten Zeit Nachrichten an das Data Item, dass unter `from` angegeben ist.

 **Timeout Messages** werden nur verschickt, solange keine Nachrichten eingegangen sind, auf die mit [if](#if) oder [else](#else) reagiert wurde.

`timeout` legt fest, wie viele Sekunden vergehen, bis die nächste SMS verschickt wird. 

`respond` enthält die SMS Nachrichten, die jeweils nach Ablauf von `timeout` verschickt werden

Der erste `timeout` gibt die Zeit an, die bis zum versenden der ersten SMS Nachricht vergeht. Der zweite `timeout` gibt die Zeit an, die bis zum Versenden der zweiten SMS Nachricht vergeht und so fort.

Reaktionen
----------

Du kannst auf zwei Arten auf eingehende SMS reagieren

### next state

Wechsel in den nächsten State wenn der eingehende SMS Text der Bedingung entspricht.

### respond

Reagiere mit einer SMS Antwort auf die eingehende SMS, wenn der eingehende SMS Text der Bedingung entspricht.

Wenn du mehrere **respond** Texte Angibst, wird jeweils mit dem nächsten Text geantwortet, jedes mal, wenn die entsprechende Bedingung eintrifft.

Wenn du sowohl `next state` als auch `respond` angibst, wird `next state` ausgelöst sobald mit dem letzte `respond` Text geantwortet wurde.

Beispiel
--------

Ziehe zunächst eine [Send SMS](sendSms.md) action auf die STAGE und formuliere eine Frage, die an "Player" gesendet wird.

![Screenshot sms out mit Fragetext](./assets/smsin_example_0.png)

Ziehe jetzt eine **Incoming SMS** auf die STAGE und benenne den State um, sodass er direkt auf den gerade erstellten folgt (hier *WeatherOrNot*).

![Screenshot Incoming SMS action mt Ja Nein abfrage und 2 else antworten](./assets/smsin_example_1.png)
![Screenshot Incoming SMS action mt Ja Nein abfrage und 2 else antworten](./assets/smsin_example_2.png)

Eingehende SMS, die "ja" im Text enthalten, unabhängig von Groß oder Kleinschreibung, Führen zu einer direkten antwort und es wird in den *Rainjacket* State gewechselt.

Eingehende SMS, die "nein" im Text enthalten führen zu einer direkten Antwort und es wird in den *Weatherproof* State gewechselt.

Die erste Eingehende SMS, die weder "nein" noch "ja" im Text enthält wird mit "Falls du glaubst, ich wäre unzureichend vollautomatisiert, ..." beantwortet. Eine weitere SMS, die weder "nein" noch "ja" enthält wird mit "Einfache Frage, einfache Antwort. ..." beantwortet und es wird in den `next state` "AskAgain" gewechselt.