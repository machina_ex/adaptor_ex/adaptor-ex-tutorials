Twilio Studio Flow
==================

**Plugin**: [Twilio](../../plugins/twilio.md) | **Mode**: [Run](../../editor.md#run-actions)

Start a [Twilio Studio Flow](https://www.twilio.com/de/docs/studio) that you have previously created in Twilio.

You may need to reload the Twilio plugin in `Game > settings` whenever you created a new Studio Flow.

![Press the reload button in the plugin](./assets/flow_plugin_reload.png)

Twilio Studio Flows is particularly useful if you want to access functions of the Twilio Phone API that are not available in adaptor:ex. Among other things, Twilio Studio allows you to evaluate key inputs during phone calls.

Settings
--------

### Twilio Phone

The Twilio Plugin phone item whose phone number is to be used in Twilio Studio Flow.

In Studio Flow, the phone number is accessible as `{{flow.channel.address}}` and can thus be used in all widgets available in Twilio Studio.

### to

Item whose phone number is to be passed as the addressee in the flow.

In the Studio Flow, the phone number is accessible as `{{contact.channel.address}}`.

### flow

The Studio Flow to start.

### parameters

*optional*

Additional variables to be passed to the flow at the start.

The parameters are accessible in the flow under `{{flow.data}}`.

Select **parameters** under `Settings` if you want to use them.

Then add parameters via `Settings` in `parameters`.

![Screenshot Click on Settings, then Add](./assets/flow_parameter.png)

The parameter "myParamter" can be used in Twilio Studio Flow as `{{flow.data.myParameter}}`.

Example
--------

Drag a new **Twilio Studio Flow** action from the Twilio actions onto the STAGE.

Select the Studio Flow you want to start and specify it as `flow` in the action.

![Screenshot studio flows in Twilio dashboard](./assets/flow_dashboard.png)

![Screenshot select the flow from the list](./assets/flow_example.png)

When the *Evaluation* state is triggered, adaptor:ex starts the Twilio Studio flow "Veronica_Registration" with the Veronica Twilio phone number as `{{flow.channel.address}}` and the phone_number property of Player as `{{contact.channel.address}}`.

Studio flows started by adaptor:ex begin with the `REST API` trigger

![Screenshot: Twilio Studio Flow example with call trigger](./assets/flow_trigger.png)

In this Studio Flow example, a call is dispatched to the phone_number of Player.

When the *Evaluation* state is triggered, adaptor:ex starts the Twilio Studio Flow "Veronica_Evaluation" with the Veronica Twilio phone number as `{{flow.channel.address}}` and the phone_number property of Player as `{{contact.channel.address}}`.

Studio Flows that are started by adaptor:ex begin with the `REST API` trigger.

![Screenshot Twilio Studio Flow example with trigger and call](./assets/flow_trigger.png)

In this Studio Flow example, a call is first made to the phone_number of Player.

Extend your **Studio Flow** action with the [Parameter](#parameters) Settings option. In this example, we add **name** as a parameter and enter the variable `[[Player.name]]`.

![Screenshot: Studio Flow Action with name parameter](./assets/flow_parameter_example.png)

In your Twilio Studio Flow you can now access the Player **name** property with `{{flow.data.name}}`.

![Screenshot: Twilio Studio Flow editor with send_message widget](./assets/flow_message_parameter.png)

If this Studio Flow is started, an SMS with personal greeting is sent to `Player`.

![Screenshot: SMS dialog with received message "Hello Ada!"](./assets/flow_example_sms.jpeg)