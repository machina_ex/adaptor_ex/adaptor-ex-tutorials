Outgoing Call
=============

**Plugin**: [Twilio](../../plugins/twilio.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Starte einen Telefonanruf von einem deiner Twilio phone Items.

Du kannst die Twilio Sprachgenerierung nutzen um Texte vorlesen zu lassen (`Say Text`) oder eigene Audiofiles abspielen (`Play Audio`).

Mehrere `Say Text` und `Play Audiofile` können in einem Anruf beliebig hintereinander gereiht werden.

Wenn alle Texte vorgelesen und Audiofiles abgespielt sind, wird der Anruf beendet. Wenn angegeben, wird mit Beenden des Anrufs zu `next state` gewechselt.

Wurde der Anruf frühzeitig beendet, weil die Verbindung abgebrochen oder frühzeitig aufgelegt wurde, wird, wenn angegeben zum `next state` in `failed` gewechselt.

Settings
--------

### Twilio Phone

Das Twilio Plugin phone Item, das den Anruf durchführt.

### To

Data Item mit `phone_number` variable, die angerufen wird.

### SayPlay

Eine Reihe von Text to Speech ausgaben oder Audiofiles die innerhalb des Anrufs abgespielt werden.

Füge Audiofiles, die du hier verwenden willst zu deinem `files` Ordner deines Games hinzu. Schau dir dazu den Glossar Eintrag [Files](../../files.md) an.

Text to Speech Texte, die in `say` angegeben werden, werden von der Stimme und in der Sprache interpretiert, die in den Settings des Twilio phone Item angegeben sind.

Du kannst die Stimme und Sprache aber auch innerhalb der **Outgoing Call** Action anpassen.

Wähle über die `Settings` innerhalb des `sayplay` eine alternative Sprache oder Stimme aus.

![Screenshot eine alternative Stimme und Sprache auswählen](./assets/call_voice.png)

### next state

*optional*

Der State in den gewechselt wird sobald die unterste `sayplay` Aktion abgeschlossen ist.

### failed

Wähle einen `next state` aus, in den gewechselt wird, sollte der Anruf vorzeitig unterbrochen werden.

### 

Beispiel
--------

Ziehe eine `Outgoing Call` action auf die STAGE.

Wähle das `Twilio Phone` aus, dass das Data Item unter `to` anrufen soll.

Wähle in `SayPlay` ein Audiofile aus deinen [Files](../../files.md) aus.

Füge eine weitere `SayPlay` Aktion mit `Add sayplay` hinzu.

Wähle für diese `SayPlay` Aktion `Say Text`.

![Screenshot eine Outgoing Call Action mit Audiofile und text to speech](./assets/call_example.png)

Wird diese Action ausgeführt, wird die Telefonnummer von "Player" von "Veronica" angerufen. Sobald "Player" den Anruf entgegennimmt, wird die Audiodatei "jingle.mp3" abgespielt. Anschließend wird der Text in `say` vom Twilio voice Service vorgelesen. `[[Player.name]]` wird zuvor durch die Variable "name" in "Player" ersetzt.

Ist der Text zu Ende vorgelesen wird der `next state` *NewJob* ausgelöst.

Sollte der Anruf zuvor unterbrochen werden wird der `next state` *Retry* ausgelöst.