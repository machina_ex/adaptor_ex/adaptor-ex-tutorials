On Socket.IO Message
====================

**Plugin**: [Socket.IO](../../plugins/socketio.md) | **Mode**: [Listen](../../editor.md#listen-actions)

React on messages sent from a website or Web APP via a [Socket.IO Namespace](../../plugins/socketio.md#setup).

Settings
--------

### Namespace

The Socket.IO namespace for which incoming messages are to be listened to.

You can add namespaces in the [Socket.IO Plugin](../../plugins/socketio.md) settings.

### room
*optional*

The id of the room in which incoming messages are to be listened to. Only messages from clients that have joined the room with [join](../../plugins/socketio.md#join) are responded to.

If you have linked a data item to a **room** via the [create](../../plugins/socketio.md#create) topic, you can also specify the data item variable here.

If you do not specify a **room**, messages from all clients that are linked to the **namespace** will be listened to.

### topic

The topic of the message.

### if
*optional*

Create a condition that the incoming message must fulfil and a response if the condition is met. If a condition matches, the the specified **next state** will be triggered or a message will be returned to the client (**respond**).

If the potential message payload contains a JS object, you can use **field** to specify which property in the object you want to check for a condition.

Use dot (`.`) notation in **field** to name nested properties in the object.

You can find out more about **if** conditions in the chapter [Conditions Queries with Conditions](../../conditions.md).

The *value* of the **if** condition is the incoming message.

With **respond** you can react directly to the message. If the corresponding condition is met, the **respond** message is returned to the client via socket.io [callback](https://socket.io/docs/v4/client-api/#socketemiteventname-args) or [emitWithAck](https://socket.io/docs/v4/client-api/#socketemitwithackeventname-args).

To return a list or an object select `string` and use javascript array or object syntax. Example:

`["rope", "bracelet", "gemstone"]`

or

`{color: "green", score: 5, friends: ["Grace", "Ada", "Tim"]}`

### else
*optional*

Go to the `next state` or return a value with `respond` if there is no match in any of the **if** conditions.

If you have not specified an **if** condition, **else** is triggered for every message sent on the specified **namespace** and **room** (optional) on the specified **topic**.

### priority
*optional*

It is possible that several **On Socket Message** actions on the same **namespace**, in the same **room** listen to the same topic. In this case, the **respond** message of the action with the highest **priority** is returned to the client.

If **priority** is the same for 2 actions, the **respond** of the action that was activated last is returned.

### keep listening and queue

Mute the **On Socket Message** listener instead of closing it completely when the state containing **On Socket Message** is exited.

The action will then no longer respond to incoming messages, but the messages will be stored in a queue.

If the state containing this **On Socket Message** action is called again, the saved messages are checked first before the listener starts listening for further messages.

!!! info
    In the chapter on [Loops](../../loops.md) you will find additional information on [keep listening and queue](../../loops.md#keep-listening-and-queue)

Action Data
------------

You can access the last received Socket.IO message via the **On Socket.IO Message** action data variable. Use for example:

`[[state.MyState.onSocketioMessage_1.message]]`

To access the last received message of the upmost **On Socket.IO Message** action in the *MyState* state.

### match

The message or the part of the message that matched in an [if](#if) condition in the **On Socket.IO Message** action.

Example:

`part of the message`

### message

Total latest message on the topic that was received by the **On Socket.IO Message** action.

Example:

`This is not the only part of the message`.

### room

The **room** `id` of the **room** in which the message was received.

### topic

The topic of the latest message that was received.

Example:

`someTopic`

Example
--------

In the [Socket.IO plugin documentation](../../plugins/socketio.md#example) you will find an example of how to use the **On Socket.IO Message** action.