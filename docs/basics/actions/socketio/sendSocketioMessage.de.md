Send Socket.IO Message
======================

**Plugin**: [Socket.IO](../../plugins/socketio.md) | **Mode**: [Run](../../editor.md#run-actions)

Versende Nachrichten an angeschlossene Webseiten oder Web APPs über einen [Socket.IO Namespace](../../plugins/socketio.de.md#einrichten).

Settings
--------

### Namespace

Der Socket.IO Namespace über den die Nachricht gesendet werden soll.

Du kannst beliebig viele Namespaces in den [Socket.IO Plugin](../../plugins/socketio.md) Einstellungen hinzufügen.

### room
*optional*

Die id des rooms in dem die Nachricht gesendet werden soll. Nur clients, die dem room mit [join](../../plugins/socketio.md#join) beigetreten sind, können die Nachricht empfangen.

Wenn du über das [create](../../plugins/socketio.md#create) topic ein Data Item mit einem **room** verknüpft hast, kannst du hier auch die Data Item variable angeben.

Gibst du keinen **room** an, wird die Nachricht an alle clients gesendet (broadcast), die mit dem **Namespace** verbunden sind.

### topic

Das Nachrichten topic.

### message

Der Nachrichten Inhalt. Wähle `string`, `number` oder `boolean` um die Nachricht mit dem entsprechenden Datentyp zu versenden.

Um Eine liste oder ein Object zu versenden wähle `string` und nutze javascript array oder object syntax. Beispiel:

`["rope", "bracelet", "gemstone"]`

bzw.

`{color: "green", score: 5, friends: ["Grace", "Ada", "Tim"]}`

### add meta data

Wenn **add meta data** angewählt ist, wird die Nachricht mit zusätzlichen Info Daten versehen. 

Die folgenden properties werden hinzugefügt:

* *timestamp*: Uhrzeit und Datum, an dem die Nachricht gesendet wurde

* *message_id*: eine eindeutige ID für die Nachricht

* *room*: Der room, an den die Nachricht gesendet wurde

* *action*: Informationen über die Send Socket.IO Message action. Enthält die action `id`, `name`, `action` typ und den `plugin` Namen.

* *state*: Der State in dem sich die Send Socket.IO action befindet. Enthält die state `id`, `name` und die `path` liste.

* *session*: Die session, in der die Send Socket.IO action ausgelöst wurde. Enthält session `_id`, `name` und Infos über das `level`.

Die Nachricht selbst wird in der *data* property versendet.

Nachrichten, die so an den client gesendet werden sind immer js Objekte.

Eine Nachricht `my message to the client` im **room** mit der ID `my_room` mit zusätzlichen Metadaten könnte so aussehen:

``` json
{
  "data": "my message to the client",
  "timestamp": 1737027084827,
  "message_id": "mNeMIgMf",
  "room": "my_room",
  "action": {
    "id": "sendSocketioMessage_7XotjX2at4",
    "name": "sendSocketioMessage_1",
    "action": "sendSocketioMessage",
    "plugin": "socketio"
  },
  "state": {
    "id": "_state_dk064rqr",
    "name": "SendObject",
    "path": [
      "main"
    ]
  },
  "session": {
    "_id": "007u7Gz82s7y05BQ",
    "name": "session_j8n1sbDj",
    "level": {
      "name": "socketio",
      "_id": "wGc1NlHMaW4JDUhe"
    }
  }
}
```

Beispiel
--------

In der [Socket.IO Plugin Dokumentation](../../plugins/socketio.de.md#beispiel) findest du ein Beispiel, wie du die **Send Socket.IO Message** Action verwendest.