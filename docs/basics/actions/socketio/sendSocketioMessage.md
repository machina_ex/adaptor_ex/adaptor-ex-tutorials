Send Socket.IO Message
======================

**Plugin**: [Socket.IO](../../plugins/socketio.md) | **Mode**: [Run](../../editor.md#run-actions)

Send messages to connected websites or web apps via  a [Socket.IO Namespace](../../plugins/socketio.md#setup).

Settings
--------

### Namespace

The Socket.IO namespace via which the message will be sent.

You can add namespaces in the [Socket.IO Plugin](../../plugins/socketio.md) settings.

### room
*optional*

The id of the room in which the message will be sent.

Only clients that have joined the room with [join](../../plugins/socketio.md#join) will receive the message.

If you have linked a data item to a **room** via the [create](../../plugins/socketio.md#create) topic, you can also specify the data item variable here.

If you do not specify a **room**, the message will be broadcasted to all clients that are connected to the **namespace**.

### topic

The topic of the message.

### message

The message content. Select `string`, `number` or `boolean` to send the message with the corresponding data type.

To send a list or an object select `string` and use javascript array or object syntax. For example:

`["rope", "bracelet", "gemstone"]`

or

`{color: "green", score: 5, friends: ["Grace", "Ada", "Tim"]}`

### add meta data

If **add meta data** is selected, the message will be provided with additional info data.

The following properties are added:

* *timestamp*: Time and date on which the message was sent

* *message_id*: a unique ID for the message

* *room*: The room to which the message was sent

* *action*: Information about the **Send Socket.IO Message** action. Contains the action `id`, `name`, `action` type and the `plugin` name.

* *state*: The state in which the **Send Socket.IO Message** action is located. Contains the state `id`, `name` and the `path` list.

* *session*: The session in which the **Send Socket.IO Message** action was triggered. Contains session `_id`, `name` and information about the `level`.

The message payload itself is in the *data* property.

Messages that are sent to the client in this way are always js objects.

A message `my message to the client` in room with id `my_room` with additional meta data might look like this:

``` json
{
  "data": "my message to the client",
  "timestamp": 1737027084827,
  "message_id": "mNeMIgMf",
  "room": "my_room",
  "action": {
    "id": "sendSocketioMessage_7XotjX2at4",
    "name": "sendSocketioMessage_1",
    "action": "sendSocketioMessage",
    "plugin": "socketio"
  },
  "state": {
    "id": "_state_dk064rqr",
    "name": "SendObject",
    "path": [
      "main"
    ]
  },
  "session": {
    "_id": "007u7Gz82s7y05BQ",
    "name": "session_j8n1sbDj",
    "level": {
      "name": "socketio",
      "_id": "wGc1NlHMaW4JDUhe"
    }
  }
}
```

Example
--------

The [Socket.IO plugin documentation](../../plugins/socketio.md#example) provides an example of how to use the **Send Socket.IO Message** action.