On Socket.IO Message
====================

**Plugin**: [Socket.IO](../../plugins/socketio.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Reagiere auf Nachrichten, die von einer Webseite oder Web APP über einen Socket.IO Namespace gesendet wurden.

Settings
--------

### Namespace

Der Socket.IO Namespace in dem auf eingehende Nachricht gehört werden soll.

Du kannst Namespaces in den [Socket.IO Plugin](../../plugins/socketio.md) Einstellungen hinzufügen.

### room
*optional*

Die id des rooms in dem auf eingehende Nachrichten gehört werden soll. Es wird nur auf Nachrichten von clients reagiert, die dem room mit [join](../../plugins/socketio.md#join) beigetreten sind.

Wenn du über das [create](../../plugins/socketio.md#create) topic ein Data Item mit einem **room** verknüpft hast, kannst du hier auch die Data Item variable angeben.

Gibst du keinen **room** an, wird auf Nachrichten von allen clients gehört, die mit dem **Namespace** verbunden sind.

### topic

Das Nachrichten topic.

### if
*optional*

Erstelle eine Bedingung, die die eingehende Nachricht erfüllen muss und eine Reaktion, wenn die Bedingung zutrifft. Wenn eine Bedingung erfüllt ist, wird in den angegebenen **next state** gewechselt oder eine Antwort Nachricht zurückgegeben (**respond**).

Wenn der potentielle Nachrichten Payload ein JS Object enthält kannst du mit **field** angeben welche Eigenschaft (Property) im Objekt du auf eine Bedingung überprüfen willst.

Nutze Punkt (`.`) Notation in **field** um verschachtelte Eigenschaften im Objekt zu benennen.

Im Kapitel [Bedingungen Abfragen mit Conditions](../../conditions.md) findest du mehr über **if** Abfragen heraus.

Der Wert (*value*) der **if** Abfrage ist dabei die eingegangene Nachricht.

Mit **respond** kannst du direkt auf die Nachricht reagieren. Trifft die entsprechende Bedingung zu wird die **respond** Nachricht über socket.io [callback](https://socket.io/docs/v4/client-api/#socketemiteventname-args) oder [emitWithAck](https://socket.io/docs/v4/client-api/#socketemitwithackeventname-args) an den client zurückgegeben.

Um Eine liste oder ein Object zurückzugeben wähle `string` und nutze javascript array oder object syntax. Beispiel:

`["rope", "bracelet", "gemstone"]`

bzw.

`{color: "green", score: 5, friends: ["Grace", "Ada", "Tim"]}`

### else
*optional*

Gehe zum `next state` oder gebe eine Wert mit `respond` zurück, wenn keine der **if** Bedingungen zutrifft.

Wenn du keine **if** Bedingung angegeben hast, wird **else** bei jeder Nachricht die auf dem angegebenen **Namespace** und ggf. **room** auf dem angegebenen **topic** gesendet wurde ausgelöst.

### priority
*optional*

Es ist möglich, dass mehrere **On Socket Message** actions auf dem selben **Namespace**, im selben **room** auf das gleiche topic hören. Dann wird an den client die **respond** Nachricht der action mit der höchsten **priority** zurückgegeben. 

Ist die **priority** bei 2 actions gleich, wird die **respond** der action zurückgegeben, die als letztes aktiviert wurde.

### keep listening and queue

Schalte den **On Socket Message** listener Stumm anstatt ihn ganz zu schließen, wenn der State, der **On Socket Message** enthält, verlassen wird.

Es wird dann zwar nicht weiter auf eingehende Nachrichten reagiert, die messages werden aber gespeichert.

Wird der State, der diese **On Socket Message** Action enthält erneut aufgerufen, werden zunächst die gespeicherten Nachrichten überprüft, bevor der listener beginnt auf weitere Nachrichten zu hören.

!!! info
    Im Kapitel über [Schleifen](../../loops.md) findest du zusätzliche Infos zu [keep listening and queue](../../loops.md#keep-listening-and-queue)

Action Daten
------------

Du kannst auf die zuletzt Eingegangene Socket.IO Message über die **On Socket.IO Message** action data variable zugreifen. Nutze etwa:

`[[state.MyState.onSocketioMessage_1.message]]`

Um auf die zuletzt eingegangenen Nachricht der obersten **On Socket.IO Message** action im State *MyState* zuzugreifen.

### match

Die Message oder der Teil der Message, die eine [if](#if) Bedingung in der **On Socket.IO Message** action erfüllt hat.

Beispiel: 

`part of the message`

### message

Gesamte zuletzt eingegangene Message auf dem Topic, dass die **On Socket.IO Message** action empfangen hat.

Beispiel:

`This is not the only part of the message`

### room

Die **room** `id` des **room** in dem die Nachricht eingegangen ist.

### topic

Das Topic der zuletzt eingegangenen Message.

Beispiel:

`someTopic`

Beispiel
--------

In der [Socket.IO Plugin Dokumentation](../../plugins/socketio.de.md#beispiel) findest du ein Beispiel, wie du die **On Socket.IO Message** Action verwendest.