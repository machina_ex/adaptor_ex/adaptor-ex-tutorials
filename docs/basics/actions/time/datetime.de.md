Date and Time
==============

**Plugin**: *Time* | **Mode**: [Listen](../../editor.md#listen-actions)

Löst den nächsten State an einem bestimmten Tag, zu einer bestimmten Uhrzeit aus.

*Date and Time* kann auch genutzt werden um am [aktuellen Tag](#aktueller-tag) zu einer bestimmten Uhrzeit den nächsten State auszulösen.

Settings
--------

### Date and Time

Gib hier das Datum und die Uhrzeit an, zu der der `next state` ausgelöst wird.

Wähle `Pick Date` aus um Datum und Uhrzeit über ein Kalender widget auszuwählen.

#### Manuelle Eingabe

Mit `Custom Date` kannst du Datum und Uhrzeit manuell im [ISO 8601 Time](https://en.wikipedia.org/wiki/ISO_8601) Format angeben. Hier hast du natürlich auch die Möglichkeit eine Variable einzufügen.

Eine ISO 8601 Zeitangabe hat das folgende Format: `YYYY-MM-DDTHH:MM:SS`. 

Jahr, Monat und Tag müssen durch einen Bindestrich `-` getrennt angegeben werden. Die Uhrzeit wird mit Leerzeichen ` ` oder `T` vom datum getrennt. Stunde, Minute und Sekunde werden durch Doppelpunkt `:` getrennt angegeben.

Den 28.06.2023 um 10:13 Uhr würdest du z.B. folgendermaßen angeben:

`2023-06-28T10:13`

Wenn du keine Uhrzeit angibst, wird `next state` um 00:00 des angegebenen Tages ausgelöst.

Wenn der angegebene Zeitpunkt in der Vergangenheit liegt, wird der `next state` sofort ausgelöst.

#### Aktueller Tag

Wenn du kein Datum angibst bezieht sich die Uhrzeit auf den Tag an dem der State, der die *Date and Time* action enthält, ausgelöst wird.

Um den `next state` am aktuellen Tag um 12:33 Uhr auszulösen gibst du z.B. folgendes an:

`12:33`

Sollte die Uhrzeit an diesem Tag in der Vergangenheit liegen, wird der `next state` sofort ausgelöst.

> Nutze die [Schedule](./schedule.md) Time action um den `next state` stattdessen beim nächsten Eintreten der Uhrzeit auszulösen. Also an diesem oder am nächsten Tag, je nachdem ob die Uhrzeit am aktuellen Tag in der Zukunft oder der Vergangenheit liegt.

### next state

Der State, der ausgelöst wird, wenn das angegebene Datum und die angegebene Zeit eintreten.

Beispiel
---------

Erstelle einen neuen State indem du die *Date and Time* (ACTIONS > TIME) action auf die STAGE ziehst.

Wähle `Pick Date` aus und klicke auf das Kalendersymbol unter `Date and Time`. Wähle nun den Zeitpunkt aus, auf den gewartet werden soll.

![Screenshot: Einen Zeitpunkt in der date and time action auswählen](./assets/datetime_example.png)

Gib unter `next state`, den Folge State an.

Wird *AwaitShow* ausgelöst, wartet der *Date and Time* listener bis zum 26.10.2022 um 20:00 Uhr und löst dann den *ShowTime* State aus.