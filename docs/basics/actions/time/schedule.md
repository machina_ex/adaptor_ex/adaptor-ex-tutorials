Schedule
========

**Plugin**: *Time* | **Mode**: [Listen](../../editor.md#listen-actions)

Triggers the next state on an upcoming day of the week, a day of the month, a minute of the current hour, or the next time a time occurs.

The schedule always refers to the next possible point in time, starting from the point in time at which the state containing the _schedule_ action was triggered.

## Settings

_Schedule_ has 4 basic configurations that allow different forms of time specification.

![Screenshot: Die Schedule Auswahlmöglichkeiten](./assets/schedule_settings_options.png)

`Clock Time` - Triggers the `next state` next time a time arrives.

`Minute of Hour` - Triggers the `next state` at a specific minute of the hour.

`Day of the Week` - Triggers the `next state` on a coming day of the week at a specific time.

`Day of the Month` - Triggers the `next state` on a coming day of the month at a specific time.

### time

_Available in: `Clock Time` | `Day of the Week` | `Day of the Month`_

The time on the scheduled day when the `next state` is triggered. Use `Pick Time` to select a time or `Custom` to make a manual entry.

_Time_ must be specified in the following format: `HH:MM:SS`

For example , to set **Time** to 4:30 p.m. and 30 seconds, enter the following:

`16:30:30`

You can omit minutes and/or seconds to indicate the full hour or full minute.

### minutes of the hour

_Available in: `Minute of Hour`_

The minute of the current or next hour when the is `next state` triggered.

For example, set **Minute of Hour** `0` to trigger `next state` on the next full hour or on to trigger on `30` the `next state` next half hour of the clock.

### day of week

_Available in: `Day of the Week`_

A day of the week on which to `next state` trigger.

Use `Select` to select the day of the week or `Custom` to specify the day of the week manually.

The day can be specified as a full English term ( `Sunday`, `Monday`...) , abbreviated ( `Su`, `Mo`...) or numerically beginning with `0` for Sunday.

The next possible arrival at the specified time ( [Time](#time) ) on the corresponding day of the week is scheduled.

If the state containing the _Schedule_ action is triggered at a later time on the same day of the week, the next day of the week is scheduled as the next possible time.

With Occorrence and/or [Month](#month), a day of the week that is later in the future can be planned or it can be determined in which week or month the specified day of the week is located.

### Occurrence Day of Week

_optional_

_Available in: `Day of the Week`_

The occurrence of the specified Day of Week within the month.

A number between `1` and `6` that indicates which appearance of the corresponding day of the week should be scheduled in the current or next month.

For `next state` example, to trigger on the 2nd Wednesday of the current or upcoming month, specify `2` **Occurrence Day of Week** .

If the **Occurrence Day of Week** is greater than the number of occurrences of the specified day of the week, the last occurrence of the specified day of the week is scheduled for the current or upcoming month.

Specify a negative number or `0` ( `-6` to `0`) to assume the last occurrence of the weekday within the month. `0`corresponds to the last occurrence of the weekday within the month.

### day-of-month

_Available in: `Day of the Month`_

The day of the month to `next state` trigger.

Enter a number between 1 and 31 to schedule the next arrival of a day of the month.

The next possible arrival of the specified time ( [Time](#time) ) is scheduled on the next possible arrival of the day of the month.

If the state containing the _Schedule_ action is triggered at a later time on the same day of the month, the corresponding day in the next month is scheduled as the next possible time.

With [Month](#month), the month of the year to which the **Day of Month** refers can be optionally specified.

If **Day of Month** is greater than the number of days in the current month, the next month, or the specified month, the last day of the month is scheduled.

Enter a negative number or `0` ( `-31` to `0`) to assume the end of the month. `0`corresponds to the last day of the month.

### Month

_optional_

_Available in: `Day of the Month` | `Day of the Week`_

The month of the current or upcoming year for which the is scheduled to be triggered `next state`.

Use `Select`to select the month or `Custom` to specify the month manually.

The month can be specified as a full English term ( `January`, `February`...) , abbreviated ( `Jan`, `Feb`...) or numerically beginning with `1`for January.

### next state

_Available in: `Clock Time` | `Minute of Hour` | `Day of the Week` | `Day of the Month`_

The state that is raised when the scheduled time arrives.

## example

Create a new state by dragging the _Schedule_ action (ACTIONS > TIME) onto the STAGE.

Rename the state to `WaitForWeekend`.

Choose `Day of the Week` from the top drop-down menu.

Under **Day of Week** , select the day of the week to wait for and click on the clock icon under `Time` to select the time on the selected day of the week to wait for.

Show `Weekend` as `next state`

![Screenshot: Einen Wochentag Planen](./assets/schedule_example.png)
Create another State with a [Log Message](../control/log.md) action and another _Schedule_ action. Select a different day of the week here and enter it `WaitforWeekend` as `next state`.

![Screenshot: Einen Wochenrhythmus erstellen](./assets/schedule_example2.png)

If the _WaitForWeekend_ State is triggered, the next Friday at 5:00 p.m. it changes to the _Weekend_ State.

The _Schedule_ action in the _Weekend State will now wait until the next Monday arrives and will switch back to the_ _WaitForWeekend_ State at 9:00 am .

![Screenshot: Der Wochenrhythmus](./assets/schedule_example3.png)
