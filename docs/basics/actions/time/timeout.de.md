Timeout
=======

**Plugin**: *Time* | **Mode**: [Listen](../../editor.md#listen-actions)

Löst den nächsten State nach Ablauf einer festgelegten Zeit aus.

Die `timeout` Eigenschaft bestimmt, wie viel Zeit vergeht, bis zu `next state` gewechselt wird.

![Screenshot: In 50 Sekunden explodiert die Bombe](./assets/timeout_example.png)

Zeit-Formatierung
-------------
Zeitangaben können in Sekunden angegeben werden oder - getrennt durch `:` - in Stunden, Minuten und Sekunden (hh:mm:ss).

Bruchteile von Sekunden können durch Punktnotation an letzter Stelle angegeben werden.

### Beispiele
`5` - löst in 5 Sekunden den `next state` aus

`125` - 2 Minuten und 5 Sekunden

`2:05` - ebenfalls 2 Minuten und 5 Sekunden (identisch mit `02:05`)

`01:30:00` - 1 Stunde und 30 Minuten

`48:00:01` - 2 Tage und 1 Sekunde

`0.5` - 500 Millisekunden

`3:33.33` - 3 Minuten, 33 Sekunden und 330 Millisekunden
