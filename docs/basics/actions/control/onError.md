On Error
========

**Plugin**: *Control* | **Mode**: [Listen](../../editor.md#listen-actions)

React on error messages that could occur when executing a [State](../../editor.md#state) during a [Session](../../live.md#session).

You can listen for any type of error in any state or narrow down on the error type and message and also the state in which the error occurred.

Settings
--------

### from

Select whether you want to react to every error in the entire session or to errors in the state in which the **On Error** action is located.

`state` - only error messages that occur in the state in which the **On Error** action is located will be caught.

`session` - error messages that occur in any other path in the current session will also be caught.

### error type name

Select whether you want to react to every error message, to any of the known `AdaptorError` errors, or only to a specific, known error.

`Error` - all error messages triggered in this session or state will be caught.

`AdaptorError` - all error messages in this session or this state that belong to the known adaptor errors are intercepted.

Select `NotFoundError`, `ConnectionError`, `DuplicateError`, `InvalidError`, `OutdatedError`, `ForbiddenError` or `LimitReachedError` to react exclusively to errors of the corresponding type.

``` mermaid
flowchart TD
    E(Error) --> A(AdaptorError)
    E --> U(Any other Error Type)
    A --> one
    subgraph one[" "]
    direction BT
    N(NotFoundError) ~~~ C(ConnectionError)
    D(DuplicateError) ~~~ I(InvalidError)
    O(OutdatedError) ~~~ F(ForbiddenError)
    L(LimitReachedError)
    end
    style U fill:#eee,stroke:#777,stroke-width:1px
    style one fill:#fff,stroke:#111,stroke-width:1px
```

### if

Adds a condition that must be met when an error message occurs. **If** allows you to check the error message in order to react only to a selection of error messages.

You can find out more about **if** queries in the [conditions chapter](../../conditions.md).

You can check error messages in **On Error** with the query types [Equals](../../conditions.md#equals), [Contains](../../conditions.md#contains), [Regular Expression](../../conditions.md#regular-expression) and [Javascript Function](../../conditions.md#javascript-functions).

The `value` within the **if** condition is the error message text.

### else

Triggers a `Next State`, regardless of a condition or if none of the specified **[if](#if)** conditions apply.

If no **[if](#if)** condition is specified, `Next State` is triggered whenever an error occurs in the current session or the associated state (**[from](#from)**) if it matches the specified **[error type name](#error-type-name)**.

Action Data
------------

Like other [Conditions](../../conditions.md), **On Error** stores [Action data](../../variables.md#action-data).

You can access the latest error via the **On Error** action data.

Use for example:

`[[state.MyState.onError_1.message]]`

To access the message of the latest error that was caught.

### name

The name or respectively the exact type of the latest error that was caught

Example: `InvalidError`

### type

The parent type of the latest error that was caught.

If it is a `NotFoundError`, `ConnectionError`, `DuplicateError`, `InvalidError`, `OutdatedError`, `ForbiddenError` or `LimitReachedError`, **type** will be **`AdaptorError`**.

### message

The text of the error message.

Example: `Failed to compile javascript condition Unexpected token '}'`

### match

The part of the error [message](#message) that was matching the **[if](#if)** condition.

Example: `javascript condition`

### state

The state in which the error occurred.

**state** is an object and has the properties `id` and `name`.

Example: `{ id: '_state_OYUN5Nju', name: 'BrokenJavascript' }`

Access the separate values using dot `.` notation. For example, use `[[state.MyState.onError_1.state.name]]` to access the states `name` property.

Examples
---------

### Errors in a specific state

!!! example-file "Example Level File"
    Download: [:material-download: error_handling_example.json](./assets/error_handling_example.json)

Use the **from** `state` option to only intercept error messages in the state in which the **On Error** action is located.

To throw a test error, create a state with [Timeout](../time/timeout.md) action and use a variable as timeout value.

!!! action inline end "Timeout"
    
    ``` json
    {
        "timeout": "[[external_time_value]]",
        "next": "OnItGoes"
    }
    ```
![](./assets/error_state_timeout.png){ style=width:60% }

Add an **On Error** action to the same state and activate the **else** option under *Settings*. Then enter a **Next State** (here `OhNo`).

![Screenshot](./assets/error_state_else.png){ style=width:47%; float: left }
![Screenshot](./assets/error_state_else2.png){ style=width:47%; float: right }

??? action "On Error"
    
    ``` json
    {
        "from": "state",
        "type": "Error",
        "else": {
            "next": "OhNo"
        }
    }
    ```

Create the state (*OhNo*) that is triggered when an error occurs with a [Log Message](./log.md) action.

!!! action inline end "Log Message"
    
    ``` json
    {
        "level": "warn",
        "message": "Something went wrong:\n\n[[state.Wait.onError_1.message]]"
    }
    ```
![Screenshot](./assets/error_state_log.png){ style=width:60% }

Switch to [Live mode :material-play-circle-outline:{ .is-live }](../../live.md), create a [Session](../../live.md#start-a-session-manually) and trigger the *Wait* state.

The specified variable `external_time_value` does not yet exist and an error occurs. **On Error** triggers the *OhNo* state.

![Screenshot](./assets/error_state_live.png)

!!! note
    Of course, via the log console, you can also find out if something has gone wrong without the need of a log action. Depending on which APIs you have integrated into your game, you can forward the error in other ways and adjust the game flow if necessary.

### Errors throughout the session

!!! example-file "Example Level File"
    Download: [:material-download: error_session_example.json](./assets/error_session_example.json)

Use the **from** `session` option to catch errors in the entire session.

Add a [Split Path](./split.md) action to the *START* state to create a separate path (here `error_handling`) for the **On Error** listener.

![Screenshot](./assets/error_session_split.png)

Use the :material-plus-box-outline: buttons to create the first states *Intro* and *ErrorHandling* of the main path (here `story`) and the error handling path.

Add an **On Error** action to the *ErrorHandling* state and select `session` under **from**. Add the **else** option under *Settings* and specify a `next state` in which the error will be processed.

!!! action inline end "On Error"
    
    ``` json
    {
        "from": "session",
        "type": "Error",
        "else": {
            "next": "AnyError"
        }
    }
    ```
![Screenshot](./assets/error_session_anyerror.png){ style=width:60% }

Now extend the *Intro* state of the `story` path with an [On Cue](./onCue.md) action, insert a cue description and forward to the state that will throw the test error (here *OpenCurtain*)

!!! action inline end "On Cue"
    
    ``` json
    {
        "cue": "Before the show begins.",
        "next": "OpenCurtain"
    }
    ```
![Screenshot](./assets/error_session_cue.png){ style=width:60% }

To generate an error, we use the [Send MQTT Message](../mqtt/sendMQTTMessage.md) action.

!!! note
    To follow this example in detail, activate the [MQTT plugin](../../plugins/mqtt.md) and connect to a broker. You can of course also provoke an error message in some other action.

Add [Send MQTT Message](../mqtt/sendMQTTMessage.md) to the *OpenCurtain* state and specify some MQTT topic (here `stage/curtain`) and a message (here `open`).

!!! action inline end "SendMQTT Message"
    
    ``` json
    {
        "topic": "stage/curtain",
        "message": "open"
    }
    ```
![Screenshot](./assets/error_session_send_mqtt.png){ style=width:60% }

Now we have to make sure that the MQTT message cannot be sent successfully.

Open the MQTT plugin settings under `GAME > settings > MQTT` and edit the broker URL so that it no longer correctly refers to an existing MQTT broker.

![Screenshot](./assets/error_session_disconnect_mqtt.png)

> If you are working locally, you can also for example disconnect your Internet connection.

Return to the Level Editor and switch to [Live mode](../../live.md). If you now start a session and trigger the *OpenCurtain* state, the **On Error** action should catch an error because the MQTT message cannot be sent.

![Screenshot](./assets/error_session_live_simple.png)

### Differentiate between error types

You may want to react to different errors in different ways.

In the previous example, you can see in the log console :material-console-line: that sending the MQTT message failed with an error called `ConnectionError`.

![Screenshot](./assets/error_session_log_conn.png)

To react only to `ConnectionError` errors, adjust the **On Error** action in the *ErrorHandling* state or create a new **On Error** action. This time select `ConnectionError` for **[error type name](#error-type-name)**.

![Screenshot](./assets/error_session_connection_error.png)

This way you can ensure, that not every error message is treated in the same way.

### Filter error messages

You can go one step further and check the error message to react to a specific, known error in a customized way.

This time we additionally use the **[if](#if)** option and filter the errors message, so that the listener only reacts to the exact MQTT connection errors from our example above.

Add the **[if](#if)** option via *Settings* to the second **On Error** action.

![Screenshot](./assets/error_session_if1.png){ style=width:47%; float: left }
![Screenshot](./assets/error_session_if2.png){ style=width:47%; float: right }

We use the **contains** condition and distinguish whether the error message contains a text section `MQTT Message`. If the exact error occurs, *SendMQTTFailed* should be triggered as **Next State**.

!!! action inline end "On Error"
    
    ``` json
    {
        "from": "session",
        "type": "ConnectionError",
        "if": [
            {
            "contains": [
                "MQTT Message"
            ],
            "case_sensitive": false,
            "next": "SendMQTTFailed"
            }
        ],
        "else": {
            "next": "SomeConnectionFailed"
        }
    }
    ```
![Screenshot](./assets/error_session_if3.png){ style=width:60% }

To be able to test the condition better, you can add another error source to the `story` path.

For example, use a [Switch Action](../logic/switch.md) with **Javascript** query and insert an intentional syntax error.

!!! action inline end "Switch"
    
    ``` json
    {
        "if": [
            {
                "value": "123",
                "javascript": "if(value) \n  return true\n}",
                "next": "NeverGonnaHappen"
            }
        ]
    }
    ```
![Screenshot](./assets/error_session_brokenjs.png){ style=width:60% }

Checkout the level in live mode :material-play-circle-outline:{ .is-live }.

If you trigger the *OpenCurtain* state, *ErrorHandling* responds with *SendMQTTFailed*. If you trigger the *BrokenJavascript* state instead, the upper **On Error** listener reacts and triggers the *AnyError* state.

![Screen Capture GiF](./assets/error_session_example.gif)