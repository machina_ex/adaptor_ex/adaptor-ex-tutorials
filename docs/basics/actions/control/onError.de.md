On Error
========

**Plugin**: *Control* | **Mode**: [Listen](../../editor.md#listen-actions)

Reagiere auf Fehlermeldungen, die beim ausführen eines [States](../../editor.md#state) in einer [Session](../../live.md#session) auftreten könnten.

Du kannst auf jede Art von Fehler in jedem State hören oder den Fehler Typ und die Fehlermeldung und auch den State in dem der Fehler aufgetreten ist eingrenzen.

Settings
--------

### from

Wähle aus, ob du auf jeden Error in der ganzen Session oder auf Errors im State, in dem sich die **On Error** Action befindet reagieren willst.

`state` - es werden nur Fehlermeldungen abgefangen, die in dem State auftreten, in dem sich die **On Error** action befindet.

`session` - es werden auch Fehlermeldungen abgefangen, die in einem anderen Pfad in der laufenden Session auftreten.

### error type name

Wähle aus, ob du auf jede Fehlermeldung, auf bekannte `AdaptorError` Fehlermeldungen, oder nur auf einen bestimmten, bekannten, Fehlertyp reagieren willst.

`Error` - es werden alle Fehlermeldungen abgefangen die in dieser Session oder diesem State ausgelöst wurden.

`AdaptorError` - es werden alle Fehlermeldungen in dieser Session oder diesem State abgefangen, die zu den bekannten Error Typen gehören.

Wähle `NotFoundError`, `ConnectionError`, `DuplicateError`, `InvalidError`, `OutdatedError`, `ForbiddenError` oder `LimitReachedError` aus um ausschließlich auf Fehlermeldungen des entsprechenden Typs zu reagieren.

``` mermaid
flowchart TD
    E(Error) --> A(AdaptorError)
    E --> U(Any other Error Type)
    A --> one
    subgraph one[" "]
    direction BT
    N(NotFoundError) ~~~ C(ConnectionError)
    D(DuplicateError) ~~~ I(InvalidError)
    O(OutdatedError) ~~~ F(ForbiddenError)
    L(LimitReachedError)
    end
    style U fill:#eee,stroke:#777,stroke-width:1px
    style one fill:#fff,stroke:#111,stroke-width:1px
```

### if

Füge eine Bedingung hinzu, die beim auftreten einer Fehlermeldung gegeben sein muss. **If** erlaubt es, die Fehlermeldung (Error message) zu überprüfen um nur auf eine Auswahl von Fehlermeldungen zu reagieren.

Im Kapitel [Bedingungen abfragen mit conditions](../../conditions.md) findest du mehr über **if** Abfragen heraus.

Du kannst Fehler Messages in **On Error** mit den Abfragetypen [Equals](../../conditions.md#equals), [Contains](../../conditions.md#contains), [Regular Expression](../../conditions.md#regular-expression) und [Javascript Function](../../conditions.md#javascript-functions) überprüfen.

Die Error Message ist jeweils der `value` innerhalb der **if** Bedingung.

### else

Löse einen `Next State` aus, unabhängig von einer Bedingung oder wenn keine der angegebenen **[if](#if)** Bedingungen zutrifft.

Wenn keine **[if](#if)** Bedingung angegeben ist wird `Next State` immer beim Auftreten einer Fehlermeldung in der laufenden Session, bzw. dem zugehörigen State (**[from](#from)**) mit dem angegebenen **[error type name](#error-type-name)** ausgelöst.

Action Data
------------

Wie andere [Conditions](../../conditions.md) speichert auch **On Error** [Action Daten](../../variables.md#action-data).

Über die **On Error** Action Daten kannst du auf die jeweils letzte Fehlermeldung zugreifen.

Nutze etwa:

`[[state.MyState.onError_1.message]]`

Um auf die Message des zuletzt abgefangenen Fehlers zuzugreifen.

### name

Der Name bzw. genaue Typ des zuletzt abgefangenen Fehlers

Beispiel: `InvalidError`

### type

Der übergeordnete Typ des zuletzt abgefangenen Fehlers. 

Wenn es sich um einen `NotFoundError`, `ConnectionError`, `DuplicateError`, `InvalidError`, `OutdatedError`, `ForbiddenError` oder `LimitReachedError` wird **type** **`AdaptorError`** sein.

### message

Der Text der Fehlernachricht.

Beispiel: `Failed to compile javascript condition Unexpected token '}'`

### match

Der Teil der Fehlermeldung ([message](#message)), der die **[if](#if)** Bedingung erfüllt hat.

Beispiel: `javascript condition`

### state

Der State, in dem der Fehler aufgetreten ist.

**state** ist ein Objekt und hat die Eigenschaften `id` und `name`.

Beispiel: `{ id: '_state_OYUN5Nju', name: 'BrokenJavascript' }`

Greife per Punkt `.` Notation auf die einzelnen Werte zurück. Nutze z.B. `[[state.MyState.onError_1.state.name]]` um auf den State Namen zuzugreifen.

Beispiele
---------

### Fehler in einem bestimmten State

!!! example-file "Beispiel Level Datei"
    Download: [:material-download: error_handling_example.json](./assets/error_handling_example.json)

Nutze die **from** `state` Option um nur Fehlermeldungen in dem State abzufangen, in dem sich die **On Error** Action befindet.

Um testweise eine Fehlermeldung zu werfen erstelle einen State mit [Timeout](../time/timeout.md) Action und nutze eine Variable als timeout Wert.

!!! action inline end "Timeout"
    
    ``` json
    {
        "timeout": "[[external_time_value]]",
        "next": "OnItGoes"
    }
    ```
![](./assets/error_state_timeout.png){ style=width:60% }

Füge eine **On Error** Action hinzu und aktiviere unter *Settings* die **else** Option. Gib anschließend einen **Next State** an (hier `OhNo`).

![Screenshot](./assets/error_state_else.png){ style=width:47%; float: left }
![Screenshot](./assets/error_state_else2.png){ style=width:47%; float: right }

??? action "On Error"
    
    ``` json
    {
        "from": "state",
        "type": "Error",
        "else": {
            "next": "OhNo"
        }
    }
    ```

Erstelle den State (*OhNo*), der ausgelöst wird, wenn ein Fehler auftritt mit einer [Log Message](./log.md) Action.

!!! action inline end "Log Message"
    
    ``` json
    {
        "level": "warn",
        "message": "Something went wrong:\n\n[[state.Wait.onError_1.message]]"
    }
    ```
![Screenshot](./assets/error_state_log.png){ style=width:60% }

Wechsle in den [Live Modus :material-play-circle-outline:{ .is-live }](../../live.md), erstelle eine [Session](../../live.md#start-a-session-manually) und löse den *Wait* State aus.

Die angegebene Variable `external_time_value` existiert noch nicht und ein Fehler tritt auf. **On Error** löst den *OhNo* State aus.

![Screenshot](./assets/error_state_live.png)

!!! note
    Du findest natürlich auch ohne eine Log Action über die Log Konsole heraus wenn etwas schief gelaufen ist. Abhängig davon welche APIs du in dein Game eingebunden hast, kannst du die Fehlermeldung aber auf anderem Wege weiterleiten und ggf. den Spielfluss anpassen.

### Fehler in der ganzen Session

!!! example-file "Beispiel Level Datei"
    Download: [:material-download: error_session_example.json](./assets/error_session_example.json)

Nutze die **from** `session` Option um Fehlermeldungen in der gesamten Session abzufangen.

Füge eine [Split Path](./split.md) Action zum *START* State hinzu um einen unabhängigen Pfad (hier `error_handling`) für den **On Error** listener zu erstellen.

![Screenshot](./assets/error_session_split.png){ style=width:60% }

Nutze die :material-plus-box-outline: Buttons um die ersten States *Intro* und *ErrorHandling* des Hauptpfades (hier `story`) und des error handling Pfades zu erstellen.

Füge eine **On Error** Action zum *ErrorHandling* State hinzu und wähle unter **from** `session` aus. Füge unter *Settings* die **else** Option hinzu und gib einen `next state` an in dem die Fehlermeldung weitergegeben werden kann.

!!! action inline end "On Error"
    
    ``` json
    {
        "from": "session",
        "type": "Error",
        "else": {
            "next": "AnyError"
        }
    }
    ```
![Screenshot](./assets/error_session_anyerror.png){ style=width:60% }

Erweitere nun den *Intro* State des `story` Pfades mit einer [On Cue](./onCue.md) Action, füge eine Cue Beschreibung ein und leite zu dem State weiter, der unseren Fehler werfen wird (hier *OpenCurtain*)

!!! action inline end "On Cue"
    
    ``` json
    {
        "cue": "Before the show begins.",
        "next": "OpenCurtain"
    }
    ```
![Screenshot](./assets/error_session_cue.png){ style=width:60% }

Um einen Fehler zu erzeugen nutzen wir die [Send MQTT Message](../mqtt/sendMQTTMessage.md) Action.

!!! note
    Um diesem Beispiel im Detail zu folgen aktiviere das [MQTT Plugin](../../plugins/mqtt.md) und verbinde dich mit einem Broker. Du kannst aber natürlich auch eine Fehlermeldung in einer anderen Action provozieren.

Füge [Send MQTT Message](../mqtt/sendMQTTMessage.md) zum *OpenCurtain* State hinzu und gib ein beliebiges MQTT Topic (hier `stage/curtain`) und eine Nachricht an (hier `open`).

!!! action inline end "SendMQTT Message"
    
    ``` json
    {
        "topic": "stage/curtain",
        "message": "open"
    }
    ```
![Screenshot](./assets/error_session_send_mqtt.png){ style=width:60% }

Nun müssen wir sicherstellen, dass die MQTT Nachricht nicht erfolgreich versendet werden kann.

Öffne die MQTT Plugin Einstellungen unter `GAME > settings > MQTT` und bearbeite die Broker URL, sodass sie nicht mehr korrekt auf einen existierenden MQTT Broker verweist.

![Screenshot](./assets/error_session_disconnect_mqtt.png)

> Wenn du lokal arbeitest kannst du z.B. auch deine Internet Verbindung trennen.

Kehre in den Level Editor zurück und wechsle in den [Live Modus](../../live.md). Startest du nun eine Session und löst den *OpenCurtain* State aus, sollte die **On Error** Action eine Fehlermeldung abfangen, da die MQTT Nachricht nicht versendet werden kann.

![Screenshot](./assets/error_session_live_simple.png)

### Fehlertypen unterscheiden

Ggf. möchtest du auf verschiedene Fehler auf unterschiedliche Art reagieren.

Im vorherigen Beispiel kannst du in der Log Konsole :material-console-line: sehen, dass das versenden der MQTT Nachricht mit einem Fehler mit dem Namen `ConnectionError` gescheitert ist.

![Screenshot](./assets/error_session_log_conn.png)

Um nur auf `ConnectionError` Fehler zu reagieren passe die **On Error** Action im *ErrorHandling* State an oder erstelle eine neue **On Error** Action. Wähle diesmal für **[error type name](#error-type-name)** `ConnectionError` aus.

![Screenshot](./assets/error_session_connection_error.png)

So stellst du sicher, das nicht jede Fehlermeldung auf die gleiche Weise behandelt wird.

### Fehlernachrichten abfragen

Du kannst noch einen Schritt weitergehen und die Fehlernachricht überprüfen um auf einen bestimmten, bekannten Fehler auf individuelle Weise zu reagieren.

Dieses mal nutzen wir auch die **[if](#if)** Option und filtern die Fehlermeldung, sodass der Listener nur auf die MQTT Verbindungsfehler aus unserem obigen Beispiel reagiert.

Füge die **[if](#if)** Option über *Settings* zur zweiten **On Error** Action hinzu.

![Screenshot](./assets/error_session_if1.png){ style=width:47%; float: left }
![Screenshot](./assets/error_session_if2.png){ style=width:47%; float: right }

Wir nutzen die **contains** Abfrage und unterscheiden ob die Fehlernachricht den Textabschnitt `MQTT Message` enthält. Wenn der genaue Fehler auftritt soll *SendMQTTFailed* als **Next State** ausgelöst werden.

!!! action inline end "On Error"
    
    ``` json
    {
        "from": "session",
        "type": "ConnectionError",
        "if": [
            {
            "contains": [
                "MQTT Message"
            ],
            "case_sensitive": false,
            "next": "SendMQTTFailed"
            }
        ],
        "else": {
            "next": "SomeConnectionFailed"
        }
    }
    ```
![Screenshot](./assets/error_session_if3.png){ style=width:60% }

Um die Abfrage besser testen zu können kannst du noch eine weitere Fehlerquelle zum `story` Pfad hinzufügen.

Verwende etwa eine [Switch Action](../logic/switch.md) mit **Javascript** Abfrage und füge einen Absichtlichen Syntax Fehler ein.

!!! action inline end "Switch"
    
    ``` json
    {
        "if": [
            {
                "value": "123",
                "javascript": "if(value) \n  return true\n}",
                "next": "NeverGonnaHappen"
            }
        ]
    }
    ```
![Screenshot](./assets/error_session_brokenjs.png){ style=width:60% }

Überprüfe den ganzen Aufbau im Live Modus :material-play-circle-outline:{ .is-live }.

Löst du den *OpenCurtain* State aus reagiert *ErrorHandling* mit *SendMQTTFailed*. Löst du stattdessen den *BrokenJavascript* State aus, reagiert der obere **On Error** Listener und löst den *AnyError* State aus.

![Screen Capture GiF](./assets/error_session_example.gif)