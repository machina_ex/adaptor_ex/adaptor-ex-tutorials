Launch Session
==============

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Starts a new session of the current level.

In the Live Mode chapter you will find out more about [Sessions](../../live.md#session).

You can use [Events](../../events.md#events-and-external-sessions) to communicate with the newly created session.

Settings
--------

### level

The level underlying the new session. You can specify the level name, or formulate a query to select a level with a specific attribute.

`Chapter5` starts the level named "Chapter5"

`{difficulty:5, area:'sonnenallee'}` starts a level whose attribute has `difficulty` value `5`and whose attribute has `area` value `sonnenallee`.

You can also use variables in the query. This is how you can formulate the above example depending on the current player item:

`{difficulty:[[Player.skill]], area:[[Player.area]]}`

### name

You can set a specific name for the new session. Note that there cannot be 2 sessions with the same name.

If you do not specify **name**, a unique name will be generated.

### reference

Specify a reference name under which you can address the session in this level as an item.

![reference name example "MyNewSession"](./assets/launch_reference_1.png)

Then you can use it to access the new session, for example to send or receive events into the session, to end the session, etc.

![dispatch event with new session as source](./assets/launch_reference_2.png)

### arguments

Pass [Level Arguments](../../variables.md#level-arguments) on to the new session.

Activate the **arguments** option in *Settings* and add any number of arguments.

![Open settings](./assets/launch_arguments_1.png){style=width:49%;float:left}

![Add arguments](./assets/launch_arguments_2.png){style=width:49%;float:right}

**name** - The name of the variable under which the argument is available in the level of the newly started session.

**value** - The value that is forwarded to the new session. Can be a string or number value, an array, an object or a reference to an item.

If the argument is defined in the *level config* of the level to be started, the data type of the value to be forwarded must correspond to the data type (`TYPE`) specified in the [Level Arguments](../../variables.md#level-arguments).

If the *level config* has an item collection as `TYPE` for an argument, it is possible to create a new item reference in the new session. To do this, **value** must be the `name` of the item or a query that refers to an item in the respective collection.

![Screenshot](./assets/launch_arguments_item_1.png){style=width:49%;float:left}

![Screenshot](./assets/launch_arguments_item_2.png){style=width:49%;float:right}