Cancel Session
==============

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Cancel another running [session](../../live.md#session) of this or another level.

**Cancel Session** interrupts all running states and actions in all paths that are active in the session to be ended.

To end your current session, use the [Quit](./quit.md) action instead.

Settings
--------

### session

Specify which session or sessions are to be canceled.

1. Use a reference to a Session that you have previously created with [Launch Session](./launch.md) or [Get Item](../data/getItem.md). e.g:

    `MySession`

2. Address one or more sessions by name, e.g:

    `session_SMAKaSAQ` - ends the session with the name "session_SMAKaSAQ"

3. Use a database query to end one or more sessions e.g:

    `'level_name':'SomeLevel'` - terminates all sessions based on the level "SomeLevel"

You can also use variables for the session name or within a query, e.g:

`created_at: {$gt:"[[functions.dateTimeOffset("now","-5:00")]]"}` - terminates all sessions that are older than 5 minutes.

To end **all** running sessions of all levels, enter an empty database query:

`{}` - ends all sessions from all levels in your game

Example
--------

Drag a [Launch Session](./launch.md) action onto the STAGE.

Enter the `name` variable of your level at **level** (`[[level.name]]`), a name for the second session at **name** (here: `two`) and a reference name for the second session at **reference** (here: `SessionTwo`).

![Screenshot: Completed launch session action form](./assets/cancel_launch.png)

Drag a **Cancel Session** action onto the STAGE.

Enter the name (`two`) or the reference name (`SessionTwo`) of your newly created session in **session**.

![Screenshot: Completed Cancel Session Action form](./assets/cancel_example.png)

Add a [Timeout](../time/timeout.md) action to the state with the [Launch Session](./launch.md) action.

Enter a small number of seconds for **timeout** (here: `3`) and select the state with the **Cancel Session** action as **Next State**.

![Screenshot: Completed timeout action form](./assets/cancel_timeout.png)

Drag a [Log](./log.md) action into your *START* state and enter a message.

![Screenshot: Completed log action form](./assets/cancel_log_start.png)

Test the example by switching to [Live Mode](../../live.md) and starting a session manually. Name the new session "one" and click on "Start Session".

A new session is started and selected.

Then trigger the *LaunchSecondSession* state by clicking on the Play button at the top left of the state.

![Screenshot: Testing the cancel example level](./assets/cancel_test_1.png)

A second session is created and cancelled after a few seconds by the **Cancel Session** action.

![Screen Capture: Testing the cancel example level](./assets/cancel_test_2.gif)

If you do not specify a name for the new session, you can also end it using the reference name (here: `SessionoTwo`). This ensures that you end the newly created session and can still create new sessions dynamically.