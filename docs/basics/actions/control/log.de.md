Log Message
===========

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Schreibt einen Text in die Log Konsole.

**Log Message** kann verwendet werden um wichtige Punkte in deinem Level zu markieren oder um zu überprüfen welchen Inhalt Variablen zu einem bestimmten Zeitpunkt haben.

![Screenshot die log action und die log konsole](./assets/log_live.png)

Hier wird Log Message verwendet um zu überprüfen welchen Wert die Variable `count` hat, wenn der "ChooseColor" State (wiederholt) ausgelöst wird.

Settings
---------

### level

Gib das "log level" an mit dem die Log Message ausgegeben wird.

Das Level gibt an wie wichtig die Nachricht ist, die ausgegeben wird.

Die Reihenfolge der Log Level ist:

1. error (sehr wichtig)
2. warn
3. info
4. debug
5. trace (unwichtig)

Log Messages mit dem "error" level sind immer in der Konsole sichtbar. Log Messages mit dem "trace" level sind nur sichtbar, wenn "trace" als level in der Konsole ausgewählt ist.

### message

Der Text, der in der Konsole angezeigt wird, wenn die **Log Message** action ausgelöst wird.
