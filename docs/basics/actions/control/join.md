Join Path
=========

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Merge paths so that this and subsequent states cannot run in parallel.

Settings
--------

### path

The name of a previously created path to be merged into the current path.

You can merge multiple paths by specifying multiple path names in `path`.

Note: In order to merge paths that do not (only) have the same [Split Path](./split.md) action as origin, you must specify the entire split path history of the path to be closed.

Add to the `path` list all pathnames that have been used since detaching from this path.

