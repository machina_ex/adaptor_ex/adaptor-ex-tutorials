Cancel Session
==============

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Beende eine oder mehrere andere laufende [Sessions](../../live.md#session) dieses oder eines anderen Levels.

**Cancel Session** unterbricht alle laufenden States und Actions in allen Pfaden, die in der zu beendenden Session aktiv sind.

Um die eigene, aktuelle Session zu beenden benutze die [Quit](./quit.md) action.

Settings
--------

### session

Gib an welche Session oder welche Sessions beendet werden sollen.

1. Verwende eine Referenz auf eine Session, die du zuvor mit [Launch Session](./launch.md) oder [Get Item](../data/getItem.md) erstellt hast. Z.B.:

    `MySession`

2. Adressiere eine oder mehrere Sessions per Name z.B.:

    `session_SMAKaSAQ` - beendet die Session mit dem Namen "session_SMAKaSAQ"

3.  Verwende eine Datenbank Query um eine oder mehrere Sessions zu beenden z.B.:

    `'level_name':'SomeLevel'` - beendet alle Sessions die auf dem level "SomeLevel" basieren 

Du kannst auch Variablen für den Session Namen oder innerhalb einer Query verwenden, z.B.:

`created_at: {$gt:"[[functions.dateTimeOffset("now","-5:00")]]"}` - beendet alle sessions die älter sind als 5 Minuten.

Um **alle** laufenden sessions von allen Levels zu beenden gib eine leere Datenbank Query an:

`{}` - beendet alle Sessions von allen Levels deines Games

Beispiel
--------

Ziehe eine [Launch Session](./launch.md) Action auf die STAGE.

Trage bei **level** die `name` variable deines Levels ein (`[[level.name]]`), bei **name** einen Namen für die zweite Session (hier: `two`) und bei **reference** einen Referenz Namen für die zweite Session (hier: `SessionTwo`).

![Screenshot: Ausgefülltes Launch Session Action Formular](./assets/cancel_launch.png)

Ziehe eine **Cancel Session** Action auf die STAGE.

Trage bei **session** den Namen (`two`) oder den Referenznamen (`SessionTwo`) deiner neu erstellten Session ein.

![Screenshot: Ausgefülltes Cancel Session Action Formular](./assets/cancel_example.png)

Füge zum State mit der [Launch Session](./launch.md) Action eine [Timeout](../time/timeout.md) Action hinzu.

Trage bei **timeout** eine kleine Sekundenzahl ein (hier: `3`) und wähle bei **Next State** den State mit der **Cancel Session** Action aus.

![Screenshot: Ausgefülltes Timeout Action Formular](./assets/cancel_timeout.png)

Ziehe eine [Log](./log.md) Action in deinen *START* State und gib eine Message an.

![Screenshot: Ausgefülltes Log Action Formular](./assets/cancel_log_start.png)

Teste das Beispiel indem du in den [Live Modus](../../live.md) wechselst und Manuell eine Session startest. Benenne die neue Session "one" und klicke auf "Start Session".

Eine neue Session wird gestartet und ausgewählt.

Löse anschließend den *LaunchSecondSession* State aus mit Klick auf den Play Button oben links am State.

![Screenshot: Das cancel Beispiel Level testen](./assets/cancel_test_1.png)

Eine zweite Session wird erstellt und nach wenigen Sekunden durch die **Cancel Session** Action wieder beendet.

![Screen Capture: Das cancel Beispiel Level testen](./assets/cancel_test_2.gif)

Wenn du keinen Namen für die neue Session angibst, kannst du sie auch über den Referenz Namen (hier: `SessionoTwo`) beenden. So stellst du sicher, dass du die neu erstellte Session beendest und kannst trotzdem beliebig neue Sessions erstellen.