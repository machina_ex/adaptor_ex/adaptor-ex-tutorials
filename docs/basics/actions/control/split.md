Split Path
==========

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Branch off new paths to make them run parallel.

You cannot use [Listen](../../editor.md#listen-actions) actions or actions that `next state` trigger one in the same state.

Settings
--------

**Split Path** is a list of new paths, each with a name and a `next state` .

### Surname

The name for the newly created path. With `name` you can move the path elsewhere in the [Join Path](./join.md) action.

Use a new unused name for each new path.

Don't use "start" as a name for new paths, since starting the level in the START state already uses the "start" path.

### next state

The first State in the newly created path. `next state` fires immediately after all previous run actions have been executed.
