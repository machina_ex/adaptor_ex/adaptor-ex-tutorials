Next
====

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

With `next` you go directly to the next state.

A state with `next` action cannot contain [Listen](../../editor.md#listen-actions) actions because the state is changed before the listeners can be queried.

The `next` action must be the last action in the state. Subsequent actions are no longer executed.

> In combination with the [Switch](../logic/switch.md) action **next** acts like an `else`. If no `if` condition within the switch action matches, the `next state` of the next action is triggered.

Settings
--------

### next state

It is the name of the state to go directly to.

Enter the state manually or select it from the list of existing states.

You can specify a state that doesn't exist yet and create the appropriate state later.
