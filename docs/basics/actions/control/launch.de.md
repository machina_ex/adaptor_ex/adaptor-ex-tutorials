Launch Session
==============

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Startet eine neue Session des aktuellen Levels.

Im Live Modus Kapitel findest du mehr über die Eigenheiten von [Sessions](../../live.md#session) heraus.

Du kannst [Events](../../events.md#kommunikation-mit-externen-sessions) nutzen um mit der neu erstellten Session zu kommunizieren.

Settings
--------

### level

Das Level, dass der neuen Session zugrunde liegt. Du kannst den Level Namen angeben, oder eine Query formulieren um etwa ein Level mit einem Bestimmten Attribut auszuwählen.

`Chapter5` startet das Level mit dem Namen "Chapter5"

`{difficulty:5, area:'sonnenallee'}` startet ein Level dessen Attribut `difficulty` den Wert `5` und dessen Attribut `area` den Wert `sonnenallee` hat.

Du kannst in der query auch Variablen verwenden. SO kannst du das obige Beispiel auch abhängig vom aktuellen Player Item formulieren:

`{difficulty:"[[Player.skill]]", area:"[[Player.area]]"}`

### name

Du kannst einen spezifischen Namen für die neue Session festlegen. Beachte, dass es keine 2 Sessions mit dem selben Namen geben kann.

Wenn du `name` nicht angibst wird ein automatischer, eindeutiger, Name generiert.

### reference

Gib einen Referenz Namen an, unter dem du die Session in diesem Level als Item referenzieren kannst.

![reference name Beispiel "MyNewSession"](./assets/launch_reference_1.png)

Anschließend kannst du damit auf die neue Session zugreifen etwa um [events](../../events.md) in die session zu senden oder zu empfangen, die Session mit [Cancel Session](./cancel.md) wieder zu beenden etc.

![dispatch event mit neuer Session als Quelle](./assets/launch_reference_2.png)

### arguments

Übergebe [Level Arguments](../../variables.md#level-arguments) an die neue Session.

Aktiviere die **arguments** option in den Settings und füge beliebig viele Argumente hinzu.

![Settings öffnen](./assets/launch_arguments_1.png){style=width:49%;float:left}

![Argumente hinzufügen](./assets/launch_arguments_2.png){style=width:49%;float:right}

**name** -  Der Name der variable unter der das Argument im level der neu gestarteten Session verfügbar ist.

**value** - Der Wert, der an die neue Session übergeben wird. Kann ein Text oder Zahlenwert, ein Array, ein Objekt oder eine Referenz auf ein Item sein.

Wenn das Argument in der Level Config des zu startenden Levels definiert ist muss der Datentyp des übergebenen Wertes, dem Datentyp der in den [Level Arguments](../../variables.md#level-arguments) angegeben ist entsprechen.

Ist in den Level Arguments eine Item collection als `TYPE` für das Argument angegeben, kannst du so auch eine neue Item Referenz in der neuen Session erstellen. Gib dafür als **value** den `name` des Items oder eine query die auf ein Item in der collection referiert an.

![Screenshot](./assets/launch_arguments_item_1.png){style=width:49%;float:left}

![Screenshot](./assets/launch_arguments_item_2.png){style=width:49%;float:right}