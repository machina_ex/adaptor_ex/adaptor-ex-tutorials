Quit
====

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Complete the current session of this level.

As soon as the **Quit** action is triggered, the current session of the level is terminated and no further states or actions can be executed and all active listener actions in all paths are closed.

Actions that follow the **Quit** action are no longer executed.
