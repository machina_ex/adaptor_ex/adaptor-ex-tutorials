Next
====

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Mit `next` geht es direkt weiter zum nächsten State.

Ein State mit `next` action kann keine [Listen](../../editor.md#listen-actions) actions enthalten, da der State gewechselt wird, bevor die listener abgefragt werden können.

Die `next` action muss die letzte action im State sein. Darauffolgende actions werden nicht mehr ausgeführt.

> In Kombination mit der [Switch](../logic/switch.md) action fungiert **next** wie ein "else". Trifft keine `if` Bedingung innerhalb der Switch action zu, wird der `next state` der next action ausgelöst.

Settings
--------

### next state

Der Name des State, in den direkt gewechselt wird.

Gib den State manuell an oder wähle ihn aus der Liste der existierenden States aus.

Du kannst einen State angeben, der noch nicht existiert und den entsprechenden State später erstellen.