Play
====

**Plugin**: [Sound](../../plugins/sound.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Play one or more sound files one after the other on your computer.

Add audio files to your [Files](../../files.md) to play them.

## Settings

### tracks

Enter one or more sound files by dragging them from the MEDIA TOOLBAR into the text field or by selecting them from the select dropdown.

The tracks are played one after the other. When a track finishes playing, the next begins.

### loop

Use `loop` if you want to repeat playback of the tracks. When the bottom track ends, the first track starts again. Enter how often the track list should be restarted (1 - x).

Specify -1 to repeat the track list indefinitely.

The loop is broken when changing to another state in the same [Paths](../../paths.md).

### next state

Switch to `next state` when all tracks have been played.

If `loop` specified, will not `next state` switch to until the appropriate number of repeats have been played.

## stop sound

The playback of sounds played with **Play** is interrupted as soon as you switch to another state in the same [Paths](../../paths.md).

For example, if you use another [listener](../../editor.md#listen-actions) in the same state as the **Play** action, playback will be interrupted if that listener is triggered before playback has finished.

You can play multiple sound files in parallel by using **play** in different paths.
