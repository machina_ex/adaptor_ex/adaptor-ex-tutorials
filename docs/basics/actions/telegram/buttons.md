Buttons
========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Send buttons with your Telegram bot and react when the buttons are clicked.

> Telegram accounts cannot send buttons. Therefore **Buttons** cannot be used with Telegram accounts.

Settings
--------

### Bot

The bot that sends the buttons and reacts to button clicks.

### to

The direct chat with a user or the group chat in which the buttons are sent.

### from

*optional*

1 or more users in the [to](#to) chat whose button clicks are responded to. If you use **from**, button clicks from other users will be ignored.

### type

Sets the button variant *inline* or *keyboard*

#### inline

Buttons are sent as a message and appear in the chat window.

![Screenshot: Telegram Messenger Inline Buttons](./assets/buttons_inline.png)

#### keyboard

Creates an alternative keyboard layout based on the specified [Buttons](#buttons-1)

![Screenshot: Telegram Messenger Button Keyboard](./assets/buttons_type_keyboard.png)

Clicks on **keyboard** buttons send the button [label](#label) as a text message and can therefore also be queried with the [On Incoming Messgage](./onMessage.md) action.

### message

The text message that is sent with or before the buttons.

### typing

*optional*

Set a delay in seconds during which the "Typing" info is displayed before the message is sent.

If you do not select this option, a delay is calculated based on the length of the [message](#message).

Set the value to 0 to skip the "Typing" delay.

### Buttons

Define which buttons are displayed and how to react on clicks.

Each button requires a `label` and can react to clicks with several `respond` and/or a `next` option.

Use `Add row` to add a new row of buttons.

Click on `Add button` to create a (further) button in the respective row.

You can create any number of rows with any number of buttons. The buttons are arranged into rows according to your layout.

You must define a **label** for each button and can specify one or more responses with **respond** and/or switch to the next state with **Next State**.

#### label

Specifies the button label text.

If you use [keyboard](#type) buttons, the **label** text is sent into the chat when the button is clicked.

#### respond

Enter a **text** howto respond to a click on the button.

With `Add response` you can specify further messages that are sent when the button is clicked again. The messages are sent with each click on the button from top to bottom.

#### Next State

Specify a state that is triggered when the button is clicked.

If you have specified one or more **respond** texts, **Next State** is triggered with the last **respond** message.

I.e. if you have specified 1 **respond**, **Next State** is triggered on the first click on the button, together with the **respond** message.

If you have specified 2 **respond** messages, **Next State** is only triggered on the second click on the same button.

### keep listening and queue

*optional*

Mute the **Buttons** listener instead of closing it completely when the state containing **Buttons** is exited.

It will then no longer react to incoming button clicks, but the clicks will be stored.

If the state containing this **Buttons** action is called up again, the stored button clicks are first checked before the listener starts to listen for further button clicks.

!!! info
    In the chapter on [Loops](../../loops.md) you will find additional information on [keep listening and queue](../../loops.md#keep-listening-and-queue)

With **keep listening and queue** you can ensure that **Buttons** does not miss any button clicks while the listener is not active, should it be possible for it to be activated again later.

If **Buttons** is reactivated with stored button clicks, the clicks are applied starting with the oldest. If you switch to the `Next State` due to a stored click, more recent button clicks remain stored.

With **enabled** you can activate or deactivate the behavior if required.

The **max queue length** option sets a limit for the number of button clicks that are stored when **Buttons** is muted. If the limit is exceeded, old button clicks are removed from the top of the list.

Set **max queue length** to 1 to save only the latest received button click.

If **max queue length** is not selected, the number of button clicks that are stored is not limited.

Deactivate **max queue length** via the *Settings* of **keep listening and queue**

!!! Warning
    A very large number of stored button clicks can impair the performance of adaptor:ex.

In [Live mode :material-play-circle-outline:{ .is-live }](../../live.md), the total number of button clicks in the queue that have not yet been responded to is displayed in the **Buttons** action :material-clock-outline:{ .is-live }

Action Data
------------

You can access action data from **buttons** via the action data variable.

Use for example:

`[[state.MyState.buttons_1.button]]`

To access the [label](#label) of the latest button clicked of the topmost **Buttons** action in the *MyState* state.

#### id

The Telegram id of the click interaction or of the incoming message for *keyboard* buttons.

#### date

The time and date at which the button was clicked.

#### from_id

The Telegram id of the user who clicked the button. Corresponds to the id specified in the corresponding item in the 'chats' or 'players' collection under `telegram.id`.

#### peer_type

The type of chat in which incoming messages are listened to. In a single chat with a player **peer_type** = `user`. In a group chat **peer_type** = `chat`.

#### peer

The Telegram id of the individual or group chat in which the message was sent.

If it is a single chat (**peer_type** is `user`) **peer** == **from_id**.

If it is a group chat (**peer_type** is `chat`) **peer** is the id of the chat and corresponds to the `telegram.id` of the item in the 'chats' collection that is specified as **to** in the **buttons** action.

#### button

The [label](#label) text of the button that was clicked on.

#### match

The [label](#label) text of the button that was clicked on.

Example
--------

![Screenshot: Gif animation showing the full example](./assets/buttons_example.gif)

!!! example-file "Example Level File"
    Download: [:material-download: buttons_example.json](./assets/buttons_example.json)

If you have not yet added a Telegram bot to your game, you can find instructions on how to do this in the [Telegram setup](../../../tutorials/telegram-settings/index.md#connect-a-telegram-bot) tutorial.

### Send buttons

Drag a **Buttons** action from the ACTIONS toolbar into the *START* state.

![Screenshot: Drag Buttons action from the toolbar to the workspace](./assets/buttons_drag_action.png)

Select a **Bot** (here: `StageManager`) that will send and react to the buttons. Under **to**, select the chat in which the buttons will be displayed (here: `Player`). Under **type** select whether the buttons should appear in the chat or in the user keyboard and under **message** enter the message to be sent with the buttons. Here:

`Would you like to try more inline buttons, or would you like me to show you keyboard buttons?`

![Screenshot: Set sender, recipient type and message](./assets/buttons_basic_options.png)

### React to button clicks

Define the label of the first button and add **respond** and **Next State** options via *Settings*.

![Screenshot: Add response and next state options](./assets/buttons_basic_buttons.png)

Enter a **respond** text <span style="colour:magenta">(1).</span>, which is sent directly when this button is clicked. Here:

`It's my pleasure.`

and enter a **Next State** <span style="colour:magenta">(2)</span>, which is triggered thereafter. Here `Inline`. Click on the :material-plus-box-outline: button next to the **Next State** <span style="colour:magenta">(3)</span> to create the new state directly.

Then click on *Add button* <span style="colour:magenta">(4)</span> to create and edit another button.

![Screenshot: Specify response and next state and add new button](./assets/buttons_button_options.png)

Enter a **label**, a **respond** text and a **Next State** for the 2nd button.

### Test buttons

To test your level, make it the default level for your bot.

Go to `Game -> Settings` and open the Telegram plugin and your Telegram bot.

Click on the *Settings* button and check the **default level** option.

![Screenshot: set bot default level in the Telegram settings](./assets/buttons_default_level.png)

then select your newly created level.

![Screenshot: Select bot default level in the Telegram settings](./assets/buttons_level_select.png)

Now return to your level and toggle into [live mode](../../live.md).

When you write a message to your bot, a new session is created and you get 2 buttons to choose from.

![Screen Capture: Write to the bot](./assets/buttons_test.gif)

### More button reactions

Let add another query to our buttons level.

First add a [Send Message](./sendMessage.md) action to the *Inline* state.

Select your **Bot** and enter a **message** that will be sent when the *Inline* state is triggered. Here:

`You can click on the same button again after a short time and I will send you the next answer. Only if you click on **apple** I will goto the next state immediately.`

![Screenshot: Sending a message with Send Message](./assets/buttons_message.png)

Now drag another **Buttons** action into the *Inline* state and edit it as described above.

Tryout different types of reactions.

To jump directly to the **Next State** without a response, do not select a **respond** option in the *Settings*.

![Screenshot: Next State only](./assets/buttons_next_only.png)

To send different responses for more frequent button clicks, add more **respond** messages.

![Screenshot: Multiple respond messages](./assets/buttons_add_response.png)

### Arrange buttons

To arrange your buttons in multiple rows click on *Add row* in the **Buttons** form

![Screenshot: Arrange buttons in multiple rows](./assets/buttons_add_row.png)

Each row can contain several buttons.

![Screenshot: Button arrangement in two rows in the editor](./assets/buttons_rows.png)

In the chat, the size of the buttons is adjusted so that all rows have the same width.

![Screenshot: Button arrangement in two rows in the chat](./assets/buttons_rows_chat.png)

### Keyboard Buttons

You can use **keyboard** buttons in the same way.

Drag another **Buttons** action into the *Keyboard* state and edit it as described above but select `keyboard` under **type**.

![Screenshot: select keyboard type](./assets/buttons_keyboard.png)

If the *keyboard* state is triggered, the user's keyboard is replaced by the buttons you defined.

![Screenshot: test keyboard type](./assets/buttons_test_keyboard.png)