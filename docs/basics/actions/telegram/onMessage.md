On Incoming Message
===================

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Respond to incoming Telegram messages in a variety of ways.

Settings
--------

### Account or Bot

The Telegram account or bot, which waits and responds to incoming messages.

> With default settings, Telegram bots cannot read all messages in a group chat. Checkout the instructions in the [Telegram Setup tutorial](../../../tutorials/telegram-settings/index.md#messages-in-group-chats).

### chat

The chat with a player or the group chat in which incoming messages are responded to.

### from

*optional*

Allows to specify a players or Telegram account item when **On Incoming Message** should listen to a message from a specific players item or account.

Only for group chats.

### priority

*optional*

Determines the priority of **On Incoming Message** if there are several listeners at the same time, for example on other paths or narrative threads of the game.

### if

*optional*

With **if** incoming messages can be checked and reacted to in various ways. You can find out more about this in [Query from conditions](#query-from-conditions).

### else

*optional*

Occurs when none of the if conditions are met. See [Conditions query](#query-from-conditions).

### while idle

*optional*

Add messages that will be sent to the chat after a certain period of time (`delay`) as long as none of the incoming messages are answered by **if** or **else**.

This option is useful for sending reminders or more specific messages.

`delay` determines the time in seconds until `message` is sent.

All **while idle** messages delays are strung together. For example, if the first **while idle** message has a `delay` of 60 seconds and the second message has a `delay` of 30 seconds, the second message will be sent after 90 seconds.

### finally

*optional*

Go to the `next state` when all [while idle](#while-idle) messages have been sent. Specify with `delay` another time delay that will be appended to the last [while idle](#while-idle) message until `next state` is triggered.

### keep listening and queue

*optional*

Mute the **On Incoming Message** listener instead of closing it completely when the state containing **On Incoming Message** is exited.

The listener will then no longer respond to incoming Telegram messages, but the messages will be stored.

If the state containing the **On Incoming Message** action is called up again, the stored messages are first checked before the listener starts listening for further messages.

!!! info
    In the chapter on [Loops](../../loops.md) you will find additional information on [keep listening and queue](../../loops.md#keep-listening-and-queue)

With **keep listening and queue** you can ensure that **On Incoming Message** does not miss any messages while the listener is not active, should it be possible for it to be activated again later.

If **On Incoming Message** is reactivated with stored messages, the messages are applied starting with the oldest. If you switch to the `Next State` due to a stored message, other, more recent messages may remain in the queue.

With **enabled** you can activate or deactivate the behavior as required.

The **max queue length** option sets a limit for the number of messages that are saved when **On Incoming Message** is muted. If the limit is exceeded, old messages are removed from the top of the list.

Set **max queue length** to 1 to always save only the latest incoming message.

If **max queue length** is not selected, the number of messages that might be stored is not limited.

Deactivate **max queue length** via the *Settings* of **keep listening and queue**

!!! Warning
    A very large number of stored messages can impair the performance of adaptor:ex.

In [Live mode :material-play-circle-outline:{ .is-live }](../../live.md), the total number of queued messages that have not yet been responded to is displayed at the **On Incoming Message** action :material-clock-outline:{ .is-live }

In this example level, **keep listening and queue** is used so that the bot reacts to a response with a deliberate delay. By temporarily switching to the *CoffeeBreak* state, the incoming message is initially stored temporarily. Only when it switches back to *LeaveMeAMessage* is the message responded to.

![Screen Recording](./assets/on_message_queue_example.gif)

!!! example-file "Example Level File"
    Download: [:material-download: telegram_queue_example.json](./assets/telegram_queue_example.json)

Conditions
----------

With **if** one or more conditions can be queried. The content of the incoming message(s) is checked as long as the state (with this **On Incoming Message** action) is active.

Use **else** to specify what happens if none of the if-conditions should have matched with an incoming message.

To add conditions to **if** click on "add condition".

![Settings then select if, then click add condition](./assets/on_message_if_add_condition.png)

In the dropdown next to **condition 1** there is now a choice between different query types.

![dropdown with query types](./assets/on_message_if_condition_types.png)

### Condition types

With the exception of **contains media**, the condition types are described in more detail in the [Conditions](../../conditions.md#query types-operators) chapter.

For the different types a condition applies if:

#### Equals

The content of the message exactly matches the or one of the texts specified here.

#### Contains

The message contains the or one of the texts specified here.

#### Contains media

The message contains media.

Select under **media** which media format will trigger a `next state` adn/or a `response`. Use **contains any of** to combine different media formats.

Select "any" to respond to any incoming message that contains a media format.

Select "downloadable" to respond to any incoming message that contains a file that can be downloaded.

With the [download](#download) option incoming files can be downloaded.

Media specific data can be retrieved using the **On Incoming Message** [action data](#action-data).

#### Less Than / Greater Than

The message is a number whose value is Greater or Less than the value specified here.

#### Regular Expression

The message matches the regex specified here.

#### Javascript Function

The javascript function with the message as passing parameter `value` returns a value.

#### Database Query

The given mongo query finds at least one item in the given collection.

### Reactions

If a condition is true, one or more of the reactions specified under the condition are triggered.

**On Incoming Message** allows 3 ways to react on incoming messages.

#### respond

Should one of the conditions occur, **respond** can be used to specify one (or more) response message(s) that the account will send as a reply to the sender ([chat](#chat)).

If you specify multiple reply messages under **respond**, the next reply in the list will be sent each time the condition is true.

> The last reply in the list will also switch to the **next state** if applicable.

If more messages arrive that match the condition, no more replies will be sent.

Select "reply" under **mode** if the incoming message is to be quoted.

![Example for quoting reply to incoming message](./assets/on_message_reply.png)

#### next state

If the condition is true it will trigger the `next state`.

If you have also specified a **respond** message, it will trigger the `next state` immediately after the response has been sent.

If you have specified several **respond** messages, the `next state` will only be triggered after the last reply has been sent. That said `next state` will only be triggered when the condition has been met as often as many **respond** messages you have specified.

If no further response is available and no **next state** is specified, but the condition matches again, nothing happens.

#### download

**download** allows to download files if incoming messages have been checked for media formats with **contains media**. The formats "photo", "image", "document", "audio", "voice", "video" and "sticker" contain media files that can be downloaded.

**directory** specifies under which file path the file should be saved. Relative paths (start without '/' or with './') refer to the location of the [File](../../files.md) folder of your adaptor:ex installation. Note that the file path must already exist.

Downloaded files have an automatically generated filename. You can specify your own name for the file with **filename**. Note that this is the name without the file extension (*.jpg, .png, .wav* ...). The file extension will be appended automatically, depending on the file type.

You can then access the full file path including file name in the [action data](#action-data) `download` property. You can also create a local variable under **variable** that contains the file path.

Example:

![filepath, name and path variable in contains media download](./assets/on_message_download_example.png)

Here the message is checked for "photo" and "image" media which are then downloaded to the [Files](../../files.md) directory under "location_scout/img/".

The filename here is variable and depends on the variables `location` and `index`. The response message contains the file path specified under the **variable** "latest_img". A downloaded photo could i.e. be found under `~/adaptorex/games/Tutorial/files/location_scout/img/backyard_3.jpg`.

Action data
------------

**On Incoming Message** stores different information depending on the message type that was received.

You can access the data via the action data variable. Use for example:

`[[state.MyState.onMessage_1.text]]`

to access the last received message text of the top **On Incoming Message** action in the state *MyState*.

### Origin data

The following properties are always contained in **On Incoming Message** action data:

#### id

The Telegram id of the incoming message.

#### date

The date and time when the message was sent to the chat.

#### from_id

The Telegram id of the user who sent the message (to the chat). Corresponds to the id specified in the corresponding item in the 'players' collection under `telegram.id`.

#### peer_type

The type of chat in which incoming messages are listened to. In a single chat with a player, **peer_type** equls `user`. In a group chat, **peer_type** equals `chat`.

#### peer

The Telegram id of the individual or group chat the message was sent to.

If it is a single chat (**peer_type** is `user`) **peer** == **from_id**.

If it is a group chat (**peer_type** is `chat`) **peer** is the id of the chat and corresponds to the `telegram.id` of the item in the 'chats' collection that is specified as **chat** in the **On Incoming Message**.

### Optional data

Some action data is only available if the last incoming message is a specific message type. **match** is only available if one of the **if** conditions matched  the last message received.

#### text

The text of the incoming text message. If media messages contain a caption it will also be stored as **text**.

#### match

The text or part of the text to which the **if** condition applied.

#### media

The media type of the incoming message. Can take one of the following values:

"photo", "image", "document", "audio", "voice", "video", "sticker", "geo", "geolive", "contact", "poll", "webpage", "venue", "game", "dice" or "invoice".

Some media messages store additional values in the action data (see below).

#### download

The file path of the downloaded media file. See [download](#download).

#### first_name

The first name specification of the received "contact" media message.

#### last_name

The last name specification of the received "contact" media message.

#### phone

The phone number specification of the received "contact" media message.

#### contact_id

The Telegram id specification of the received "contact" media message.

#### lat

The longitude (latitude) specification of the received "geo", "geolive" or "venue" media message.

#### long

The latitude (longitude) specification of the received "geo", "geolive" or "venue" media message.

#### title

The name of the location of the received "venue" media message.
#### address

The address of the location, of the received "venue" media message.

#### provider

The provider of the location, of the received "venue" media message.

#### venue_id

The Telegram id of the venue, of the received "venue" media message.

#### venue_type

The category of the venue, of the received "venue" media message.

#### value

The dice value of the received "dice" media message.

#### emoticon

The used emoticon of the received "dice" media message.