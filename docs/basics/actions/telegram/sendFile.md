
Send File
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

With the Telegram action **Send File**, files in various formats can be sent from the account or bot to the player or chat.

[Here](../../../tutorials/telegram-send-media/index.md){target=_blank} you can find a tutorial on this action.

## Settings

### Telegram Account

The Telegram account that should send the file.

### To

The player(s) to whom the file is to be sent.

### File

The file name of the file to be sent.

Files can either be dragged and dropped from the left toolbar "Media" directly into the field (dropdown selection: *Drop File*), or selected via dropdown (dropdown selection: *Select File*).

The toolbar tab "Media" contains the files that have been stored on the adaptor:ex server under 'files/' for this game.

![screenshot](../../../tutorials/telegram-send-media/assets/4.png){ style=width:40% }

[Here](../../files.md) you can find more information about files in adaptor:ex.

Further settings (can be added via "Settings"):

### format

The format in which the file will be sent.

*auto* converts most files into a suitable format. For example, images are displayed directly up to a certain size.

See below fror more details about Media Formats.

### Pin

Sets the message as a pinned message in the chat.

### Caption

Adds a message/description/caption to the file.

## Image files

If we put an image file (.png, .jpeg) in **file**, the telegram account will send a photo to the players.

![balloon dog](../../../tutorials/telegram-send-media/assets/7.png){ style=width:350px }
Photo by <a href="https://unsplash.com/@charlesdeluvio?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Charles Deluvio</a> on <a href="https://unsplash.com/s/photos/balloon-dog?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

A URL to an image on the Internet will also cause the account to send that image. You can recognize an image link by the fact that there is a file extension at the end of the URL, e.g. .png or .jpeg.

Please make sure that the rights of use of this file permit your use.

**format**: for an image, 'auto' should be set here. As long as the image is less than 10 MB in size, it will be displayed immediately, provided that the person receiving it allows this in their Telegram app.

Pictures over 10 MB are displayed to the recipient as a download by default.

## Audio Files and Voice Messages

Audio files have file extensions such as .mp3, .wav and .ogg.

.mp3 and .wav are always displayed to players as audio download links.

.ogg files can also be sent as audio download links.

However, if **format** is set to `voice`, the .ogg file is correctly encoded (OPUS codec) and if it is not larger than 1MB, Telegram will display the file as a voice message, as if the Telegram account had recorded it itself.

![screenshot](../../../tutorials/telegram-send-media/assets/16.png){ style=width:51% }

This is how the settings should look so that an audio file is sent as a voice message.

![screenshot](../../../tutorials/telegram-send-media/assets/17.png){ style=width:51% }

This is what a voice message looks like.

## Videos

With **format** on `video`, video files can also be displayed directly as videos in Messenger.

## Other files

Other file types are sent as a download link.

The display of the files depends not only on the file format and the size of the file, but also on the settings in the Telegram app of the receiving person.

Example
-------

Drag a Telegram **Send File** action to the STAGE. Select an account or bot and specify who to send the file to.

In file we select one of the files located in the files directory of our game.

![Screenshot: a Send File action](./assets/send_file_logo.png)

When we trigger the *SendLogo* state, the image file is sent to the player.

![Screenshot: Telegram chat with Stage Manager Bot](./assets/send_file_logo_chat.png)