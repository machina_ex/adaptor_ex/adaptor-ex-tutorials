
Send Geo Location
=================

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Mit *Send Geo* kann eine digitale Straßenkarte mit Markierung versendet werden. 

## Settings

### from

Der Telegram-Account oder Bot, der die Geo Location versenden soll.

### To

Die Spieler:in oder Spieler:innen, an die die Geo location geschickt werden soll. 

### Latitude

Der Längengrad der Geo Location. zum Beispiel:

`52.438015`

### Longitude

Der Breitengrad der Geo Location. zum Beispiel:

`13.231191`

Die Werte müssen jeweils englisch notiert werden, also mit Punkt anstatt eines Kommas.

Latitude und longitude von einem Ort sind z.B. über Open Street Maps oder Google Maps zu finden. 

![screenshot](../../../tutorials/telegram-send-media/assets/13-1.png){ style=width:45% }

![screenshot](../../../tutorials/telegram-send-media/assets/15.png){ style=width:45% }
