Create Chat
===========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Create a new Telegram group chat or channel.

The selected telegram account creates a new group chat or channel and invites other Telegram accounts to the chat.

In adaptor:ex, a [data item](../../variables.md#collections-and-data-items) is created in the `chats` collection at the same time to represent the chat.

> Telegram bots cannot create chats themselves with **Create Chat**. However, you can add bots to a group chat as [member](#members). Afterwards, the bot can customize the chat with [Edit Chat](./editChat.md) if it has the appropriate permissions.

Settings
--------

You can set most of the options when you create the chat. If you want to change certain options later, use the [Edit Chat](./editChat.md) action.

### Telegram Account

The account that will create the chat. This account is automatically the admin of the newly created group.

### type

Specify whether you want to create a group chat (`chat`) or channel (`channel`). Here is an explanation of the difference: [https://telegram.org/faq#f-was-ist-der-unterschied-zwischen-gruppen-und-kanalen](https://telegram.org/faq#f-was-ist-der-unterschied-zwischen-gruppen-und-kanalen)

### name

Set a name for your group or channel. If you do not set a `title`, `name` will appear in the title of the chat.

If `name` does not contain special characters or spaces you can use `name` to address the chat in another level or session. For example: `[[chats.chatname]]` `.

Note that in this case `name` must be unique for each chat.

To uniquely address the chat in the current level or session, we recommend using `reference`.

### title

Enter a title for the chat.

### members

A list of members to add to the group.

Use data items, players or telegram accounts that are connected to a telegram user.

Examples:

`Player` - add the player from the Player level argument.

`players.{'telegram.first_name':'Amanda'}` - add the player entry from the `players` collection that has the first name "Amanda" in their Telegram account

`telegram.accounts.Chris` - invite the telegram account `Chris` to the chat.

### photo

Upload a group picture for the chat.

Use a picture file from your [Files](../../files.md).

### about

Add a text description for the group.

### create invite link

Select **create invite link** to create a public link where users can join this group chat.

The link is then stored in the chat item as a variable `link`. You can retrieve it like this e.g. within an action:

`[[chats.MyChat.link]]`

### permissions

Specify which interactions are allowed for members of the chat who are not admins.

Prohibit an interaction option by deselecting the permission.

![The different ways to restrict interactions](./assets/chat_permissions.png)

In this example, group members without admin status cannot send media files, GIFs or stickers.

### reference

Enter a `reference` value when creating the chat. This way you can easily address the chat in the same level afterwards.

If you specify the reference in the Telegram actions as `chat` or `to`, the Telegram chat will be addressed. In other actions, the reference points to the `chat` data item and you can access its properties.
 
Chat data
---------

The data item created with the chat contains information about the chat that is constantly updated.

> Note: if the adaptor:ex server is not active while changes to the chat are happening, these changes will not be reflected in the data item.

Using `reference` or `name` you can address the chat item and use the chat data as variables in other actions.

The following may can be included in the chat item

**`name`** - the name of the chat. `name` is not necessarily the title of the chat.

**`title`** - the display name of the chat.

**`members`** - list of telegram ids of the group members.

**`member_count`** - Number of group members. The account that created the chat is not counted here!

**`telegram`** - Telegram properties of this chat like `id` and `participants_count`.

* **`polls`** - Polls created in this chat with [Send Poll](./sendPoll.md) or [Send Quiz](./sendQuiz.md). `polls` contains [Send Poll Survey Data](./sendPoll.md#poll-data)

Chat events
------------

Changes to the settings of a chat trigger events that can be fetched with the [On Event](../logic/onEvent.md) action.

Specify in [On Event](../logic/onEvent.md) **from** a reference to the item in the 'chats' collection to react to events of the corresponding group chat.

Specify one of the following event names in **event** to react to the corresponding event.

### titleChanged

The title of the chat has been changed.

payload:

**`title`** - the new chat title
### membersAdded

one or more users have been added to the chat

payload:

**`ids`** - list of Telegram ids of the added users
**`count`** - number of users added to the chat

### memberJoined

A user has joined the chat (e.g. via an invite link).

payload:

**`id`** - Telegram id of the joined user.
**`inviter_id`** Telegram id of the user who sent the invitation to the user that joined the chat.
### memberLeft

A user has left the chat.

payload:

**`id`** - Telegram id of the user that left the chat.

### memberRemoved

A user has been removed from the chat by an administrator.

**`id`** - Telegram id of the user that was removed from the chat.

Example
--------

![Create a new group chat](./assets/create_chat_example.png)

Creates a new group chat "Better Together" for the telegram account "Thekla", inviting the Telegram account of "Player" and the player with the `name` variable "Arvid".

![The new group chat in Telegram](./assets/new_telegram_chat_android.png)

Since we have given a `reference` value when creating the chat, we can address the chat afterwards in the same level e.g. in the [Send Message](./sendMessage.md) action.

![Send a message to a group chat](./assets/send_message_to_chat.png)

![The message as it is received in the group chat](./assets/to_telegram_chat_android.png)

You can also address the chat via the `chats` collection, e.g. if you want to use it in a level that does not have the reference.

Address the chat via a query:

`[[chats.{name: "Better Together"}]]`.

If you have not used special characters or whitespace, you can also use the name directly, e.g.:

`[[chats.BetterTogether]]`

![Address a group chat by name](./assets/send_message_to_chat_name.png)
