Send Poll
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Erstelle und versende eine Umfrage in einem Gruppenchat.

Umfragen können anschließend mithilfe von Chat [Events](#events) und der [Umfragedaten](#umfragedaten) ausgewertet werden.

Settings
--------

### from

Der telegram account oder bot, der die Umfrage versendet. Der Agent muss Mitglied im `to` Gruppenchat sein.

### to

Der Gruppenchat an den die Umfrage gesendet wird.

### Name

Der name unter dem die Umfrage im Chat Data Item gespeichert wird. Mit `name` kannst du die Umfrage adressieren und Auswerten.

Umfragen werden im Chat Dokument in der *chats* Data Collection unter "polls" gespeichert. Rufe die Umfragedaten z.B. innerhalb einer Variable auf: `[[chats.Chatname.polls.Pollname]]`

### Question

Der Fragetext, der in der Umfrage gestellt wird.

### Answers

Eine Liste von Antwortmöglichkeiten. Füge beliebig viele Antworten mit `Add Answer` hinzu.

### Anonymous Vote

Gib an, ob eingesehen werden wie die anderen Gruppenmitglieder abgestimmt haben.

Wähle die Option an, um das Abstimmungsverhalten verdeckt zu lassen.

### Multiple Choice

Gib an, ob mehrere Antworten ausgewählt werden können.

Events
------

Du kannst auf Änderungen in einer von einem deiner Accounts oder Bots erstellten Umfrage mithilfe von Events reagieren.

Das jeweilige Event wird von dem Gruppenchat ausgelöst, in dem die Umfrage stattfindet.

Ein **pollUpdate** Event wird ausgelöst, wenn ein Mitglied des Gruppenchats in der Umfrage oder dem Quiz abgestimmt hat.

Ein **pollComplete** Event wird ausgelöst, wenn alle Mitglieder des Gruppenchats mindestens eine Stimme in der Umfrage oder dem Quiz abgegeben haben. **pollComplete** wird auch ausgelöst wenn der Account der die Umfrage oder das Quiz erstellt hat selbst nicht abgestimmt hat.

Sowohl der payload von **pollUpdate** als auch der payload von **pollComplete** enthält `name` und `id` der Umfrage und die aktualisierten Poll Daten `voters`, `voters_count`, `results`, `ranking` und `ambiguous`.

Details zu den aktualisierten Umfragedaten findest du unter [Aktualisierte Daten](#aktualisierte-daten).

Nutze die `if` option der [Event action](../logic/onEvent.md) um das Event auf Umfragedaten zu überprüfen.

![Überprüfen ob ein bestimmter Player abgestimmt hat](./assets/poll_event_example.png)

In der `field` Option der if Abfrage kannst du die verschiedenen Umfrage Daten angeben um sie zu überprüfen. Im obigen Beispiel wird überprüft, ob der player, der als `PlayerA` im level referenziert wird bereits eine Stimme abgegeben hat.

> Das **pollComplete** Event wird ausgelöst unabhängig davon ob der account, der die Umfrage erstellt hat selbst eine Stimme abgegeben hat. Mit einer Ausnahme: Die Stimme des accounts wurde in einer anonymen Umfrage in einem anderen Telegram Client wie der offiziellen Telegram App abgegeben.

Umfragedaten
------------------

Die Daten der Umfrage werden im entsprechenden Gruppenchat Item in der "polls" variable abgespeichert. Um verschiedene Umfragen unterscheiden zu können ist jede Umfrage unter der `name` Eigenschaft abgelegt.

> Beachte, dass, wenn du eine Umfrage mit der selben `name` Eigenschaft im selben Chat erneut erstellst, die Umfragedaten der vorherigen Umfrage überschrieben werden.

Zum einen kannst du so auf die Ausgangsdaten wie `question` und `answers` zugreifen. Zum anderen werden die Daten mit jeder Stimmabgabe eines Gruppenmitglieds aktualisiert und geben z.B. die Anzahl der Mitglieder an, die bereits abgestimmt haben (`voters_count`).

### Ausgangsdaten

* **`name`** - der Name der Umfrage. 

  *Beispiel*: `[[Groupchat.polls.my_poll.name]]` ergibt "my_poll"

* **`from_id`** - telegram id des accounts oder bot, der die Umfrage erstellt hat. 

  *Beispiel*: `[[Groupchat.polls.my_poll.from_id]]` ergibt "1234567890"

* **`question`** - Der Fragetext. 

  *Beispiel*: `[[Groupchat.polls.my_poll.question]]`

* **`answers`** - Liste der Antwortmöglichkeiten beginnend mit `0`

  *Beispiel*: `[[Groupchat.polls.my_poll.answers.2]]` gibt den Antworttext der 3. Antwort aus.

* **`private`** - Boolean ("true" oder "false") der angibt, ob die Umfrage offen oder geheim stattfindet.

  *Beispiel*: `[[Groupchat.polls.my_poll.anonymous]]` ergibt "true"

* **`multiple_choice`** - Anonymous Vote boolean ("true" oder "false") der angibt, ob mehrere Antworten ausgewählt werden können sind.

* **`correct_answer`** - die richtige Antwort (Nur Quiz!)

  Beispiel: `[[Groupchat.polls.my_poll.correct_answer]]` ergibt "Ja", wenn "Ja" die korrekte Antwortoption ist.


### Aktualisierte Daten

* **`voters_count`** - Anzahl der Gruppenmitglieder die bisher an der Umfrage teilgenommen haben.

  *Beispiel*: `[[Groupchat.polls.my_poll.voters_count]]` ergibt 1 wenn ein Gruppenmitglied abgestimmt hat

* **`voters`** - Liste der Telegram ids der Mitglieder die bereits an der Umfrage teilgenommen haben.

  *Beispiel*: `[[Groupchat.polls.my_poll.voters.0]]` ergibt 1234567 wenn das erste Gruppenmitglied, dass an der Umfrage teilgenommen hat die Telegram id 1234567 hat

* **`from_voted`** - Gibt an ob der Account, der die Umfrage erstellt hat selbst eine Stimme abgegeben hat.

  `from_voted` wird auch in anonymen Umfragen "true" gesetzt, wenn die Stimme mit der [Cast Vote](./castVote.md) action abgegeben wurde. Wird die Stimme in einer anonymen Umfrage mit einem anderen Telegram Client, z.B. einer offiziellen telegram App, abgegeben bleibt `from_voted` "false".

  *Beispiel*: `[[Groupchat.polls.my_poll.from_voted]]` ergibt "true" wenn der Account zuvor eine Stimme abgegeben hat.

  > Bots können nicht selbst an Umfragen teilnehmen, daher ist dieser Wert immer `false`, wenn ein Bot die Umfrage erstellt hat.

* **`ambiguous`** - Boolean ("true" oder "false") der angibt ob es genau eine oder mehrere meist gewählte Antwortoptionen gibt.

  `ambiguous` ist "true" wenn mehrere Antwortoption die gleiche Stimmzahl bekommen haben, aber nur, wenn es sich um die höchste Stimmzahl handelt.

  *Beispiel*: `[[Groupchat.polls.my_poll.ambiguous]]` ergibt "false" wenn es ein Eindeutiges Ergebnis für die erste Wahl gab.

* **`results`** - Liste der aktuellen Umfrageergebnisse in der Reihenfolge der `Answers`. Jeder Eintrag in `results` hat die folgenden Eigenschaften:

  * `votes` - Anzahl der abgegebenen Stimmen für diese Antwort
  * `voters` - Liste von telegram ids der Nutzer, die diese Antwort gewählt haben (nur wenn Daten abgefragt werden können. Siehe [Nutzer Daten](#nutzer-daten))
  * `answer` - Text der Antwortoption
  * `rank` - Position im Ranking der meisten abgegebenen Stimmen für die Antwortoption (0 für die meisten Stimmen, 1 für die zweitmeisten etc.)
  * `correct` - Boolean ("true", "false") ob dies die richtige Antwort ist (nur Quiz!)

  Nutze den entsprechenden PLatz in der Liste der Antworten (beginnend bei `0`) um das entsprechende Antwortergebnis aufzurufen.

  *Beispiel*: `[[Groupchat.polls.my_poll.results.2.votes`]] ergibt 5, wenn für die 3. Antwortmöglichkeit 5 Stimmen abgegeben wurden

* **`ranking`** - Liste der aktuellen Umfrageergebnisse in der Reihenfolge der meisten abgegebenen Stimmen für die Antwortoption. Jeder Eintrag in `ranking` hat die folgenden Eigenschaften:

  * `votes` - Anzahl der abgegebenen Stimmen für diese Antwort
  * `voters` - Liste von telegram ids der Nutzer, die diese Antwort gewählt haben (nur wenn Daten abgefragt werden können. Siehe [Nutzer Daten](#nutzer-daten))
  * `answer` - Text der Antwortoption
  * `index` - Position in der reihenfolge in der die Antwortoptionsn ursprünglich angeordnet sind (0 für die erste Antwort, 1 für die zweite etc.)
  * `correct` - Boolean ("true", "false") ob dies die richtige Antwort ist (nur Quiz!)

  *Beispiel*: `[[Groupchat.polls.my_poll.ranking.0.answer]]` ergibt "Ja", wenn für die die Antwortoption "Ja" die meisten Stimmen abgegeben wurden.

  Beachte, dass `ranking` bei gleicher Anzahl Stimmen die ursprünglich obere Antwort bevorzugt!

### Nutzer Daten

Es ist möglich herauszufinden welches Gruppenmitglied welche Antwortoption gewählt hat.

Die Umfrage darf dabei aber nicht [anonym](#anonymous-vote) sein und der Account, der die Umfrage erstellt hat, muss selbst an der Umfrage teilgenommen haben.

Nutze die [Cast Vote](./castVote.md) action um nach Erstellen der Umfrage eine oder mehrere Stimmen mit dem Account abzugeben.

Die telegram ids der Gruppenmitglieder die an der Umfrage teilgenommen haben findest du in den [Umfragedaten](#umfragedaten) als `voters`.

Die telegram id in den Umfragedaten entspricht dem Eintrag in 'telegram.id' eines Item in der `players` collection.

Für Gruppenmitglieder, die bereits als `players` Items in deinem Game gespeichert sind, wird ihre Auswahl zudem im jeweiligen Item gespeichert.

Dem Item wird ein Eintrag `polls` hinzugefügt, in dem, wie im chats Item, die Umfrage unter ihrem Namen zu finden ist. Hier findest du unter `votes` eine Liste der Stimmen, die der player abgegeben hat.

Wenn etwa das `players` Item mit `name` Eigenschaft "Ada" bei der "my_poll" Umfrage die 2. und 5. Option gewählt hat, wird diese Variable:

`[[players.Ada.polls.my_poll.votes]]`

eine Liste mit den Werten `1` und `4` ergeben (als array: `[1,4]`). Beachte, dass die Umfrageoptionen von `0` an gezählt werden.

Beispiel
---------

> Level File: [:material-file-code-outline: poll_example.json](./assets/poll_example.json)

In diesem Beispiel erstellen wir einen neuen Gruppenchat und starten eine Umfrage. Wenn alle User in der Chat Gruppe an der Umfrage teilgenommen haben, werten wir die Antworten aus und reagieren auf das Ergebnis.

Erstelle einen neuen chat mit [Create Chat](./createChat.md) und gib als `reference` "Groupchat" an oder verwende einen existierenden Chat, den du mit [Get Item](../data/getItem.md) als "Groupchat" in das Level geladen hast.

Ziehe eine **Send Poll** action auf die STAGE und gib dem neuen State den Namen "LeakItOrNot". Gib den unter `from` den Telegram Account oder Bot an und unter `to` den Chat "Groupchat". Gib einen kurzen Namen unter `name` an, hier "leak" und formuliere die Frage (`Question`): "Was soll ich mit der CD tun?".

Klicke unter `Answer Options` auf `Add Answer` und füge zwei Antwortmöglichkeiten hinzu.

![Die send poll action](./assets/poll_send_example.png)

Nun erstellen wir zunächst eine Abfrage ob die Umfrage durchgeführt wurde.

Ziehe eine [On Event](../logic/onEvent.md) action in den "LeakItOrNot" State. Setze unter *from* `Groupchat` ein und als event `pollCopmplete`. Wähle in *Settings* die `if` option an und füge mit `Add Condition` eine neue If Bedingung hinzu. Gib als *field* `name` und unter *equals* `leak` um sicherzustellen, dass wir die richtige Umfrage im Groupchat überprüfen.

Gib als `next state` "CheckPoll" an.

![Abfragen ob alle Gruppenmitglieder gewählt haben](./assets/poll_all_voted.png)

Um die Umfrage Auswertung nicht bedingungslos davon abhängig zu machen, dass alle Gruppenmitglieder mitmachen wollen, fügen wir noch eine [Timeout](../time/timeout.md) action zu "LeakItOrNot" hinzu.

![5 Minuten bis zur Auswertung](./assets/poll_timeout.png)

Wenn nach 5 Minuten nicht alle Gruppenmitglieder abgestimmt haben wird einfach mit den bestehenden Stimmen ausgewertet.

Ziehe jetzt eine [Switch](../logic/switch.md) action auf die STAGE und benenne den neuen State in "CheckPoll" um.

Hier überprüfen wir welche Antwortoption die meisten Stimmen bekommen hat.

Erstelle drei neue conditions. 

In der 1. Bedingung überprüfen wir ob das Ergebnis eindeutig war. Gib für **value** `[[Groupchat.polls.leak.ambiguous]]` an und für **equals** `true`. Gib für **next state** `Undecided` an.

In der 2. Bedingung überprüfen wir ob die erste Antwortoption die meisten Stimmen erhalten hat. Gib für **value** `[[Groupchat.polls.leak.results.0.rank]]` und für **equals** die Zahl `0` an. Gib für **next state** `Destroy` an.

In der 3. Bedingung überprüfen wir ob die zweite Antwortoption die meisten Stimmen erhalten hat. Gib für **value** `[[Groupchat.polls.leak.results.1.rank]]` und für **equals** die Zahl `0` an. Gib für **next state** `Leak` an.

![Die Umfrage mit der switch action evaluieren](./assets/poll_evaluate.png)

Es gibt also 3 Mögliche Ausgänge der Umfrage, mit denen wir rechnen müssen. Für jede Variante erstellen wir neue States.

![Das Level in der Übersicht](./assets/poll_full_example.png)

Wenn der State "LeakItOrNot" ausgelöst wird, wird eine neue Umfrage in "Groupchat" gesendet und darauf gewartet, dass alle Gruppenmitglieder an der Umfrage teilnehmen.

![Die Umfrage im telegram Messenger](./assets/poll_chat_lol.png)

Sobald die beiden anderen Gruppenmitglieder ihre Stimme abgegeben haben oder sobald der timeout abgelaufen ist, wird die Umfrage ausgewertet und der entsprechende State ausgelöst.

![Die Abgeschlossene Umfrage im telegram Messenger](./assets/poll_chat_lol2.png)