Send Message
============

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Versende eine Text Nachricht von einem deiner Telegram Accounts oder Bots in einen Einzel- oder Gruppenchat

Settings
--------

### from

Der Telegram-Account oder Bot, der die Nachricht versenden soll.

### To

Die Spieler:in oder der Gruppenchat, an den die Nachricht geschickt werden soll.

### text

Die Nachricht, die versendet werden soll.

### typing

Lege eine Verzögerung in Sekunden fest, während der die "Tippt" Info angezeigt wird, bevor die Nachricht gesendet wird.

Wenn du diese Option nicht auswählst, wird eine Verzögerung auf der Grundlage der Länge der Nachricht berechnet.

Setze den Wert auf 0, um die "Tippt" Anzeige zu überspringen.

### pin

Setzt die Nachricht als gepinnte Nachricht in den Chat.