Cast Vote
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Vote in a poll or select a quiz answer using your Telegram account.

You can only vote in a poll that a Telegram account in this game has previously created with the[Send Poll](./sendPoll.md) or [Send Quiz](./sendQuiz.md) action.

> Telegram bots cannot vote in polls. Therefore **Cast Vote** cannot be used with Telegram bots.

Settings
--------

### Telegram Account

The telegram account that will vote in the poll or select an quiz answer. The account must be a member of the `to` group chat.

### to

The group chat in which the poll or quiz was sent.

### poll

The name of the poll or quiz in which the account is casting a vote or selecting an answer.

### options

The selection that the account will vote for in the poll or quiz.

**option** can be the index of the answer, starting from `0` for the first answer choice.

**option** can be the text of the answer. The text must then exactly match the answer text in the corresponding `Answer` entry.

If it is a multiple choice survey, multiple options can be specified.