Send Quiz
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Erstelle und versende eine Quiz Umfrage in einem Gruppenchat.

Quiz ist eine besondere Form der Umfrage bei der eine der Antworten als richtige Antwort (`Correct Answer`) festgelegt wird.

Zudem lassen Quiz Umfragen keine Mehrfach Auswahl (`Multiple Choice`) zu.

Sieh dir den Eintrag zur [Send Poll](./sendPoll.md) action an um mehr über Umfragen herauszufinden.

Settings
--------

Zusätzlich zu den Settings in [Send Poll](./sendPoll.md) gibt es eine weitere Option.

### Correct Answer

Gib hier die richtige Antwort auf die `Question` an.

**Correct Answer** kann der index der Frage sein, die als richtige Antwort gewertet wird, beginnend bei `0` für die erste Antwortmöglichkeit.

**Correct Answer** kann der Fragetext der richtigen Antwort sein. Der Text muss dann exakt mit dem Antworttext im entsprechenden `Answer` Eintrag übereinstimmen.