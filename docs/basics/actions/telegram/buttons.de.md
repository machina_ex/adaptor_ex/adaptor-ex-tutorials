Buttons
========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Versende Buttons mit deinem Telegram Bot und reagiere darauf, wenn die Buttons geklickt werden.

> Telegram Accounts können keine Buttons versenden. Deshalb kann **Buttons** nicht mit Telegram Accounts verwendet werden.

Settings
--------

### Bot

Der Bot, der die Buttons versendet und auf Button Klicks reagiert.

### to

Der chat mit eine:r Spieler:in, bzw. der Gruppenchat, in den die Buttons gesendet werden.

### from

*optional*

1 oder mehrere User:innen im [to](#to) chat auf deren Button Eingaben reagiert wird. Button Klicks von anderen User:innen werden dann ignoriert.

### type

Setzt die Button Variante *inline* oder *keyboard*

#### inline

Buttons werden als Nachricht versendet und erscheinen im Chat Verlauf.

![Screenshot: Telegram Messenger Inline Buttons](./assets/buttons_inline.png)

#### keyboard

Erstellt ein alternatives Keyboard layout auf Basis der angegebenen [Buttons](#buttons-1)

![Screenshot: Telegram Messenger Button Keyboard](./assets/buttons_type_keyboard.png)

Klicks auf **keyboard** buttons senden das Button [label](#label) als Text message und können dadurch auch mit der [On Incoming Messgage](./onMessage.md) action abgefragt werden.

### message

Die Text Message die mit, bzw. vor den Buttons versendet wird.

### typing

*optional*

Lege eine Verzögerung in Sekunden fest, während der die "Tippt" Info angezeigt wird, bevor die Nachricht gesendet wird.

Wenn du diese Option nicht auswählst, wird eine Verzögerung auf der Grundlage der Länge der Nachricht ([message](#message)) berechnet.

Setze den Wert auf 0, um die "Tippt" Anzeige zu überspringen.

### Buttons

Lege fest welche Buttons angezeigt werden und wie auf Klicks reagiert wird.

Jeder Button benötigt ein `label` und kann mit mehreren `respond` und/oder einer `next` option auf Klicks reagieren.

Mit `Add row` fügst du eine neue Button reihe hinzu.

Klicke auf `Add button` um einen (weiteren) Button in der jeweiligen Reihe zu erstellen.

Du kannst beliebig viele Reihen mit beliebig vielen Buttons erstellen. Die Buttons werden entsprechend deiner Aufteilung in Reihen Angeordnet.

Du musst für jeden Button ein **label** festlegen und kannst mit **respond** eine oder mehrere Antworten angeben und/oder mit **Next State** in den nächsten State wechseln.

#### label

Gibt an, welcher Text auf dem Button steht.

Wenn du [keyboard](#type) Buttons verwendest wird der **label** text bei Klick auf den Button in den Chat gesendet.

#### respond

Gib einen **text** an, mit dem auf einen Klick auf den Button reagiert wird.

Mit `Add response` kannst du weitere Nachrichten angeben, die gesendet werden, wenn der Button ein weiteres mal geklickt wird. Die Nachrichten werden mit jedem Klick auf den Button von oben nach unten gesendet.

#### Next State

Gib einen State an, der ausgelöst wird, wenn der Button geklickt wird.

Wenn du eine oder mehrere **respond** Texte angegeben hast, wird **Next State** mit dem letzten **respond** ausgelöst. 

D.h., wenn du 1 **respond** angegeben hast, wird **Next State** beim ersten Klick auf den Button, zusammen mit der **respond** message ausgelöst.

Wenn du 2 **respond** angegeben hast, wird **Next State** erst beim zweiten Klick auf den selben Button ausgelöst.

### keep listening and queue

*optional*

Schalte den **Buttons** listener Stumm anstatt ihn ganz zu schließen, wenn der State, der **Buttons** enthält, verlassen wird.

Es wird dann zwar nicht weiter auf eingehende Button Klicks reagiert, die Klicks werden aber gespeichert. 

Wird der State, der diese **Buttons** Action enthält erneut aufgerufen, werden zunächst die gespeicherten Button Klicks überprüft, bevor der listener beginnt auf weitere Button Klicks zu hören.

!!! info
    Im Kapitel über [Schleifen](../../loops.md) findest du zusätzliche Infos zu [keep listening and queue](../../loops.md#keep-listening-and-queue)

Mit **keep listening and queue** kannst du sicherstellen, dass **Buttons** keine Button Klicks verpasst, während der listener nicht aktiv ist, sollte es möglich sein, dass er später ein weiteres mal aktiviert wird.

Wird **Buttons** mit gespeicherten Button Klicks neu aktiviert, werden die Klicks beginnend mit dem ältesten angewandt. Wird aufgrund eines gespeicherten Klicks in den `Next State` gewechselt, bleiben ggf. weitere, jüngere Button Klicks gespeichert.

Mit **enabled** kannst du das Verhalten bei Bedarf aktivieren oder deaktivieren.

Die option **max queue length** legt eine Grenze für die Anzahl der Button Klicks fest, die gespeichert werden, wenn **Buttons** stumm geschaltet ist. Wird die Grenze überschritten, werden alte Button Klicks vom Anfang der Liste entfernt.

Setze **max queue length** auf 1 um immer nur den zuletzt eingegangenen Button Klick zu speichern.

Ist **max queue length** nicht ausgewählt, ist die Anzahl der Button Klicks, die gespeichert werden nicht begrenzt.

Deaktiviere **max queue length** über die *Settings* von **keep listening and queue**

!!! Warning
    Eine sehr große Anzahl gespeicherter Button Klicks kann die Performance von adaptor:ex beeinträchtigen.

Im [Live Modus :material-play-circle-outline:{ .is-live }](../../live.md) wird die Gesamt Anzahl der eingereihten Button Klicks, auf die bisher nicht reagiert wurde, an der **Buttons** Action angezeigt :material-clock-outline:{ .is-live }

Action Daten
------------

Du kannst auf Action Daten von **Buttons** über die action data variable zugreifen. Nutze etwa:

`[[state.MyState.buttons_1.button]]`

Um auf das [label](#label) des zuletzt geklickten Button der obersten **Buttons** action im State *MyState* zuzugreifen.

#### id

Die Telegram id der Klick Interaktion bzw. der eingehenden message bei *keyboard* buttons.

#### date

Der Zeitpunkt an dem der Button geklickt wurde.

#### from_id

Die Telegram id des users, der den Button geklickt hat. Entspricht der id, die im entsprechenden Item in der 'chats' oder 'players' collection unter `telegram.id` angegeben ist.

#### peer_type

Die Art des chats in dem auf eingehende Nachrichten gehört wird. In einem einzelchat mit einem player ist **peer_type** = `user`. In einem Gruppenchat ist **peer_type** = `chat`.

#### peer

Die Telegram id des Einzel- oder Gruppenchats in den die Nachricht geschickt wurde. 

Handelt es sich um einen Einzelchat (**peer_type** ist `user`) ist **peer** == **from_id**.

Handelt es sich um einen Gruppenchat (**peer_type** ist `chat`) ist **peer** die id des chats und entspricht der `telegram.id` des Items in der 'chats' collection, dass in der **Buttons** Action als **to** angegeben ist.

#### button

Der [label](#label) text des Buttons auf den geklickt wurde.

#### match

Der [label](#label) text des Buttons auf den geklickt wurde.

Beispiel
--------

![Screenshot: Gif animation, die das fertige Beispiel zeigt](./assets/buttons_example.gif)

!!! example-file "Beispiel Level Datei"
    Download: [:material-download: buttons_example.json](./assets/buttons_example.json)

Wenn du noch keinen Telegram Bot zu deinem Game hinzugefügt hast, findest du im [Telegram Einrichten](../../../tutorials/telegram-settings/index.md#connect-a-telegram-bot) Tutorial eine Anleitung dazu.

### Buttons versenden

Ziehe eine **Buttons** action aus der ACTIONS Toolbar in den *START* State.

![Screenshot: Buttons action aus der Toolbar auf die Arbeitsfläche ziehen](./assets/buttons_drag_action.png)

Wähle einen **Bot** aus (hier: `StageManager`) der die Buttons anzeigen wird. Wähle unter **to** den Chat aus, in dem die Buttons angezeigt werden (hier: `Player`). Wähle unter **type** aus, ob die Buttons im Chat oder im User Keyboard erscheinen sollen und gib unter **message** die Nachricht an die mit den Buttons verschickt werden sollen. Hier:

`Would you like to try more inline buttons, or would you like me to show you keyboard buttons?`

![Screenshot: Sender, Empfänger type und message festlegen](./assets/buttons_basic_options.png)

### Auf Button Klicks reagieren

Lege das label des ersten Button fest und füge über die *Settings* **respond** und **Next State** hinzu.

![Screenshot: Response und Next State Options hinzufügen](./assets/buttons_basic_buttons.png)

Gib einen **respond** text an <span style="color:magenta">(1).</span>, der direkt gesendet wird, wenn dieser Button angeklickt wird. Hier:

`It's my pleasure.`

und gib einen **Next State** an <span style="color:magenta">(2)</span>, der anschließend ausgelöst wird. Hier `Inline`. Mit Klick auf den :material-plus-box-outline: Button neben dem **Next State** <span style="color:magenta">(3)</span> kannst du den neuen State direkt  erstellen.

Klicke dann auf *Add button* <span style="color:magenta">(4)</span> um einen weiteren Button zu erstellen und bearbeiten.

![Screenshot: Response und Next State angeben und neuen Button hinzufügen](./assets/buttons_button_options.png)

Gib auch für den 2. Button ein **label**, einen **respond** text und einen **Next State** an.

### Buttons testen

Um dein Level zu testen gib es als default level für deinen Bot an.

Wechsle dazu in die `Game -> Settings` und öffne das Telegram Plugin und deinen Telegram Bot.

Klicke auf den *Settings* Button und füge die **default level** Option hinzu.

![Screenshot: bot Default Level setzen in den Telegram Settings](./assets/buttons_default_level.png)

und wähle dann dein neu erstelltes Level aus.

![Screenshot: Bot Default Level auswählen in den Telegram Settings](./assets/buttons_level_select.png)

Kehre jetzt in dein Level zurück und aktiviere den Live Modus.

Wenn du deinen Bot anschreibst wird eine neue Session erstellt und du bekommst 2 Buttons zur Auswahl.

![Screen Capture: Den Bot anschreiben](./assets/buttons_test.gif)

### Mehr Button Reaktionen

Füge nun eine [Send Message](./sendMessage.md) Action zu dem *Inline* State hinzu.

Wähle deinen **Bot** aus und gib eine **message** an, die gesendet wird, wenn der *Inline* State ausgelöst wird. Hier:

`You can click on the same button again after a short time and I will send you the next answer. Only if you click on **apple** I will goto the next state immediately.`

![Screenshot: Eine Nachricht mit Send Message senden](./assets/buttons_message.png)

Ziehe nun eine weitere **Buttons** Action in den *Inline* State und bearbeite sie wie oben beschrieben.

Um ohne Antwort direkt in den **Next State** zu springen wähle keine **respond** Option in den *Settings* an.

![Screenshot: Nur Next State](./assets/buttons_next_only.png)

Um bei häufigeren Button Klicks verschiedene Antworten zu schicken füge weitere **respond** Nachrichten hinzu.

![Screenshot: Mehrere respond Nachrichten](./assets/buttons_add_response.png)

### Buttons anordnen

Um deine Buttons in mehreren Reihen anzuordnen klicke im **Buttons** Formular auf *Add row*

![Screenshot: Buttons in mehreren Reihen anordnen](./assets/buttons_add_row.png)

Jede Reihe kann mehrere Buttons enthalten. 

![Screenshot: Button Anordnung in zwei Reihen im Editor](./assets/buttons_rows.png)

Im Chat wir die Größe der Buttons so angepasst, dass alle Reihen die gleiche Länge besitzen.

![Screenshot: Button Anordnung in zwei Reihen im Chat](./assets/buttons_rows_chat.png)

### Keyboard Buttons

Auf die selbe Art kannst du auch **keyboard** Buttons nutzen.

Ziehe eine weitere **Buttons** Action in den *Keyboard* State und bearbeite sie wie oben beschrieben aber wähle unter **type** `keyboard` aus.

![Screenshot: keyboard type auswählen](./assets/buttons_keyboard.png)

Wird der *Keyboard* State ausgelöst wird das Keyboard des Users durch die Buttons ersetzt.

![Screenshot: keyboard type ausprobieren](./assets/buttons_test_keyboard.png)

