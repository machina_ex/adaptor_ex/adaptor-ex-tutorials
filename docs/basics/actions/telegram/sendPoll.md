Send Poll
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Create and send a poll in a group chat.

Polls can then be analyzed using Chat [Events](#events) and the [Poll Data](#poll-data).

Settings
--------

### from

The telegram account or bot that sends the poll. The agent must be a member of the `to` group chat.

### to

The group chat to which the poll is sent.

### Name

The name under which the poll is saved in the chat data item. With `name` you can address and evaluate the poll.

Polls are stored in the chat document in the *chats* data collection under "polls". Call the poll data e.g. within a variable: `[[chats.Chatname.polls.Pollname]]`.

### Question

The question text asked for in the poll.

### Answers

A list of possible answers. Add as many answers as you like with `Add Answer`.

### Anonymous Vote

Specify whether the other group members can see how they voted.

Select the option to hide the voting behavior.

### Multiple Choice

Specify whether multiple answers can be selected.

Events
------

You can react to changes in a survey created by one of your accounts using events.

The event is triggered by the group chat where the poll is taking place.

A **pollUpdate** event is triggered when a member of the group chat has voted in the poll or quiz.

A **pollComplete** event is triggered when all members of the group chat have cast at least one vote in the poll or quiz. A **pollComplete** event is also triggered if the account that created the poll or quiz did not vote themselves.

Both the payload of **pollUpdate** and the payload of **pollComplete** contain `name` and `id` of the poll and the updated poll data `voters`, `voters_count`, `results`, `ranking` and `ambiguous`.

Details about the updated poll data can be found at [Updated data](#updated-data).

Use the `if` option of the [Event action](../logic/onEvent.md) to check the event for poll data.

![Check if a specific player has voted](./assets/poll_event_example.png)

In the `field` option of the if query you can specify the different poll data to be checked. In the example above, we check if the player referenced as `PlayerA` in the level has already cast a vote.

> The **pollComplete** event is triggered regardless of whether the account that created the poll has cast a vote. With one exception: The vote of the account was cast in an anonymous poll in another Telegram client like the official Telegram app.

Poll data
----------

The poll data is stored in the corresponding group chat item in the 'polls' variable. To distinguish between different polls, each poll is stored under it's 'name' property.

> Note that if you create a poll with the same `name` property in the same chat again, the poll data of the previous poll will be overwritten.

On the one hand, this allows you to access the initial data such as `question` and `answers`. On the other hand, the data is updated with each vote of a group member and indicates, for example, the number of members who have already voted (`voters_count`).

### Origin data

* **`name`** - the name of the poll.

  *Example*: `[[Groupchat.polls.my_poll.name]]` results in "my_poll".

* **`from_id`** - telegram id of the account or bot that created the survey.

  *Example*: `[[Groupchat.polls.my_poll.from_id]]` results in "1234567890".

* **`question`** - The question text.

  *Example*: `[[Groupchat.polls.my_poll.question]]`

* **`answers`** - List of answer choices starting with `0`.

  *Example*: `[[Groupchat.polls.my_poll.answers.2]]` outputs the answer text of the 3rd answer.

* **`private`** - Boolean ("true" or "false") indicating whether the poll is open or secret.

  *Example*: `[[Groupchat.polls.my_poll.anonymous]]` returns "true".

* **`multiple_choice`** - Anonymous Vote boolean ("true" or "false") indicating whether multiple answers can be selected.

* **`correct_answer`** - the correct answer (Quiz only!)

  *Example*: `[[Groupchat.polls.my_poll.correct_answer]]` returns "Yes" if "Yes" is the correct answer option.


### Updated data

* **`voters_count`** - Number of group members who have participated in the survey so far.

  *Example*: `[[Groupchat.polls.my_poll.voters_count]]` results in 1 if one group member has voted.

* **`voters`** - List of Telegram ids of members who have already participated in the poll.

  *Example*: `[[Groupchat.polls.my_poll.voters.0]]` results in 1234567 if the first group member who participated in the poll has the Telegram id 1234567.

* **`from_voted`** - Indicates whether the account that created the poll cast a vote itself (value is "true" or "false").

  `from_voted` is also set to "true" in anonymous polls if the vote was cast with the [Cast Vote](./castVote.md) action. If the vote is cast in an anonymous poll with another Telegram client, e.g. an official telegram app, `from_voted` remains "false".

  *Example*: `[[Groupchat.polls.my_poll.from_voted]]` returns "true" if the account has previously cast a vote.

  > Bots cannot participate in polls themselves, so this value is always `false` if a bot created the poll.

* **`ambiguous`** - Boolean ("true" or "false") indicating if there is exactly one or more most chosen answer options.

  `ambiguous` is "true" if several answer options have received the same number of votes, but only if it is the highest number of votes.

  *Example*: `[[Groupchat.polls.my_poll.ambiguous]]` is "false" if there was a Unique result for the first choice.

* **`results`** - List of current poll results in order of `answers`. Each entry in `results` has the following properties:

  * `votes` - number of votes cast for this answer.
  * `voters` - List of telegram ids of users who have chosen this response (only if data can be retrieved. See [user data](#user-data)).
  * `answer` - text of the answer option
  * `rank` - position in the ranking of most votes cast for the answer option (0 for most votes, 1 for second most, etc.)
  * `correct` - Boolean ("true", "false") whether this is the correct answer (quiz only!)

  Use the corresponding PLatz in the list of answers (starting with `0`) to call up the corresponding answer result.

  *Example*: `[[Groupchat.polls.my_poll.results.2.votes`]] results in 5, if 5 votes were cast for the 3rd answer choice

* **`ranking`** - List of current poll results in order of most votes cast for the answer option. Each entry in `ranking` has the following properties:

  * `votes` - number of votes cast for this answer.
  * `voters` - List of telegram ids of users who have chosen this response (only if data can be retrieved. See [user data](#user-data)).
  * `answer` - text of the answer option
  * `index` - position in the order in which the answer options were originally ranked (0 for the first answer, 1 for the second etc.)
  * `correct` - Boolean ("true", "false") whether this is the correct answer (quiz only!)

  *Example*: `[[Groupchat.polls.my_poll.ranking.0.answer]]` results in "yes" if the answer option "yes" has the most votes.

  Note that `ranking` favours the original top answer if the number of votes is equal!

### User data

It is possible to find out which group member has chosen which answer option.

However, the poll must not be [anonymous](#anonymous-vote) and the account who created the poll must have participated in the poll himself.

Use the [Cast Vote](./castVote.md) action to cast one or more votes with the account after creating the poll.

The telegram ids of the group members who participated in the poll can be found in the [poll data](#poll-data) as `voters`.

The telegram id in the poll data corresponds to the entry in 'telegram.id' of an item in the `players` collection.

For group members that are already stored as `players` item in your game, their selection is also stored in the respective item.

An entry `polls` is added to the item, where, like in the chats item, the poll can be found under its name. Here you will find under `votes` a list of the votes the player has cast.

For example, if the `players` item with `name` property "Ada" has chosen the 2nd and 5th option in the "my_poll" poll, this variable:

`[[players.Ada.polls.my_poll.votes]]`

will result in a list with values `1` and `4` (as array: `[1,4]`). Note that the poll options are counted starting at `0`.

Example
---------

> Level File: [:material-file-code-outline: poll_example.json](./assets/poll_example.json)

In this example, we create a new group chat and start a poll. Once all users in the chat group have participated, we analyze the answers and react to the result.

Create a new chat with [Create Chat](./createChat.md) and enter "Groupchat" as `reference` or use an existing chat that you have loaded into the level as "Groupchat" (for example with [Get Item](../data/getItem.md)).

Drag a **Send Poll** action onto the STAGE and give the new state the name "LeakItOrNot". Enter the Telegram Account or Bot in `from` that will create the poll and under `to` the chat "Groupchat". Enter a short name under `name`, here "leak" and formulate the `Question`: "What should I do with the CD?

Click on `Add Answer` under `Answer Options` and add two answer options.

![The send poll action](./assets/poll_send_example.png)

Now we first create a query whether the poll has been carried out.

Drag an [On Event](../logic/onEvent.md) action into the "LeakItOrNot" state. Put `Groupchat` under *from* and `pollCopmplete` as event. In *Settings* select the `if` option and add a new If condition with `Add Condition`. Enter `name` as *field* and `leak` under *equals* to make sure we check the correct poll in the groupchat.

Set "CheckPoll" as `next state` .

![Query whether all group members have voted](./assets/poll_all_voted.png)

In order not to make the poll evaluation unconditionally dependent on all group members wanting to participate, we add a [Timeout](../time/timeout.md) action to "LeakItOrNot".

![5 minutes until evaluation](./assets/poll_timeout.png)

If not all group members have voted after 5 minutes, the existing votes are used for evaluation.

Now drag a [Switch](../logic/switch.md) action onto the STAGE and rename the new state to "CheckPoll".

Here we will find out which answer option has received the most votes.

Create three new conditions.

In the first condition we check whether the result was unique. For **value** enter `[[Groupchat.polls.leak.ambiguous]]` and for **equals** enter `true`. For **next state**, specify `Undecided`.

In the 2nd condition we check whether the first answer option has received the most votes. For **value** enter `[[Groupchat.polls.leak.results.0.rank]]` and for **equals** enter `0`. For **next state**, enter `Destroy`.

In the 3rd condition we check whether the second answer option has received the most votes. For **value** enter `[[Groupchat.polls.leak.results.1.rank]]` and for **equals** enter `0`. For **next state**, enter `leak`.

![Evaluate the poll with the switch action](./assets/poll_evaluate.png)

So there are 3 possible outcomes of the poll that we have to reckon with. For each variant we create new states.

![The level in the overview](./assets/poll_full_example.png)

When the state "LeakItOrNot" is triggered, a new poll is sent to "Groupchat" and we wait for all group members to participate in the poll.

![The poll in telegram Messenger](./assets/poll_chat_lol.png)

As soon as the other two group members have cast their votes or as soon as the timeout has expired, the poll is evaluated and the corresponding state is triggered.

![The completed poll in telegram Messenger](./assets/poll_chat_lol2.png)