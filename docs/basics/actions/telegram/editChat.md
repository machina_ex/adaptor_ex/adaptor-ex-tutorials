Edit Chat
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Change the settings of a Telegram Group Chat.

For example, change the description, name or profile picture of your group chat or channel.

Settings
--------

### Account or Bot

The account or bot that is to handle the chat. This account must be the admin of the group.

### chat

The chat to edit. Use the `reference` name or address a chat in the `chats` collection.

### add members

Add Telegram user to the chat.

Enter telegram accounts or player data items that are connected to a telegram user here.

### remove members

Remove Telegram user from the chat.

Specify here telegram accounts or player data items that are connected to a telegram user and are in this chat.

### photo

Upload a group profile picture or change the current picture.

Use a picture file from your [Files](../../files.md).

### about

Change the group description.

### permissions

Specify which interactions are allowed for members of the chat who are not admins.

Prohibit an interaction option by deselecting the permission.

The different ways to restrict interactions](./assets/chat_permissions.png)

In this example, group members without admin status cannot send media files, GIFs or stickers.