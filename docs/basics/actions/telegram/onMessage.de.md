
On Incoming Message
===================

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Reagiere auf vielfältige Weise auf eingehende Telegram Nachrichten.

Settings
--------

### Account or Bot

Der Telegram Account oder Bot, welcher auf eingehende Nachrichten wartet und reagiert.

> Telegram Bots können nicht ohne weiteres alle Nachrichten in einem Gruppenchat lesen. Sieh dir die Anleitung dazu im [Telegram Setup tutorial](../../../tutorials/telegram-settings/index.md#messages-in-group-chats) an.

### chat

Der chat mit eine:r Spieler:in, bzw. der Gruppenchat, in dem auf eingehende Nachrichten reagiert wird

### from

*optional*

Erlaubt ein players oder Telegram Account Item anzugeben, wenn **On Incoming Message** auf eine Nachricht von einem bestimmten players Item oder account hören soll. 

Nur für Gruppenchats.

### priority

*optional*

Bestimmt die Priorität von **On Incoming Message**, falls es mehrere listener gleichzeitig gibt etwa auf anderen Pfaden bzw. Erzählsträngen des Games.

### if

*optional*

Mit **if** können eingehende Nachrichten auf verschiedenste Weise überprüft und darauf reagiert werden. Mehr dazu findest du unter [Abfrage von Bedingungen (Conditions)](#abfrage-von-bedingungen-conditions) heraus.

### else

*optional*

Tritt ein, wenn keine der if-Bedingungen erfüllt wurde. Siehe [Abfrage von Bedingungen (Conditions)](#abfrage-von-bedingungen-conditions)

### while idle

*optional*

Füge Nachrichten hinzu, die nach einer bestimmten Zeitspanne (`delay`) in den chat gesendet werden solange keine der eingehenden Nachrichten durch **if** oder **else** beantwortet werden.

Diese option eignet sich etwa um Erinnerungen oder genauere Hinweise zu senden.

`delay` bestimmt die Zeit in Sekunden, bis `message` versendet wird.

Alle **while idle** Nachrichten delays werden aneinander gereiht. Wenn etwa die erste **while idle** Nachricht ein `delay` von 60 Sekunden und die zweite Nachricht ein `delay` von 30 Sekunden hat, wird die zweite Nachricht nach 90 Sekunden versendet.

### finally

*optional*

Gehe zum `next state` wenn alle [while idle](#while-idle) Nachrichten versendet wurden. Gib mit `delay` eine weitere Zeitverzögerung an, die an die letzte [while idle](#while-idle) Nachricht angehängt wird bis `next state` ausgelöst wird.

### keep listening and queue

*optional*

Schalte den **On Incoming Message** listener Stumm anstatt ihn ganz zu schließen, wenn der State, der **On Incoming Message** enthält, verlassen wird.

Es wird dann zwar nicht weiter auf eingehende Telegram Nachrichten reagiert, die messages werden aber gespeichert. 

Wird der State, der diese **On Incoming Message** Action enthält erneut aufgerufen, werden zunächst die gespeicherten Nachrichten überprüft, bevor der listener beginnt auf weitere Nachrichten zu hören.

!!! info
    Im Kapitel über [Schleifen](../../loops.md) findest du zusätzliche Infos zu [keep listening and queue](../../loops.md#keep-listening-and-queue)

Mit **keep listening and queue** kannst du sicherstellen, dass **On Incoming Message** keine Nachrichten verpasst, während der listener nicht aktiv ist, sollte es möglich sein, dass er später ein weiteres mal aktiviert wird.

Wird **On Incoming Message** mit gespeicherten Nachrichten neu aktiviert, werden die messages beginnend mit der ältesten angewandt. Wird aufgrund einer gespeicherten Nachricht in den `Next State` gewechselt, bleiben ggf. weitere, jüngere Nachrichten gespeichert.

Mit **enabled** kannst du das Verhalten bei Bedarf aktivieren oder deaktivieren.

Die option **max queue length** legt eine Grenze für die Anzahl der Nachrichten fest, die gespeichert werden, wenn **On Incoming Message** stumm geschaltet ist. Wird die Grenze überschritten, werden alte Nachrichten vom Anfang der Liste entfernt.

Setze **max queue length** auf 1 um immer nur die zuletzt eingegangene Nachricht zu speichern.

Ist **max queue length** nicht ausgewählt, ist die Anzahl der Nachrichten, die gespeichert werden nicht begrenzt.

Deaktiviere **max queue length** über die *Settings* von **keep listening and queue**

!!! Warning
    Eine sehr große Anzahl gespeicherter Nachrichten kann die Performance von adaptor:ex beeinträchtigen.

Im [Live Modus :material-play-circle-outline:{ .is-live }](../../live.md) wird die Gesamt Anzahl der eingereihten Nachrichten, auf die bisher nicht reagiert wurde, an der **On Incoming Message** Action angezeigt :material-clock-outline:{ .is-live }

In diesem Beispiel Level wird **keep listening and queue** genutzt damit der Bot absichtlich verzögert auf eine Antwort reagiert. Durch den zeitweisen Wechsel in den *CoffeeBreak* State wird die eingehende Nachricht zunächst nur zwischengespeichert. Erst mit dem Wechsel zurück in *LeaveMeAMessage* wird auf die Nachricht reagiert.

![Screen Recording](./assets/on_message_queue_example.gif)

!!! example-file "Example Level File"
    Download: [:material-download: telegram_queue_example.json](./assets/telegram_queue_example.json)

Abfrage von Bedingungen (Conditions)
------------------------------------

Mit **if** kann eine oder mehrere Bedingungen abgefragt werden. Dabei wird der Inhalt der Nachricht(en) überprüft, die eingehen, solange der State (mit dieser **On Incoming Message** action) aktiv ist. 

Nutze **else** um anzugeben was passiert, falls bei einer eingehenden Nachricht keine der if-Bedingungen eingetreten sein sollte. 

Um Bedingungen zu **if** hinzuzufügen klicke auf "add condition".

![Settings dann if auswählen, dann Klick auf add condition](./assets/on_message_if_add_condition.png)

Im Dropdown neben **condition 1** besteht nun die Wahl zwischen verschiedenen Abfragearten.

![Dropdown mit abfrage arten](./assets/on_message_if_condition_types.png)

### Abfragen

Mit Ausnahme von **contains media** sind die Abfragearten im Kapitel [Bedingungen Abfragen mit Conditions](../../conditions.md#abfragetypen-operatoren) genauer beschrieben.

Die Bedingung trifft zu wenn:

#### Equals

Der Inhalt der Nachricht entspricht exakt dem oder einem der hier angegebenen Texte. 

#### Contains

Die Nachricht beinhaltet den oder einen der hier angegebenen Texte.

#### Contains media

Die Nachricht beinhaltet ein Medienformat. 

Wähle unter **media** aus, welches medienformat den `next state` oder `response` auslösen wird. Nutze **contains any of** um dabei verschiedene medienformate zu kombinieren.

Wähle "any" um auf jede eingehende Nachricht, die ein Medienformat enthält zu reagieren.

Wähle "downloadable" um auf jede eingehende Nachricht, die eine Datei enthält, die heruntergeladen werden kann zu reagieren.

Mit der [download](#download) option können eingehende Dateien heruntergeladen werden.

Medienspezifische Daten können über die [action Daten](#action-daten) abgerufen werden.

#### Less Than / Greater Than 

Die Nachricht ist eine Zahl dessen Wert Größer oder Kleiner als der hier angegebene Wert ist.

#### Regular Expression

Die Nachricht matched die hier angegebene RegEx.

#### Javascript Function

Die javascript Funktion mit der Nachricht als Übergabe Parameter `value` gibt einen `return` Wert zurück.

#### Database Query

Die angegebene mongo query findet mindestens ein Item in der angegebenen collection.

### Reaktionen

Wenn eine Bedingung zutrifft, wird eine oder mehrere der unter der condition angegebenen Reaktionen ausgelöst.

**On Incoming Message** erlaubt 3 Arten um auf eingehende Nachrichten zu reagieren.

#### respond

Sollte eine der Bedingungen eintreten, kann unter **respond** eine (oder mehrere) Antwort-Nachricht(en) angegeben werden, die der Account oder Bot als Antwort an den Absender ([chat](#chat)) schickt.

Wenn du mehrere Antwortnachrichten unter **respond** angibst, wird mit jedem Mal, dass die Bedingung zutrifft, die jeweils nächste Antwort in der Liste gesendet.

> Mit der letzten Antwort der Liste wird ggf auch in den **next state** gewechselt.

Kommen weitere Nachrichten an, die auf die Bedingung zutreffen, werden keine weiteren Antworten geschickt.

Wähle unter **mode** "reply" aus, wenn dabei die eingehende Nachricht zitiert werden soll.

![Beispiel für zitierende Antwort auf eingehende Nachricht](./assets/on_message_reply.png)

#### next state

Wenn die Bedingung zutrifft wird in den `next state` gewechselt.

Wenn du gleichzeitig eine **respond** Nachricht angegeben hast, wird direkt, nachdem die Antwort verschickt wurde, in den `next state` gewechselt.

Wenn du gleichzeitig mehrere **respond** Nachrichten angegeben hast, wird erst, im Anschluss an die letzte Antwort in den `next state` gewechselt. D.h. `next state` wird erst ausgelöst, wenn die Bedingung so oft auf eingehende Nachrichten zutraf, wie du **respond** Nachrichten angegeben hast.

Wenn keine weitere Antwort zur Verfügung steht und kein **next state** angegeben ist, die Bedingung aber erneut eintrifft, passiert nichts.

#### download

**download** erlaubt es Dateien herunterzuladen, wenn mit **contains media** eingehende Nachrichten auf Medienformate überprüft wurden. Die Formate "photo", "image", "document", "audio", "voice", "video" und "sticker" enthalten Mediendateien die heruntergeladen werden können. 

Gib unter **directory** an, unter welchem Dateipfad die Datei gespeichert werden soll. Relative Pfade (beginnen ohne '/' oder mit './') beziehen sich auf den Ort des [File](../../files.md) Ordners deiner adaptor:ex Installation. Beachte, dass der Dateipfad bereits existieren muss.

Heruntergeladene Dateien haben einen automatisch generierten Dateinamen. Du kannst mit **filename** einen eigenen Namen für die Datei festlegen. Beachte, dass es sich dabei um den Namen ohne Dateiendung (*.jpg, .png, .wav* ...) handelt. Die Dateiendung wird, je nach Dateityp, automatisch angehängt.

Auf den vollen dateipfad inklusive Dateinamen kannst du anschließend in der action variable zugreifen. Du kannst unter **variable** zusätzlich eine lokale variable erstellen, die den Dateipfad enthält.

Beispiel:

![Dateipfad, Name und Pfad variable in contains media download](./assets/on_message_download_example.png)

Hier wird die Nachricht auf "photo" und "image" Medien überprüft die anschließend ins [Files](../../files.md) Verzeichnis unter "location_scout/img/" heruntergeladen werden.

Der filename ist hier variabel und hängt von den variablen `location` und `index` ab. Die response message enthält den unter der **variable** "latest_img" angegebenen Dateipfad. Ein heruntergeladenes Photo könnte etwa unter `~/adaptorex/games/Tutorial/files/location_scout/img/backyard_3.jpg` zu finden sein.

Action Daten
------------

**On Incoming Message** speichert abhängig vom message Typ unterschiedliche Informationen ab.

Du kannst auf die Daten über die action data variable zugreifen. Nutze etwa:

`[[state.MyState.onMessage_1.text]]`

Um auf den zuletzt eingegangenen Nachrichtentext der obersten **On Incoming Message** action im State *MyState* zuzugreifen.

### Ausgangsdaten

Die folgenden Eigenschaften sind immer in **On Incoming Message** action Daten enthalten:

#### id

Die Telegram id der eingehenden message.

#### date

Der Zeitpunkt an dem die Nachricht in den chat gesendet wurde.

#### from_id

Die Telegram id des users, der die Nachricht (in den chat) geschickt hat. Entspricht der id, die im entsprechenden Item in der 'chats' oder 'players' collection unter `telegram.id` angegeben ist.

#### peer_type

Die Art des chats in dem auf eingehende Nachrichten gehört wird. In einem einzelchat mit einem player ist **peer_type** = `user`. In einem Gruppenchat ist **peer_type** = `chat`.

#### peer

Die Telegram id des Einzel- oder Gruppenchats in den die Nachricht geschickt wurde. 

Handelt es sich um einen Einzelchat (**peer_type** ist `user`) ist **peer** == **from_id**.

Handelt es sich um einen Gruppenchat (**peer_type** ist `chat`) ist **peer** die id des chats und entspricht der `telegram.id` des Items in der 'chats' collection, dass in der **On Incoming Message** Action als **chat** angegeben ist.

### Optionale Daten

Einige action Daten sind nur verfügbar, wenn es sich bei der letzten eingegangenen Nachricht um einen bestimmten Nachrichten Typ handelt. **match** ist nur verfügbar, wenn eine der **if** Bedingungen auf die letzte eingegangene Nachricht zutraf.

#### text

Der Text der eingehenden Textnachricht. Wenn Medien Nachrichten eine "caption" enthalten wird diese auch als **text** gespeichert.

#### match

Der text oder Teil des Textes, auf den die **if** Bedingung zutraf.
#### media

Der Medien Typ der eingegangenen Nachricht. Kann einen der Folgenden Werte annehmen:

"photo", "image", "document", "audio", "voice", "video", "sticker", "geo","geolive", "contact", "poll", "webpage", "venue", "game", "dice" oder "invoice"

Einige Medien Nachrichten speichern zusätzliche Werte in den action Daten ab (s.u.).

#### download

Der Dateipfad der heruntergeladenen Medien Datei. Siehe [download](#download).
#### first_name

Die Vorname Angabe der eingegangenen "contact" Media Message.

#### last_name

Die Nachname Angabe der eingegangenen "contact" Media Message.

#### phone

Die Telefonnummer Angabe der eingegangenen "contact" Media Message.

#### contact_id

Die Telegram id Angabe der eingegangenen "contact" Media Message.

#### lat

Der Längengrad (Latitude) der eingegangenen "geo", "geolive" oder "venue" Media Message.

#### long

Der Breitengrad (Longitude) der eingegangenen "geo", "geolive" oder "venue" Media Message.

#### title

Der Name des Ortes, der eingegangenen "venue" Media Message
#### address

Die Adresse des Ortes, der eingegangenen "venue" Media Message.

#### provider

Der Betreiber des Ortes, der eingegangenen "venue" Media Message.

#### venue_id

Die Telegram id des Ortes, der eingegangenen "venue" Media Message.

#### venue_type

Die Kategorie des Ortes, der eingegangenen "venue" Media Message.

#### value

Der Würfelwert der eingegangenen "dice" Media Message.

#### emoticon

Das verwendete emoticon der eingegangenen "dice" Media Message.