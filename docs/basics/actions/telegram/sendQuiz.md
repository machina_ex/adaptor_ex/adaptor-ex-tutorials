Send Quiz
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Create and send a quiz survey in a group chat.

Quiz is a special form of poll where one of the answers is set as the correct answer.

In addition, quiz polls do not allow multiple choice.

See the entry on the [Send Poll](./sendPoll.md) action to find out more about polls.

Settings
--------

In addition to the settings in [Send Poll](./sendPoll.md) there is one more option.

### Correct Answer

Specify the correct answer to the `Question` here.

**Correct Answer** can be the index of the question that will be counted as the correct answer, starting at `0` for the first answer choice.

**Correct Answer** can be the question text of the correct answer. The text must then exactly match the answer text in the corresponding `Answer` entry.