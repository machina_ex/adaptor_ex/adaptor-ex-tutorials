
Send Geo Location
=================

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

With *Send Geo* a digital road map with marker can be sent.

## Settings

### from

The Telegram account or bot that is to send the geolocation.

### To

The player(s) to whom the geolocation should be sent.

### Latitude

The Latitude value. For Example:

`52.438015`

### Longitude

The Longitude value. For Example:

`13.231191`

Latitude and longitude of a place can be found e.g. via Open Street Maps or Google Maps.

![screenshot](../../../tutorials/telegram-send-media/assets/13-1.png){ style=width:45% }

![screenshot](../../../tutorials/telegram-send-media/assets/15.png){ style=width:45% }
