Cast Vote
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Stimme in einer Umfrage ab oder wähle eine Quiz-Antwort mit deinem Telegramm Account aus.

Du kannst nur in einer Umfrage abstimmen, die ein Telegram Account in diesem Game zuvor mit der Aktion [Send Poll](./sendPoll.md) oder [Send Quiz](./sendQuiz.md) erstellt hat.

> Telegram Bots können nicht in Umfragen abstimmen. Deshalb kann **Cast Vote** nicht mit Telegram Bots verwendet werden.

Settings
--------

### Telegram Account

Der telegram account, der die Umfrage oder das Quiz beantwortet. Der Account muss Mitglied im `to` Gruppenchat sein.

### to

Der Gruppenchat in dem die Umfrage oder das Quiz gesendet wurde.

### poll

Der Name der Umfrage oder des Quiz, in dem der Account eine Stimme abgibt.

### options

Die Auswahl, die der Account in der Umfrage oder dem Quiz auswählen wird.

**option** kann der index der Antwortsein, beginnend bei `0` für die erste Antwortmöglichkeit.

**option** kann der Text der Antwort sein. Der Text muss dann exakt mit dem Antworttext im entsprechenden `Answer` Eintrag übereinstimmen.

Wenn es sich um eine "multiple choice" Umfrage handelt können mehrere Optionen angegeben werden.