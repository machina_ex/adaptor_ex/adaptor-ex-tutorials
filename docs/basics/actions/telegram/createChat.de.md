Create Chat
===========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Erstelle einen neuen Telegram-Gruppenchat oder -Kanal.

Der gewählte Telegram Account erstellt einen neuen Gruppenchat oder Kanal und lädt andere Telegram-Accounts in den Chat ein.

In adaptor:ex wird gleichzeitig ein [Data Item](../../variables.md#collections-und-data-items) in der `chats` collection angelegt, das den Chat repräsentiert.

> Telegram bots können selbst keine Chats mit **Create Chat** erstellen. Du kannst Bots aber als [member](#members) zu einem Gruppenchat hinzufügen. Anschließend kann der Bot den Chat, wenn er die entsprechenden Berechtigungen Besitzt, mit [Edit Chat](./editChat.md) anpassen.

Settings
--------

Du kannst bereits beim Erstellen des Chats die meisten Optionen festlegen. Wenn du bestimmte Optionen später ändern willst, nutze die [Edit Chat](./editChat.md) action.

### Telegram Account

Der Account, der den Chat erstellen soll. Dieser Account ist automatisch Admin der neu erstellten Gruppe.

### type

Lege fest, ob du einen Gruppenchat (`chat`) oder Kanal (`channel`) erstellen willst. Hier ist der Unterschied erklärt: [https://telegram.org/faq#f-was-ist-der-unterschied-zwischen-gruppen-und-kanalen](https://telegram.org/faq#f-was-ist-der-unterschied-zwischen-gruppen-und-kanalen)

### name

Lege einen Namen für deine Gruppe oder deinen Kanal fest. Wenn du keinen `title` festlegst erscheint `name` im Titel des Chats.

Wenn `name` keine Sonderzeichen oder Leerzeichen enthält kannst du `name` nutzen um den Chat in einem anderen Level bzw. in einer anderen Session zu adressieren. Etwa: `[[chats.Chatname]]`

Bedenke, dass `name` in diesem Fall für jeden Chat einzigartig sein muss.

Um den Chat im aktuellen Level bzw. der aktuellen Session eindeutig zu adressieren empfehlen wir `reference` verwenden.

### title

Gib einen Titel für den Chat an.
### members

Eine Liste der Mitglieder, die der Gruppe hinzugefügt werden sollen.

Verwende Data Items, player oder telegram accounts, die mit einem telegram user verbunden sind.

Beispiele:

`Player` - füge den player aus dem level Argument Player hinzu

`players.{'telegram.first_name':'Amanda'}` - füge den Spieler-Eintrag aus der `players` collection hinzu, der in seinem Telegram Account den Vornamen "Amanda" angegeben hat

`telegram.accounts.Chris` - lade den Telegram Account `Chris` in den Chat ein.

### photo

Lade ein Gruppenbild für den Chat hoch. 

Nutze dafür eine Bilddatei aus deinen [Files](../../files.md).

### about

Füge eine Textbeschreibung für die Gruppe hinzu.

### create invite link

Wähle **create invite link** an um einen öffentlichen Link zu erzeugen, über den User diesem Gruppenchat beitreten können.

Der link ist anschließend im chat Item als variable `link` gespeichert. Du kannst ihn z.B. so innerhalb einer action abrufen:

`[[chats.MyChat.link]]`


### permissions

Lege fest, welche Interaktionen für Mitglieder des Chats, die nicht Admins sind, erlaubt sind.

Verbiete eine Interaktionsmöglichkeit, indem du die permission abwählst.

![Die verschiedenen Möglichkeiten Interaktionen zu beschränken](./assets/chat_permissions.png)

Gruppenmitglieder ohne Admin-Status können in diesem Beispiel keine Mediendateien, GIFs oder Sticker versenden.

### reference

Gib beim Erstellen des chats einen `reference` Wert an. So kannst du den Chat anschließend im selben Level einfach adressieren. 

Wenn du die Referenz in den Telegram actions als `chat` oder `to` angibst, wird der Telegram Chat adressiert. In anderen actions verweist die Referenz auf das `chat` Data Item und du kannst auf seine Eigenschaften zugreifen.
 
Chat Daten
---------

Das Data Item, das mit dem Chat erstellt wurde, enthält Informationen über den Chat, die beständig aktualisiert werden.

> Beachte: wenn der adaptor:ex Server nicht aktiv ist während Änderungen am Chat passieren, werden diese Änderungen nicht im Data Item angepasst

Über `reference` oder `name` kannst du das Chat Item adressieren und die Chat Daten als variablen in anderen actions verwenden.

Die folgenden Daten können im Chat Item enthalten sein

* **`name`** - der Name des Chats. `name` ist nicht notwendigerweise der Titel des Chats

* **`title`** - Der Anzeigename des Chats

* **`members`** - Liste der telegram ids der Gruppenmitglieder

* **`member_count`** - Anzahl der Gruppenmitglieder. Der Account, der den Chat erstellt hat wird hier nicht mitgezählt!

* **`telegram`** - Telegram Eigenschaften dieses chats wie `id` und `participants_count`

* **`polls`** - Umfragen, die in diesem Chat mit [Send Poll](./sendPoll.md) oder [Send Quiz](./sendQuiz.md) erstellt wurden. `polls` enthält [Send Poll Umfragedaten](./sendPoll.md#umfragedaten)

Chat Events
------------

Änderungen an den Einstellungen eines chats lösen Events aus, die mit der [On Event](../logic/onEvent.md) action abgefangen werden können.

Gib in [On Event](../logic/onEvent.md) **from** eine Referenz auf das Item in der 'chats' collection an um auf Events des entsprechenden Gruppenchats zu reagieren.

Gib einen der folgenden Eventnamen unter **event** an um auf das entsprechende Event zu reagieren.

### titleChanged

Der Titel des chats wurde geändert.

payload:

**`title`** - der neue chat Titel
### membersAdded

Ein- oder mehrere user wurden dem chat hinzugefügt

payload:

**`ids`** - Liste der Telegram ids der hinzugefügten user
**`count`** - Anzahl der dem chat hinzugefügten user

### memberJoined

Ein user ist dem chat (etwa über einen invite link) beigetreten.

payload:

**`id`** - Telegram id des beigetretenen users
**`inviter_id`** Telegram id des users, der die Einladung an den beigetretenen user verschickt hat.
### memberLeft

Ein user hat den chat verlassen.

payload:

**`id`** - Telegram id des users der den chat verlassen hat.

### memberRemoved

Ein user wurde von einem Administrator aus dem chat entfernt.

**`id`** - Telegram id des users der aus dem chat entfernt wurde.

Beispiel
--------

![Einen neuen Gruppenchat erstellen](./assets/create_chat_example.png)

Erstellt einen neuen Gruppenchat "Better Together" für den Telegram Account "Thekla", in den der Telegram-Account von "Player" und dem player mit der `name` variable "Arvid" eingeladen wird.

![Der neue Gruppenchat in Telegram](./assets/new_telegram_chat_android.png)

Da wir beim Erstellen des chats einen `reference` Wert angegeben haben, können wir den Chat anschließend im selben Level z.B. in der [Send Message](./sendMessage.md) action adressieren.

![Eine Nachricht an einen Gruppenchat senden](./assets/send_message_to_chat.png)

![Die Nachricht im Gruppenchat](./assets/to_telegram_chat_android.png)

Du kannst den Chat auch über die `chats` collection adressieren, z.B. wenn du ihn in einem Level verwenden willst, das nicht über die Referenz verfügt.

Adressiere den Chat über eine Query:

`[[chats.{name:"Better Together"}]]`

Wenn du keine Leerzeichen verwendet hast, kannst du den Namen auch direkt verwenden, z.B.:

`[[chats.BetterTogether]]`

![Einen Gruppenchat per Name adressieren](./assets/send_message_to_chat_name.png)
