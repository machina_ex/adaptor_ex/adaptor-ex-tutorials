
Send File
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Mit der Telegram action **Send File** können Dateien in verschiedenen Formaten vom Account oder Bot in einen Chat gesendet werden. 

[Hier](../../../tutorials/telegram-send-media/index.md){target=_blank} findest du ein Tutorial zu dieser action. 

## Settings

### from

Der Telegram Account oder Bot, der die Datei versenden soll. 

### To

Die Spieler:in oder Spieler:innen, an die die Datei geschickt werden soll. 

### file

Der Dateiname der zu versendenden Datei. 

Dateien können entweder via Drag and Drop aus der linken Toolbar "Media" direkt in das Feld gezogen werden (Dropdown-Auswahl: *Drop File*), oder via Dropdown ausgewählt werden (Dropdown-Auswahl: *Select File*). 

Im Toolbar Tab "Media" befinden sich die Dateien, die auf dem adaptor:ex-Server unter 'files/' für dieses Game abgelegt wurden.

![screenshot](../../../tutorials/telegram-send-media/assets/4.png){ style=width:40% }

[Hier](../../files.md) findest du mehr Infos zu Dateien in adaptor:ex.

Weitere Einstellungen (Können über "Settings" hinzugefügt werden):

### format

Das Format in dem die Datei versendet wird.

*auto* wandelt die meisten Dateien in ein passendes Format. Bilder werden z.B.bis zu einer bestimmten Größe direkt angezeigt.

Unten findest du mehr Details zu Medienformaten.

### pin

Setzt die Nachricht als gepinnte Nachricht in den Chat

### caption

Fügt der Datei eine Nachricht/Beschreibung/Bildunterschrift hinzu

## Bilddateien

Wenn wir in **file** eine Bilddatei (.png, .jpeg) einsetzen, wird der Telegram Account ein Foto an die Spieler:innen versenden.

![balloon dog](../../../tutorials/telegram-send-media/assets/7.png){ style=width:350px }
Photo by <a href="https://unsplash.com/@charlesdeluvio?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Charles Deluvio</a> on <a href="https://unsplash.com/s/photos/balloon-dog?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

Auch eine URL zu einem Bild im Internet führt dazu, dass der Account dieses Bild versendet. Einen Bild-Link erkennst du daran, dass sich am Ende der URL eine Dateiendung, z.B. .png oder .jpeg befindet.

Bitte achte darauf, dass die Nutzungsrechte dieser Datei deine Verwendung erlauben.

**format**: für ein Bild sollte hier `auto` eingestellt sein. Das Bild wird, solange es unter 10 MB groß ist, dann auch sofort eingeblendet, vorausgesetzt die empfangende Person erlaubt dies in ihrer Telegram App.

Bilder über 10MB werden den Empfangenden standardmäßig als Download angezeigt.

## Audio Files und Voice Messages

Audio-Dateien haben Dateiendungen wie .mp3, .wav und .ogg.

.mp3 und .wav werden den Spieler:innen immer als Audio-Downloadlink angezeigt.

.ogg Dateien können auch als Audio-Downloadlink verschickt werden.

Wenn jedoch **format** auf `voice` eingestellt wird, die .ogg Datei korrekt encoded ist (OPUS codec) und wenn sie nicht größer als 1MB ist, wird Telegram die Datei als Voice Message anzeigen, so als hätte der Telegram Account sie selbst eingesprochen.

![screenshot](../../../tutorials/telegram-send-media/assets/16.png){ style=width:51% }

So sollten die Einstellungen aussehen, damit eine Audiodatei als Voice Message versendet wird.

![screenshot](../../../tutorials/telegram-send-media/assets/17.png){ style=width:51% }

So sieht eine Voice Message aus.

## Videos

Mit **format** auf `video` können auch Videodateien direkt als Video im Messenger angezeigt werden.

## Andere Files

Andere Dateitypen werden als Download Link verschickt. 

Die Darstellung der Dateien hängt nicht nur von Dateiformat und der Größe der Datei, sondern auch von den Einstellungen in der Telegram App der empfangenden Person ab.

Example
-------

Ziehe eine Telegram **Send File** action auf die STAGE. Wähle einen Account oder Bot aus, gib an, an wen die Datei gesendet werden soll.

In file wählen wir eine der Dateien aus, die im files Verzeichnis unseres Games liegen.

![Screenshot: eine Send File action](./assets/send_file_logo.png)

Wenn wir den *SendLogo* State auslösen, wird die bilddatei an den Spieler, die Spielerin verschickt.

![Screenshot: Telegram chat mit Stage Manager Bot](./assets/send_file_logo_chat.png)