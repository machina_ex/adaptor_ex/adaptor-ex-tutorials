Send Message
============

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Send a text message from one of your telegram accounts or bots to a single or group chat

Settings
--------
### from

The Telegram account or bot that should send the message.

### To

The player or group chat to whom the message is to be sent.

### text

The message to be sent.

### typing

Set a delay in seconds to show the 'typing' indicator before the message is sent. 

If you do not select this option, a delay will be calculated based on the length of the message. 

Set to 0 to not show any typing indicator.

### pin

Pins the message in the chat.