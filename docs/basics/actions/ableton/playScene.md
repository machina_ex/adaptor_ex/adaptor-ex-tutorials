Play Scene
==========

**Plugin**: [Ableton Live](../../plugins/ableton.md) | **Mode**: [Run](../../editor.md#run-actions)

Start all clips in a scene in the Liveset.

![Scenes in ableton Liveset](./assets/ableton_scene_start.png)

Scenes cannot be selected until you give them a unique name:

![right click on the scene to rename it](./assets/ableton_change_scene_name.png)
