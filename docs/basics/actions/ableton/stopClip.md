Stop Clip
=========

**Plugin**: [Ableton Live](../../plugins/ableton.md) | **Mode**: [Run](../../editor.md#run-actions)

Stop a clip in the Liveset.

![A clip in the liveset](./assets/ableton_clip_stop.png)