Stop Track
==========

**Plugin**: [Ableton Live](../../plugins/ableton.md) | **Mode**: [Run](../../editor.md#run-actions)

Stop all clips in a track in the Liveset.

![A track in the liveset](./assets/ableton_track_stop.png)

Stop all clips in the Liveset by selecting the 'master' track.
