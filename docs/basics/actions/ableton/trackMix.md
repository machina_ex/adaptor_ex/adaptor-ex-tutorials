Set Track Mix
=============

**Plugin**: [Ableton Live](../../plugins/ableton.md) | **Mode**: [Run](../../editor.md#run-actions)

Change the mixer settings of a track in the Liveset.

Settings
--------

Select the settings you want to change in the mix from the 'Settings' menu in the action editor.

![Add Track Mix Settings](./assets/ableton_track_mix_settings.png)

![The Track Mix elements in ableton live](./assets/ableton_track_mix.png)

### liveset

The Liveset to which the Set Track Mix message is sent.

### track

The track in `Liveset` whose mix is to be changed.

### panning

Change the track in the stereo mix. A value between `-1` and `1`. `-1` for far left, 1 for far right, `0` for centre.

### volume

Change the volume of the track throughout the mix. A value between `0` and `1`.

### track_activator

Activate or deactivate the track. `1` activates the track, `0` deactivates the track.

### Sends

Change the portion of the respective *sends* channel.

Add more sends via 'Settings'.

![Add more sends with ADD](./assets/ableton_add_sends.png)

Enter the send channel in capital letters as `Property Name` (e.g. `C`) and add it with `Add`.
