Play Clip
=========

**Plugin**: [Ableton Live](../../plugins/ableton.md) | **Mode**: [Run](../../editor.md#run-actions)

Start a single clip in the live set.

![A clip in the liveset is played](./assets/ableton_clip_play.png)

Select the clip from the available clips in your Liveset (`select`) ...

![select clip](./assets/ableton_play_clip_select.png)

... or use a variable (`custom`).

![Select Clip](./assets/ableton_play_clip_custom.png)
