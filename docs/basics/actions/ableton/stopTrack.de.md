Stop Track
==========

**Plugin**: [Ableton Live](../../plugins/ableton.md) | **Mode**: [Run](../../editor.md#run-actions)

Stoppe alle Clips in einer Spur im Liveset.

![Eine Spur im Liveset](./assets/ableton_track_stop.png)

Stoppe alle Clips im Liveset, indem du den `master` Track auswählst.
