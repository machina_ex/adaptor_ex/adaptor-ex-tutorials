Play Clip
=========

**Plugin**: [Ableton Live](../../plugins/ableton.md) | **Mode**: [Run](../../editor.md#run-actions)

Starte einen einzelnen Clip im Liveset.

![Ein Clip im Liveset wird abgespielt](./assets/ableton_clip_play.png)

Wähle den Clip aus den verfügbaren Clips deines Livesets aus (`select`) ...

![Clip auswählen](./assets/ableton_play_clip_select.png)

... oder verwende eine Variable (`custom`).

![Clip auswählen](./assets/ableton_play_clip_custom.png)
