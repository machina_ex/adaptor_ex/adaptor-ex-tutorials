On Event
========

**Plugin**: *Logic* | **Mode**: [Listen](../../editor.md#listen-actions)

Waits for the occurrence of a specified event and switches to a next state when the specified event occurs.

In addition, it is possible to query the content (payload or message) of an event for a condition and to switch to a next state only if the condition is true.

For example, react to events that were triggered with [Dispatch Event](./dispatch.md).

Also [Devices](../../plugins/devices.md) trigger events when the device sends a message to adaptor:ex.

**On Event** waits for events until one of the events occurs (and any condition matches) or the state is changed due to another action or a manual operation.

Like all listener actions, **On Event** will only terminate on a state change within the same path. See [Paths](../../paths.md) to find out more about the behavior of paths and their effects on listener actions like **On Event**.

In the chapter about [Events](../../events.md) you will learn more about how to use events in your levels.

Under [Conditions](../../conditions.md) you learn how to query or compare variables in the **On Event** and other actions.

Settings
--------

### from

The session, game or item whose events are to be listened to.

Set **from** to "game" to listen to global, level-spanning, game events that you triggered with [Dispatch Event](./dispatch.md).

Set **from** to "session" or omit it altogether to listen to local events within the current session of your level that you triggered with [Dispatch Event](./dispatch.md).

Some plugin items can trigger events. Set a reference to a plugin item for **from** to listen for events from that plugin item.

You can also trigger events from data items. Set a reference to a data item for **from** to listen for events from the respective data item.

### event

The name of the event that is being waited for.

### if

*optional*

Check the payload of the event for one or more **if** [conditions](../../conditions.md). If a condition is met, it will switch to the specified **next state**.

If the potential event payload contains a JS object you can use **field** to specify which property in the object you want to check for a condition.

Use dot (`.`) notation in **field** to name nested properties in the object.

### else

*optional*

Switch to **next state** as soon as the specified event arrives, independent of a payload.

Will only be triggered if no **if** condition applies beforehand.

### keep listening and queue

*optional*

Mute the **On Event** listener instead of closing it completely when the state containing **On Event** is exited.

The listener will then no longer respond to incoming events, but the event messages will be stored.

If the state containing the **On Event** action is called again, the stored events are checked first, before the listener starts to listen for further events.

!!! info
    In the chapter on [Loops](../../loops.md) you will find additional information on [keep listening and queue](../../loops.md#keep-listening-and-queue)

With **keep listening and queue** you can ensure that **On Event** does not miss any events while the listener is not active, should it be possible for it to be activated again later.

If **On Event** is reactivated and events have been stored in the meantime, the event messages are applied starting with the oldest. If you switch to the `Next State` due to a saved event, more recent events remain in the queue.

You can use **enabled** to activate or deactivate the behavior as required.

The **max queue length** option sets a limit for the number of events that are saved when **On Event** is muted. If the limit is exceeded, old events are removed from the top of the list.

Set **max queue length** to 1 to only ever save the latest event received.

If **max queue length** is unselected, the number of events that are saved is not limited.

![Screenshot: max queue length deactivated in the settings](./assets/on_event_max_queue_off.png){style=width:350px}

Deactivate **max queue length** via the *Settings* of **keep listening and queue**

!!! Warning
    A very large number of stored events can impair the performance of adaptor:ex.

In [Live mode :material-play-circle-outline:{ .is-live }](../../live.md), the total number of queued messages that have not yet been responded to is displayed at the **On Event** action :material-clock-outline:{ .is-live }

![Screen Recording](./assets/on_event_queue_example.gif)

!!! example-file "Example Level File"
    Download: [:material-download: event_queue.json](./assets/event_queue.json)