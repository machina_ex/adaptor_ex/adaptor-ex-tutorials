Function
========

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Führe eine Javascript Funktion aus.

Im Kapitel [Javascript Funktionen](../../functions.md) findest du heraus wie du Funktionen erstellst und zu deinem Game hinzufügst.

Settings
--------

### function

Der Name der Funktion die ausgeführt wird. Wähle eine Funktion aus den verfügbaren Funktionen aus.

### arguments

Das Argument oder die Argumente die der Funktion als Parameter übergeben werden. Nutze `Add arg` um Argumente hinzuzufügen.

#### String Argument

Wähle *string* wenn du einen Text, eine Variable oder ein JS Objekt übergeben willst.

Nutze eckige Klammern (`[[` und `]]`) um den Wert einer Variable zu übergeben. Z.B.:

`[[Player.name]]`

Nutze geschwungene Klammern (`{` und `}`) um ein JS Object zu übergeben. Z.B.:

`{color:"green",score:15}`

#### Array Argument

Wähle *array* wenn du mehrere Argumente in einem in einer Liste übergeben willst.

#### Argumente in der Javascript Funktion

In der Funktion werden **arguments** als erster Parameter übergeben. Dabei handelt es sich immer um ein array (eine Liste) von Argumenten.

``` js
function test(args, {session, game}) {
    session.log(args[0]) // Schreibe das erste Argument in die Log Konsole
}
```

### next

Lege fest, welcher `next state` ausgelöst wird, wenn die Funktion einen `next` value zurück gibt (`return`)

Wenn die Funktion `{next:<index>}` zurückgibt, löst die Aktion den `next state` aus, der unter **next** an der Position <index> beginnend mit `0` definiert ist.

Wenn du zum Beispiel 3 **next**-Einträge hinzufügst, wird eine Funktion, die `{next:2}` zurückgibt, den letzten deiner **next**-Einträge auslösen.

Wenn die Funktion einen `next` Wert zurückgibt, aber kein `next state` definiert ist oder am gegebenen Index kein `next state` existiert, wird **next** ignoriert.

Action Data
------------

Nachdem eine **Function** action ausgeführt wurde, ist der return value der Funktion als [Action data](../../variables.md#action-data) verfügbar. Wenn die Funktion ein JS Objekt zurück gibt, können die Eigenschaften des Objekts per Punkt Notation `.` aufgerufen werden.

Die Eigenschaft `someValue` aus dem return value der folgenden Beispielfunktion

``` js
function someFunction(args, {session, game}) {
  return {someValue: "abc", anotherValue: 123}
}
```

Kann z.B. folgendermaßen als Variable adressiert werden:

`[[state.MyState.function_1.someValue]]`

Beispiel
--------

Wir nutzen die Funktion "random" um eine Zufallszahl zu generieren. Durch Argumente können wir den minimalen und maximalen Wert der Zufallszahl bestimmen.

Das erste Argument bestimmt den (inklusiven) minimal Wert, hier `1`. Das zweite Argument bestimmt den (inklusiven) maximal Wert, hier `10`.

![Beispiel für die function action](./assets/functions_example.png)

Die "random" Funktion nutzt die eingebettete Javascript Funktion [Math.random](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random) und sieht im wesentlichen folgendermaßen aus:

``` js
function random(args, {session, game}) {
  let min = Math.ceil(args[0])
  let max = Math.floor(args[1])
  return Math.floor(Math.random() * (max - min + 1) + min)
}
```

Um den return value der Funktion zu nutzen verwenden wir die entsprechende [Action data variable](../../variables.md#action-data) hier: `[[state.TwistOfFate.function_1]]`

![Function Action data verwenden](./assets/functions_example_actiondata.png)

Nachdem "random" ausgeführt wurde enthält die variable eine Ganzzahl zwischen 1 und 10

![Log Konsole mit Ausgabe der Action data variable](./assets/functions_example_log.png)
### Abzweigungen mit Next

Die "random" Funktion hat eine zweite Verwendungsweise. Wenn wir keine Argumente angeben, gibt sie statt des Zufallswertes einen `next` value aus. Je nachdem wie viele `next states` wir angeben gibt sie einen `next` Wert zwischen `0` und der Anzahl der `next states` aus.

Das heißt die Funktion wählt zufällig einen der `next states` aus die wir in der action angegeben haben. Hätte sie nur diese Einsatzmöglichkeit sähe "random" folgendermaßen aus:

``` js
function random(args, {session, game}) {
  const min = 0
  const max = session.action.payload.next.length -1
  return {next: Math.floor(Math.random() * (max - min + 1) + min)}
}
```

Um diese Variante zu nutzen entfernen wir die `arguments` option und fügen stattdessen die `next` Option zu unserer Function action hinzu.

> Es ist natürlich bei vielen Funktionen möglich sowohl arguments als auch next states anzugeben

![function mit next outlets](./assets/functions_example_next.png)

Gibt die "random" Funktion `{next:0}` zurück wird als nächstes *ThisWay* ausgelöst. Gibt sie stattdessen `{next:1}` zurück geht es weiter mit *OrThatWay*, bzw. bei `{next:2}` mit *OrMaybeThisWay*.

![function als Abzweigung](./assets/functions_example_overview.png)

Wird der *TwistOfFate* State ausgelöst wird mit gleich verteilter Wahrscheinlichkeit einer der drei `next states` ausgelöst.

Hier die vollständige "random" Funktion:

``` js
function random(args, {session, game}) {
  let min, max
  
  if(Array.isArray(args) && args.length > 0) {
    if(args.length == 1) {
      min = 0
      max = Math.floor(args[0])
    } else if(args.length == 2) {
      min = Math.ceil(args[0])
      max = Math.floor(args[1])
    }
  } else {
    if(session.action.payload.next && session.action.payload.next.length > 0) {
      min = 0
      max = session.action.payload.next.length -1
      return {next: Math.floor(Math.random() * (max - min + 1) + min)}
    }
  }
  
  return Math.floor(Math.random() * (max - min + 1) + min)
}
```

Seit adaptor:ex Version 2.1. ist sie Teil der mitgelieferten Funktionen. Nutzt du eine vorherige Version Füge "random" per Hand zu deinen Game Funktionen hinzu: [Add Functions](../../functions.md#add-functions)