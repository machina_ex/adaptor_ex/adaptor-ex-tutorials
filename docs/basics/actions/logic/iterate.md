Iterate
==============

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Create a `for loop` to iterate over an array, a [Collection](../../../basics/variables.md#collections-and-data-items), objects or strings.

Each time an **Iterate** action is called, the next element in a set of elements is made available.

Use the action inside a [Loop](../../loops.md) to create [For Each Loops](../../loops.md#for-each-loop).

Settings
--------

### with

Array, collection or object to be iterated over.

If you specify an integer for **with**, the number range from `0` to the specified number is iterated.

### Variable name

Name of the variable that designates the current element. 

### Next State

Next state in which the current element is to be used. 

### Repeat

If this option is activated, the first element is restarted at the end of the previous iteration. The iteration is endless unless it is interrupted externally. If not activated, the following action is triggered in the state.

### Reverse order

If this option is activated, the sequence is reversed from the last to the first element.

Variables
---------

### loop
*type*: Integer

Number of loops iterated so far from `1` to `n`. **loop** can only exceed `1` if **repeat** is selected.

### index
*type*: integer

Contains the current value of the iteration.

Examples
---------

### Array

An array can either be defined in advance, e.g. via the action [set variable](../data/set.md) or directly in the [with](#with) field:

![iterate_array_example](./assets/iterate_arry_example.png)

Now each `location` can be output one after the other:

![iterate_array_example_full](./assets/iterate_array_example_full.png)

The log message shows a different value for each run. When the loop is finished, the `next` action is triggered in the `iterate` state and the level is ended.

### String

You can also define a string to be iterated over. The following example iterates over the letters of the alphabet:

![iterate_string_example](./assets/iterate_string_example.png)

### Number

Enter an integer for **width** to perform a number of iterations.

In this case, the value of the element is the number of the iteration and corresponds to the **[index](#index)** action variable `+1`.

![iterate_number_example](./assets/iterate_number_example.png){ style=width:400px; }

### Object

When you iterate over an object, the element of each iteration is the next one of the `key: value` pairs. You can access the property name within the iteration with `element.key` and the corresponding value with `element.value`.

![iterate_object_example](./assets/iterate_object_example.png){ style=width:47%; float:left }
![iterate_object_example](./assets/iterate_object_log_example.png){ style=width:47%; float:right }

### Collection

A special feature of the `iterate` action is that it can also be iterated over entire collections. 

Given the collection `MEDIA`, which contains a collection of media files. It has the following entries:

![Iterate Collection Example](./assets/iterate_collection_example.png)

The `path` attribute contains information about where the file is stored. See [Files and Media Files](../../files.md) to learn more about this topic. 

The `type` attribute can be either `talk` or `atmo` and the `ID` defines the order in which the files are to be played. The `iterate` action can now be used in the following way:

> For pragmatic reasons, we use the JSON notation of the level from here on and no longer the screenshots. The JSON codes can simply be copied into your own level.

#### Unordered

```js
{
  "with": "[[MEDIA.{}]]",
  "variable": "file",
  "next": "your_loop_state",
  "repeat": true,
  "reverse": false
}
```

The `with` field contains the name of the collection together with empty curly brackets. In this way, the collection `MEDIA` is iterated from top to bottom. 

#### Ordered

Unordered playback is not necessarily the goal in this case, as an order was previously defined. For this reason, it is possible to further customize the `with` field:

```JSON
{
  "with": "[[MEDIA.{type:'atmo', $sort:{ID:1}}]]",
  "variable": "file",
  "next": "your_loop_state",
  "repeat": true,
  "reverse": false
}
```

In this example, the curly brackets are not empty, but contain an [inline query](../../variables.md#inline-query) (Strictly speaking, `{}` is also an inline query that simply returns every document from the collection). The first part of the query determines which `type` of files should be played. In the second part of the query, the `$sort` modifier is used to define the order of the elements along the `ID`. 

#### Referencing the document

A state with the name `your_loop_state` can now be created and a [Log Message](../control/log.md) with the following content can be created in it:

```js
{

  "level": "info",
  "message": "Path: [[file.path]],\nType: [[file.type]],\nID: [[file.ID]]\n"
}
```


Best in conjunction with a [Timeout Action](../time/timeout.md), otherwise the process runs so fast that it cannot be tracked.


