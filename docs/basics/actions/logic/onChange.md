On Change
=========

**Plugin**: *Logic* | **Mode**: [Listen](../../editor.md#listen-actions)

Waits for a variable to take a value or satisfy a condition.

With **On Change** it is possible to monitor every change in a variable and to switch to the next state if it meets a condition.

As soon as you change to the state in which the **On Change** action is located, On Change checks all specified conditions. If a variable already fulfills the specified condition when changing to the state, it changes directly to the next connected state.

As in the [Switch](./switch.md) action, it may go directly to the next state. However, On Change then continues to check the specified conditions for as long as the state is active.

Like all listener actions, On Change is only terminated if there is a state change within the same path. See [Paths](../../paths.md) to find out more about how paths behave and how they affect listener actions like On Change.

Under [Conditions](../../conditions.md) you will learn how to query or compare variables in the **On Change and other actions.**
