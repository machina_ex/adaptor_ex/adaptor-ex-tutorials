Iterate
==============

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Erstelle einen `for loop`, um über ein Array, eine [Collection](../../../basics/variables.de.md#collections-und-data-items), Objects oder Strings zu iterieren.

Mit jedem Aufruf einer **Iterate** Action ist jeweils das nächste Element in einer Menge von Elementen verfügbar.

Nutze die Action in einer [Schleife](../../loops.md) um [For Each Loops](../../loops.md#for-each-loop) zu erstellen.

Settings
--------

### with

String, Array, Collection oder Object über das iteriert werden soll.

Gibst du für **with** eine Ganzzahl an, wird der Zahlenraum von `0` bis zur angegebenen Zahl iteriert.

### variable name

Name der Variable, die das aktuelle Element bezeichnet. 

### Next State

Nächster State, in dem das aktuelle Element verwendet werden soll. 

### Repeat

Wenn diese Option aktiviert ist, wird bei Ende der vorherigen Iteration mit dem ersten Element neu begonnen. Die Iteration ist endlos, es sei denn, sie wird extern unterbrochen. Wenn nicht aktiviert wird die nachfolgende Action im State ausgelöst.

### Reverse Order

Wenn diese Option aktiviert ist, wird in umgekehrter Reihenfolge vom letzten zum ersten Element vorgegangen.

Variablen
---------

### loop

*type*: Integer

Anzahl der bisher iterierten Runden von `1`-n. **loop** kann nur `1` überschreiten wenn **repeat** ausgewählt ist.

### index

*type*: integer

Enthält den aktuellen Wert der Iteration.

Beispiele
---------

### Array

Ein Array kann entweder vorab, z.B. über die action [set variable](../data/set.de.md) oder direkt im [with](#with) Feld definiert werden:

![iterate_array_example](./assets/iterate_arry_example.png)

Nun kann jede `location` hintereinander ausgegeben werden:

![iterate_array_example_full](./assets/iterate_array_example_full.png)

Die Log Nachricht zeigt bei jedem Durchlauf einen anderen Wert an. Wenn die Schleife beendet ist, wird im `iterate` State die `next` Action ausgelöst und das Level beendet.

### String

Es kann auch ein String definiert werden, über den iteriert werden soll. Nachfolgendes Beispiel iteriert über die Buchstaben des Alphabets:

![iterate_string_example](./assets/iterate_string_example.png)

### Number

Gib für **width** eine Ganzzahl an um eine Anzahl Iterationen durchzuführen.

Der Wert des Elements ist in diesem Fall die Nummer der Iteration und entspricht der **[index](#index)** Action Variable `+1`.

![iterate_number_example](./assets/iterate_number_example.png){ style=width:400px; }

### Objekt

Wenn du über ein Objekt iterierst, ist das Element jeder Iteration jeweils eines der `key: value` Paare. Du kannst innerhalb der Iteration mit `element.key` auf den Property Namen und mit `element.value` auf den dazugehörigen Wert zugreifen.

![iterate_object_example](./assets/iterate_object_example.png){ style=width:47%; float:left }
![iterate_object_example](./assets/iterate_object_log_example.png){ style=width:47%; float:right }

### Collection

Eine besondere Eigenschaft der `iterate` Action ist, dass auch über ganze Collections iteriert werden kann. 
Gegeben sei die Collection `MEDIA`, die eine Sammlung von Mediendateien enthält. Sie hat folgende Einträge:


![Iterate Collection Example](./assets/iterate_collection_example.png)

Das `path` Attribut enthält Informationen darüber, wo die Datei gespeichert ist. Siehe [Dateien und Media Files](../../files.de.md), um mehr über das Thema zu erfahren. 

Das `type` Attribut kann entweder `talk` oder `atmo` sein und die `ID` definiert die Reihenfolge, in der die Dateien abgespielt werden sollen. Die `iterate` Action kann nun in folgender Weise genutzt werden:

> Aus pragmatischen Gründen verwenden wir ab hier die JSON Schreibweise des Levels und nicht mehr die Bildschirmfotos. Die JSON Codes können einfach in das eigene Level rein kopiert und angepasst werden. 
 
#### Ungeordnet

```js
{
  "with": "[[MEDIA.{}]]",
  "variable": "file",
  "next": "your_loop_state",
  "repeat": true,
  "reverse": false
}
```

Im `with` Feld steht der Name Collection zusammen mit leeren geschweiften Klammern. So ausgedrückt wird die Collection `MEDIA` von oben nach unten iteriert. 

#### Geordnet

Das ungeordnete Abspielen ist in diesem Fall nicht unbedingt das Ziel, da ja zuvor eine Reihenfolge festgelegt wurde. Deshalb gibt es die Möglichkeit das `with` Feld noch weiter anzupassen:


```JSON
{
  "with": "[[MEDIA.{type:'atmo', $sort:{ID:1}}]]",
  "variable": "file",
  "next": "your_loop_state",
  "repeat": true,
  "reverse": false
}
```

In diesem Beispiel sind die geschweiften Klammern nicht leer, sondern enthalten eine [Inline Query](../../variables.de.md#inline-query) (Genau genommen ist auch `{}` eine Inline Query, die einfach jedes Dokument aus der Collection zurückgibt). Der erste Teil der Query bestimmt welcher `type` von Dateien abgespielt werden soll. Im zweiten Teil der Query wird der `$sort` modifier genutzt, um die Reihenfolge der Elemente entlang der `ID` zu definieren. 

#### Das Dokument referenzieren

Nun kann ein state mit dem Namen `your_loop_state` erstellt werden und darin eine [Log Message](../control/log.de.md) mit folgendem Inhalt:

```js
{
  "level": "info",
  "message": "Path: [[file.path]],\nType: [[file.type]],\nID: [[file.ID]]\n"
}
```

Am Besten in Verbindung mit einer [Timeout Action](../time/timeout.de.md), sonst läuft der Prozess so schnell, dass er nicht verfolgt werden kann.
