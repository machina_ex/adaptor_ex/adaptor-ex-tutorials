Function
========

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Execute a javascript function.

The [Javascript Function](../../functions.md) chapter can help to find out how to create functions and add them to your game.

## Settings

### function

The name of the function being executed. Choose a function from the available functions list.

### arguments

The argument or arguments passed to the function as parameters. Use `Add arg` to add new arguments.

#### String argument

Choose *string* if you want to pass a text, a variable or a JS object.

Use square brackets (`[[` and `]]`) to pass the value of a variable. E.G.:

`[[Player.name]]`

Use curly brackets (`{` and `}`) to pass a JS object. E.G.:

`{color: "green",score:15}`

#### Array argument

Choose *array* if you want to pass multiple arguments in one in a list.

#### Arguments inside the Javascript function

Inside the function **arguments** are passed as the first parameter. The function argument is always an array (a list) of arguments.

``` js
function test(args, {session, game}) {
    session.log(args[0]) // Write the first argument into the log console
}
```

### next

Set where to go next if the function returns a `next` value.

If the function returns `{next:<index>}` the action will trigger the `next state` that is defined in **next** at the <index> position starting with `0`.

For example, if you add 3 **next** entries, a function that returns `{next:2}` will trigger the last of your **next** entries.

If the function returns a `next` value but no `next state` is defined (at the returned index) it will be ignored.

Action Data
------------

After a **function** action is executed, the return value of the function is available as [Action data](../../variables.md#action-data). If the function returns a JS object, the object's properties can be called via dot notation `.`.

The `someValue` property from the return value of the following example function

``` js
function someFunction(args, {session, game}) {
  return {someValue: "abc", anotherValue: 123}
}
```

can be addressed as a variable in example as follows:

`[[state.MyState.function_1.someValue]]`

## Example

We use the "random" function to generate a random number. Through arguments we can determine the minimum and maximum value of the random number.

The first argument determines the (inclusive) minimum value, here `1`. The second argument determines the (inclusive) maximum value, here `10`.

![Example for the function action](./assets/functions_example.png)

The "random" function uses the embedded javascript function [Math.random](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random) and looks basically like this:

``` js
function random(args, {session, game}) {
  let min = Math.ceil(args[0])
  let max = Math.floor(args[1])
  return Math.floor(Math.random() * (max - min + 1) + min)
}
```

To use the return value of the function we use the corresponding [Action data variable](../../variables.md#action-data) here: `[[state.TwistOfFate.function_1]]`

![Use function action data](./assets/functions_example_actiondata.png)

After "random" has been executed the variable contains an integer between 1 and 10

![Log console with output of the action data variable](./assets/functions_example_log.png)
### Branching with next

The "random" function has a second use. If we do not specify any arguments, it returns a `next` value instead of the random value. Depending on how many `next states` we specify, it outputs a `next` value between `0` and the number of `next states`.

This means that the function randomly selects one of the `next states` we specified in the action. If it had only this option, "random" would look like this:

``` js
function random(args, {session, game}) {
  const min = 0
  const max = session.action.payload.next.length -1
  return {next: Math.floor(Math.random() * (max - min + 1) + min)}
}
```

To use this variant we remove the `arguments` option and add the `next` option to our Function action instead.

> Of course for many functions it is possible to specify both arguments and next states.

![function with next outlets](./assets/functions_example_next.png)

If the "random" function returns `{next:0}` then *ThisWay* is triggered next. If it returns `{next:1}` instead it continues with *OrThatWay*, or for `{next:2}` with *OrMaybeThisWay*.

![function as branch](./assets/functions_example_overview.png)

When the *TwistOfFate* state is triggered, one of the three `next states` is triggered with equally distributed probability.

Here is the complete "random" function:

``` js
function random(args, {session, game}) {
  let min, max
  
  if(Array.isArray(args) && args.length > 0) {
    if(args.length == 1) {
      min = 0
      max = Math.floor(args[0])
    } else if(args.length == 2) {
      min = Math.ceil(args[0])
      max = Math.floor(args[1])
    }
  } else {
    if(session.action.payload.next && session.action.payload.next.length > 0) {
      min = 0
      max = session.action.payload.next.length -1
      return {next: Math.floor(Math.random() * (max - min + 1) + min)}
    }
  }
  
  return Math.floor(Math.random() * (max - min + 1) + min)
}
```

Since adaptor:ex version 2.1. it is part of the included functions. If you are using a previous version, add "random" to your game functions manually: [Add Functions](../../functions.md#add-functions)