On Event
========

**Plugin**: *Logic* | **Mode**: [Listen](../../editor.md#listen-actions)

Wartet auf das Eintreffen eines bestimmten Ereignisses (Event) und wechselt in einen nächsten State, wenn das angegebene Event eintrifft. 

Zusätzlich ist es möglich den Inhalt (Payload oder Message) eines Event auf eine Bedingung abzufragen und nur bei zutreffender Bedingung in einen nächsten State zu wechseln.

Reagiere etwa auf Ereignisse, die mit [Dispatch Event](./dispatch.md) ausgelöst wurden.

Auch [Devices](../../plugins/devices.md) lösen Events aus, wenn das Device eine Nachricht an adaptor:ex schickt.

**On Event** wartet solange auf Events, bis eines der Events, und ggf. der Bedingungen eintrifft oder aufgrund einer anderen action oder einer manuellen Operation der State gewechselt wird.

Wie alle listener actions wird **On Event** nur bei einem State wechsel innerhalb des selben Pfades beendet. Unter [Pfade](../../paths.md) findest du mehr über das Verhalten von Pfaden und ihre Auswirkungen auf listener actions wie **On Event** heraus.

Im Kapitel zu [Events](../../events.md) erfährst du mehr darüber wie du Events in deinen Levels verwenden kannst.

Unter [Bedingungen (Conditions)](../../conditions.md) erfährst du, wie du variablen in der **On Event** und anderen actions abfragst oder vergleichst.

Settings
--------

### from

Die session, das game oder das Item, auf dessen events gehört werden soll. 

Setze für **from** "game" ein um auf globale, level übergreifende, game events zu hören die du mit [Dispatch Event](./dispatch.md) ausgelöst hast.

Setze für **from** session ein oder lasse die Option ganz weg um auf lokale Events innerhalb der aktuellen session deines Levels zu hören die du mit [Dispatch Event](./dispatch.md) ausgelöst hast.

Einige Plugin Items können events auslösen. Setze für **from** eine Referenz auf ein Plugin Item ein um auf events von dem jeweiligen Plugin Item zu hören.

Du kannst auch events von Data Items ausgehend auslösen.Setze für **from** eine Referenz auf ein Data Item ein um auf events von dem jeweiligen Data Item zu hören.

### event

Der Name des Ereignisses auf das gewartet wird.

### if

*optional*

Überprüfe den Payload des Event auf eine oder mehrere if [Bedingungen](../../conditions.md). Wenn eine Bedingung erfüllt ist, wird in den angegebenen **next state** gewechselt.

Wenn der potentielle Event Payload ein JS Object enthält kannst du mit **field** angeben welche Eigenschaft (Property) im Objekt du auf eine Bedingung überprüfen willst.

Nutze Punkt (`.`) Notation in **field** um verschachtelte Eigenschaften im Objekt zu benennen.

### else

*optional*

Wechsle in den **next state** sobald das angegebene Event eintrifft, unabhängig von einem Payload.

Wird nur ausgelöst, wenn nicht zuvor eine **if** Bedingung zutrifft.

### keep listening and queue

*optional*

Schalte den **On Event** listener Stumm anstatt ihn ganz zu schließen, wenn der State, der **On Event** enthält, verlassen wird.

Es wird dann zwar nicht weiter auf eingehende Events reagiert, die Event messages werden aber gespeichert. 

Wird der State, der diese **On Event** Action enthält erneut aufgerufen, werden zunächst die gespeicherten Events überprüft, bevor der listener beginnt auf weitere Events zu hören.

!!! info
    Im Kapitel über [Schleifen](../../loops.md) findest du zusätzliche Infos zu [keep listening and queue](../../loops.md#keep-listening-and-queue)

Mit **keep listening and queue** kannst du sicherstellen, dass **On Event** keine Events verpasst, während der listener nicht aktiv ist, sollte es möglich sein, dass er später ein weiteres mal aktiviert wird.

Wird **On Event** mit gespeicherten Events neu aktiviert, werden die Event messages beginnend mit der ältesten angewandt. Wird aufgrund eines gespeicherten Events in den `Next State` gewechselt, bleiben ggf. weitere, jüngere Events gespeichert.

Mit **enabled** kannst du das Verhalten bei Bedarf aktivieren oder deaktivieren.

Die option **max queue length** legt eine Grenze für die Anzahl der Events fest, die gespeichert werden, wenn **On Event** stumm geschaltet ist. Wird die Grenze überschritten, werden alte Events vom Anfang der Liste entfernt.

Setze **max queue length** auf 1 um immer nur das zuletzt eingegangene Event zu speichern.

Ist **max queue length** nicht ausgewählt, ist die Anzahl der Events, die gespeichert werden nicht begrenzt.

![Screenshot: max queue length in den Settings deaktiviert](./assets/on_event_max_queue_off.png){style=width:350px}

Deaktiviere **max queue length** über die *Settings* von **keep listening and queue**

!!! Warning
    Eine sehr große Anzahl gespeicherter Events kann die Performance von adaptor:ex beeinträchtigen.

Im [Live Modus :material-play-circle-outline:{ .is-live }](../../live.md) wird die Gesamt Anzahl der eingereihten Nachrichten, auf die bisher nicht reagiert wurde, an der **On Event** Action angezeigt :material-clock-outline:{ .is-live }

![Screen Recording](./assets/on_event_queue_example.gif)

!!! example-file "Example Level File"
    Download: [:material-download: event_queue.json](./assets/event_queue.json)