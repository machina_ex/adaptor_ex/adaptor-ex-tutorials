Dispatch Event
==============

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Löse ein Event aus, auf das an anderer Stelle mit [On Event](./onEvent.md) reagiert werden kann.

Settings
--------

### name

Der Name des Event. Über diesen Namen, kann auf dieses Event anderswo reagiert werden.
### source

*optional*

Ohne **source** Angabe kann das Event nur innerhalb der Session dieses Levels in der es ausgelöst wird empfangen werden.

Event source kann jedes Data oder Plugin Item und jede Session sein. Unter [Variablen, Daten, Referenzen](../../variables.md) findest du mehr darüber heraus wie du Items und Sessions adressierst.

Setze source auf "game", wenn du das Event in jeder Session jedes Level in deinem Game empfangen können willst.

### message

*optional*

Hänge weitere Informationen als Payload an dein Event an.

Du kannst JS Object Notation verwenden um eine JS Object Message an das Event anzuhängen.