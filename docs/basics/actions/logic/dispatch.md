Dispatch Event
==============

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Trigger an event that can be reacted to elsewhere with [On Event](./onEvent.md).

Settings
--------

### name

The name of the event. This name can be used to respond to this event elsewhere.
### source

*optional*

Without **source** the event can only be received within the session of this level in which it is triggered.

Event source can be any data or plugin item and any session. See [Variables, Data, References](../../variables.md) for more information on how to address items and sessions.

Set source to "game" if you want to receive the event in every session of every level in your game.

### message

*optional*

Attach an additional payload message to your event.

You can use JS object notation to attach a JS object message to the event.