Javascript Funktionen
=====================

Javascript Funktionen erweitern die Möglichkeiten eines adaptor:ex games um die komplette Palette der Skript Programmiersprache Javascript. Einige Funktionen sind bereits in der adaptor:ex server Installation enthalten. Es ist zudem möglich eigene Funktionen zu erstellen und einzubinden.

Funktionen hinzufügen
---------------------

So wie [Dateien und Media Files](./files.md) werden Funktionen im adaptor:ex data Ordner in `games` im jeweiligen Game Ordner abgelegt.

Erstelle eine oder mehrere javascript (`.js`) Dateien im `functions` Ordner deines Games, die deine Funktionen enthalten.

```
data
├── games
|   ├── Playground
│       ├── files
│       ├── functions
│           ├── myfunctions.js
├── log
├── nedb
├── config.json
```

!!! warning "Verzeichnistiefe"
    Funktionen innerhalb des `functions` Ordners werden maximal in einem Ordner Verzeichnistiefe registriert. Du kannst also unterordner verwenden um etwa die Funktionen eines git repository zu Klonen, jedoch müssen alle `.js` Dateien, die Funktionen exportieren die du verwenden willst, in diesem Ordner an oberster Stelle liegen.

Die Dateien werden als [nodejs Module](https://www.w3schools.com/nodejs/nodejs_modules.asp) in dein Game geladen. Du musst deine Funktionen also mit `module.exports` für adaptor:ex verfügbar machen

``` js
async function playerCount(args, {session, game}) {
    all_player = await game.db.players.find({})

    return all_player.length
}

function reverse(args, {session, game}) {
    let reversed = args[0].split("").reverse().join("")
    return reversed
}

module.exports = {
  playerCount:playerCount,
  reverse:reverse
}
```

Wenn du diese Zeilen in eine neue Datei mit `.js` Endung kopierst und in den functions Ordner in deinem Game verschiebst, kannst du in deinem Game auf zwei neue Funktionen "reverse" und "playerCount" zugreifen.

Du kannst beliebig viele Funktionen in einer Datei hinzufügen oder die Funktionen auf mehrere Dateien aufteilen. Die Dateinamen sind beliebig und Spielen keine Rolle.

Funktionen ausführen
--------------------

Du kannst Funktionen sowohl in der eigenständigen [Function](./actions/logic/function.md) action, als auch in allen Formularfeldern die Variablen als Eingabe erlauben, einsetzen.

### Die Function action

Mit der logic Function action kannst du Funktionen aufrufen, ihren return Wert nutzen und `next states` auslösen. Hier findest du mehr über die action heraus: [Function](./actions/logic/function.md)

### Funktionen als Variablen

Gib den Funktionsnamen, eingefasst in zwei eckige Klammern (`[[` und `]]`), mit einem vorangestellten `functions` in einer action Einstellung an, die Variablen als Eingabe erlaubt.

Der Funktionsname wird durch je eine geöffnete und geschlossene Klammer `()` ergänzt. Hier können auch Argumente, getrennt durch ein Komma (`,`) angegeben werden, um sie der Funktion zu übergeben.

Beispiel für eine Funktion als Variable:

`[[functions.test("hallo","welt")]]`

Innerhalb der Funktion werden "hallo" und "welt" als *array* übergeben:

``` js
function test(args, {session, game}) {
  session.log("1. " + args[0]) // 1. hallo
  session.log("2. " args[1]) // 2. welt
}
```

Der return Wert der Funktion wird an der Stelle Eingesetzt an der die Funktion eingesetzt ist.

Du findest alle Funktionen deines Games auch in der [VARIABLES TOOLBAR](./editor.md#toolbar) und kannst sie von dort in die jeweilige action ziehen.

![Suche nach "functions" und dragge und droppe die Funktion in das Action Form Field](./assets/function_inline_drop.png "Suche in VARIABLES nach "functions" und Drag'n'Drop die Funktion in das Action Form Field")

#### Beispiel

Verwende die oben erstellte Function "playerCount" in einer [Switch](./actions/logic/switch.md) action.

![Funktion als Variable in der switch action](./assets/function_as_variable_example.png){ style=width:60% }

Ist die Anzahl der Items in der player Collection größer als 10, geht es weiter im `next state` *Party*. Sonst geht es weiter mit *NetflixAndChill*

Funktionen schreiben
--------------------

!!! note
    Dieses Kapitel ist bei weitem nicht vollständig. Melde dich bei uns wenn du Fragen hast unter [tech@machinaex.de](mailto:tech@machinaex.de) oder auf dem [machina commons discord server](https://discord.gg/quHbQAMvF6)

Du kannst alle Eigenschaften der [nodejs](https://nodejs.org/de/) Javascript Runtime nutzen. 

Um externe libraries einzubinden die nicht in adaptor:ex enthalten sind, installiere sie global oder im `functions` Ordner deines games, sodass sie dort im `node_modules` Ordner abgelegt werden.

Alle Übergabe Parameter, die von der level session übergeben werden sind im ersten argument enthalten.

Das zweite argument enthält immer Variablen und Funktionen aus dem adaptor:ex game und session context.

``` js
async function playerCount(args, context) {
    all_player = await context.game.db.players.find({})

    return all_player.length
}

function reverse(args, {session, game}) {
    let reversed = args[0].split("").reverse().join("")
    return reversed
}

module.exports = {
  playerCount:playerCount,
  reverse:reverse
}
```

`args` enthält die im Level angegebenen Übergabeparameter. Eine Mehrzahl an Parametern muss als *array* übergeben werden.

!!! note
    Beachte, dass "args" immer ein Array ist, wenn die Funktion [als Variable aufgerufen](#funktionen-als-variablen) wird. Deshalb ist es empfehlenswert "args" stets als array zu behandeln. Auf diese Weise kannst du die Funktion in jedem Kontext einsetzen.

`context` ist ein Object, das `session` und `game` enthält.

`context.session` enthält Funktionen und Variablen, die sich auf die Session beziehen, die die Funktion aufgerufen het

`context.game` enthält Funktionen und Variablen, die sich auf das Game beziehen in dem die Session läuft, die die Funktion aufgerufen hat

Zudem hast du Zugriff auf die globale Variable `adaptor`, die Funktionen und Variablen enthält, die sich auf den adaptor:ex server beziehen, der das Game mit der entsprechenden Session hosted.

Wir werden versuchen die verschiedenen Funktionen und Variablen so bald wie möglich an dieser Stelle zu erläutern.

Eingebettete Funktionen
-----------------------

Die adaptor:ex Installation liefert einige Funktionen aus, die du direkt verwenden kannst.

Verwende sie wie in [Funktionen ausführen](#funktionen-ausführen) beschrieben.

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L10 "goto function in source code") test

Return arguments the way they came in. Use it to find out how functions work.
 
**Returns**: `array` - the arguments that came in 

**Arguments** - Can be anything

**Example**

```js
[[functions.test("some value", 3)]]
``` 

will return the following array: `["some value", 3]`

Assign the test function return value to a variable with [Set variable](./actions/data/set.md) and log the two array entries with [Log Message](./actions/control/log.md).

![Set action with test function](./assets/function_test_example_1.png){ style=width:47%; float:left } ![Log action with test function return value](./assets/function_test_example_2.png){ style=width:52%; float:right }

The Log will show: `First Argument was some text. Second Argument was 3.`

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L27 "goto function in source code") random

Get a random Integer value. Provide maximum only to get a number between 0 (inclusive) and max. Provide min and max argument to get a number between min (inclusive) and max (inclusive)

When used with [Function Action](./actions/logic/function.md), if you provide no arguments but one or more `next states`, random will randomly output to one of the next outlets with an even chance for each.

**Returns**: `integer | next state` - A random integer value or, if applicable, triggers one of the specified `next states` randomly. 

**Arguments**

| # | Type | Name | Description |
| - | -------- | ------- | --- |
| 0 |`integer` | Min/Max | With two arguments this is the minimum value. With one argument only, this is the maximum value with a minimum of 0
| 1 |`integer` | Max     | Maximum value |

**Inline Example**

``` js
[[functions.random(5, 10)]]
``` 
Outputs an integer number between `5` and `10`.

Use it as **timeout** value in the [Timeout Action](./actions/time/timeout.md) and wait for an uncertain amount of time.

![Timeout action with random timeout range between 5 and 10 seconds](./assets/function_random_example_1.png){ style=width:47% }

**Function Action Example**

Add a [Function Action](./actions/logic/function.md) to the stage and click on *Settings*. Deselect **arguments** and select the **next** option.

![Use next in function action](./assets/function_random_example_2.png){ style=width:47% }

Add two or more **Next State** entries. 

![Add Next states o random function](./assets/function_random_example_3.png){ style=width:47% }

When run, the action will randomly select one of the given Next States and trigger it.

![Live Editor View with Random function triggers](./assets/function_random_example_4.gif)

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L73 "goto function in source code") dateTimeOffset

Get the date that lies a certain amount of time past or future of some other date.

The original Date is in [ISO](https://en.wikipedia.org/wiki/ISO_8601) Time Format. You can use `"now"` to set it to the current date and time.

The offset argument is formatted `"HH:MM:SS"` for example: `"1:00"` add 1 minute or `"-23:30:00"` subtract 23 and half an hour.

**Returns**: `date string` - A date that lies a certain amount of time before or after the original date. 

**Arguments**

| # | Type | Name | Description |
| - | -------- | ------- | --- |
| 0 |`date string` | Original Date | The Date that is to be added or subtracted from
| 1 |`time string` | Time Offset     | The amount of time to add or subtract from the original date

**Examples**

``` js
[[functions.dateTimeOffset("2023-06-22T12:00","15:00")]]
``` 
Outputs the date value `2023-06-22T12:15` which is 15 minutes after 2023/06/22 12 o`clock.

``` js
[[functions.dateTimeOffset("2023-12-24T12:00","-48:30:00")]]
``` 

Outputs the date value `2023-12-22T11:30` which is 2 days and 30 minutes before 2023/06/22 12 o`clock.

``` js
[[functions.dateTimeOffset("now","30")]]
``` 

Outputs the date and time value 30 seconds from the current date and time.

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L113 "goto function in source code") dateTimeFormat

Convert an [ISO](https://en.wikipedia.org/wiki/ISO_8601) Formatted Date and Time string into another kind of date and or time format string.

Uses the common javascript [Intl.DateTimeFormat](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/DateTimeFormat) to convert date and time.

You can pass an open amount of any of the formatting options [Intl.DateTimeFormat](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/DateTimeFormat) allows. Define options as a two part string separated by a whitespace. The parameter value comes first, the parameter name comes second, e.g. `"short weekday"`. Each option is provided as separate argument.

Pass a locale indicator as last argument to set the date and or time locale. E.g. `"de-DE"` for german or `"en-GB"` for British english locale.

**Returns**: `string` - A formatted date and or time. 

**Arguments**

| # | Type | Name | Description |
| - | -------- | ------- | --- |
| 0 |`date string` | Date | The [ISO](https://en.wikipedia.org/wiki/ISO_8601) Date that is to be formatted. Use `"now"` to provide the current Date and Time.
| 1 - n |`string` | Options | Add one or more formatting options. Make sure the locale option is the last function argument.

**Examples**

``` js
[[functions.dateTimeFormat("2023-06-22","short date","en-US")]]
``` 
Becomes `06/22/23`.

``` js
[[functions.dateTimeFormat("2023-06-22T17:35:12.345","weekday","hour","minute","1 secondDigits","de")]]
``` 
Becomes `Donnerstag, 17:35:12,3`.

``` js
[[functions.dateTimeFormat("now","long month","fr")]]
``` 
Will become the current month in french language, e.g. during March that would be `mars`.

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L239 "goto function in source code") atIndex

Get the value of an element in an array or string at a specific index position.

**Returns**: The value at given index

**Arguments**

| # | Type   | Name  | Description                              |
| - | -----  | ------| ---------------------------------------- |
| 0 |`array` | Array | The Array or String where to find element at index
| 1 |`number`| Index | The index number

**Examples**

- **string**
``` js
[[functions.atIndex("Thundercloud",3)]]
``` 
Returns the 4th character in `Thundercloud` and that would be `n`.

- **array**

Create an array using the [Set Variable](./actions/data/set.md) action.

!!! action inline end "Set Variable"
    
    ``` json
    {
      "variable": "inventory",S
      "value": "[\"rope\",\"bottle\",\"pen\"]",
      "multiple": true
    }
    ```
![Set action with array value](./assets/function_atIndex_set.png){ style=width:60%; float:left }


Use the array as first argument in your **atIndex** function to get the respective element.

!!! action inline end "Log Message"
    
    ``` json
    {
      "level": "info",
      "message": "Second Item in your inventory is [[functions.atIndex(inventory,1)]]"
    }
    ```
![Log action with atIndex function](./assets/function_atIndex_log.png){ style=width:60%; float:left }

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L249 "goto function in source code") length

Get the number of elements inside an array or the number of characters in a string.

**Returns** `number` - The number of characters or elements

**Arguments**

| # | Type   | Name  | Description                              |
| - | -----  | ------| ---------------------------------------- |
| 0 |`array` | Array | The Array or String to get the length from

**Examples**

- **string**
``` js
[[functions.length("Thundercloud")]]
``` 
Returns `12`

- **array**
``` js
[[functions.length(inventory)]]
``` 
Returns `3` given the `inventory` array defined in the [atIndex](#atindex) example.


*[ISO]: Date and time strings in adaptor:ex are according to the ISO Time Format. Whenever you provide a date or time make sure it is formatted like `YY-MM-DDThh:mm:ss`. To define a date only use `YY:MM:DD`. If required, time will then be `00:00:00`. To define time only use `hh:mm:ss`. If required, date will then be today.