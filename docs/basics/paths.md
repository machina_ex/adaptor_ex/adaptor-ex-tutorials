Paths
=====

Paths allow you to create multiple parallel directions of states within a level.

Starting with the START state, only one state can actually be active per [session](./live.md#session) of a level. This means that the listener actions such as [onEvent](./actions/logic/onEvent.md) and [onChange](./actions/logic/onChange.md) are active only in the current state.

If the state is changed, the previous state is closed, regardless of whether the next state is before, after or far away.

Use the [Split](./actions/control/split.md) Path action to keep listener actions from different states active.

*Split Path* creates new paths that run simultaneously, independently of each other. Separate paths can simultaneously contain listeners in different states waiting for something.

Paths separated by *Split Path* are still linked to the state that contains *Split Path* **before**. If states are active in many of the newly created routes, they are terminated as soon as a state that is **before** the *Split Path* action is activated.

If separate paths are merged again, the same is true for all states **behind** the union.

Paths can be joined using a `next state` within an action or by using the [Join](./actions/control/join.md) Path action.

> A view of the paths in the adapter: ex editor is still pending. As soon as the time comes, we will also integrate the documentation and add examples from the editor. If you want to learn more about paths and their complex possible uses, check out our internal [concept for paths](./assets/adaptorex_pfade.drawio.html)

States that are not connected to the START state are executed in their own unnamed path. "Free" states cannot use [Split](./actions/control/split.md) Paths to create new paths.

You can use [local events](./events.md#local-events) to communicate between paths within a session.