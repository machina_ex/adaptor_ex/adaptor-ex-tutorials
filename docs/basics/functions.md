Javascript Functions
=====================

Javascript functions extend the possibilities of an adaptor:ex game by the complete range of the script programming language Javascript. Some functions are already included in the adaptor:ex server installation. It is also possible to create and integrate your own functions.

## Add Functions

Like [files and media files](files.md), functions are stored in the adaptor:ex data folder  `games` in the respective game folder.

Create one or more javascript  files ( `.js` ) in your game's folder `functions`  containing your functions.

```
data
├── games
|   ├── Playground
│       ├── files
│       ├── functions
│           ├── myfunctions.js
├── log
├── nedb
├── config.json
```

!!! warning "Directory depth" 
    Functions within the `functions` folder are registered in a maximum of one level directory depth. That means you can, for example, use subfolders to clone the functions of a git repository, but all `.js` files that export functions you may want to use, must be at the top level of this subfolder.

The files are loaded into your game as [nodejs modules .](https://www.w3schools.com/nodejs/nodejs_modules.asp) So you need to make your functions available to adaptor:ex using `module.exports`

```javascript
async function playerCount(args, {session, game}) {
    all_player = await game.db.players.find({})

    return all_player.length
}

function reverse(args, {session, game}) {
    let reversed = args[0].split("").reverse().join("")
    return reversed
}

module.exports = {
  playerCount:playerCount,
  reverse:reverse
}
```

If you copy these lines into a new file with the `.js` extension and move them to the functions folder in your game, you can access two new functions "reverse" and "playerCount" in your game.

You can add as many functions as you want in one file or split the functions across multiple files. The file names are arbitrary and play no role.

Call Functions
--------------------

You can use functions both in the independent [Function](./actions/logic/function.md) action and in all form fields that allow variables as input.

### The Function action

With the logic function action you can call functions, use their return value and trigger `next states` based on their output. Here you can find out more about the action: [Function](./actions/logic/function.md)

### Functions as Variables

Specify the function name enclosed in two square brackets ( `[[` and `]]` ), prepended by `functions`, in an action setting that accepts variable input.

The function name is supplemented by an open and a closed bracket `()`. Arguments can also be specified here, separated by a comma ( `,` ), to pass them to the function.

Example of a function as a variable:

`[[functions.test("hello","world")]]`

Within the function, "hello" and "world" are passed as an _array_ :

```javascript
function test(args, {session, game}) {
  session.log("1. " + args[0]) // 1. hello
  session.log("2. " args[1]) // 2. world
}
```

The return value of the function is inserted at the point at which the function is placed.

You can also find all the functions of your game in the [VARIABLES TOOLBAR](./editor.md#toolbar) from where you can drag them into the respective action.

![Search for 'functions in VARIABLES and drag and drop them onto the action form field](./assets/function_inline_drop.png "Search for 'functions in VARIABLES and drag and drop them onto the action form field")

#### example

Use the "playerCount" function created above in a [Switch](./actions/logic/switch.md) action.

![Function as a variable in the switch action](./assets/function_as_variable_example.png){ style=width:60% }

If the number of items in the players collection is greater than 10, the `next state` *Party* continues. Otherwise *NetflixAndChill* will be triggered.


## Write functions

!!! note
    This chapter is far from complete. Contact us if you have any questions at [tech@machinaex.de](mailto:tech@machinaex.de) or on the [machina commons discord server](https://discord.gg/quHbQAMvF6)

You can use all functions of the [Nodejs](https://nodejs.org/en/docs/guides/) Javascript Runtime.

To add external libraries that are not included with adaptor:ex, install them globally or in the `functions` folder of your game so that they are placed inside a `node_modules` folder in the `functions` directory.

All the parameters passed by the level session are included in the first argument.

The second argument always contains variables and functions from the adaptor:ex game and session context.

```javascript
async function playerCount(args, context) {
    all_player = await context.game.db.players.find({})

    return all_player.length
}

function reverse(args, {session, game}) {
    let reversed = args[0].split("").reverse().join("")
    return reversed
}

module.exports = {
  playerCount:playerCount,
  reverse:reverse
}
```

`args` contains the transfer parameters specified in the level. A number of parameters must be passed as an _array_ .

!!! note
    "args" is always an array when the [function is called as variable](#functions-as-variables). Therefore it is recommended to always treat "args" as an array. This way you can use the function in any context.

`context` is an Object that contains `session` and `game`.

`context.session` contains functions and variables related to the session that called the function.

`context.game` contains functions and variables related to the game in which the session that called the function is running.

You also have access to the global variable `adaptor` that contains functions and variables related to the adaptor:ex server hosting the game with the corresponding session.

We will try to explain the various functions and variables here as soon as possible.

Embedded Functions
-------------------

Adaptor:ex ships with some functions that can be used as shown in [Call Functions](#call-functions)

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L10 "goto function in source code") test

Return arguments the way they came in. Use it to find out how functions work.
 
**Returns**: `array` - the arguments that came in 

**Arguments** - Can be anything

**Example**

```js
[[functions.test("some value", 3)]]
``` 

will return the following array: `["some value", 3]`

Assign the test function return value to a variable with [Set variable](./actions/data/set.md) and log the two array entries with [Log Message](./actions/control/log.md).

![Set action with test function](./assets/function_test_example_1.png){ style=width:47%; float:left } ![Log action with test function return value](./assets/function_test_example_2.png){ style=width:52%; float:right }

The Log will show: `First Argument was some text. Second Argument was 3.`

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L27 "goto function in source code") random

Get a random Integer value. Provide maximum only to get a number between 0 (inclusive) and max. Provide min and max argument to get a number between min (inclusive) and max (inclusive)

When used with [Function Action](./actions/logic/function.md), if you provide no arguments but one or more `next states`, random will randomly output to one of the next outlets with an even chance for each.

**Returns**: `integer | next state` - A random integer value or, if applicable, triggers one of the specified `next states` randomly. 

**Arguments**

| # | Type | Name | Description |
| - | -------- | ------- | --- |
| 0 |`integer` | Min/Max | With two arguments this is the minimum value. With one argument only, this is the maximum value with a minimum of 0
| 1 |`integer` | Max     | Maximum value |

**Inline Example**

``` js
[[functions.random(5, 10)]]
``` 
Outputs an integer number between `5` and `10`.

Use it as **timeout** value in the [Timeout Action](./actions/time/timeout.md) and wait for an uncertain amount of time.

![Timeout action with random timeout range between 5 and 10 seconds](./assets/function_random_example_1.png){ style=width:47% }

**Function Action Example**

Add a [Function Action](./actions/logic/function.md) to the stage and click on *Settings*. Deselect **arguments** and select the **next** option.

![Use next in function action](./assets/function_random_example_2.png){ style=width:47% }

Add two or more **Next State** entries. 

![Add Next states o random function](./assets/function_random_example_3.png){ style=width:47% }

When run, the action will randomly select one of the given Next States and trigger it.

![Live Editor View with Random function triggers](./assets/function_random_example_4.gif)

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L73 "goto function in source code") dateTimeOffset

Get the date that lies a certain amount of time past or future of some other date.

The original Date is in [ISO](https://en.wikipedia.org/wiki/ISO_8601) Time Format. You can use `"now"` to set it to the current date and time.

The offset argument is formatted `"HH:MM:SS"` for example: `"1:00"` add 1 minute or `"-23:30:00"` subtract 23 and half an hour.

**Returns**: `date string` - A date that lies a certain amount of time before or after the original date. 

**Arguments**

| # | Type | Name | Description |
| - | -------- | ------- | --- |
| 0 |`date string` | Original Date | The Date that is to be added or subtracted from
| 1 |`time string` | Time Offset     | The amount of time to add or subtract from the original date

**Examples**

``` js
[[functions.dateTimeOffset("2023-06-22T12:00","15:00")]]
``` 
Outputs the date value `2023-06-22T12:15` which is 15 minutes after 2023/06/22 12 o`clock.

``` js
[[functions.dateTimeOffset("2023-12-24T12:00","-48:30:00")]]
``` 

Outputs the date value `2023-12-22T11:30` which is 2 days and 30 minutes before 2023/06/22 12 o`clock.

``` js
[[functions.dateTimeOffset("now","30")]]
``` 

Outputs the date and time value 30 seconds from the current date and time.

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L113 "goto function in source code") dateTimeFormat

Convert an [ISO](https://en.wikipedia.org/wiki/ISO_8601) Formatted Date and Time string into another kind of date and or time format string.

Uses the common javascript [Intl.DateTimeFormat](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/DateTimeFormat) to convert date and time.

You can pass an open amount of any of the formatting options [Intl.DateTimeFormat](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/DateTimeFormat) allows. Define options as a two part string separated by a whitespace. The parameter value comes first, the parameter name comes second, e.g. `"short weekday"`. Each option is provided as separate argument.

Pass a locale indicator as last argument to set the date and or time locale. E.g. `"de-DE"` for german or `"en-GB"` for British english locale.

**Returns**: `string` - A formatted date and or time. 

**Arguments**

| # | Type | Name | Description |
| - | -------- | ------- | --- |
| 0 |`date string` | Date | The [ISO](https://en.wikipedia.org/wiki/ISO_8601) Date that is to be formatted. Use `"now"` to provide the current Date and Time.
| 1 - n |`string` | Options | Add one or more formatting options. Make sure the locale option is the last function argument.

**Examples**

``` js
[[functions.dateTimeFormat("2023-06-22","short date","en-US")]]
``` 
Becomes `06/22/23`.

``` js
[[functions.dateTimeFormat("2023-06-22T17:35:12.345","weekday","hour","minute","1 secondDigits","de")]]
``` 
Becomes `Donnerstag, 17:35:12,3`.

``` js
[[functions.dateTimeFormat("now","long month","fr")]]
``` 
Will become the current month in french language, e.g. during March that would be `mars`.

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L239 "goto function in source code") atIndex

Get the value of an element in an array or string at a specific index position.

**Returns**: The value at given index

**Arguments**

| # | Type   | Name  | Description                              |
| - | -----  | ------| ---------------------------------------- |
| 0 |`array` | Array | The Array or String where to find element at index
| 1 |`number`| Index | The index number

**Examples**

- **string**
``` js
[[functions.atIndex("Thundercloud",3)]]
``` 
Returns the 4th character in `Thundercloud` and that would be `n`.

- **array**

Create an array using the [Set Variable](./actions/data/set.md) action.

!!! action inline end "Set Variable"
    
    ``` json
    {
      "variable": "inventory",S
      "value": "[\"rope\",\"bottle\",\"pen\"]",
      "multiple": true
    }
    ```
![Set action with array value](./assets/function_atIndex_set.png){ style=width:60%; float:left }


Use the array as first argument in your **atIndex** function to get the respective element.

!!! action inline end "Log Message"
    
    ``` json
    {
      "level": "info",
      "message": "Second Item in your inventory is [[functions.atIndex(inventory,1)]]"
    }
    ```
![Log action with atIndex function](./assets/function_atIndex_log.png){ style=width:60%; float:left }

### [:material-file-code:](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/functions.js#L249 "goto function in source code") length

Get the number of elements inside an array or the number of characters in a string.

**Returns** `number` - The number of characters or elements

**Arguments**

| # | Type   | Name  | Description                              |
| - | -----  | ------| ---------------------------------------- |
| 0 |`array` | Array | The Array or String to get the length from

**Examples**

- **string**
``` js
[[functions.length("Thundercloud")]]
``` 
Returns `12`

- **array**
``` js
[[functions.length(inventory)]]
``` 
Returns `3` given the `inventory` array defined in the [atIndex](#atindex) example.


*[ISO]: Date and time strings in adaptor:ex are according to the ISO Time Format. Whenever you provide a date or time make sure it is formatted like `YY-MM-DDThh:mm:ss`. To define a date only use `YY:MM:DD`. If required, time will then be `00:00:00`. To define time only use `hh:mm:ss`. If required, date will then be today.