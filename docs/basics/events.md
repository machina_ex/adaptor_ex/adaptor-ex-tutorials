Events
======

Plugins and plugin items like [Devices](./plugins/devices.md) can trigger events to which you can react with the [On Event](./actions/logic/onEvent.md) action.

With the [Dispatch Event](./actions/logic/dispatch.md) action you can also trigger events yourself and thus shape the course of a game across paths and sessions.

Use cases for events are e.g.:


* Communicate between 2 paths within a level ([Local Events](#local-events)).
* Send messages from one session to another session ([Events and external sessions](#events-and-external-sessions))
* Respond to events from devices or external software ([Plugin Events](#plugin-events))
* Trigger and receive events from items ([Items as Event source](#items-as-event-source))
* Send global events across the game ([Game Events](#game-events))

Local Events
---------------

Events triggered with the [Dispatch Event](./actions/logic/dispatch.md) action can be received in the same level (and the same session) with [On Event](./actions/logic/onEvent.md).

Without specifying a source (*source*), events are sent within a session of a level. For example, the course of a [Path](./paths.md) can be changed by another path.

![Send an event within the session](./assets/events_dispatch_local.png)

Send an event within the session ...

![Receive an event from within the session](./assets/events_catch_local.png)

... and receive.

This allows to react to events in other paths without having to merge the paths.

![A main path intercepts events from other paths](./assets/events_local_path.gif)

Here in the "CommonPath" state with [On Event](./actions/logic/onEvent.md) is checked which of the other two paths first arrived at the respective "Finished" state and a corresponding event dispatched.

Events and external sessions
----------------------------

If an external [Session](./live.md#session) is specified as the event source (*source*) in the [Dispatch Event](./actions/logic/dispatch.md) action, the event can be received within the external session as a local event.

``` mermaid
flowchart LR
    dispatchEvent-->onEvent
    subgraph A[Session A]
        dispatchEvent[Dispatch Event]
        end
    subgraph B[Session B]
        onEvent[On Local Event]
        end
    style B fill:#faf,stroke:#e9e,stroke-width:4px
```

Likewise, local events dispatched from the external session can be received in other sessions.

``` mermaid
flowchart LR
    dispatchEvent-->onEvent
    subgraph A[Session A]
        onEvent[On Event\nfrom: 'Session B']
        end
    subgraph B[Session B]
        dispatchEvent[Dispatch Local Event]
        end
    style B fill:#faf,stroke:#e9e,stroke-width:4px
```

> Note: All sessions also automatically trigger the 'quit' event when they are terminated (e.g. with the [Quit](./actions/control/quit.md) action) or aborted.

### Example

We create a new session with the [Launch Session](../basics/actions/control/launch.md) action and then send and receive events from the newly created session.

In the [Launch Session](../basics/actions/control/launch.md) action, enter a name for the local reference to the newly created session next to the name of the level (here "sidequest").

![Launch an external session](./assets/events_launch_external_session.png)

Below that, we add an [On Event](./actions/logic/onEvent.md) action to wait for a local event within the newly created "sidequestSession".

![Intercept local events of an external session](./assets/events_on_event_external_session.png)

The On Event Listener is triggered as soon as a [Dispatch Event](./actions/logic/dispatch.md) action in the "sidequestSession" triggers the local event "foundSomething". In the "sidequest" level there is a dispatch event action without *source* specification at the corresponding position:

![Dispatch events from an external session](./assets/events_dispatch_external_session.png)

This way the newly created session can communicate with the original session.

![Adaptor Live Mode: Catch event from an external session](./assets/events_from_external_session.gif)

In the left browser window is the origin session which creates a new session of the 'sidequest' level. In the right browser window is the 'sidequest' level that dispatches an event that is then caught in the origin session.

Conversely, events can be triggered in the source session, with the reference to the 'sidequest' session (here 'SideQuestSession') as *from*. These in turn can be caught with an [On Event](./actions/logic/onEvent.md) action in the newly created session of the 'sidequest' level.

![Send an event from to the external session](./assets/events_to_external_session.png)

We create a [Dispatch Event](./actions/logic/dispatch.md) action in the origin level that has "SidequestSession" as the event *source* (left browser window). In the 'sidequest' level, we now only need to listen for local events within our own session. So we create an [On Event](./actions/logic/onEvent.md) action (right browser window) without *from* specification with the matching event *name* (here: 'timeToReturn').

Plugin Events
-------------

Events that can be triggered by plugins or plugin items can be received and reacted to with the [On Event](./actions/logic/onEvent.md) action.

Which events a plugin can trigger you can see in the documentation of the respective plugin.

If you have created an item and it can trigger an event, you can select it as event source (*from*) in the [On Event](./actions/logic/onEvent.md) action.

Names of events that can be sent by plugins and plugin items you have installed can be selected in *event*.

![select device item as event source](./assets/events_incoming_osc_message.png)

You can also specify items that trigger events via references or level arguments. Select 'Custom Source' to get a free text input.

![Specify item reference as event source](./assets/events_incoming_osc_message_reference.png)

> - How to create references to items and use them as variables can be found in the chapter [Variables, Data, References](./variables.md#collections-and-data-items).
>
> - In the [Devices](./plugins/devices.md#events) documentation you can find out more about the "incomingMessage" event.

Items as Event source
---------------------

The [Dispatch Event](./actions/logic/dispatch.md) action can be used to trigger events from any Item.

Events triggered by Items can be received with the [On Event](./actions/logic/onEvent.md) action and the Item as event source (*from*).

### Example

With the [Add Item](./actions/data/addItem.md) action you can create a new Item in the Level Flow.

Add the *reference* option under *Settings* and specify a reference name (here: `Player`).

![Create a new item with Add Item](./assets/events_item_new.png)

We create a new Item with *name* `Ada` in the players collection.

You can now use the Item by reference in a [Dispatch Event](./actions/logic/dispatch.md) action as event *source*.

![Trigger an event from an Item](./assets/events_item_dispatch.png)

Receive events from Items with [On Event](./actions/logic/onEvent.md) by setting a reference to the Item under *from*.

![Receive an Item event](./assets/events_item_on_event.png)

Since we know the name of the Item (here: "Ada") we can simply reference the Item with: `players.Ada`.

This way you can receive the event anywhere in your game as long as the reference is to the same Item that was specified as *source* in the [Dispatch Event](./actions/logic/dispatch.md) action.

> See the chapter about [variables](./variables.md#item-container-and-level-arguments) for more information about how to use Items in your game.

Game Events
------------

Game events can be received in any session of any level within a game.

Use "game" as *source* in the [Dispatch Event](./actions/logic/dispatch.md) action to trigger global events.

![Trigger a global event with "game" as source](./assets/events_dispatch_global.png)

Global game events can be received with the [On Event](./actions/logic/onEvent.md) action by selecting "game" as source under *from*.

![Catch a global event](./assets/events_on_event_global.png)

In all sessions where an [On Event](./actions/logic/onEvent.md) listener is active, the corresponding game event will result in the *next state* if the event matches the specified conditions.

![Game events can be received in all sessions](./assets/events_global.gif)

Here, in the left browser window, in the "GlobalEvent" state, the game event "hello_everyone" is fired. Both the middle and right browser windows show a session that has an active [On Event](./actions/logic/onEvent.md) listener listening for the game event "hello_everyone".

Game events can of course also be received in sessions of the same level and in the same session in other paths.