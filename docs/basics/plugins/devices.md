Devices
=======

With the **Devices Plugin**, external devices and software can be connected to adaptor:ex via _HTTP_ , _TCP_ , _UDP_ , _OSC_ or _Serial_ interface.

In order to use HTTP, TCP, UDP or OSC devices that are not located locally on your computer, you need a local network - for example a home network, as created by any ordinary router.

If you want to use HTTP, TCP, UDP or OSC devices that are outside of your local network, or if you have installed adaptor:ex on a (foreign) server, your computer or the server and the device must be included be connected and accessible to the Internet.

So you can send messages from your device to adaptor:ex if adaptor:ex is reachable via a static IP or a URL.

If your device is reachable via a static IP or a URL, you can also send messages from adaptor:ex to the device.

Serial Devices are only available if the system on which adaptor:ex is installed has a USB port.

## Add device plugin
-------------------------
Open the game settings under `Game > Settings`. Below the active plugins you will find the list of inactive plugins. Click `+` the Devices Plugin button.

You can then create new devices, send messages to your devices with the  [Send Message](../actions/devices/send.md) action and react to incoming messages from devices in the [On Event](../actions/logic/onEvent.md).

## Create devices
------------------

Open the Devices plugin and under click `Items` on the communication protocol your device or software is using. A new device is created.

![Ein neues Device erstellen](./assets/devices_add_item.png)

You can create unlimited items from each protocol. Note, however, that if you use two ports within one protocol on the same computer, port conflicts may arise for some interfaces.

The setting options are different for each interface.

## Use devices
-----------------
### Actions

If you have created a new device, you can use it in the [Send Message](../actions/devices/send.md) and the [On Event](../actions/logic/onEvent.md) action.

You can address and use the device by selecting it in the corresponding dropdown list.

### Events

When a device receives an incoming message it triggers an `incomingMessage` event. You can intercept the event with the [On Event](../actions/logic/onEvent.md) action and if necessary query the event payload for conditions.

Select the device in the **source** option and specify `incomingMessage` as event name.

![Query an incomingMessage event by On Event](./assets/devices_events_example.png)

Example: A request to the "Hitchhiker" HTTP device with payload `{"answer":42}` triggers the **next state** *CorrectAnswer*.

![Browser URL input that meets the above event condition](./assets/devices_get_request_url.png)

### Status Variables

Each device that receives incoming data stores the last received message in `incoming` and the last sent message in the `outgoing` variable.

You can use them in all action input fields that accept variables by addressing the device and the `incoming` or `outgoing` variable. 
For example with
`[[devices.http.Numpad.incoming]]`
you can access the last message received from your HTTP device `Numpad`.

As soon as your device has sent or received a message, you will also find the variables in the ASSETS toolbar.

When your device receives or sends JSON messages, only the fields sent are overwritten. For example, you can access multiple sensors on your device via the `incoming` variable.

## Tutorials

Integrate an Arduino microcontroller into your adaptor:ex game via Serial: [Arduino Serial](../../tutorials/arduino-serial/index.md)

Use adaptor:ex as HTTP server and client to integrate network devices: [Network Devices](../../tutorials/networkdevices/index.md)
