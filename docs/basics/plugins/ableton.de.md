Ableton Live
============

Mit dem Ableton Live Plugin kannst du Tracks, Clips und Scenes starten und stoppen oder Mixer-Einstellungen in deinem Ableton Liveset verändern.

Liveset einrichten
------------------

Um ein Liveset mit adaptor:ex zu steuern, musst du den max4live [Ableton Live adaptor](https://gitlab.com/machina_ex/adaptor_ex/ableton_live_adaptor) als Audio-Effekt in das Liveset einbinden.

Lade das max4live patch und die dazugehörige Javascript-Datei von folgendem gitlab repository herunter: [https://gitlab.com/machina_ex/adaptor_ex/ableton_live_adaptor](https://gitlab.com/machina_ex/adaptor_ex/ableton_live_adaptor){target=_blank}

![Der Download button oben rechts neben clone](./assets/ableton_download.png)

Entpacke das Archiv und füge die Datei `adaptorforlive.amxd` einem beliebigen Track in deinem Liveset per Drag & Drop als Audio-Effekt hinzu.

![Einen Audio-Effekt hinzufügen per drag and drop](./assets/ableton_add_audio_effect.png)

![Der Live Adaptor m4l patch im liveset](./assets/ableton_add_live_adaptor.png)

Gehe nun in adaptor:ex zu `Game > settings` und füge das Ableton Plugin hinzu.

Erstelle anschließend ein neues `Liveset`.

![Ein neues Liveset erstellen mit dem plus button](./assets/ableton_add_liveset.png)

Passe den Namen des neuen Livesets an. Wenn du nur ein Liveset einsetzt, verwende die vorgegebenen Ports. Wenn das Liveset nicht auf derselben Maschine wie adaptor:ex läuft, passe die IP an.

![Einstellungen des neuen liveset verwalten](./assets/ableton_create_liveset.png)

Drücke auf `SAVE`, um das Liveset zu erstellen.

Wenn du weitere Livesets einrichtest, verwende einen anderen `Liveset Port` und einen anderen `Adaptor Port`, um Konflikte zu vermeiden. Verwende jeweils dieselben Ports im max4live Audio-Effekt wie im zugehörigen adaptor:ex Liveset.

Actions
-------

Nun kannst du Ableton Live actions verwenden, um dein Liveset zu steuern.

Starte oder stoppe einzelne Clips oder ganze Tracks und Scenes mit

- [Play Clip](../actions/ableton/playClip.md)
- [Stop Clip](../actions/ableton/stopClip.md)
- [Play Scene](../actions/ableton/playScene.md)
- [Stop Track](../actions/ableton/stopTrack.md)

Ändere die Mixer-Einstellungen einzelner Tracks mit

- [Set Track Mix](../actions/ableton/trackMix.md)

Liveset und Actions aktualisieren
--------------------------------------

Sobald du den `Refresh` Button drückst, stehen alle Clips, Tracks und Scenes, die du im Liveset in der Session Ansicht erstellt oder hinzugefügt hast, in den adaptor:ex actions zur Verfügung.

![Der Refresh button im Audio-Effekt](./assets/ableton_refresh.png)

Nutze den `Refresh` Button erneut, wenn du Änderungen am Liveset vorgenommen hast.

Nutze `Refresh` auch dann, wenn du dein Liveset erst erstellt hast, nachdem du adaptorforlive in dein Liveset eingebunden hast.
