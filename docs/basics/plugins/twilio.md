Twilio
======

Make calls and send SMS messages and respond to incoming calls and SMS messages via the [Twilio](https://www.twilio.com/) Voice and SMS API.

To use this plugin you need to have a Twilio account and order a phone number there. Ordering and using phone numbers on Twilio is a paid service.

In order for adaptor:ex to respond to incoming SMS and manage calls, the adaptor:ex server must be connected to the internet and accessible from the internet.

Making adaptor:ex accessible from the internet
----------------------------------------------

Start your adaptor:ex server with the option `--host` to pass a web address as hostname under which the computer or server on which adaptor:ex is running can be reached from the internet. E.G.:

`adaptorex --host https://myveryownwebdomain.org`

Of course, you can also specify the static IP

`adaptorex --host 123.456.7.890`

If you want to use adaptor:ex with Twilio on a local machine you have to use an IP forwarding service.

For example, if you own a Fritzbox, you can set up a [MyFritz](https://myfritz.net) address on your Fritzbox and set up port forwarding to the adaptor:ex server on your machine.

Another possibility is to use a service like [ngrok](https://ngrok.com/) which takes over the forwarding for you and provides a domain.

Set up a phone number
------------------------------

In adaptor:ex, open `Game > settings` and add the Twilio plugin to your game.

![Add the Twilio plugin to the game](./assets/twilio_add_plugin.png)

To connect Twilio and adaptor:ex you need your Twilio **SID**, your Twilio **Token** and a Twilio **phone number**.

### SID and token

Register on the Twilio website: [www.twilio.com/](https://www.twilio.com/)

Open your Twilio account to access the dashboard. On the home page you will find your **SID** and **Token**.

![The twilio Dashboard with SID and Token](./assets/twilio_id_and_token.png)

Copy the two values and enter them in adaptor:ex in the Twilio Plugin Settings in the respective field.

![Specify SID and Token in the Twilio Plugin Settings](./assets/twilio_settings.png)

### Phone number

To add a phone number select `Phone Numbers > Manage > Buy A Number` in your Twilio account dashboard. Make sure you select a number that has both "Voice" and "SMS" capability.

![Buy a Twilio phone number](./assets/twilio_buy_number.png)

> National and Twilio internal regulations for buying a phone number are always changing, so unfortunately we cannot provide exact instructions here. Twilio will ask you to provide proof before you can finally order a number.

Once you have purchased a phone number, create a new *PHONE* item in the Twilio plugin.

![Screenshot, create a new phone item with the plus button](./assets/twilio_add_phone.png)

Select the `info` tab to give your Twilio phone a name. The Twilio phone in the example has the name "Kuhlmann". A character from our Messenger Adventure *Lockdown*.

Enter the Twilio phone number (`phone_number`) under `settings`.

![Screenshot, the settings for the twilio phone](./assets/twilio_save_phone.png)

> Make sure that the `phone_number` does not contain spaces and starts with a `+`.

then click on `SAVE` to create the item.

Your phone number can now send SMS via the Twilio API with [Send SMS](../actions/twilio/sendSms.md).

### Set up webhooks

To receive SMS and make calls you still need to point Twilio to your adaptor:ex server.

Open the Twilio Plugin Settings in adaptor:ex and select the phone item you want to connect to the Twilio account.

Copy the information under 'webhooks'.

![Screenshot, webhooks in the phone item settings](./assets/twilio_phone_webhooks.png)

and paste it into your twilio account in the phone number settings (`Phone Numbers > Manage > Active Numbers`):

![screenshot, specify call webhooks in Twilio](./assets/twilio_call_webhook.png)

![Screenshot, specify sms webhooks in Twilio](./assets/twilio_sms_webhook.png)

### Specify default level

You can specify the `default level` in the settings of the Twilio phone. Select it from the levels you have created in your game.

![Screenshot game twilio plugin settings phone item default level](./assets/twilio_default_level.png)

If your Twilio phone is contacted by SMS or call and is not already in an [Incoming SMS](../actions/twilio/onSms.md) or [Incoming Call](../actions/twilio/onCall.md) action with this phone number, a session of the `default level` is started.

Also if the corresponding `default level` is already active, no further `default level` session will be started, even without an active SMS or Call action.

The contacting *player* [Data Item](../variables.md#item-container-and-level-arguments) is addressable within the level as "Player" argument.

If there is no *player* data item with the contacting phone number, a new data item is created in the *player* collection.

### Telephone numbers from different accounts

You can set separate account data for each phone by specifying `sid` and `token` in the phone's `Settings`.

Only phone items that do not specify a sid and token themselves are connected via `sid` and `token` of the Twilio plugin.

Actions
-------

Send an SMS from a Twilio phone number with [Send SMS](../actions/twilio/sendSms.md).

Respond to incoming SMS with [On Incoming SMS](../actions/twilio/onSms.md).

Start a call with [Outgoing Call](../actions/twilio/call.md).

Respond to incoming calls with [On Incoming Call](../actions/twilio/onCall.md).

Start a Studio Flow you have created in Twilio Studio with [Twilio Studio Flow](../actions/twilio/flow.md)

