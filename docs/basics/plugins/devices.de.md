Devices
=======

Mit dem **Devices** Plugin können externe Geräte und Software per *HTTP*, *TCP*, *UDP*, *OSC* oder *Serial* Schnittstelle mit adaptor:ex verbunden werden.

Um HTTP-, TCP-, UDP- oder OSC-Devices zu nutzen, die sich nicht lokal auf deinem Rechner befinden, benötigst du ein lokales Netzwerk - zum Beispiel ein Heimnetzwerk, wie es jeder gewöhnliche Router erstellt.

Wenn du HTTP-, TCP-, UDP- oder OSC-Devices nutzen möchtest, die sich außerhalb deines lokalen Netzwerks befinden, oder wenn du adaptor:ex auf einem (fremden) Server installiert hast, müssen dein Rechner oder der Server und das Device mit dem Internet verbunden und zugänglich sein. 

Du kannst also Nachrichten von deinem Device an adaptor:ex senden, wenn adaptor:ex über eine statische IP oder eine URL erreichbar ist. 

Wenn dein Device über eine statische IP oder eine URL erreichbar ist, kannst du auch Nachrichten von adaptor:ex an das Device senden.

Serial Devices sind nur verfügbar, wenn das System, auf dem adaptor:ex installiert ist, über einen USB-Anschluss verfügt.

Devices Plugin hinzufügen
-------------------------

Öffne die Game-Einstellungen unter `Game > Settings`. Unterhalb der aktiven Plugins findest du die Liste der inaktiven Plugins. Klicke auf den `+` Button des Devices Plugin. 

Anschließend kannst du neue Devices erstellen, Nachrichten mit der [Send Message](../actions/devices/send.md) action an deine Devices schicken und auf eingehende Nachrichten von Devices in der [On Event](../actions/logic/onEvent.md) action reagieren.

Devices erstellen
------------------

Öffne das Devices Plugin und klicke unter `Items` auf das Kommunikationsprotokoll, das dein Gerät oder deine Software verwendet. Ein neues Device wird erstellt. 

![Ein neues Device erstellen](./assets/devices_add_item.png)

Du kannst von jedem Protokoll unbegrenzt Items erstellen. Beachte jedoch, dass für manche Schnittstellen Port-Konflikte auftauchen können, wenn du zwei Ports innerhalb eines Protokolls auf demselben Computer verwendest.

Die Einstellungsmöglichkeiten sind für jede Schnittstelle unterschiedlich.

Devices verwenden
-----------------

### Actions
Wenn du ein neues Device erstellt hast, kannst du es in der [Send Message](../actions/devices/send.md) und der [On Event](../actions/logic/onEvent.md) action nutzen.

Über die Auswahl in der entsprechenden Dropdown-Liste kannst du das Device adressieren und verwenden.

### Events

Wenn ein Device, eine eingehende Nachrichten empfängt löst es ein `incomingMessage` Event aus. Du kannst das Event mit der [On Event](../actions/logic/onEvent.md) action abfangen und ggf. den Event Payload auf Bedingungen abfragen.

Wähle das Device in der **source** Option aus und gib `incomingMessage` als Event Name an.

![Ein incomingMessage Event per On Event abfrageb](./assets/devices_events_example.png)

Beispiel: Ein Request auf das "Hitchhiker" HTTP Device mit Payload `{"answer":42}` löst den **next state** *CorrectAnswer* aus.

![Browser URL eingabe das die obige Event bedingung erfüllt](./assets/devices_get_request_url.png)

### Status Variablen

Jedes Device, das eingehende Daten empfängt, speichert die zuletzt eingegangene Nachricht in der `incoming` und die zuletzt versendete Nachricht in der `outgoing` variable.

Du kannst sie in allen action Eingabefeldern, die Variablen zulassen, verwenden, indem du das Device und die `incoming` oder `outgoing` variable adressierst. Mit 

`[[devices.http.Numpad.incoming]]`

kannst du etwa auf die letzte eingegangene Nachricht deines HTTP-Device `Numpad` zugreifen.

Sobald dein Device eine Nachricht versendet oder empfangen hat, findest du die Variablen auch in der ASSETS Toolbar.

Wenn dein Device JSON-Nachrichten empfängt oder sendet, werden immer nur die jeweils gesendeten Felder überschrieben. So kannst du z.B. auf mehrere Sensoren an deinem Device über die `incoming` variable zugreifen.

Tutorials
---------

Binde einen Arduino Microcontroller per Serial in dein adaptor:ex Game ein: [Arduino Serial](../../tutorials/arduino-serial/index.md)

Nutze adaptor:ex als HTTP Server und Client um Netzwerkgeräte einzubinden: [Network Devices](../../tutorials/networkdevices/index.md)
