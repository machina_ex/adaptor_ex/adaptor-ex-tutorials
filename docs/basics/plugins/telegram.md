Telegram
=========

Send messages via the instant messenger Telegram and respond to incoming messages in individual and group chats.

To use Telegram in adaptor:ex, adaptor:ex must be able to access the internet.

You can create Telegram bots as well as accounts, i.e. real phone numbers, and use them to send messages and respond to incoming messages.

Tutorials
---------

In [Telegram Basics: Settings / Setup / Add Account](../../tutorials/telegram-settings/index.md) you will learn how to set up the Telegram Messenger in adaptor:ex and connect your phone number or a telegram bot.

[Storytelling with Telegram and adaptor eX](../../tutorials/telegram-basics/index.md) is the introduction to sending and receiving text messages.

Details on sending media files and location data can be found in [adaptor:ex telegram plugin media files and other special functions](../../tutorials/telegram-send-media/index.md)

Actions
-------

Send text messages with [Send Message](../actions/telegram/sendMessage.md).

Respond to incoming messages with [On Incoming Message](../actions/telegram/onMessage.md).

Send files, pictures, voice and video messages with [Send File](../actions/telegram/sendFile.md).

Send location data in the telegram map view with [Send Geo](../actions/telegram/sendGeo.md).

Create and send a poll with [Send Poll](../actions/telegram/sendPoll.md).

Create and send a poll that has a correct answer with [Send Quiz](../actions/telegram/sendQuiz.md).

Answer a poll or quiz with one of your Telegram accounts [Cast Vote](../actions/telegram/castVote.md) *(Accounts Only)*.

Create group chats with [Create Chat](../actions/telegram/createChat.md) *(Accounts Only)*.

Change the settings of group chats and invite members to a group with [Edit Chat](../actions/telegram/editChat.md).

> :material-alert-outline: Some actions cannot be used or are limited when used with bots *(accounts only)*. You can find out more in the action descriptions.