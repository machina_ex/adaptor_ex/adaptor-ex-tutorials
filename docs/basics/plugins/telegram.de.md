Telegram
=========

Versende Nachrichten über den Instant Messenger Telegram und reagiere auf eingehende Nachrichten in Einzel- und Gruppenchats.

Um Telegram in adaptor:ex nutzen zu können, muss adaptor:ex auf das Internet zugreifen können.

Du kannst sowohl Telegram Bots als auch Accounts, also echte Telefonnummern, anlegen und darüber Nachrichten versenden und auf eingehende Nachrichten reagieren.

Tutorials
---------

In [Telegram Basics: Settings / Setup / Add Account](../../tutorials/telegram-settings/index.md) erfährst du, wie du den Telegram Messenger in adaptor:ex einrichtest und einen Account (Telefonnummer) oder einen Telegram Bot anlegst.

[Storytelling mit Telegram und adaptor eX](../../tutorials/telegram-basics/index.md) ist der Einstieg in das Versenden und empfangen von Textnachrichten. 

Details zum versenden von Mediendateien und Standortdaten findest du in [adaptor:ex telegram plugin media files und andere Spezialfunktionen](../../tutorials/telegram-send-media/index.md) 

Actions
-------

Versende Textnachrichten mit [Send Message](../actions/telegram/sendMessage.md).

Reagiere auf eingehende Nachrichten mit [On Incoming Message](../actions/telegram/onMessage.md).

Versende Dateien, Bilder, Sprach- und Videonachrichten mit [Send File](../actions/telegram/sendFile.md).

Versende Standortdaten in der telegram Kartenansicht mit [Send Geo](../actions/telegram/sendGeo.md).

Erstelle und versende eine Umfrage mit [Send Poll](../actions/telegram/sendPoll.md).

Erstelle und versende eine Umfrage mit richtiger Antwort mit [Send Quiz](../actions/telegram/sendQuiz.md).

Beantworte eine Umfrage oder ein Quiz mit einem deiner Telegram Accounts [Cast Vote](../actions/telegram/castVote.md) *(Nur Accounts)*.

Erstelle Gruppenchats mit [Create Chat](../actions/telegram/createChat.md) *(Nur Accounts)*.

Ändere die Einstellungen von Gruppenchats und lade Mitglieder in eine Gruppe ein mit [Edit Chat](../actions/telegram/editChat.md).

> :material-alert-outline: Manche Actions können nicht *(Nur Accounts)* oder nur eingeschränkt mit Bots verwendet werden. Genaueres findest du in den Action Beschreibungen heraus.