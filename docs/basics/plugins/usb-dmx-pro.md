USB-DMX-PRO
===========

Control DMX light signals via a dmx-usb-pro box.

To connect the dmx-usb-pro box, you must have adaptor:ex installed on a computer with a USB serial port.

Tutorials
---------

In the tutorial [Controlling light with adaptor eX and the ENTTEC DMXUSB Pro Box](../../tutorials/lightcontrol-dmx-usb-pro/index.md) you will learn how to use the **USB-DMX-PRO** plugin.

Actions
-------

Control the channels of your dmx-box with [Set Lights](../actions/usb-dmx-pro/setLights.md).