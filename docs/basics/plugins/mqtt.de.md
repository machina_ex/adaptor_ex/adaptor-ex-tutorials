MQTT
====

Verbinde adaptor:ex mit einem MQTT Netzwerk und versende Nachrichten oder abonniere topics. adaptor:ex verbindet sich als mqtt client mit einem separaten mqtt broker.

MQTT ist ein beliebtes Netzwerkprotokoll, das insbesondere für IoT Anwendungen genutzt wird.

Du benötigst einen eigenen mqtt broker auf den deine adaptor:ex installation zugreifen kann oder kannst einen der öffentlichen test broker nutzen.

Hier findest du mehr über das MQTT Netzwerkprotokoll heraus: [mqtt.org](https://mqtt.org/)

Einrichten
----------

Füge das MQTT Plugin unter `Game` -> `Settings` deinem Game hinzu und gib die vollständige URL deines MQTT Brokers an.

![Screenshot der game settings mit MQTT plugin geöffnet](./assets/mqtt_settings_url.png)

Unter `Settings` kannst du weitere Optionen für die Verbindung zum Broker angeben wie Nutzername und Passwort.

![Screenshot der game settings mit MQTT plugin und Optionen geöffnet](./assets/mqtt_settings_options.png)

Das Plugin basiert auf dem gleichnamigen [NPM Modul](https://www.npmjs.com/package/mqtt). Details zu den optionalen Settings findest du hier: [https://github.com/mqttjs/MQTT.js#client](https://github.com/mqttjs/MQTT.js#client)

Actions
-------

Die run action [Send MQTT Message](../actions/mqtt/sendMQTTMessage.md) erlaubt es dir MQTT Nachrichten an alle subscriber eines Topic zu versenden.

Mit der listen action [On MQTT Message](../actions/mqtt/onMQTTMessage.md) kannst du MQTT topics abonnieren und auf eingehende Nachrichten auf dem Topic reagieren.

Beispiel
--------

> Level File: [:material-file-code-outline: mqtt_example.json](./assets/mqtt_example.json){:download="mqtt_example.json"}

Versende mit [Send MQTT Message](../actions/mqtt/sendMQTTMessage.md) eine Nachricht die du dann, neben anderen möglichen Geräten in deinem MQTT Netzwerk, mit [On MQTT Message](../actions/mqtt/onMQTTMessage.md) empfängst.

Füge das MQTT Plugin hinzu und verbinde dich mit einem Broker ([Einrichten](#einrichten))

Öffne ein neues oder bestehendes Level im [Level Editor](../editor.md) und füge eine [Send MQTT Message](../actions/mqtt/sendMQTTMessage.md) action zur STAGE hinzu.

Füge als `topic` "echo/chamber" ein und als `message` "Hello adaptor".

![Screenshot: Eine mqtt sendMessage action auf die STAGE ziehen und bearbeiten](./assets/mqtt_example_send_message.png)

Füge nun eine [On MQTT Message](../actions/mqtt/onMQTTMessage.md) zu deinem Level hinzu (1) und gib das selbe topic "echo/chamber" an (2).

Wähle unter `Settings` (3) die `if` option an (4) klicke auf `Add condition` (5). 

![Screenshot: Eine mqtt onMessage action auf die STAGE ziehen und bearbeiten](./assets/mqtt_example_on_message.png)

Wir nutzen die `equals` Bedingung um abzufragen ob die eingehende MQTT Message, auf dem "echo/chamber" Topic, "Hello adaptor" ist.

![Screenshot: Eine if On MQTT Message Bedingung definieren](./assets/mqtt_example_on_message_condition.png)

Um das Beispiel zu testen erstellen wir einen State, der ausgelöst wird, wenn die Bedingung zutrifft. Mit der [Log](../actions/control/log.md) Action können wir noch einen Blick in die Eingegangene Nachricht werfen. Mit der folgenden Log Message können wir Message und Topic der eingegangenen Nachricht in der Konsole anzeigen lassen:

```
Incoming MQTT Message: [[state.AwaitHello.onMQTTMessage_1.message]] on Topic [[state.AwaitHello.onMQTTMessage_1.topic]]
```

Außerdem benennen wir den State mit der **On MQTT Message** um und verbinden ihn mit dem *START* State.

![Screenshot: Das fertige Beispiel level mit On MQTT Message, Log Action und Send MQTT Message](./assets/mqtt_example_on_message_log.png)

Im [Live Modus](../live.md) erstellen wir eine neue Session. Der *AwaitHello* State wartet nun auf eingehende MQTT Messages. Wenn alles geklappt hat und wir den *Hello* State auslösen sollte direkt im Anschluss der *IncomingHelloMessage* State ausgelöst werden.

![Screen capture: Klick auf Start Session, Session auswählen, Konsole öffnen, Klick auf Play Button von Hello State](./assets/mqtt_example_on_message_live.gif)

In der **Console** können wir einen Blick auf die eingegangene MQTT Nachricht werfen.