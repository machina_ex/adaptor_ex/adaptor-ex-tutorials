MQTT
====

Publish messages and subscribe to topics in a MQTT network. adaptor:ex will connect as a mqtt client to a separate mqtt broker.

MQTT is a popular network protocol used especially for IoT applications.

You need a separate mqtt broker that your adaptor:ex installation can access or you can use one of the public test brokers.

Here you can find out more about the MQTT network protocol: [mqtt.org](https://mqtt.org/)

Setup
-----

Add the MQTT plugin to your game under `Game` -> `Settings` and enter the full URL of your MQTT broker.

![Screenshot of the game settings with MQTT plugin open](./assets/mqtt_settings_url.png)

Under 'Settings' you can specify further options for the connection to the broker like `username` and `password`.

![Screenshot of the game settings with MQTT plugin and options opened](./assets/mqtt_settings_options.png)

The plugin is based on the [NPM module](https://www.npmjs.com/package/mqtt) with the same name. Details about optional settings can be found here: [https://github.com/mqttjs/MQTT.js#client](https://github.com/mqttjs/MQTT.js#client)

Actions
-------

The run action [Send MQTT Message](../actions/mqtt/sendMQTTMessage.md) allows you to send MQTT messages to all subscribers of a topic.

The listen action [On MQTT Message](../actions/mqtt/onMQTTMessage.md) allows you to subscribe to MQTT topics and react on incoming messages on the topic.

Example
--------

> Level File: [:material-file-code-outline: mqtt_example.json](./assets/mqtt_example.json){:download="mqtt_example.json"}

In this example we will send a message with [Send MQTT Message](../actions/mqtt/sendMQTTMessage.md) which you then catch, among other possible devices in your MQTT network, with [On MQTT Message](../actions/mqtt/onMQTTMessage.md).

Add the MQTT plugin and connect to a broker ([Setup](#setup)).

Open a new or existing level in the [Level Editor](../editor.md) and add a [Send MQTT Message](../actions/mqtt/sendMQTTMessage.md) action to the STAGE.

Set "echo/chamber" as `topic` and "Hello adaptor" as `message`.

![Screenshot: Drag and drop a mqtt sendMessage action to the STAGE](./assets/mqtt_example_send_message.png)

Now add an [On MQTT Message](../actions/mqtt/onMQTTMessage.md) action to your level (1) and specify the same "echo/chamber" value as above for topic (2).

Under `Settings` (3) select the `if` option (4) and click on `Add condition` (5).

![Screenshot: Dragging an mqtt onMessage action to the STAGE and editing it](./assets/mqtt_example_on_message.png)

We use the `equals` condition to query whether the incoming MQTT message, on the "echo/chamber" topic, is "Hello adaptor".

![Screenshot: Defining an if On MQTT Message condition](./assets/mqtt_example_on_message_condition.png)

To test the example, we create a state that is triggered when the condition matches. With the [Log](../actions/control/log.md) action we can then take an additional look at the incoming message. With the following log message we can display the message and topic of the incoming message in the console:

```
Incoming MQTT Message: [[state.AwaitHello.onMQTTMessage_1.message]] on Topic [[state.AwaitHello.onMQTTMessage_1.topic]]
```

We also rename the state with the **On MQTT Message** and connect it to the *START* state.

![Screenshot: The finished example level with On MQTT Message, Log Action and Send MQTT Message](./assets/mqtt_example_on_message_log.png)

In [Live Mode](../live.md) we create a new session. The *AwaitHello* state is now waiting for incoming MQTT messages. If everything worked and we click the play button on the *Hello* state, the *IncomingHelloMessage* state should be triggered directly afterwards.

![Screen capture: Click on Start Session, select session, open console, click on play button of Hello State](./assets/mqtt_example_on_message_live.gif)

In the **Console** we can then take a look at the MQTT message that was just received.