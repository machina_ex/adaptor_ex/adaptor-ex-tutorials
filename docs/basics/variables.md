Variables, data, references
============================

Variables allow you to use changeable values in your actions. Variables are placeholders for text, numerical values or more complex data that only get a fixed value once the action is executed.

Variables are identified in adaptor:ex by enclosing two square brackets. That might look like this:

`[[Player.name]]`

At the moment when we design our level, it is still unclear which `Player` will play the level later - and therefore the `name` is still _variable_ .

If a session of the level is then started in [live mode](./live.md) and the action is executed, `[[Player.name]]` is replaced by the name of the player that is currently playing the level.

In this Twilio [Send SMS](./actions/twilio/sendSms.md) action we use a variable to personalize how the player is being addressed:

![Example action Twilio send sms](assets/smsout_example.png)

If the level is played by a player for whom the name 'Ada Lovelace' was saved somewhere else, the SMS will look like this: `Hello Ada Lovelace! How is it going?`

Variables can be used in almost any action. You can drag a variable from the [TOOLBAR](./editor.md#toolbar) 's VARIABLES MENU into your action or type it in yourself.

In the VARIABLES TOOLBAR you will find the variables that adaptor:ex has collected from existing data. However, there can also be variables that are not (yet) listed there, and the variables in the TOOLBAR could also refer to nothing at the moment they are called.

Variables, especially [Items](#item-container-and-level-arguments), can contain other variables. Therefore, the TOOLBAR is structured similar to a file directory. If you drag a variable over, the path that leads to the actual variable would be added.

![Drag a variable from the TOOLBAR to the Send SMS Action](./assets/variables_toolbar_drop.png)

There are different places in adaptor:ex where variable data is stored and different ways of *referencing* that data. In the following paragraph, you will find out how to create, change and use variable data in adaptor:ex.

## Local Variables

Local variables are data that only last for the course of a level session.

For example, with the [Set](../basics/actions/data/set.md) Data action, you can create a local variable and assign it a value, or modify an existing one.

For example, you can save the information at one point in the level as to whether a player has taken the left or the right path, so that you can go back to it at the end of the game.

![set local variable](assets/set_local_variable.png)

You can later use this variable in a text or use it in the [Switch](../basics/actions/logic/switch.md) Logic action to determine which state to continue with.

## Level Attributes

In the `level -> config` you can specify `Attributes`  for your level. `Attributes` allow you to set properties for your level.

![Level Config Attributes](./assets/variables_level_attributes.png)

Address `Attributes` at the appropriate level by prefixing the variable `level`, i.e. :

`[[level.reward]]`

Each live session of the level accesses the same variable here. So if a level attribute is changed in one session, it will be changed for all sessions.

Levels `Attribute` can be used well in the [launch session](./actions/control/launch.md). Then you can determine the level that should be started based on its attributes.

Level Arguments
----------------

If a level session is started, it is possible to pass on data or values to the session as arguments. The forwarded values are then accessible via variables in the newly started session.

Arguments can be edited, removed and added in the `level config`. Here we can also specify *default values*, which are used in the level session if no other value was specified for this argument the moment the session was started. We can also specify of what type of data the respective argument should be.

You can pass collection items as level arguments. Under [Items as level arguments](#items-as-level-arguments) you can find out more about it.

### Edit arguments

To specify and edit level arguments, open the level config via the menu `LEVEL > config`.

![Screenshot](./assets/variables_level_arguments_config.png)

Some plugins pass the `Player` argument when automatically starting a session. Therefore, each level already has a `Player` argument from the beginning, which is defined in the config.

![Screenshot](assets/variables_level_arguments_default.png)

You can add further arguments with `ADD NEW`. Each argument requires a name (`NAME`) under which you can then use it when editing the level.

![Screenshot](./assets/variables_level_arguments_example.png)

You can optionally specify the data type (`TYPE`) of the level argument. `TYPE` can either be a data type `string`, `number`, `array` or `object` or the name of one of your collections.

If you set a default value (`DEFAULT VALUE`), each session of the level where no value is forwarded for this argument will use the default value.

### Forwarding arguments

Which arguments are passed to a newly started session is determined when the session is started. If you manually create a session in live mode, you can specify the arguments values beforehand.

Switch to live mode to start a new session. Then enter a value for the level argument or change the default value.

![Screenshot](./assets/variables_level_arguments_forward.png)

The control action [Launch Session](../basics/actions/control/launch.md) also allows level arguments to be specified via the `arguments` option.

![Screenshot](./assets/variables_level_arguments_launch.png){ style=width:300px }

Add the arguments option via `Settings` and specify all arguments you want to forward when launching the session.

!!! action inline end "Launch Session"
    
    ``` json
    {
        "level": "first_steps",
        "arguments": [
            {
                "name": "Season",
                "value": "Summer"
            }
        ]
    }
    ```
![](./assets/variables_level_arguments_launch_add.png){ style=width:60% }

Instead of a specific string or number, you can always use a variable an array or object as argument `value`.

!!! note
    When starting a level session with the [Launch Session](../basics/actions/control/launch.md) action, it is possible to forward arguments that were not defined in the `level config`. They are then available in the new level session via the name specified in the action.

### Accessing arguments

In the level editor we can access arguments as variables. To do this, place the arguments name between two square brackets `[[ArgName]]`.

Arguments that you have specified in the level config can be found in the VARIABLES TOOLBAR.

![Screenshot](./assets/variables_level_arguments_toolbar.png)

You can drag them into an action or type them yourself.

![Screenshot](./assets/variables_level_arguments_drop.png)

Use dot notation to access properties within the argument if it is an object, array or an item reference.

!!! info
    If you create a variable with [Set variable](../basics/actions/data/getItem.md) or another action that has the same name as one of the level arguments, the argument will be overwritten with the new value or reference.

Collections and Data Items
--------------------------

Each Data Item is a data set that can have different properties and is associated with a collection of items.

!!! note
    A good introduction to working with collections and items is the tutorial on [creating and using a synonym dictionary](../tutorials/messenger-thesaurus/index.md).

Under `Game -> settings -> collections` you will find the data items that currently exist in your game. Here you can also add new items, change the properties of existing items or create a completely new collection.

![The Collections Overview](./assets/variables_settings_collections.png)

With the [Add Item](./actions/data/addItem.md) action you can create new items in one of your collections during the course of a level.

Data items can be used in every level and persist if the session of a level is finished. This is important e.g. if your game is played by different players, who make different progress during the game, collect points, change their name and so on.

Plugins like [telegram](./plugins/telegram.md) and [twilio](./plugins/twilio.md) automatically create new items in the `players` collection, e.g. when a game is contacted by a previously unknown phone number. Therefore every adaptor:ex game has a `players` collection from the beginning.

There are several ways to use data items in a level.

### Name Reference

Data items that have a `name` property that is already known can be used with it directly.

`[[players.Ada]]`

gives you the data item with the `name` property `Ada` in the `players` collection.

To get a property of the data item, add it to the end, separated with dot notation. E.g.:

`Hello Ada, your score is [[players.Ada.score]]`

!!! info
    Names that are used in this way must not contain spaces.

### Inline Query

The database type that feeds adaptor:ex behind the scenes allows us to use _find queries_ to refer to items. _find queries_ can be very complex and impressive. Most of the time, however, we only need very simple, easy-to-understand queries to get the right data.

Mark inline queries by enclosing them in curly brackets. Usually, we use a _key_ and a _value_ to make a query to the database.

`[[players.{name:'Ada'}.score]]` returns the same item and value as the variable above, which works with the `name` property. But we can do more with queries.

For example, we can also use any other properties that an item has acquired throughout the game to identify it.

`[[players.{location:'At the pub'}.score]]` gives us the `score` of the `players` collection who is `At the pub`  at the moment. Note that as long as the _value is a_ _string_, it must be enclosed in quotes.

Queries offer many other ways of finding items. If you want to learn more about it, take a look at the documentation of [mongo DB](https://www.mongodb.com/docs/drivers/node/current/fundamentals/crud/query-document/) - the database type that adaptor:ex works with.

### Referencing items

With the data action [Get Item](../basics/actions/data/getItem.md) you can link items to a variable name during the level.

Using a query (see above) or the item's `name` property, you can address it and give it a name that will be valid throughout the level. You can then use the item's properties via dot notation. For example: `[[MyItem.some_value]]`

The following [Get Item](../basics/actions/data/getItem.md) action:

![Get Item action with assignment of an item from the locations collection](./assets/variables_reference_get_item.png)

Will create a reference `CurrentLocation` to e.g. the following item from the *locations* collection:

![locations collection with item "Bahnhof Prückwitz"](./assets/variables_reference_collection.png)

All following actions in this level can then access the item with `CurrentLocation`:

![Log action with CurrentLocation call](./assets/variables_reference_log.png)

Also some other actions (e.g. [Add Item](./actions/data/addItem.md)) allow to directly reference items created by the action. The item is created independently of the level session and is still present at the end of the session, but it can be read and modified directly in the level with the specified reference name.

### Items as level arguments

If you pass items as arguments on to another level, you can access the item via the argument name in the other level.

There are three ways to specify which items are to be forwarded as arguments.

1. To define item references in the **level config**, add an argument and enter the appropriate collection under `TYPE`.

![Screenshot](./assets/variables_reference_argument_add.png)

Enter a query or the `name` of an item for `DEFAULT VALUE` if an item is to be passed at session start even if none is forwarded.

2. If you **manually** start a new session in live mode, you can pass an item from the corresponding collection (here `teams`) via the items `name` or using a query. To do this, you must have previously defined the corresponding argument in the [Level Config](#edit-arguments).

![Screenshot](./assets/variables_reference_arguments_set.png)

3. The control action **[Launch Session](../basics/actions/control/launch.md)** also allows you to forward items as level arguments using the `arguments` option.

To forward an **existing** item of the current level, enter the reference name under `value` in square brackets (here: `[[Player]]`).

To create a **new** reference at session start, enter a suitable query or the item `name` under `value` (here `{ status: "pending"}`). Note that in this case the collection in which the item is to be found must be specified in the level config under arguments `TYPE`.

!!! action inline end "Launch Session"
    
    ``` json
    {
        "level": "intro",
        "arguments": [
            {
                "name": "Player",
                "value": "[[Player]]"
            },
            {
                "name": "Team",
                "value": "{ status: \"pending\" }"
            }
        ]
    }
    ```
![Screenshot](./assets/variables_reference_arguments_launch.png){ style=width:60% }

You can then use the name of the argument to access the item inside the remote level. For example, if you want to access the `Player` *phone_number* property, use dot notation, just like with [name](#name-reference) and [query](#inline-query) references.

`[[Player.phone_number]]`

Depending on which `players` item was forwarded at level session start, `[[Player.phone_number]]` refers to the *phone_number* property of the respective player.

As with other [item references](#items-referencing), we can as well edit or delete the forwarded item.

It is also possible to forward item arguments that refer to [multiple items](#multiple-items-reference) within one collection.

!!! note
    Plugins such as [telegram](./plugins/telegram.md) and [twilio](./plugins/twilio.md) automatically forward an item as a level argument if there are incoming messages from contacts for which no level is currently active. If such a *default level* is started, the system checks whether an item with corresponding contact data (e.g. a telephone number) exists in the `players` collection in order to pass it on to the level session as the `Player` argument. If no suitable item exists, a new one is created and then forwarded to the level session as `Player`.

### Referencing multiple items

It is possible to refer to several items at the same time under one reference name. This can be the case if you formulate an [Inline Query](#inline-query) or the *find query* in [Get Item](../basics/actions/data/getItem.md) in such a way that it matches several items in a collection.

The query `players.{score:3}` refers to **all** items in the players collection **that have a score of 3**. This can be none, 1 or several items.

The query `players.{}` even refers to **all** items that are in the players collection.

If you use a query that refers to several items, for example in the [Set](../basics/actions/data/set.md) action, the respective variables in all items that match the query are changed or created.

With the settings option **multiple items** all DATA actions allow you to specify whether all items or only 1 item should be effected by the action.

Some actions always use only one item, even if several items are referenced. This is the case, for example, when a variable of an item is used inside a text.

E.G.: Even if `players.{score:3}` refers to several items in the players collection, `[[players.{score:3}.name]]` will only output one `name`.

If you want to make sure what value will be used, try to formulate the corresponding query more precisely or take a look at the [$sort, $limit and $skip](#sort-and-narrow-down-item-references) operators.

### Sort and narrow down item references

In some cases the order in which items are referenced or returned might be important. Use `$sort` to determine within a [query](#inline-query) how the found items should be sorted.

Specify the parameter sorting is based on within an object, together with the direction, `1` for ascending and `-1` for descending. For example, `$sort:{the_value:1}` to sort in ascending order according to `the_value`.

`[[teams.{$sort:{score:1}}.name]]` for example results in the `name` of the team with the highest `score` value.

If no sorting is specified, data items are arranged in the order in which they were added to the collection.

![Screenshot](./assets/variables_sort_collection.png "Goto GAMES > settings > collections and open one of the existing collections or create a new one.")

With the `$sort` query we can adjust the order for further use. `[[teams.{$sort:{score:1}}.name]]` would then result in `green` in the teams list above.

| name         | level   | score |
|--------------|:-------:|:-----:|
| ***green***  | ***3***     | ***12***  |
| red          | 1       | 8     |
| blue         | 2       | 6     |
| yellow       | 3       | 5     |
| purple       | 1       | 3     |

You can use more than one parameter to determine the order of items.

`[[teams.{$sort:{level:-1, name:1}}.name]]` will results in the `name` of the team with the lowest `level`. If there are several teams with the same level value, they are sorted by `name`. (here: `purple`)

| name         | level   | score |
|--------------|:-------:|:-----:|
| ***purple***     | ***1***     | ***3***   |
| red          | 1       | 8     |
| blue         | 2       | 6     |
| green        | 3       | 12    |
| yellow       | 3       | 5     |

!!! note
    In the [Get Item query](./actions/data/getItem.md#query) or with [Iterate](./actions/logic/iterate.md#with) you can of course use `$sort` to access the complete, then sorted list of referenced items.

In addition to `$sort` you can use the operators `$skip` and `$limit` to determine more precisely which items are returned within the query selection.

With `$skip` you can determine how many items will be left out up front. `skip: 2` in example will make the query only return or reference the 3rd item that corresponds to the query or rather the items from the 3rd onwards.

In our example case `[[teams.{$skip:2}.name]]` will result in `green`, being the first items name that is not skipped.

| name         | level   | score |
|--------------|:-------:|:-----:|
| blue         | 2       | 6     |
| red          | 1       | 8     |
| ***green***        | ***3***       | ***12***    |
| ***purple***       | ***1***       | ***3***     |
| ***yellow***       | ***3***       | ***5***     |

With `$limit` you can set a maximum number of items that will be returned or referenced.

For example, `{$limit:3}` will result in a maximum of 3 items being referenced or returned.


| name         | level   | score |
|--------------|:-------:|:-----:|
| ***blue***         | ***2***       | ***6***     |
| ***red***          | ***1***       | ***8***     |
| ***green***        | ***3***       | ***12***    |
| purple       | 1       | 3     |
| yellow       | 3       | 5     |

`$limit` obviously has no effect if you only access the value of a single item or have set [multiple](#multiple-items-reference) to `false`.

!!! note
    The `$sort`, `$limit` and `$skip` query operators follow the syntax of the corresponding mongodb cursor methods [sort()](https://www.mongodb.com/docs/manual/reference/method/cursor.sort/#mongodb-method-cursor.sort), [limit()](https://www.mongodb.com/docs/manual/reference/method/cursor.limit/) and [skip()](https://www.mongodb.com/docs/manual/reference/method/cursor.skip/).

You can of course also, in addition to  other query parameters, use all 3 operators within one query .

For example, use

``` json
{score: {$gt:3}, $sort:{level:1}, $skip:1, $limit:2}
```

to limit the query to teams that have a score > 3 and only get the teams 2 and 3 from the teams with the highest level value.

| name         | level   | score |
|--------------|:-------:|:-----:|
| green        | 3       | 12    |
| ***yellow***       | ***3***       | ***5***     |
| ***blue***         | ***2***       | ***6***     |
|  purple      |  1      |  3    |
| red          | 1       | 8     |

Action Data
------------

Some actions save data that occurs during the game and thus make it possible to access it throughout the level.

Such action data can be read using the keyword `state`, the state name in which the action is located, and the name of the action:

`[[state.MyState.onSms_1.text]]` contains the text of this SMS as soon as the state `MyState` has been triggered and an SMS has been received.

The `VARIABLES` menu in the sidebar shows you which of your actions in the course of the level can contain data and how to address them.

![Screenshot](./assets/variables_action_data_toolbar.png)

From there you can simply drag them into the action in which you want to use them as variables.

![Screenshot](./assets/variables_action_data_toolbar_drop.png)

In [Live mode](./live.md) you can view action data that has been stored by an action by clicking on the inbox :material-inbox-outline:{.is-live} icon.

![Screenshot](./assets/variables_action_data_live.png)

Functions
----------

It is also possible to use the range of [functions](./functions.md), which can be easily expanded, to add text variably, to check conditions or to change items.

A function variable is replaced with the respective _return value_ of the function when the corresponding action is executed.

`It is [[functions.getTime()]] now` results in `It is 16:00 now`.

You can find all available functions in the VARIABLES TOOLBAR under FUNCTIONS.

In the [Javascript functions](functions.md) chapter you will learn how to use functions and how to create and add functions to your adaptor:ex game.

## Missing and empty variables

If a variable does not result in a value or item, it is **undefined**. This can be the case because the referenced property or item does not (yet) exist, or because the variable contains a (typo) error.

In a text that contains an empty or missing variable, **undefined** is also added at this point. Like:  `Hello [[Player.name]]!`,  if no player exists or has `Player` no `name`property, to `Hello undefined!`
