Variablen, Daten, Referenzen
============================

Variablen erlauben es dir, veränderbare Werte in deinen actions einzusetzen. Variablen sind Platzhalter für Texte, Zahlenwerte oder komplexere Daten, die erst in dem Moment einen festen Wert bekommen, in dem die action ausgeführt wird.

Variablen werden in adaptor:ex durch zwei umschließende eckige Klammern gekennzeichnet. Das sieht dann z.B. so aus:

`[[Player.name]]`

In dem Moment, in dem wir unser Level designen, ist noch offen, welche Spieler:in (`Player`) das Level später spielen wird - und somit ist auch der Name (`name`) noch *variabel*.

Wird dann eine Session des Levels im Live Modus gestartet und die action ausgeführt, wird `[[Player.name]]` durch den Namen derjenigen Spieler:in ersetzt, die das Level gerade spielt.

In dieser Twilio [Send SMS](./actions/twilio/sendSms.md) action nutzen wir eine Variable, um die Ansprache zu personalisieren:

![Beispiel action twilio send sms](assets/smsout_example.png)

Wird das Level etwa von einer Spieler:in gespielt, für die an einer anderen Stelle der Name 'Ada Lovelace' gespeichert wurde, sieht die sms dann so aus: `Hello Ada Lovelace! How is it going?`

Variablen können in fast allen actions eingesetzt werden. Du kannst eine variable aus dem VARIABLES-Menü der [TOOLBAR](./editor.md#toolbar) in deine action ziehen oder sie selbst eintippen.

In der VARIABLES TOOLBAR findest du die Variablen, die adaptor:ex aus den bestehenden Daten gesammelt hat. Es kann aber auch Variablen geben die dort (noch) nicht aufgelistet sind und die Variablen in der TOOLBAR könnten auch in dem Moment wo sie aufgerufen werden ins Leere verweisen. 

Variablen, insbesondere [Items](#item-container-und-level-arguments), können andere Variablen enthalten. Deshalb ist die TOOLBAR ähnlich einem Dateiverzeichnis aufgebaut. Wenn du eine Variable herüberziehst wird der Pfad, der zur eigentlichen Variable führt ggf. ergänzt.

![Eine Variable aus der TOOLBAR in die Send Message action ziehen](./assets/variables_toolbar_drop.png)

Es gibt verschiedene Orte in adaptor:ex, an denen variable Daten gespeichert werden, und verschiedene Wege, diese Daten zu *referenzieren*, also mit Variablen auf diese Daten zu verweisen. Im folgenden findest du heraus wie du variable Daten erstellen, verändern und einsetzen kannst.

Lokale Variablen
----------------

Lokale Variablen sind Daten, die nur für den Verlauf einer Level Session Bestand haben.

Mit der Data action [Set](../basics/actions/data/set.md) etwa kannst du eine lokale Variable erstellen und ihr einen Wert zuweisen, oder eine bereits existierende verändern.

So kannst du z.B. an einer Stelle des Levels die Information speichern, ob eine Spieler:in den linken oder den rechten Weg eingeschlagen hat, um am Ende des Spiels wieder darauf einzugehen.

![set local varible](assets/set_local_variable.png)

Später kannst du diese variable dann z.B. in einem Text verwenden oder in der logic action [Switch](../basics/actions/logic/switch.md) nutzen, um zu bestimmen, mit welchem state es weiter geht.

Level Attributes
----------------

In der `level -> config` kannst du `Attributes` für dein Level angeben. `Attributes` ermöglichen es Eigenschaften für dein Level festzulegen.

![Level Config Attributes](./assets/variables_level_attributes.png)

Adressiere `Attributes` im entsprechenden Level indem du der variable `level` voranstellst, z.B.:

`[[level.reward]]`

Jede Live Session des Levels greift hier auf die selbe variable zu. Wenn also ein Level Attribut in einer Session geändert wird, wird Sie für alle Sessions geändert.

Level `Attribute` sind gut geeignet um in der [Launch Session](./actions/control/launch.md) darauf zurückzugreifen. So kannst du das level, dass gestartet werden soll auf Grund seiner Attribute bestimmen.

Level Arguments
----------------

Wenn eine session von einem level gestartet wird, ist es möglich der Session Daten bzw. Werte als Argumente (*arguments*) mitzugeben. Die übergebenen Werte sind dann über Variablen in der neu gestarteten Session zugänglich.

Argumente können in der `level config` bearbeitet, entfernt und hinzugefügt werden. Hier können wir auch Standard Werte (*default values*) angeben, die in der level session eingesetzt werden, wenn beim Start der level session kein anderer Wert für dieses Argument angegeben wurde. Außerdem können wir bestimmen um welche Art Daten es sich bei dem jeweiligen Argument handeln soll.

Du kannst auch Collection Items als Level Argument übergeben. Unter [Items als Level Arguments](#items-als-level-arguments) findest du mehr dazu heraus.

### Arguments bearbeiten

Um Level Arguments zu anzugeben und zu bearbeiten öffne die Level Config über das Menü `LEVEL > config`.

![Screenshot](./assets/variables_level_arguments_config.png)

Einige Plugins übergeben beim automatischen starten einer Session das `Player` Argument. Deshalb hat jedes level zu Beginn bereits ein `Player` argument, dass in der config definiert ist.

![Screenshot](assets/variables_level_arguments_default.png)

Mit `ADD NEW` kannst du weitere Argumente hinzufügen. Gib dem Argument anschließend einen Namen (`NAME`) unter dem du es dann beim editieren des Levels verwenden kannst.

![Screenshot](./assets/variables_level_arguments_example.png)

Du kannst optional auch bereits festlegen von welchem Datentyp (`TYPE`) das Level Argument ist. Gib entweder einen Datentypen `string`, `number`, `array` oder `object` oder den Namen einer deiner collections an.

Wenn du einen Standard Wert (`DEFAULT VALUE`) angibst, wird jede Session des Levels bei der beim Start kein Wert für dieses Argument übergeben wird, der hier angegebene verwendet.

### Arguments übergeben

Welche Argumente einer neu gestarteten Session übergeben werden, wird beim Start der Session festgelegt. Wenn du eine Session manuell im Live Modus erstellst kannst du die Werte für die Argumente vor dem Start angeben.

Wechsle in den Live Modus um eine neue Session zu starten. Gib anschließend einen Wert für das Level Argument an oder ändere den Standard Wert.

![Screenshot](./assets/variables_level_arguments_forward.png)

Auch die control action [Launch Session](../basics/actions/control/launch.md) erlaubt es über die `arguments` Option, level Argumente anzugeben.

![Screenshot](./assets/variables_level_arguments_launch.png){ style=width:300px }

Füge die Arguments Option über `Settings` hinzu und gebe alle Argumente an, die beim Start der Session übergeben werden sollen.

!!! action inline end "Launch Session"
    
    ``` json
    {
        "level": "first_steps",
        "arguments": [
            {
                "name": "Season",
                "value": "Summer"
            }
        ]
    }
    ```
![](./assets/variables_level_arguments_launch_add.png){ style=width:60% }

Statt eines konkreten Wertes kannst du für `value` auch eine Variable angeben.

!!! note
    Beim Start einer Level Session mit der [Launch Session](../basics/actions/control/launch.md) action können auch Argumente übergeben werden, die nicht in der `level config` definiert wurden. Sie sind anschließend über den Namen, der in der Action angegeben wurde in der neuen Level Session verfügbar.

### Auf Arguments zugreifen

Über den Namen können wir Argumente im Level als variablen verwenden. Setze dafür den Argument Namen zwischen zwei eckige Klammern `[[ArgName]]`

Argumente, die du in der Level Config angegeben hast findest du auch in der VARIABLES TOOLBAR.

![Screenshot](./assets/variables_level_arguments_toolbar.png)

Du kannst sie so in eine Action ziehen oder eigenhändig ausformulieren. 

![Screenshot](./assets/variables_level_arguments_drop.png)

Nutze Punkt Notation um auf Eigenschaften innerhalb des Arguments zuzugreifen, wenn es sich um ein Objekt, Array oder eine Item Referenz handelt.

!!! note
    Wenn du mit [Set variable](../basics/actions/data/getItem.md) oder einer anderen action eine Variable erstellst, die den selben Namen hat wie eines der Level Argumente, wird das Argument mit dem neuen Wert oder der neuen Referenz überschrieben.

Collections und Data Items
--------------------------

Jedes Data Item ist ein Datensatz, der verschiedene Eigenschaften haben kann und einer Sammlung von Items (*collection*) zugeordnet ist. 

!!! note
    Ein guter Einstieg in die Arbeit mit collections und Items ist das Tutorial zum [erstellen und verwenden eines Synonymwörterbuchs](../tutorials/messenger-thesaurus/index.md).

Unter `Game -> settings -> collections` findest du die Data Items, die derzeit in deinem game existieren. Hier kannst du auch neue Items hinzufügen, die Eigenschaften von bestehenden Items ändern oder eine ganz neue collection erstellen.

![Die Collections Übersicht](./assets/variables_settings_collections.png)

Mit der [Add Item](./actions/data/addItem.md) action kannst du im Laufe eines Levels neue Items in einer deiner Collections erstellen. 

Data Items können in jedem Level verwendet werden und bleiben bestehen, auch wenn die session eines Levels beendet wurde. Das ist z.B. wichtig, wenn dein game von verschiedenen Spieler:innen gespielt wird, die im Laufe des Spiels unterschiedliche Fortschritte machen, Punkte sammeln, ihren Namen anpassen und so fort.

Plugins wie [telegram](./plugins/telegram.md) und [twilio](./plugins/twilio.md) erstellen automatisch neue Items in der `players` collection, z.B. wenn ein game von einer bisher unbekannten Telefonnummer kontaktiert wird. Deshalb hat jedes adaptor:ex game bereits von Beginn an eine `players` collection.

Es gibt verschiedene Möglichkeiten, Data Items in einem Level einzusetzen.

### Name Reference

Data Items, die eine `name` Eigenschaft haben, die bereits bekannt ist, können darüber direkt verwendet werden. 

`[[players.Ada]]`

ist ein Verweis auf das Data Item mit der `name` Eigenschaft `Ada` in der `players` collection.

Um eine Eigenschaft des Data Item zu nutzen, fügst du sie mit Punktnotation getrennt hinten an. Etwa:

`Hello Ada, your score is [[players.Ada.score]]`

!!! info
    Namen, die auf diese Weise verwendet werden, dürfen keine Leerzeichen enthalten.

### Inline Query

Der Datenbank-Typ, der adaptor:ex im Hintergrund mit Daten versorgt, erlaubt es, dass wir so genannte *find querys* verwenden können, um auf items zu verweisen. *find queries* können sehr komplex und beeindruckend sein. Meist benötigen wir aber nur sehr einfache, leicht verständliche querys, um an die richtigen Daten zu kommen.

Markiere inline querys, indem du sie mit geschweiften Klammern umrandest. Für gewöhnlich verwenden wir einen *key* und einen *value*, um eine Anfrage an die Datenbank zu stellen.

`[[players.{name:'Ada'}.score]]` liefert das gleiche Item und den gleichen Wert wie die obige variable, die mit der `name` Eigenschaft funktioniert. Wir können mit queries aber mehr tun.

Zum Beispiel können wir auch alle anderen Eigenschaften, die ein item im Laufe des game erhalten hat, nutzen, um es zu identifizieren.

`[[players.{location:'At the pub'}.score]]` gibt uns den `score` des `players`, der oder die sich gerade `At the pub` aufhält. Beachte, dass der *value*, solange es sich um einen *string* handelt, in Anführungszeichen gesetzt werden muss.

Queries bieten viele weitere Möglichkeiten, items zu finden. Wenn du mehr darüber erfahren möchtest, schau dich einmal in der Dokumentation von [mongo DB](https://www.mongodb.com/docs/drivers/node/current/fundamentals/crud/query-document/) um - der Datenbank typ, mit dem adaptor:ex arbeitet.

### Items Referenzieren

Mit der data action [Get Item](../basics/actions/data/getItem.md) kannst du im Laufe des Levels items mit einem variablen Namen verknüpfen.

Über eine Query (siehe oben) oder die `name` Eigenschaft des Items, kannst du es adressieren und ihm einen Namen geben, der im Verlauf des Level Gültigkeit hat. Du kannst dann die Eigenschaften des Items per Punkt Notation nutzen. Etwa: `[[MyItem.some_value]]`

Die folgende [Get Item](../basics/actions/data/getItem.md) action:

![Get Item action mit Zuweisung Eines Items aus der locations collection](./assets/variables_reference_get_item.png)

Wird eine Referenz `CurrentLocation` auf z.B. das folgende Item aus der *locations* collection erstellen:

![locations collection mit Eintrag "Bahnhof Prückwitz"](./assets/variables_reference_collection.png)

Alle folgenden actions in diesem Level können dann mit `CurrentLocation` auf das Item zugreifen:

![Log action mit CurrentLocation Aufruf](./assets/variables_reference_log.png)

Auch einige andere actions (Z.B. [Add Item](./actions/data/addItem.md)) erlauben es, items, die durch die action erstellt werden, direkt zu referenzieren. Das item wird unabhängig von der level session erstellt und ist auch zum Session-Ende noch vorhanden, es kann aber im level direkt mit dem angegebenen Referenznamen gelesen und geändert werden.

### Items als Level Arguments

Wenn du Items als Argument an ein Level übergibst, kannst du über den Argument Namen auf das Item Zugreifen.

Du kannst in der Level Config, beim manuellen Starten einer Session und mit der [Launch Session](../basics/actions/control/launch.md) festlegen welche Items als Argumente übergeben werden sollen.

Um Item Referenzen in der Level Config anzugeben füge ein Argument hinzu und gib unter `TYPE` die passende collection an.

![Screenshot](./assets/variables_reference_argument_add.png)

Gib für `DEFAULT VALUE` eine query oder den `name` eines Item an wenn in jedem Fall ein Item bei Session Start übergeben werden soll.

Wenn du eine neue Session im Live Modus manuell startest, kannst du ein Item aus der entsprechenden collection (hier `teams`) über den Item `name` oder eine query übergeben. Dafür musst du das entsprechende Argument zuvor in der [Level Config](#arguments-bearbeiten) angelegt haben.

![Screenshot](./assets/variables_reference_arguments_set.png)

Auch die control action [Launch Session](../basics/actions/control/launch.md) erlaubt es über die `arguments` option, Items als level Argumente anzugeben.

Um eine existierende Item Referenzen des aktuellen Levels als Argument  weiterzugeben, gib den Referenz Namen unter `value` in eckigen Klammern an (hier: `[[Player]]`).

Um eine neue Referenz bei Session Start zu erstellen gib eine passende query oder den Item `name` unter `value` an (hier `{ status: "pending"}`). Beachte, dass in diesem Fall die collection, in der das Item zu finden ist, in der Level Config in den Arguments unter `TYPE` angegeben sein muss.

!!! action inline end "Launch Session"
    
    ``` json
    {
        "level": "intro",
        "arguments": [
            {
                "name": "Player",
                "value": "[[Player]]"
            },
            {
                "name": "Team",
                "value": "{ status: \"pending\" }"
            }
        ]
    }
    ```
![Screenshot](./assets/variables_reference_arguments_launch.png){ style=width:60% }

Mit dem Namen des Arguments können wir im Level auf das Item zugreifen. Wollen wir etwa die *phone_number*-Eigenschaft des `Player` Arguments nutzen, verwenden wir, wie bei [name](#name-reference)- und [query](#inline-query)-Referenzen, Punktnotation.

`[[Player.phone_number]]`

Je nachdem, welches `players` item beim level session start übergeben wurde, bezieht sich `[[Player.phone_number]]` auf die Eigenschaft *phone_number* des jeweiligen players.

Wie auch bei anderen [Item Referenzen](#items-referenzieren) können wir das Item ebenso bearbeiten oder löschen. 

Es ist auch möglich Item Referenzen als Argumente zu übergeben, die auf [mehrere Items](#mehrere-items-referenzieren) innerhalb einer Collection verweisen.

!!! note
    Plugins wie [telegram](./plugins/telegram.md) und [twilio](./plugins/twilio.md) übergeben automatisch ein Item als Level-Argument, wenn es eingehende Nachrichten von Kontakten gibt, für die bisher kein level aktiv ist. Wird so ein *default level* gestartet, wird überprüft, ob in der `players` collection ein item mit entsprechenden Kontaktdaten (z.B. einer Telefonnummer) existiert, um dieses item an die level session als `Player` argument weiterzugeben. Existiert kein passendes item, wird ein neues erstellt und dann an die level session als `Player` Argument weitergegeben.

### Mehrere Items referenzieren

Es ist möglich mit einem Referenz Namen auf mehrere Items gleichzeitig zu verweisen. Das kann der Fall sein wenn du etwa eine [Inline Query](#inline-query) oder die *find query* in [Get Item](../basics/actions/data/getItem.md) so formulierst, dass sie mehreren Items in einer collection zugeordnet werden kann.

Die query `players.{score:3}` verweist auf **alle** Items in der players collection, **die einen score von 3 haben**. Das können keine, 1 oder mehrere Items sein.

Die query `players.{}` verweist sogar auf **alle** Items die sich in der players collection befinden.

Nutzt du eine query, die auf mehrere Items verweist etwa in der [Set](../basics/actions/data/set.md) action, werden die variablen in allen Items auf die die query verweist geändert oder erstellt.

Alle DATA actions erlauben es dir mit der Settings option **multiple items**, anzugeben ob jeweils alle Items oder nur 1 Item angepasst, gelöscht oder geladen werden soll.

Einige Actions verwenden immer nur 1 Item, auch wenn mehrere items referenziert werden. Das ist etwa der Fall, wenn eine variable eines Items z.B. in einem Text eingesetzt wird.

Z.B.: Auch wenn `players.{score:3}` auf mehrere Items in der players Kollektion verweist, wird `[[players.{score:3}.name]]` nur einen `name` ausgeben.

Wenn du sicherstellen willst welche variable verwendet wird versuche die entsprechende query genauer zu formulieren oder nutze die Optionen [$sort, $limit und $skip](#item-referenzen-sortieren-und-eingrenzen)

### Item Referenzen sortieren und eingrenzen

In einigen Fällen kann es wichtig sein, in welcher Reihenfolge Items referenziert sind oder verwendet werden. Nutze `$sort` um innerhalb einer [query](#inline-query) zu bestimmen wie die gefundenen Items sortiert werden sollen.

Gib den Parameter, auf dessen Basis sortiert werden soll innerhalb eines Objekts, zusammen mit der Richtung, `1` für aufsteigend und `-1` für absteigend, an. Also etwa `$sort:{the_value:1}` um die Reihenfolge aufsteigend nach `the_value` festzulegen.

`[[teams.{$sort:{score:1}}.name]]` ergibt z.B. den `name` des teams mit dem höchsten `score` Wert.

Ohne Angabe einer Sortierung sind Data Items in der Reihenfolge, in der sie der collection hinzugefügt wurden angeordnet.

![Screenshot](./assets/variables_sort_collection.png "Goto GAMES > settings > collections and open one of the existing collections or create a new one.")

Mit der `$sort` query können wir diese Anordnung anpassen. `[[teams.{$sort:{score:1}}.name]]` würde in der obigen teams liste dann `green` ergeben.

| name         | level   | score |
|--------------|:-------:|:-----:|
| ***green***  | ***3***     | ***12***  |
| red          | 1       | 8     |
| blue         | 2       | 6     |
| yellow       | 3       | 5     |
| purple       | 1       | 3     |

Du kannst die Reihenfolge der Items auf mehreren Parametern basierend bestimmen. Die Sortierung erfolgt dabei vorrangig nach den vorderen Parametern im Objekt.

`[[teams.{$sort:{level:-1, name:1}}.name]]` ergibt z.B. den `name` des teams im niedrigsten `level`. Gibt es mehrere teams mit dem selben level Wert, wird nach `name` sortiert. (hier: `purple`)

| name         | level   | score |
|--------------|:-------:|:-----:|
| ***purple***     | ***1***     | ***3***   |
| red          | 1       | 8     |
| blue         | 2       | 6     |
| green        | 3       | 12    |
| yellow       | 3       | 5     |

!!! note
    Du kannst `$sort` z.B. auch  in der [Get Item query](./actions/data/getItem.md#query) oder mit [Iterate](./actions/logic/iterate.md#with) nutzen und auf die vollständige, dann sortierte Liste Referenzierter Items zugreifen.

Neben `$sort` kannst du die Operatoren `$skip` und `$limit` nutzen um genauer zu bestimmen welche Items innerhalb der query Auswahl zurückgegeben werden.

Mit `$skip` kannst du festlegen, wie viele Items bei der query Abfrage übersprungen werden. Verwende z.B. `skip: 2` um erst das 3. Item, dass der Query entspricht bzw. alle Items ab dem 3. zurückzugeben oder zu referenzieren.

`[[teams.{$skip:2}.name]]` wird in unserem Beispiel `green` ergeben.

| name         | level   | score |
|--------------|:-------:|:-----:|
| blue         | 2       | 6     |
| red          | 1       | 8     |
| ***green***        | ***3***       | ***12***    |
| ***purple***       | ***1***       | ***3***     |
| ***yellow***       | ***3***       | ***5***     |

Mit `$limit` beschränkst du die Anzahl der Items die zurückgegeben bzw. referenziert werden. In einer [Get Item query](./actions/data/getItem.md#query) oder mit [Iterate](./actions/logic/iterate.md#with) etwa kannst du `$limit` verwenden um die Anzahl der Items zu beschränken.

`{$limit:3}` wird z.B. dazu führen, dass höchstens 3 Items referenziert oder zurückgegeben werden.


| name         | level   | score |
|--------------|:-------:|:-----:|
| ***blue***         | ***2***       | ***6***     |
| ***red***          | ***1***       | ***8***     |
| ***green***        | ***3***       | ***12***    |
| purple       | 1       | 3     |
| yellow       | 3       | 5     |

Solange du nur das vorderste der zurückgegebenen Items verwenden kannst oder verwenden willst, z.B. weil du [multiple](#mehrere-items-referenzieren) auf `false` gesetzt hast, hat `$limit` keinen Einfluss auf deine Auswahl. 

!!! note
    Die `$sort`, `$limit` und `$skip` query Operatoren folgen der Syntax der entsprechenden mongodb cursor Methoden [sort()](https://www.mongodb.com/docs/manual/reference/method/cursor.sort/#mongodb-method-cursor.sort), [limit()](https://www.mongodb.com/docs/manual/reference/method/cursor.limit/) und [skip()](https://www.mongodb.com/docs/manual/reference/method/cursor.skip/).

Du kannst natürlich alle 3 Operatoren auch innerhalb einer query zusätzlich zu den anderen query Parametern verwenden.

Verwende z.B. 

``` json
{score: {$gt:3}, $sort:{level:1}, $skip:1, $limit:2}
```

um die Abfrage auf die Teams zu beschränken, die einen score > 3 haben, und nur die teams 2 und 3 von den teams mit höchstem level zu bekommen.

| name         | level   | score |
|--------------|:-------:|:-----:|
| green        | 3       | 12    |
| ***yellow***       | ***3***       | ***5***     |
| ***blue***         | ***2***       | ***6***     |
|  purple      |  1      |  3    |
| red          | 1       | 8     |


Action Data
------------

Einige actions speichern Daten ab, die während des Spiels anfallen, und ermöglichen es so, im Laufe des Levels darauf zuzugreifen.

Solche Action Daten sind über das keyword `state`, den State-Namen, in dem die action sich befindet, und den Namen der action lesbar:

`[[state.MyState.onSms_1.text]]` enthält, sobald der state `MyState` getriggert wurde und eine SMS eingegangen ist, den Text dieser SMS.

Das `VARIABLES`-Menü in der Sidebar zeigt dir an, welche deiner actions im Laufe des Levels Daten enthalten können und wie sie adressiert werden.

![Screenshot](./assets/variables_action_data_toolbar.png)

Von dort kannst du sie einfach in die Action in der du sie als variable verwenden willst ziehen.

![Screenshot](./assets/variables_action_data_toolbar_drop.png)

Im [Live Modus](./live.md) kannst du Action Daten, die in einer action angefallen sind mit Klick auf den inbox :material-inbox-outline:{.is-live} icon einsehen.

![Screenshot](./assets/variables_action_data_live.png)

Functions
----------

Es ist auch möglich die Palette der [functions](./functions.md), die sich einfach erweitern lässt, zu nutzen, um Texte variabel zu ergänzen, Bedingungen zu überprüfen oder items zu verändern.

Eine function variable wird, wenn die entsprechende action ausgeführt wird, mit dem jeweiligen Rückgabewert (*return value*) der Funktion ersetzt.

`Es ist jetzt [[functions.getTime()]] Uhr` ergibt also z.B. `Es ist jetzt 16:00 Uhr`.

Du findest alle zur Verfügung stehenden functions in der VARIABLES TOOLBAR unter FUNCTIONS.

Im Kapitel [Javascript Funktionen](./functions.md) erfährst du wie du Funktionen einsetzt und wie du eigene Funktionen erstellen und zu deinem adaptor:ex Game hinzufügen kannst.

Fehlende und leere Variablen
----------------------------

Führt eine Variable zu keinem Wert oder keinem item ist sie **undefined**. Das kann der Fall sein, weil die referenzierte Eigenschaft oder das item (noch) nicht existiert, oder weil die variable einen (Tipp-)Fehler enthält.

In einem Text, der eine leere oder fehlende Variable enthält, wird an dieser stelle auch **undefined** ergänzt. Etwa: `Hallo [[Player.name]]!` würde, wenn kein Player existiert oder `Player` keine `name` Eigenschaft hat, zu `Hallo undefined!`
