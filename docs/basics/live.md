Live Mode
==========

In Live mode you can control the live situation of your game. You can observe what is happening and intervene in the course of the game.

Click on the Live icon :material-play-circle-outline:{.is-live} in the navigation bar, the small triangle in a circle in the upper right corner. This will switch you to live mode.

![screenshot live mode toggle button](./assets/live_toggle.png)

You can leave the live mode at any time by clicking on the live icon.

In live mode you have access to the same pages as in editor mode, but you see additional information and have other interaction possibilities.

In the [Level Overview](#level-overview) you can monitor and manage the game status of your levels. You can take a closer look at the current progress of individual levels in the [Level Editor](#level-editor). Here you can also intervene in the progress. The [Dashboard](#dashboard) shows you the status of your plugin and data items.

The currently played live versions of a level are called "sessions". Read more about this in the [Session](#session) chapter.

Level Overview
--------------

Under 'GAME -> overview' in live mode, in addition to your levels, the active [sessions](#session) of the respective levels are listed. The status of each session is displayed here. You can directly go to the respective session, cancel a session (:material-trash-can-outline:) or start a new session (:material-plus-circle-outline:).

![screenshot](./assets/live_overview.png)

Dashboard
---------

Under `GAME -> dashboard` you can track which [data items](variables.md) have recently been modified and see immediately when changes take place. For example, you can keep an eye on your players and see when a new player has been added.

![Screenshot: Game Menü Dashboard](./assets/live_menu_dashboard.png)

The `LATEST` view shows the items that were last modified at the top. Items that you mark as favorites (star symbol) are `FAVORITES` collected in the view.

![Screenshot: Die Dashboard Übersicht](./assets/live_dashboard.png)

Level Editor
------------

Under `LEVEL -> edit`in the [Level Editor](editor.md) you can monitor and control the [sessions](#session) of the corresponding level. You see all sessions that are running in the open LEVEL and can select them to see which STATES are currently active in the session and change the STATE if necessary.

You can find the active sessions of a level in the right sidebar.

If a session is in progress, or even if something is broken or stuck, you will see the session there.

![screenshot](./assets/live_session_view.png)

There are two sessions from the level opened here. The upper session in the list is selected and we see that the STATE _HelpMe_ is active there.

If you cannot see a session at the moment, this is due to the fact that no session has been started yet or the last session has already ended.

**You cannot edit the level in live mode**. First switch back to the editor mode by clicking on the live button again if you want to change something in the level.

Session
-------

Each level that you created and edited in the level editor can be played many times at the same time.

These live versions of the level can start at different times and progress in different ways, be played by different players, etc.

So a **session** is a running live version of a level.

> It is not always necessary or useful to be particularly adept at handling sessions. For some projects it might be best to manually create exactly one session of each (or the only) level and not end it ever again.

### Start a session manually

To start a session manually, click on `Create Session` below the list of active sessions in the [Level Editor](#level-editor).

![Start a session](./assets/live_create_session.png)

You can give the new session a name or leave the field empty to use an automatically generated name.

Below the name entry, you can specify which [Data Items](./variables.md#collections-and-data-items) are to be handed to this session as [Level Arguments](./variables.md#item-container-and-level-arguments). You can also leave this field empty if you do not use level arguments within your level.

When the session has been created, it is displayed in the list and automatically selected.

The same way you can start Sessions manually from within the [Level overview](#level-overview) if you are in live mode.

### Triggering states in a session

If a session is selected in the sidebar, you can see which STATES are currently active in this session or which STATES were triggered last within each path. Active states are highlighted.

You can trigger a state manually by clicking on the circled arrow in the top left-hand corner of the state ![pink circle with arrow](./assets/play_icon.png).

![Level editor with arrow pointing to the state trigger button](./assets/live_trigger_state.png)

### Edit a session

You can edit the level on which the sessions are based at any time and, if necessary, also change how the current sessions continue.

Sessions always use the latest version of the level. With the following restrictions:

1) Active states within a session are not directly adjusted by level changes. For changes to an active state to take effect, it must be triggered again.

2) If you change, add or remove arguments in the level, the level arguments of running sessions don't change `Level -> config` . For changes to the level arguments to take effect, you must restart the sessions of the respective level.

### Start sessions automatically

Sessions can be started manually or automatically and also ended manually or automatically.

For example, you can specify a level for [Telegram](./plugins/telegram.md) Accounts, [Twilio](./plugins/twilio.md#default-level-angeben) Phones and [devices](./plugins/devices.md#devices) which will automatically start with a new session as soon as an unknown contact is reported.

Sessions can also be started within sessions. With the [Launch Session](./actions/control/launch.md) ACTION it is possible to specify within a level that a session, within the same level or in another level, should be started at this point.

This is a good way to automatically switch from one level to the next. But you can also use it to create a superordinate _meta_ level that, under certain conditions, starts sessions from other levels.

### Active states

Within a session there is usually 1 active _current_ STATE. This means that the session is in the status of the STATE that was last triggered. This STATE 's [RUN ACTIONS](./editor.md#run-actions) are those most recently triggered and its [LISTEN ACTIONS](./editor.md#listen-actions) are currently listening to something.

As soon as the STATE is changed, all LISTEN ACTIONS that were active in the previous STATE are terminated.

[Paths](./paths.md) are an exception to this rule. Paths make it possible for several STATES to be active within a session. In turn, only one STATE can be active in each path.

### Cancel sessions

You can end each session manually in the [Level Editor](#level-editor) as well as in the [Level Overview](#level-overview) by clicking on the trash bin icon next to the session name (:material-trash-can-outline:).

To end the current session automatically during the course of a level, use the [Quit](./actions/control/quit.md) action.

To end one or more (other) sessions at a specific point during the course of a level, use the [Cancel Session](./actions/control/cancel.md) action.