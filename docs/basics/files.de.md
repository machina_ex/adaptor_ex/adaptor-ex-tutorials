Dateien und Media Files
========================

Um Dateien in Actions zu verwenden lege sie unter `files` im jeweiligen `games` Ordner des adaptor:ex Daten Verzeichnis ab.

Wenn du adaptor:ex mit den Standarteinstellungen installiert hast, wurde dort, wo du die Anwendung ausführst ein neuer Ordner `adaptorex` erstellt. Um Dateien in deinem Game zu verwenden, kopiere oder verschiebe sie im Dateisystem nach `adaptorex/games/<game name>/files`.

Um etwa deinem Game "Playground" eine Audiodatei "ambient_starlight.wav" hinzuzufügen lege sie im entsprechenden `files` Ordner ab.

```
adaptorex
├── games
|   ├── Playground
│   |   ├── files
│   |   |   ├── ambient_starlight.wav
│   |   ├── functions
├── log
├── nedb
├── config.json
```

Du kannst auch weitere Unterordner zu `files` hinzufügen um die Dateien zu sortieren.

In der [server configuration readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#configuration) findest du heraus wie du das Daten Verzeichnis ändern kannst

Um eine Datei in einer Action zu verwenden wähle sie im entsprechenden Formular Feld mit `Select File` aus dem Dropdown aus 

![Eine Datei auswählen](./assets/files_select_file.png)

oder verwende `Drop File` um sie aus der TOOLBAR zu kopieren.

Im adaptor:ex Editor in der TOOLBAR unter `Media` sind alle Dateien aufgelistet. Ziehe die Datei, die du in einer Action verwenden willst in das entsprechende Formularfeld.

![Dateien finden und verwenden](./assets/files_drop_file.png)

Wie du siehst, wird jeweils lediglich der Dateipfad angegeben und die action sucht die Datei entsprechend. Du kannst die Dateien also auch per Hand eingeben.

Ausgangspunkt für den relativen Dateipfad ist der `files` Ordner des jeweiligen Games. Du kannst aber auch jeden absoluten Pfad verwenden, der auf eine Datei in dem Dateisystem verweist auf dem der adaptor:ex Server läuft.

Manche actions können auch Dateien über hyperlinks, also mit einer URL, aus dem Internet laden. Solche Dateiverweise müssen mit `http` oder `https` beginnen. Also zum Beispiel: `https://docs.adaptorex.org/assets/header.png`

Kopiere die URL in das entsprechende Formularfeld der Action.