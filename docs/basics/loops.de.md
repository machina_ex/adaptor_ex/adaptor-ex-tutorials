Loops und Iterationen
=====================

Es gibt unterschiedliche Möglichkeiten in adaptor:ex Abfolgen zu wiederholen. Abläufe können jederzeit im Kreis angeordnet werden und mit *Logic* Actions wie [Switch](./actions/logic/switch.md) und [Iterate](./actions/logic/iterate.md) lassen sich komplexe Schleifen realisieren. Auch andere Actions können in sich wiederholenden Abläufen eine besondere Funktion einnehmen. Dieses Kapitel soll eine Auswahl verschiedener Loop Varianten erläutern und zeigen wie gängige Programmierroutinen in adaptor:ex umgesetzt werden können.

Abläufe Wiederholen
-------------------

Eine Abfolge von States kann jederzeit wiederholt werden. Sobald ein späterer State auf einen vorherigen State verweist wird dieser und ggf. folgende States ein weiteres mal ausgelöst.

![Screenshot](./assets/loops_simple.gif)

Eine Schleife, die nicht unterbrochen wird, läuft solange bis die zugehörige Session beendet wird.

Eine Schleife kann beliebig viele States umfassen und jede Art von Action enthalten.

Mit den Time Actions [Timeout](./actions/time/timeout.md), [Date and Time](./actions/time/datetime.md) und [Schedule](./actions/time/schedule.md) lassen sich so z.B. wiederkehrende Ereignisse programmieren.

![Screenshot: adaptor:ex level das täglich um 9 Uhr ein Katzen GIF generiert durch den webservice cataas per telegram versendet](./assets/loops_schedule.png)

!!! example-file "Example Level File"
    Download: [:material-download: cat_each_day.json](./assets/cat_each_day.json)

In diesem Beispiel etwa, wird jeden Tag um 9 Uhr ein Katzen GIF durch den webservice [Cat as a service](https://cataas.com/) generiert und von einem Telegram Bot versendet.

Loops beenden
-------------

Jede Action, die einen `Next State` auslösen kann, kann auch dazu genutzt werden eine Schleife zu unterbrechen.

Das ist dann Vergleichbar mit einer While Schleife, die solange läuft, bis eine Bedingung eintrifft, die die Schleife beendet.

Ein gängiges Beispiel: Eine counter variable `count` wird mit jeder Iteration um `1` erhöht. Eine [Switch](./actions/logic/switch.md) action überprüft, ob `count` größer ist als `4`, also `5` ist und löst dann einen `Next State` aus (hier *Proceed*), der sich nicht innerhalb der Schleife befindet.

![Screenshot](./assets/loops_simple_end.png)

Also in etwa `while count < 5; count++`

Auch die [Iterate](./actions/logic/iterate.md) Action kann genutzt werden um eine Schleife wie im obigen Beispiel auf eine Anzahl Wiederholungen zu begrenzen. Eine Ganzzahl in **with** legt fest, wie viele Iterationen durchgeführt werden bis [Iterate](./actions/logic/iterate.md) übersprungen wird.

![Screenshot](./assets/loops_for_each_range.png)

Wenn sich der Loop in einem separaten [Path](./paths.md) befindet, kann die Schleife auch mit der [Join Path](./actions/control/join.md) Action von einem parallel laufenden Pfad an beliebiger Stelle beendet werden.

![Screenshot](./assets/loop_join.gif)

Loops werden natürlich auch beendet, wenn die Session beendet wird, in der der Loop läuft. Mit der [Cancel Session](./actions/control/cancel.md) Action lassen sich also auch potentielle Schleifen, die in anderen Levels bzw. Sessions laufen beenden.

For Each Loop
-------------

Die [Iterate](./actions/logic/iterate.md) Action ermöglicht es mit jeder Runde eines Loops auf einen anderen Eintrag aus einem Datensatz zuzugreifen.

So kannst du eine oder mehrere Aktionen mit dem jeweils nächsten Element aus einem Array, einem Objekt oder einer [Collection](./variables.md#collections-and-data-items) ausführen.

Du kannst die Elemente, die Iteriert werden sollen innerhalb der [Iterate](./actions/logic/iterate.md) Action angeben oder über die Elemente einer variable iterieren, die an anderer Stelle erstellt wurde.

Sobald über alle Elemente iteriert wurde, wird [Iterate](./actions/logic/iterate.md) übersprungen und wird nicht mehr einen `Next State` auslösen. 

So lässt sich auf einfache weise ein For Each Loop bauen. Also etwa:

```
for each item in collection:
  do something to item
```
*Auszug aus Wikipedia Artikel [Foreach Loop](https://en.wikipedia.org/wiki/Foreach_loop){target=_blank}*

In diesem Beispiel wird eine Auswahl an Items aus der players Collection geholt und an jeden player die selbe Telegram Nachricht gesendet. Dabei wird auch die Ansprache mit der `name` variable personalisiert und zusätzlich ein Eintrag im jeweiligen players Item geändert.

![Screenshot](./assets/loops_for_each.png)

Wurde die Aktion für jedes players Item durchgeführt, wird [Iterate](./actions/logic/iterate.md) in der folgenden Schleife übersprungen und durch die darunter stehenden [Next State](./actions/control/next.md) Action geht es mit *Done* weiter.

Listener und Loops
------------------

Ein Ereignis, dass eine Reaktion hervorruft kann jederzeit zum ursprünglichen Ereignis Listener zurückführen.

Das ist besonders praktisch um Ereignisse zu verarbeiten, die zwar eine Reaktion hervorrufen, aber nicht den Haupt Strang des Levels weiterführen.

### Subroutinen

In diesem Level Abschnitt eines Telegram Adventures wird im *Angekommen* State auf eine Nachricht "angekommen" durch den player gewartet. Um sicher zu gehen, dass die benötigte Geo location den player erreicht hat, wird diese bei jeder Antwort, die nicht explizit abgefragt wird erneut verschickt.

![Screenshot](./assets/loops_listener.png)

Die Telegram [On Incoming Message](./actions/telegram/onMessage.md) Action ermöglicht es auch innerhalb der Action auf eingehende Nachrichten zu reagieren ohne den State zu verlassen. Geo Locations können aber nicht als so eine "direkt Antwort" gesendet werden. Kommt eine unbekannte Nachricht rein, wird also der *NichtGefunden* State ausgelöst. Sobald die Geo Location mit [Send Geo Location](./actions/telegram/sendGeo.md) versendet wurde, wird in den *Angekommen* State zurück gewechselt und wieder auf eingehende Nachrichten gehört.

### Keep listening and queue

Ein Problem, dass bei dieser Art Loops auftritt ist, dass Ereignisse, die zur Unzeit eintreffen, verpasst werden können.

Sobald wir den State mit dem Listener verlassen wird dieser geschlossen und wird nicht weiter auf eingehende Nachrichten oder andere Ereignisse reagieren. Die Dauer, die der Listener nicht "zuhört" lässt sich zwar in manchen Fällen auf einen kurzen Zeitraum reduzieren, etwa mit einem separaten Pfad, der die Folge Aktionen asynchron ausführt. Es wird aber immer ein Zeitfenster entstehen, in dem wir nicht sicherstellen können ob wir alles mitbekommen haben.

[On Incoming Message](./actions/telegram/onMessage.md) und andere Listener wie [On Event](./actions/logic/onEvent.md) und [On MQTT Message](./actions/mqtt/onMQTTMessage.md) haben die Möglichkeit "Stumm" geschaltet zu werden, anstatt sie ganz zu deaktivieren. Aktivierst du die Option **keep listening and queue**, wird die listener Action weiter auf eingehende Nachrichten hören, ohne direkt darauf zu reagieren.

![Screenshot](./assets/loops_queue_option.png)

Der Listener wird dann eingehende Ereignisse bzw. Nachrichten in einer Liste einreihen. Wird der Listener dann erneut getriggert, werden zunächst die eingereihten Nachrichten auf matches überprüft.

![Screen Recording](./assets/loops_listener.gif)

!!! example-file "Example Level File"
    Download: [:material-download: listener_and_loop.json](./assets/listener_and_loop.json)

Die Nachricht `Achso, ja habe ich!` wird hier in dem kurzen Zeitfenster gesendet, indem der [On Incoming Message](./actions/telegram/onMessage.md) Listener im *Angekommen* State nicht aktiv ist. Auf die Nachricht wird aber trotzdem kurz darauf reagiert, da **keep listening and queue** aktiviert ist.

Insbesondere auch IoT Geräte, die eine Subroutine auslösen, wenn sie eine Bestimmte Nachricht schicken, etwa per [MQTT](./plugins/mqtt.md), können so zuverlässig eingebunden werden.