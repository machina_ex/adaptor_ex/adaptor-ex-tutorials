Live Modus
==========

Im Live Modus kannst du die Live Situation deines Game kontrollieren. Du kannst beobachten was gerade passiert und in den Verlauf eingreifen.

Klicke in der Navigationsleiste auf das Live-Icon :material-play-circle-outline:{.is-live}, das kleine Dreieck im Kreis, in der oberen rechten Ecke. Damit wechselst du in den Live Modus.

![screenshot live mode toggle button](./assets/live_toggle.png)

Du kannst den Live Modus mit einem Klick auf das Live-Icon jederzeit wieder verlassen.

Im Live Modus hast du Zugriff auf die selben Seiten wie im Editor Modus, siehst aber zusätzliche Informationen und hast andere Interaktionsmöglichkeiten.

In der [Level Overview](#level-overview) kannst den Spielstatus deiner Level beobachten und verwalten. Einen genauen Blick auf den aktuellen verlauf einzelner Level kannst du im [Level Editor](#level-editor) werfen. Hier kannst du auch in den Verlauf eingreifen. Das [Dashboard](#dashboard) zeigt dir den status deiner Plugin und Data Items an.

Die aktuell gespielten Live Versionen eines Levels nennen wir "Sessions". Im [Session](#session) Kapitel ist näher erläutert was es damit auf sich hat.

Level Overview
--------------

Unter `GAME -> overview` sind im Live Modus zusätzlich zu deinen Levels die aktiven [Sessions](#session) der jeweiligen Levels gelistet. Zudem wird der Status jeder Session angezeigt. Du kannst von hier direkt in die jeweilige Session wechseln, eine Session beenden (:material-trash-can-outline:) oder eine neue Session starten (:material-plus-circle-outline:)

![screenshot](./assets/live_overview.png)

Dashboard
---------

unter `GAME -> dashboard` kannst du verfolgen welche [Data Items](./variables.md#collections-und-data-items) kürzlich modifiziert wurden und siehst sofort, wenn Änderungen stattfinden. So behältst du z.B. deine player im Blick und siehst, wenn ein neuer player hinzugefügt wurde.

![Screenshot: Game Menü Dashboard](./assets/live_menu_dashboard.png)

Die `LATEST` Ansicht zeigt zu oberst die Items an, die als letztes modifiziert wurden. Items die du hier als Favoriten markierst (Stern Symbol) werden in der `FAVORITES` Ansicht gesammelt.

![Screenshot: Die Dashboard Übersicht](./assets/live_dashboard.png)

Level Editor
------------

Unter `LEVEL -> edit` im [LEVEL EDITOR](editor.md) kannst du die [Sessions](#session) des entsprechenden Levels beobachten und kontrollieren. Du siehst alle Sessions die im geöffneten LEVEL laufen und kannst sie auswählen um zu sehen welche STATES in der Session gerade aktiv sind.

Die aktiven Sessions eines levels findest du in der rechten Seitenleiste.

Wenn eine Session im Gange ist, oder auch wenn etwas kaputt gegangen ist oder feststeckt, siehst du die Session dort. 

![screenshot](./assets/live_session_view.png)

Von dem hier geöffneten Level gibt es zwei Sessions. Die obere Session in der Liste ist ausgewählt und wir sehen, dass dort der STATE *HelpMe* aktiv ist.

Wenn du gerade keine Session sehen kannst, liegt das daran, dass noch keine Session gestartet wurde oder die letzte Session bereits wieder beendet ist.

**Du kannst das Level im Live Modus nicht bearbeiten.** Wechsle zunächst zurück in den Editor Modus indem du erneut auf den Live Button klickst wenn du etwas am Level ändern möchtest.

Session
-------

Jedes Level, dass du im Level Editor erstellt und bearbeitet hast, kann viele Male gleichzeitig gespielt werden. 

Diese Live Versionen des Levels können zu unterschiedlichen Zeiten beginnen und ganz unterschiedlich verlaufen, von unterschiedlichen Spielern gespielt werden usw.

Eine **Session** ist also eine laufende live Version eines Levels.

> Es ist nicht immer notwendig oder sinnvoll besonders versiert mit Sessions umzugehen. Für manche Projekte kann es das beste sein von jedem (oder dem einzigen) Level genau eine Session Manuell zu erstellen und diese nicht wieder zu beenden.

### Eine Session manuell starten

Um manuell eine Session zu starten klicke im [Level Editor](#level-editor) auf `Create Session` Unterhalb der liste der aktiven Sessions.

![Eine Session starten](./assets/live_create_session.png)

Du kannst der neuen Session einen Namen geben oder das Feld frei lassen um einen automatisch generierten Namen zu verwenden.

Unterhalb der Namenseingabe kannst du angeben welche [Data Items](./variables.md#collections-und-data-items) an diese Session als [Level Argumente](./variables.md#item-container-und-level-arguments) übergeben werden sollen. Du kannst auch dieses Feld freilassen wenn du innerhalb deines Levels keine Level Argumente verwendest.

Wenn die Session erstellt wurde, wird sie in der Liste angezeigt und automatisch ausgewählt.

Du kannst auch in der [Level Übersicht](#level-overview) eine session von einem der Levels starten, wenn du dich im Live Modus befindest.

### States in einer Session auslösen

Wenn eine Session in der Seitenleiste ausgewählt ist siehst du welche STATES derzeit in dieser Session aktiv sind bzw. welche STATES als letztes innerhalb eines Pfades ausgelöst wurden. Aktive States sind farbig hervorgehoben.

Mit klick auf den eingekreisten Pfeil in der linken oberen Ecke des States ![pinker kreis mit pfeil](./assets/play_icon.png) kannst du einen State manuell auslösen.

![Level Editor mit Hinweis Pfeil auf den State trigger button](./assets/live_trigger_state.png)

### Eine Session bearbeiten

Du kannst das Level auf dem die Sessions jeweils basieren jederzeit bearbeiten und änderst damit ggf. auch wie es in den laufenden Sessions weitergeht.

Sessions greifen jeweils auf die aktuellste Version des Levels zurück. Mit folgenden Einschränkungen:

1) Aktive STATEs innerhalb einer Session werden durch Änderungen am Level nicht direkt angepasst. Um Änderungen an einem aktiven State wirksam werden zu lassen muss dieser erneut ausgelöst werden.

2) Wenn du in der `Level -> config` [Level Argumente](./variables.md#item-container-und-level-arguments) änderst, hinzufügst oder entfernst, ändern sich die Level Argumente laufender Sessions nicht. Um Änderungen an den Level Argumenten wirksam werden zu lassen, musst du die Sessions des jeweiligen Levels neu starten.

### Sessions automatisch starten

Sessions können manuell oder automatisch gestartet und ebenso manuell oder automatisch beendet werden.

Z.B. kannst du für [Telegram Accounts](./plugins/telegram.md), [Twilio Phones](./plugins/twilio.md#default-level-angeben) und [Devices](./plugins/devices.md#devices) level angeben von denen automatisch eine Session gestartet wird sobald sich ein unbekannter Kontakt meldet.

Sessions können auch innerhalb von Sessions gestartet werden. Mit der [Launch Session](./actions/control/launch.md) ACTION kannst du innerhalb eines Levels angeben, dass an dieser Stelle eine Session von diesem oder einem anderen Level gestartet werden soll.

Das ist eine gute Möglichkeit automatisch von einem Level ins nächste zu wechseln. Damit kannst du aber auch ein übergeordnetes *Meta* Level erstellen, dass unter bestimmten Bedingungen Sessions von anderen Levels startet.

### Aktive States

Innerhalb einer Session gibt es für gewöhnlich 1 aktiven, *current* STATE. Das heißt, die Session befindet sich in dem Status des STATE der zuletzt ausgelöst wurde. Die [RUN ACTIONS](./editor.md#run-actions) dieses STATE sind die, die zuletzt ausgelöst wurden und dessen [LISTEN ACTIONS](./editor.md#listen-actions) hören aktuell auf etwas. 

Sobald der STATE gewechselt wird werden alle LISTEN ACTIONS beendet die im vorigen STATE aktiv waren.

Eine Ausnahme von dieser Regel bilden [Pfade](./paths.md). Durch Pfade ist es Möglich das innerhalb einer Session mehrere STATES aktiv sind. In jedem Pfad kann wiederum immer nur ein STATE aktiv sein.

### Sessions beenden

Du kannst sowohl im [Level Editor](#level-editor) als auch in der [Level Übersicht](#level-overview) jede Session manuell beenden, indem du auf den Mülleimer Icon neben dem Session Namen klickst (:material-trash-can-outline:).

Um die aktuelle Session an einem bestimmten Punkt im Level Verlauf automatisch zu beenden nutze die [Quit](./actions/control/quit.md) Action.

Um eine oder mehrere (andere) Sessions an einem bestimmten Punkt im Level Verlauf zu beenden nutze die [Cancel](./actions/control/cancel.md) Action.