Pfade
=====

Pfade ermöglichen es, innerhalb eines Levels, mehrere parallel laufende Stränge von States zu erstellen.

Ausgehend vom *START* State kann eigentlich jeweils nur ein State pro [Session](./live.md#session) eines Levels aktiv sein. Das heißt nur im einen aktuellen State sind listener actions wie [On Event](./actions/logic/onEvent.md) und [On Change](./actions/logic/onChange.md) aktiv.

Wird der State gewechselt, wird der vorangehende geschlossen, egal ob der folgende State davor, dahinter, oder weit entfernt ist.

Nutze die [Split Path](./actions/control/split.md) action um listener actions von unterschiedlichen States aktiv zu halten.

*Split Path* erstellt neue Pfade die gleichzeitig, unabhängig voneinander laufen. Die getrennten Pfade können listener in unterschiedlichen States enthalten, die gleichzeitig auf etwas Warten.

Pfade die durch *Split Path* getrennt wurden sind **vor** dem State, der *Split Path* enthält weiterhin verbunden. Sind in mehreren der neu erstellten Pfade States aktiv, werden diese Beendet, sobald ein State, der **vor** der *Split Path* action liegt ausgelöst wird.

Werden getrennt Pfade wieder zusammengeführt gilt dasselbe für alle States die **hinter** der Zusammenführung liegen.

Pfade können über ein `next state` innerhalb einer action oder mit Hilfe der [Join Path](./actions/control/join.md) action zusammengeführt werden.

> Eine Visualisierung der Pfade im adaptor:ex Editor steht noch aus. Sobald es soweit ist, werden wir auch die Dokumentation ergänzen und Beispiele aus dem Editor hinzufügen. Wenn du dich jetzt schon genauer über Pfade und ihre komplexen Anwendungsmöglichkeiten informieren willst, schau einmal in unser internes [Konzept zu Pfaden](./assets/adaptorex_pfade.drawio.html)

States, die nicht mit dem START State verbunden sind laufen in einem eigenen, nicht benannten, Pfad. "Freie" States können [Split Path](./actions/control/split.md) nicht verwenden um neue Pfade zu erstellen.

Du kannst [lokale Events](./events.md#zwischen-pfaden-kommunizieren-mit-lokalen-events) nutzen um innerhalb einer Session zwischen Pfaden zu kommunizieren.