Der Editor
==========

Jedes Game besteht aus einem oder mehreren Levels die du im Level-Editor bearbeitest. Hier arrangierst du was wann passiert und wie die Elemente deines Projektes zusammenhängen. Was du im Editor programmierst, kann anschließend in einer [Session](./live.md#session) gespielt oder abgefahren werden.

Im Level-Editor gibt es zwei verschiedene Bereiche: die STAGE und die TOOLBAR.

![screenshot](assets/editor-01-1.jpg)

## TOOLBAR

Alles, was du in der TOOLBAR findest, kannst du via drag and drop direkt auf die Bühne ziehen.

![screenshot](assets/actions1-1.png){ style=width:200px align=left }

Die TOOLBAR ist in drei Tabs unterteilt: 

### ACTIONS 

ACTIONS sorgen dafür, dass in deinem Level etwas passiert. Du kannst die Palette der verfügbaren ACTIONs erweitern indem du **Plugins** unter `GAME -> settings` zu deinem Game hinzufügst.

### VARIABLES 

Hier findest du Daten Container, deren Inhalte sich im Laufe des Games ändern können. Wenn du eine Variable innerhalb einer ACTION verwenden willst ziehe sie aus der TOOLBAR per Drag'n'Drop in das entsprechende Textfeld. In der ACTION wird sie dann mit zwei eckigen Klammern umfasst `[[variable]]` und in die richtige Form gebracht, damit adaptor:ex sie als Variable identifizieren kann. 

Variablen können wiederum Variablen enthalten, ähnlich wie Dateiordner. Wenn du eine verschachtelte Variable verwendest wirst du sehen, dass die Ebenen durch Punkte `[[Player.inventory.rope]]` voneinander getrennt sind.

Sieh dir das Kapitel über [Variablen Daten und Referenzen](variables.md) an um mehr zu erfahren.

### MEDIA 

MEDIA enthält Dateien, die du in dein Game einbinden kannst. Die Dateien die du hier findest, sind in deinem adaptor:ex file Ordner hinterlegt. Wenn du eine Datei in das Formularfeld einer ACTION ziehst in dem eine Datei erwartet wird, wird dort der entsprechende Dateipfad eingetragen.

Sieh im Kapitel [Dateien und Media Files](./files.md) nach wie du deinem Game weitere Dateien hinzufügen kannst.

## STAGE

Die STAGE ist der größte Bereich im Level-Editor. 
Du kannst mit Scrollen oder Klicken & Ziehen auf der STAGE navigieren.
Auf der STAGE organisierst und verknüpfst du die STATES deines Games. 

![screenshot](./assets/editor-02.jpg)

Dieser Screenshot zeigt die STAGE mit einigen verknüpften STATES.

### STATE
  
Ein STATE ist ein Zustand, in dem sich dein Level zu einem bestimmten Zeitpunkt befindet, bis es zum nächsten STATE springt.

In einem neuen Level siehst du bereits die zwei STATES START und QUIT.

Wenn eine neue Session eines Levels gestartet wird, wird automatisch in den START STATE gewechselt. Du kannst beliebig ACTIONS zu START hinzufügen oder die [next](./actions/control/next.md) ACTION nutzen um direkt in den nächsten STATE zu wechseln. 

Den QUIT STATE kannst du nutzen um die Session automatisch zu beenden. Wird in den QUIT STATE gewechselt sorgt die [quit](./actions/control/quit.md) ACTION dafür, dass die Session beendet wird. Er markiert das Ende deines Levels.

STATES lassen sich auf der STAGE via drag and drop bewegen: Einfach mit dem Mauszeiger über der Kopfzeile hovern, klicken und dann bewegen.

![screenshot](./assets/editor-03.jpg)

Um einen STATE umzubenennen, klicke auf seinen Namen. 

Du kannst einen STATE auch kommentieren, um zum Beispiel eine Notiz für dich oder andere zu hinterlassen. Dazu klicke auf die Sprechblase neben dem Namen. Eine gefüllte Sprechblase zeigt an, dass bereits ein Kommentar geschrieben wurde. Die Kommentare haben keinerlei Auswirkungen auf den Verlauf des Games.

Du kannst einen neuen STATE erstellen, indem du eine ACTION aus der TOOLBAR auf die STAGE ziehst.

Du kannst jedem STATE beliebig viele ACTIONS hinzufügen.

### ACTIONS

ACTIONS sind die kleinsten Bausteine für jedes adaptor:ex-Level. Hier passieren die wirklich wichtigen Dinge.

Damit auf der Bühne auch irgendwas geschieht, müssen wir ACTIONS von links aus der TOOLBAR nach rechts auf die STAGE ziehen. 

Du kannst eine ACTION öffnen, indem du auf ihren Namen klickst. Dann kannst du ihre Inhalte bearbeiten. Um die ACTION wieder zu schließen, klicke auf den CLOSE-Button, oder nochmal auf ihren Namen. 

Jeder [STATE](../basics/editor.md#state) enthält eine oder mehrere ACTIONS.

Je nachdem, woran du und dein Team arbeiten, kann es sinnvoll sein, mehrere ACTIONS in einem STATE zu sammeln, oder für jede ACTION einen eigenen STATE anzulegen und diese über die [next](./actions/control/next.md) ACTION miteinander zu verknüpfen.

> Für Bühnenstücke hat sich z.B. gezeigt, dass alle actions, die zum selben Zeitpunkt passieren auch im selben STATE sein sollten. So kann einfacher zwischen den STATES gesprungen werden, etwa wenn bei den Proben Abschnitte wiederholt werden sollen. Alle szenischen Elemente wie Licht oder Sound Atmosphäre werden dann für die entsprechende Szene eingestellt.

Finde im [Actions Glossar](./actions/control/next.md){target=_blank} mehr über die verschiedenen ACTIONS heraus.

ACTIONS werden nacheinander von oben nach unten ausgeführt. Zunächst werden alle [RUN ACTIONS](#run-actions) ausgeführt, anschließend werden die [LISTEN ACTIONS](#listen-actions) gestartet.

![screenshot](./assets/editor-06.jpg){ style=width:400px }

#### RUN ACTIONS

RUN ACTIONS erkennst du an dem kleinen Pfeil links. 

Alle RUN ACTIONS innerhalb eines STATE werden nacheinander von oben nach unten ausgeführt.

Ein Beispiel für eine RUN ACTION ist die Telegram [Set Variable](./actions/data/set.md){target=_blank} ACTION. 

#### LISTEN ACTIONS

LISTEN ACTIONS erkennst du an dem kleinen Halbkreis links. Manche sagen auch, es wäre ein Ohr.

Diese ACTIONS warten, bzw. "hören" auf ein bestimmtes Ereignis. Wenn dieses eintritt, dann legen sie los. Meist enthalten sie eine Bedingung. Wenn diese eintrifft, wird in den dort angegebenen `next state` gewechselt. 

Ein Beispiel für eine LISTEN ACTION ist die [On Event](./actions/logic/onEvent.md){target=_blank} ACTION. 

Wird der STATE in dem sich die LISTEN ACTION befindet verlassen und in einen anderen STATE gewechselt, wird die LISTEN ACTION beendet und wird nicht weiter auf Ereignisse reagieren. Die LISTEN ACTION ist dann inaktiv.

