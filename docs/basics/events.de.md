Events
======

Plugins und Plugin Items wie [Devices](./plugins/devices.md) können Events (engl. für Ereignisse) auslösen, auf die du mit der [On Event](./actions/logic/onEvent.md) action reagieren kannst.

Mit der [Dispatch Event](./actions/logic/dispatch.md) action kannst du auch selbst Events auslösen und so den Verlauf eines Games über Pfade und Sessions hinweg gestalten.

Anwendungsfälle für Events sind z.B.:


* Innerhalb eines Levels zwischen 2 Pfaden kommunizieren ([Lokale Events](#lokale-events))
* Nachrichten von einer Session an eine andere Session senden ([Events und externe Sessions](#events-und-externe-sessions))
* Auf Events von Geräten oder externer Software reagieren ([Plugin Events](#plugin-events))
* Events von Items ausgehend auslösen und empfangen ([Items als Event Quelle](#items-als-event-quelle))
* Globale Events über das gesamte Game hinweg Senden ([Game Events](#game-events))

Lokale Events
---------------

Events die mit der [Dispatch Event](./actions/logic/dispatch.md) action ausgelöst werden, können im selben Level (und der selben Session) mit [On Event](./actions/logic/onEvent.md) empfangen werden.

Ohne Angabe einer Quelle (*source*) werden Events innerhalb einer Session eines Levels gesendet. So kann z.B. der Verlauf eines [Pfades](./paths.md) durch einen anderen Pfad geändert werden.

![Ein Event innerhalb der Session senden](./assets/events_dispatch_local.png)

Ein Event innerhalb der Session senden ...

![Ein Event von innerhalb der Session empfangen](./assets/events_catch_local.png)

... und empfangen.

So kann auf Ereignisse in anderen Pfaden reagiert werden ohne die Pfade zusammen führen zu müssen.

![Ein Haupt Pfad fängt events aus anderen Pfaden ab](./assets/events_local_path.gif)

Hier wird im "CommonPath" State mit [On Event](./actions/logic/onEvent.md) überprüft welcher der anderen beiden Pfade zuerst beim jeweiligen "Finished" State angelangt ist und ein entsprechendes Event Dispatched.

Events und externe Sessions
----------------------------

Wenn als Event Quelle (*source*) in der [Dispatch Event](./actions/logic/dispatch.md) action eine externe [Session](./live.md#session) angegeben wird, kann das Event innerhalb der externen Session als lokales Event empfangen werden.

``` mermaid
flowchart LR
    dispatchEvent-->onEvent
    subgraph A[Session A]
        dispatchEvent[Dispatch Event]
        end
    subgraph B[Session B]
        onEvent[On Local Event]
        end
    style B fill:#faf,stroke:#e9e,stroke-width:4px
```

Ebenso können lokale Events, die von der externen Session dispatched werden, in anderen Sessions empfangen werden. 

``` mermaid
flowchart LR
    dispatchEvent-->onEvent
    subgraph A[Session A]
        onEvent[On Event\nfrom: 'Session B']
        end
    subgraph B[Session B]
        dispatchEvent[Dispatch Local Event]
        end
    style B fill:#faf,stroke:#e9e,stroke-width:4px
```

> Hinweis: Alle Sessions lösen zudem automatisch das 'quit' Event aus, wenn sie beendet (z.B. mit der [Quit](./actions/control/quit.md) action) oder abgebrochen wurden.
### Beispiel

Wir erstellen eine neue Session mit der [Launch Session](../basics/actions/control/launch.md) action und senden und empfangen anschließend Events von der neu erstellten Session.

Gib dafür in der [Launch Session](../basics/actions/control/launch.md) action, neben dem Namen des Levels (hier "sidequest") einen Namen für die lokale Referenz auf die neu erstellte Session an.

![Eine externe Session starten](./assets/events_launch_external_session.png)

Darunter fügen wir eine [On Event](./actions/logic/onEvent.md) action hinzu, um auf ein lokales Event innerhalb der neu erstellten "SidequestSession" zu warten.

![Lokale events einer externe Session abfangen](./assets/events_on_event_external_session.png)

Der [On Event](./actions/logic/onEvent.md) Listener wird ausgelöst, sobald in der "SidequestSession" eine [Dispatch Event](./actions/logic/dispatch.md) action das lokale event "foundSomething" auslöst. Im "sidequest" level findet sich an entsprechender Stelle also eine Dispatch Event action ohne *source* angabe:

![Events von einer externen Session dispatchen](./assets/events_dispatch_external_session.png)

Auf diese Weise kann von der Neu erstellten Session mit der Ursprungs Session kommuniziert werden.

![Adaptor Live Modus: Event einer externen Session abfangen](./assets/events_from_external_session.gif)

Im linken Browser Fenster ist die Ursprungs Session die eine neue Session des 'sidequest' Level erstellt. Im rechten Browser Fenster das 'sidequest' Level, das ein Event Dispatched, dass dann in der Ursprungs Session abgefangen wird.

Umgekehrt können Events in der Ursprungs Session, mit der Referenz auf die 'sidequest' Session (hier 'SideQuestSession') als *from*, ausgelöst werden. Diese können wiederum mit einer [On Event](./actions/logic/onEvent.md) action in der neu erstellten Session des 'sidequest' Level abgefangen werden.

![Ein Event von in die externe Session senden](./assets/events_to_external_session.png)

Wir erstellen im Ursprungs Level eine [Dispatch Event](./actions/logic/dispatch.md) action, die "SidequestSession" als Event *source* hat (linkes Browserfenster). Im 'sidequest' level müssen wir nun lediglich auf lokale Events innerhalb der eigenen Session hören. Wir erstellen also eine [On Event](./actions/logic/onEvent.md) action (rechtes Browserfenster) ohne *from* Angabe mit dem passenden Event *name* (hier: 'timeToReturn').

Plugin Events
-------------

Events die von Plugins oder Plugin Items ausgelöst werden können kannst du mit der [On Event](./actions/logic/onEvent.md) action empfangen und darauf reagieren.

Welche Events ein Plugin auslösen kann siehst du in der Dokumentation des jeweiligen Plugin.

Wenn du ein Item erstellt hast und es ein Event auslösen kann, kannst du es als Event Quelle (*from*) in der [On Event](./actions/logic/onEvent.md) action auswählen.

Namen der Events, die durch Plugins und Plugin Items, die du installiert hast, gesendet werden können, kannst du unter *event* auswählen.

![Device Item als Event Quelle auswählen](./assets/events_incoming_osc_message.png)

Du kannst Items, die Events auslösen, auch über Referenzen oder Level Argumente angeben. Wähle `Custom Source` um eine freie Texteingabe zu erhalten.

![Item Referenz als Event Quelle angeben](./assets/events_incoming_osc_message_reference.png)

> - Wie du Referenzen auf Items erstellst und als Variablen verwendest findest du im Kapitel  [Variablen, Daten, Referenzen](./variables.md#collections-und-data-items) heraus.
>
> - In der [Devices](./plugins/devices.md#events) Dokumentation findest du mehr über das "incomingMessage" event heraus

Items als Event Quelle
----------------------

Mit der [Dispatch Event](./actions/logic/dispatch.md) action können Events von jedem Item ausgehend ausgelöst werden.

Events die von Items ausgelöst werden, können wiederum mit der [On Event](./actions/logic/onEvent.md) action und dem Item als Event Quelle (*from*) empfangen werden.

### Beispiel

Mit der [Add Item](./actions/data/addItem.md) action kannst du im Level Flow ein neues item erstellen.

Füge unter *Settings* die *reference* Option hinzu und gib einen Referenz Namen an (hier: `Player`).

![Ein neues Item erstellen mit Add Item](./assets/events_item_new.png)

Wir erstellen ein neues Item mit *name* `Ada` in der player Collection.

Du kannst das Item nun über die Referenz in einer [Dispatch Event](./actions/logic/dispatch.md) action als Event *source* angeben

![Ein Event von einem Item ausgehend auslösen](./assets/events_item_dispatch.png)

Empfange Events von Items mit [On Event](./actions/logic/onEvent.md) indem du eine Referenz auf das Item unter *from* angibst.

![Ein Item Event empfangen](./assets/events_item_on_event.png)

Da wir den Namen des Item kennen (hier: "Ada") können wir das Item einfach mit `players.Ada` referenzieren.

So kannst du das Event überall in deinem Game empfangen solange die Referenz sich auf das selbe Item bezieht, das in der [Dispatch Event](./actions/logic/dispatch.md) action als *source* angegeben wurde.

> Im Kapitel über [Variablen](./variables.md#item-container-und-level-arguments) findest du mehr darüber heraus wie du Items in deinem Game verwenden kannst.

Game Events
------------

Game events können in jeder Session jedes Levels innerhalb eines Games empfangen werden.

Nutze "game" als *source* in der [Dispatch Event](./actions/logic/dispatch.md) action um globale Events auszulösen.

![Ein globales Event auslösen mit "game" als Quelle](./assets/events_dispatch_global.png)

Globale Game events können mit der [On Event](./actions/logic/onEvent.md) action empfangen werden indem "game" als Quelle unter *from* ausgewählt wird.

![Ein globales Event abfangen](./assets/events_on_event_global.png)

In allen Sessions in denen ein [On Event](./actions/logic/onEvent.md) listener aktiv ist, führt das entsprechende Game Event zum *next state* wenn das Event der angegebenen Bedingungen entspricht.

![Game Events können in allen Sessions empfangen werden](./assets/events_global.gif)

Hier wird im linken Browser Fenster, im "GlobalEvent" State, das Game Event "hello_everyone" ausgelöst. Sowohl im mittleren, als auch im rechten Browserfenster ist eine Session zu sehen, die einen aktiven [On Event](./actions/logic/onEvent.md) listener besitzt der auf das Game Event "hello_everyone" hört.

Game Events können natürlich auch in Sessions des selben Levels und in der selben Session in anderen Pfaden empfangen werden.