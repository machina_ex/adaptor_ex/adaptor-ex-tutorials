---
title: adaptor:ex
authors: 
    - Lasse Marburg
---

![adaptor:ex](./assets/header.png)
====================================

!!! info ""
    Klicke hier um zur [deutschen Version](https://docs.adaptorex.org/de/index.html) der Dokumentation zu gelangen.

adaptor:ex helps you to create game theater, interactive installations, chatbot adventures, escape rooms or any other kind of playful live event. The graphical programming interface is optimized to bring dramaturgy and game mechanics together and arrange them in a visually concise way.

![Screenshot: Create and edit Live Games in adaptor:ex](./assets/editor_example.png "Create and edit Live Games in adaptor:ex")

You can make quick adjustments during rehearsals, monitor and control the live flow during tests and live shows.

![Screenshot: Monitoring and controlling the flow of an adaptor:ex Game](./basics/assets/events_local_path.gif "Monitoring and controlling the flow of an adaptor:ex Game")

The adaptor:ex editor is opened in your web browser, and you can edit the same game with several people at the same time.

![Screenshot: Collaborate with multiple users](./assets/collaboration_example.png "Collaborate with multiple users")

[Plugins](./basics/plugins/ableton.md) extend the capabilities of your adaptor:ex setup. You can connect many different software interfaces and devices in one game or story. Besides basic interfaces like HTTP, OSC and USB Serial, adaptor:ex install includes Plugins for Telegram, Twilio, MQTT and Ableton Live.

On these pages you will find out more about how to use adaptor:ex to connect and control your storyline, game mechanics, software and hardware.

Follow the [Getting Started](./getting_started.md) guide and try out adaptor:ex right away.

Start with a [Tutorial](./tutorials/arduino-serial/index.md) if you want to get to know one of the many ways you can use adaptor:ex.

In the [Glossary](./basics/editor.md) you find information about all basic elements, and [Plugins](./basics/plugins/ableton.md). Here you can find out how to use the [Editor](./basics/editor.md) and how to control your [Live Show](./basics/live.md).

adaptor:ex is a web application that you install and host yourself on your computer or a server. Under [Install](./install.md) you will find the different ways to set up the application.

Take a look at our [News Blog](./blog/index.md) if you want to get information about current updates, bugfixes and games that have been developed with adaptor:ex.

If you need help, discovered a bug or want to suggest a feature: create an [:material-cards-outline: Issue on Gitlab](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/issues) write us an [:fontawesome-solid-envelope: e-mail](mailto:tech@machinaex.de) or a message in our [:fontawesome-brands-discord: Discord Channel](https://discord.gg/quHbQAMvF6). There you can also discuss with others and get news about updates and bugfixes.

-------------------------

This is a [machina commons](https://commons.machinaex.org) project by 

[![machina eX](./assets/machina-eX-logo-color-small.png){ style=width:400px }](https://machinaex.de)

sponsored by

![Logo Senate Department for Culture and Europe Berlin](./assets/logo_senatsverwaltung_berlin.jpg){ style=width:330px }

Senate Department for Culture and Europe Berlin