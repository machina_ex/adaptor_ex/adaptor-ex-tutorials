Network Devices
================

In many interactive theater situations, we want to connect external things to adaptor:ex.

If we, for example, want to control an interactive prop like the bomb from the machina eX play
'15 000 Gray', we need to send it signals in the HTTP protocol...

> In the following step by step tutorial we assume that we want to communicate with a device with HTTP interface, but adaptor:ex of course supports other network protocols (TCP, UDP, UDP/OSC) as well as serial (USB) interfaces (provided we work in adaptor:ex in local mode).

Send HTTP requests
----------------------

1. after we have selected or created our game, we choose Game > Settings
2. in Settings click on Devices and under HTTP click on the Plus button to create a new HTTP device.

    ![screenshot](./assets/1.png)
    ![screenshot](./assets/2.png)

    Devices exist on the game level, i.e. one and the same device can be reached from any level within a game.

3. The new device gets a name. In our example we simply call it "Bomb".

    ![screenshot](./assets/3.png)

    Under this name it will be available in the level editor, both as a device and as an event source (more about this later).

4. under Settings we select "to Device", because we want to send something to the device bomb.

    ![screenshot](./assets/4.png)

5. There is potentially more to set here. But we'll keep it simple for now and just fill in the URL field. This is the URL (e.g. `http://192.168.178.4:3000`) of the bomb on our network.

    Then we click SAVE.

    ![screenshot](./assets/5.png)

> Because we can reach the bomb in our internal network via WLAN, the URL in our case is `http://` plus the IP address of the bomb plus `:3000` because our bomb listens on port 3000. But of course the bomb could also be a device behind a domain (e.g. if it would be accessed via an internet service or otherwise DNS resolution takes place).

6. Now we create a level or change to the level from which we want to address our bomb (GAME > OVERVIEW > Select or create LEVEL).

7. In the Level Editor we now find on the left in the Action Bar, the area "DEVICES" and the action "Send Message".

    ![screenshot](./assets/6.png)

8. We drag the Send Message Action onto our stage to create a new state with the Send Message Action.

    ![screenshot](./assets/7.png)

9. We rename the state (e.g. to TRIGGER_BOMB because we want to start the bomb) and select the device "Bomb" under "to" in the action form, if it is not already selected.

    ![screenshot](./assets/8.png)

10. We know from the documentation of our bomb requisite that the bomb can be started with a GET request to the endpoint/path `/AN`.

    So let's look at Settings to see what other fields we could select in the Send Message Action:

    ![screenshot](./assets/9.png)

    `message` is the message we want to send to the device. The message is sent in the HTTP request as a so-called query parameter. We do not need this in the case of the bomb, but it is good to know.

    method` determines which HTTP method will be used. If we don't specify anything here (and nothing else is set in the settings), an HTTP GET request will be sent. Because our bomb only listens to GET requests, we don't need this here. But also good to know.

    path` is the path to which the HTTP request is sent. Aha! The bomb is started with a message to `/AN`. So we select `path` and write `/AN` in it.
    
    ![screenshot](./assets/10.png)

    So the send message action, when we arrive in this state in the level, sends an HTTP GET request to the url of the bomb (`http://192.168.178.4:3000` as we determined in Step 5.) and also appends the path `/AN` to the url (so our message goes to `http://192.168.178.4:3000/AN` in our example)
    
<br/>

11. So we connect our new state (of course add another NEXT action, etc.), click on the LIVE MODE button in the upper right and start a session.

    ![screenshot](./assets/11.png)

    Once the session arrives at our state `TRIGGER_BOMB`, our send message action is activated and we send an HTTP GET request to our bomb and start it.

    Yikes, it's ticking! Now hurry up!!!

<br/>

Perfect. So now we know how to create a new device and send it HTTP requests! Excellent!

But what if we have a device that should send HTTP requests to our adaptor:ex server/computer?

<br/>

Receiving HTTP Requests - with Webhooks
---------------------------------------

Our example prop, the time bomb from 15'000 Gray, sends an HTTP GET REQUEST to the IP address of the computer running adaptor:ex in case its timer has expired (Boom!) as well as in case it has been successfully defused by the audience (Phew!).

In order for adaptor:ex to respond to these messages, we need to go back to the Game Settings:

1. There we select the Settings button of the device bomb and see that besides to_device there is also `from-device`. We select this.

    ![screenshot](./assets/15.png)

2. Because we expect GET requests from the bomb, we don't change anything, but click SAVE once and then click the reload button on the top right (the circle icon)

    ![screenshot](./assets/12.png)
    ![screenshot](./assets/14.png)

    After a short reload, the indicator should now light up green and we see a URL in the `webhook` field. Our bomb should now send its signals to this URL.

3. So we configure our bomb (or any other device) to send to our webhook address.
In our example the bomb now sends a HTTP GET request with the query parameter `?timeout=true` if it explodes or `?solved=true` if it is defused.

    So now we want to respond to these two events.

4. We switch back to the level editor and drag an 'ON EVENT' ACTION onto the stage.

    ![screenshot](./assets/16.png)

5. Under *Settings* we add the `from` option and select the http device `Bombe` in the drop down menu under `Select Source`. As `event` we specify "incomingMessage". So we want something to happen when an incomingMessage event occurs at the network device Bombe.

6. For simplicity we first select the `else` option in the *Settings* and now link this listener action with `QUIT`.

    ![screenshot](./assets/17.png)
    ![screenshot](./assets/18.png)

7. So now when the bomb sends an HTTP GET request to its `webhook`, the event is triggered and we arrive at the end of the level.


### Filter incoming HTTP REQUESTS


But how do we now distinguish between a request with `?timeout=true` and one with `?solved=true`?

7. Instead of `else` as in step 5. we select `if` in the action *Settings*. The url queries fall directly into the event. So we can look at the individual queries (`timeout` and `solved`) directly from adaptor:ex and check them to determine which state may be triggered in which case.

    So we add a condition (`add condition`) and enter in the field `field` which query adaptor:ex should look for (`timeout` or `solved`) and which value this query parameter must have for something to happen (`true` in our example).

    Finally, in `next state` we enter the name of the state that should occur if this is     
    
    ![screenshot](./assets/19.png)

    After we have created a WIN State (which could later, for example, play the victory music or control the light etc.), the level now looks something like this:

    ![screenshot](./assets/19a.png)

    ![screenshot](./assets/21.png)

    And if we switch to live mode and start a new session and then mark it, we see: The session stops at the state `ON_BOOM` and waits for the events we defined.

    ![screenshot](./assets/20.png)

    Now when the bomb prop sends a HTTP GET request to our webhook and sends `?solved=true` with it, we see how the WIN state is triggered.

    ![screenshot](./assets/22.png)

> There are more possibilities to read the content of the bomb event (e.g. we can find the data of the onEvent also in the ACTION BAR under Variables and can edit it there etc.).

So now we can have our props talk to adaptor:ex as well as send signals from adaptor:ex to external devices.

Of course you can also control other software on the network and on the same computer and even 3rd party services on the network (although for the latter we usually recommend to program your own plugin if possible).
