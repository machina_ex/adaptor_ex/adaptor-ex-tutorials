Dateien und Geo Locations per Telegram versenden
================================================

adaptor:ex ist Teil des Projekts MACHINA COMMONS und wird im Programm "Digitale Entwicklung im Kulturbereich" der Berliner Senatsverwaltung für Kultur und Europa gefördert.

adaptor:ex ist eine open-source Software Engine, die es ermöglicht, interaktive Theatergames und -installationen umzusetzen. Ein Teilbereich davon sind inter(re)aktive Performances innerhalb des Messaging Services "Telegram".

Tutorial
----------

Dieses Tutorial ist Teil der Tutorialsammlung von adaptor:ex.

Es baut auf dem ersten Tutorial [Storytelling mit adaptor:ex](../telegram-basics/index.md){target=_blank} auf und setzt Wissen von dort vorraus. Wenn du das erste Tutorial noch nicht durchgearbeitet hast, nimm dir das bitte zuerst vor.

In diesem Tutorial lernen wir erweiterte Funktionen des Telegram Plugins kennen. Diese ermöglichen uns 

- [Fotos](#fotos-und-bilder-verschicken) 
- [Sprach- & Audionachrichten](#audio-files-und-voice-messages)
- [Dateien](#andere-files)
- [Karten & Koordinaten](#sende-kartenausschnitt-mit-ort)

via unserer von adaptor:ex kontrollierten Telegram Accounts zu versenden.

<div class="page"/>

Schritt für Schritt
-------------------

Du kannst mit einem leeren Level beginnen oder mit einem Level, in dem es bereits einige States und Actions gibt. In diesem Tutorial beginnen wir mit einem leeren Level:

![screenshot](./assets/0-2.png){ style=width:80%}

### Fotos und Bilder verschicken

1. aus der **TOOLBAR** (links) unter dem Abschnitt Telegram ziehen wir nun die **ACTION** **[Send File](../../basics/actions/telegram/sendFile.md)** auf einen leeren Abschnitt der **STAGE** (rechts). Wir nennen den neu entstandenen STATE in diesem Tutorial "sendFile" und verbinden die 3 **STATES** via **next**

    ![screenshot](./assets/1-1.png)
    
    ![screenshot](./assets/2-1.png)

2. Nun klappen wir die **[Send File](../../basics/actions/telegram/sendFile.md)** ACTION in unserem neuen State auf und sehen folgendes Formular:

    ![screenshot](./assets/3-1.png){ style=width:300px}

    Dort wählen wir unseren Account (z.B. Thekla) aus und unter **to** tragen wir 'Player' ein, da wir dies an die Spieler:in in dieser Session schicken wollen. (Später lernen wir auch wie wir hier statt verschiedener Player auch in einen Gruppenchat schicken können, aber das ist zu viel für dieses Tutorial)

3. Nun können wir ausserdem etwas unter **file** eintragen. Nämlich die Datei, die wir gerne an die Spieler:in schicken wollen. Die einfachste Art wäre, die Dateien zu benutzen, die unser Admin auf dem adaptor:ex server für dieses **Game** freigegeben hat. Wenn dort Daten liegen sollten, tauchen diese in der **TOOLBAR** auf. Wenn wir auf den Reiter/Tab **MEDIA** klicken, kann das in etwa so aussehen:

    ![screenshot](./assets/4.png){ style=width:40%}

    Diese Files liegen auf dem Server im Gameverzeichnis unter 'files/'. Je nachdem wie der adaptor:ex server konfiguriert wurde haben wir Zugriff, um Dateien selbst dort hochzuladen oder nicht. In unserem Beispiel liegen ein paar Dateien zum Testen auf dem Server. Im Glossar Kapitel [Files](../../basics/files.md) findest du mehr dazu heraus.

4. Die Dateinamen können wir jetzt via drag and drop einfach in das Feld **file** ziehen:

    ![screenshot](./assets/5-1.png){ style=width:80%}
    
    ![screenshot](./assets/6-1.png){ style=width:40%}

5. Thekla (unser Account) wird nun jeder Spieler:in, die das Level aktiviert, ein Foto schicken:

    ![screenshot](./assets/7.png){ style=width:350px}
    
    Photo by <a href="https://unsplash.com/@charlesdeluvio?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Charles Deluvio</a> on <a href="https://unsplash.com/s/photos/balloon-dog?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

6. Aber was wenn wir keinen Zugang zu dem adaptor:ex Fileserver haben? Oder wenn wir einfach eine andere Datei verschicken wollen?

    Keine Sorge: Wir können einfach auch eine URL zu einer Datei aus dem Internet schicken. Einen Link zum Bild erkennst du daran, dass sich am Ende der URL eine Dateiendung, z.B. .png oder .jpeg befindet.
    Dabei musst du natürlich darauf achten, dass die Nutzungsrechte dieser Datei deine Verwendung erlauben.
    Versuchs doch mal mit dem adaptor:ex Logo: `https://machinacommons.world/img/logo-quer-700px.png`
    
    ![screenshot](./assets/8.png){ style=width:80%}

    Das funktioniert dann auch:

    ![screenshot](./assets/9.png){ style=width:350px}
  
> **Tipp:** Wenn wir Bilder schicken ist unter **format** die Einstellung `auto` perfekt. Das Bild wird, erfahrungsgemäß, solange es unter 10 MB groß ist, dann auch sofort eingeblendet, vorausgesetzt die empfangende Person erlaubt dies in ihrer Telegram App. Bilder über 10MB werden den Empfangenden standardmäßig als Download angezeigt.

### Audio Files und Voice Messages

Wir können via Telegram auch Audio Dateien wie .mp3, .wav und .ogg verschicken.

.mp3 und .wav werden immer als Audio Downloadlink verschickt.

.ogg Dateien können als voice message (**format** zu `voice`) oder audio file verschickt werden. Wenn die .ogg Datei korrekt encoded ist (OPUS codec) und wenn sie nicht größer als 1MB ist, wird Telegram die Datei als voice message anzeigen, so als hätte unsere Figur Thekla sie selbst mit Telegram eingesprochen.

![screenshot](./assets/16.png){ style=width:40%}

![screenshot](./assets/17.png){ style=width:38%}

Wenn irgend etwas nicht korrekt ist für die voice message sendet Telegram die Datei als normalen Downloadlink. Die meisten kann mensch auch einfach mit einem Klick auf den Link direkt anhören.

#### .mp3 zu .ogg vorbis mit opus codec umwandeln

Es gibt Online-Converter für Sounddateien. Dafür kannst du mit einer Suchmaschine deiner Wahl zum Beispiel nach "mp3 zu ogg" suchen und den Anweisungen folgen. Achte auf die Sicherheit der Seite, die Datenschutzbestimmungen, und ob es Informationen darüber gibt, wo und wie lang deine hochgeladene Sounddatei gespeichert wird. 

Auch die verbreitete Audio Editing Software [Audacity](https://manual.audacityteam.org/man/ogg_vorbis_export_options.html) kann ogg vorbis Dateien exportieren.

Eine andere Möglichkeit ist die Nutzung des Kommandozeilenprogramm [**ffmpeg**](https://ffmpeg.org/download.html).

auf MacOS benutzen wir das nach erfolgreicher Installation mit diesem Befehl im Terminal um die Datei input.mp3 in eine Datei output.ogg umzuwandeln:

`ffmpeg -i input.mp3 -c:a libopus output.ogg`

### Andere Files

Andere Dateitypen werden als Downloadlink verschickt, mit der Ausnahme einiger Video Dateien, die können mit **format** `video` auch im play live mode gesendet werden. Wie bei Bildern und Soundfiles, hängt aber die Darstellung nicht nur am Dateiformat und der Größe des Files, sondern auch an den Einstellungen der Telegram App der empfangenden Person.

### Sende Kartenausschnitt mit Ort

1. um eine Karte zu senden, ziehen wir eine **[Send Geo Location](../../basics/actions/telegram/sendGeo.md)** ACTION aus der TOOLBAR auf die STAGE

    ![screenshot](./assets/10-1.png){ style=width:80%}
    
    ![screenshot](./assets/11-1.png){ style=width:80%}
    

2. **Send Geo Location** braucht neben **Telegram Account** und **to** nur noch die Felder latitude (Breitengrad) und longitude (Längengrad) (ACHTUNG: Beide müssen englisch notiert werden, also mit Punkt anstatt eines Kommas!).

    Latitude und longitude von einem Ort kannst du dir z.B. über Open Street Maps oder Google Maps heraussuchen. Falls du nicht suchen möchtest, probier es doch mit diesen hier:

    lat: 52.438015   
    long: 13.231191

    ![screenshot](./assets/13-1.png){ style=width:45%}

3. Jetzt bekommt die Spieler:in eine digitale Straßenkarte mit Markierung zugesandt, die sie auch direkt benutzen kann um ihr Navigationsprogramm zu öffnen. Das kann sehr nützlich sein für Theater-Performances/Führungen/Games, die die Spieler:innen an verschiedene Orte in der Stadt führen.

    ![screenshot](./assets/14.png){ style=width:35%}
    ![screenshot](./assets/15.png){ style=width:44%}
