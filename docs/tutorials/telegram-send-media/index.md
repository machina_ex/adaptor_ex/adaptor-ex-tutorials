Send files and geo locations via Telegram
==========================================

adaptor:ex is part of the MACHINA COMMONS project and is funded in the program "Digital Development in the Cultural Sector" of the Berlin Senate Department for Culture and Europe.

adaptor:ex is an open-source software engine that enables the implementation of interactive theater games and installations. One part of it are inter(re)active performances within the messaging service "Telegram".

Tutorial
----------

This tutorial is part of the adaptor:ex tutorial collection.

It builds on the first tutorial [Storytelling with adaptor:ex](../telegram-basics/index.md){target=_blank} and assumes knowledge from there. If you haven't worked through the first tutorial yet, please do that first.

In this tutorial we will learn about advanced features of the Telegram plugin. These allow us to send

- [Photos](#send-photos-and-images)
- [Voice & audio messages](#audio-files-and-voice-messages)
- [files](#other-files)
- [maps & coordinates](#send-map-section-with-location)

via our Telegram accounts controlled by adaptor:ex.

<div class="page"/>

Step by step
-------------------

You can start with an empty level or with a level that already has some states and actions. In this tutorial we will start with an empty level:

![screenshot](./assets/0-2.png){ style=width:80%}

### Send photos and images

1. From the **TOOLBAR** (left) under the Telegram section we now drag the **ACTION** **[Send File](../../basics/actions/telegram/sendFile.md)** to an empty section of the **STAGE** (right). We call the newly created STATE in this tutorial "sendFile" and connect the 3 **STATES** via **next**.

    ![screenshot](./assets/1-1.png)
    
    ![screenshot](./assets/2-1.png)

2. Now we unfold the **[Send File](../../basics/actions/telegram/sendFile.md)** ACTION in our new state and see the following form:

    ![screenshot](./assets/3-1.png){ style=width:300px}

    There we select our account (e.g. Thekla) and under **to** we enter 'Player', because we want to send this to the players in this session. (Later we will also learn how to send here to a group chat instead of different players, but that's too much for this tutorial).

3. Now we can also enter something under **file**. Namely the file we would like to send to the players. The easiest way would be to use the files that our admin has shared on the adaptor:ex server for this **game**. If there should be data there, it will show up in the **TOOLBAR**. If we click on the **MEDIA** tab, it may look something like this:

    ![screenshot](./assets/4.png){ style=width:40%}

    These files are located on the server in the game directory under 'files/'. Depending on how the adaptor:ex server is configured, we have access to upload files there ourselves or not. In our example there are some files for testing on the server. See the glossary chapter [Files](../../basics/files.md) for more information.

4. We can now simply drag and drop the file names into the **file** field:

    ![screenshot](./assets/5-1.png){ style=width:80%}
    
    ![screenshot](./assets/6-1.png){ style=width:40%}

5. Thekla (our account) will now send a photo to each player who activates the level:

    ![screenshot](./assets/7.png){ style=width:350px}
    
    Photo by <a href="https://unsplash.com/@charlesdeluvio?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Charles Deluvio</a> on <a href="https://unsplash.com/s/photos/balloon-dog?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

6. But what if we don't have access to the adaptor:ex fileserver? Or if we just want to send another file?

    Don't worry: We can simply send a URL to a file from the Internet. You can recognize a link to the image by the file extension at the end of the URL, e.g. .png or .jpeg.
    Of course you have to make sure that the rights of use of this file allow your use.
    Try the adaptor:ex logo: `https://machinacommons.world/img/logo-quer-700px.png`
 
    ![screenshot](./assets/8.png){ style=width:80%}

    This then also works:

    ![screenshot](./assets/9.png){ style=width:350px}
  
> **Tip:** When we send pictures, under **format** the setting `auto` is perfect. The image will, from experience, as long as it is under 10MB in size, then also fade in immediately, provided that the receiving person allows this in their Telegram app. Images over 10MB are displayed to the recipient as a download by default.

### Audio Files and Voice Messages

We can also send audio files like .mp3, .wav and .ogg via Telegram.

.mp3 and .wav are always sent as audio download links.

.ogg files can be sent as voice message (**format** to `voice`) or audio file. If the .ogg file is correctly encoded (OPUS codec) and if it is not larger than 1MB, Telegram will display the file as a voice message, as if our character Thekla had recorded it herself with Telegram.

![screenshot](./assets/16.png){ style=width:40%}

![screenshot](./assets/17.png){ style=width:38%}

If anything is not correct for the voice message Telegram sends the file as a normal download link. Most of them can also be listened to directly by clicking on the link.

#### convert .mp3 to .ogg vorbis with opus codec

There are online converters for sound files. For this, you can use a search engine of your choice to search for "mp3 to ogg", for example, and follow the instructions. Pay attention to the site's security, privacy policy, and whether there is information about where and how long your uploaded sound file will be stored.

The popular audio editing software [Audacity](https://manual.audacityteam.org/man/ogg_vorbis_export_options.html) can also export ogg vorbis files.

Another option is to use the command line utility [**ffmpeg**](https://ffmpeg.org/download.html).

on MacOS we use it after successful installation with this command in the terminal to convert the input.mp3 file into an output.ogg file:

`ffmpeg -i input.mp3 -c:a libopus output.ogg`

### Other files

Other file types are sent as download links, with the exception of some video files, which can also be sent in play live mode with **format** `video`. As with images and sound files, the display depends not only on the file format and the size of the file, but also on the settings of the Telegram app of the receiving person.

### Send map section with location

1. to send a map, we drag a **[Send Geo Location](../../basics/actions/telegram/sendGeo.md)** ACTION from the TOOLBAR to the STAGE

    ![screenshot](./assets/10-1.png){ style=width:80%}
    
    ![screenshot](./assets/11-1.png){ style=width:80%}


2. **Send Geo Location** needs beside **Telegram Account** and **to** only the fields latitude and longitude.

    Latitude and longitude of a place can be found e.g. via Open Street Maps or Google Maps. If you don't want to search, try these:

    lat: 52.438015
    long: 13.231191

    ![screenshot](./assets/13-1.png){ style=width:45%}

3. now the player gets a digital street map with marker sent to her, which she can also use directly to open her navigation program. This can be very useful for theater performances/guided tours/games that take the players to different places in the city.

    ![screenshot](./assets/14.png){ style=width:35%}
    ![screenshot](./assets/15.png){ style=width:44%}
