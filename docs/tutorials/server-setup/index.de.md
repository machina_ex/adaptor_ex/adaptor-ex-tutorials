Server Setup Guide für adaptor:ex
=================================

Dieses Tutorial hilft dir, wenn du adaptor:ex als Webservice nutzen möchtest der 24/7 läuft und von überall, wo es eine stabile Internetverbindung gibt, erreicht werden kann.

Wir erstellen ein Setup das auf jedem geläufigen linux Server funktionieren sollte. Auch wenn du ein abweichendes Setup hast, das aber noch Probleme bereitet, könntest du hier fündig werden. 

Ziel ist es den adaptor:ex editor als auch alle web fähigen APIs über das Netz auf deiner eigenen Domain erreichbar zu machen und mit Nutzer Authentifizierung und let's encrypt Zertifikaten zu sichern.

Solltest du Probleme bei der Konfiguration haben schau unter [Hilfe erhalten](#hilfe-erhalten)


Was du benötigst
----------------



### Web Server

Du brauchst einen eigenen (virtuellen) Webserver den du per [SSH](https://de.wikipedia.org/wiki/Secure_Shell) bedienen kannst.

Das Betriebssystem des Servers sollte eine geläufige Linux Distribution sein (z.B. Ubuntu oder Debian). Die meisten Anweisungen in diesem Tutorial sollten aber auch auf anderen Betriebssystemen anwendbar sein.

### Subdomains

Du benötigst einen Domain Namen für den du Subdomains erstellen und die du auf die IP deines Servers verweisen kannst.

Wie du neue Subdomains für deinen Domainnamen anlegen kannst hängt von deinem Domain provider ab.

Wir benötigen für den adaptor:ex Server und den adaptor:ex Client jeweils eine Subdomain.

Du kannst für einen von beiden (wir empfehlen für den Client) natürlich auch den Haupt Domain Namen verwenden, wenn du noch keine  Webseite oder einen Webservice damit bedienst

Im folgenden Beispiel ist überall dort, wo du deine eigene Domain angeben musst für den adaptor:ex Server `adaptor-server.myurl.org` und für den adaptor:ex Client `adaptor.myurl.org` angegeben.

Wenn du zwei Domain Namen eingerichtet hast, verweise sie jeweils auf die IP deines Servers. Du kannst die IP auf einem Linux Server mit dem Befehl `ifconfig` herausfinden.

Die IP auf die wir die Domains verweisen findest du in der mit `ifconfig` angezeigten Liste für gewöhnlich unter `eth0`.

Passe für deine Domainnamen den `A-Record` Eintrag an indem du in beiden Fällen die IPv4 deines Servers dort angibst.

> Wenn du keine eigene Domain hast, dann kannst du auch deine __öffentliche IPv4 Adresse__ angeben. 
> Beachte jedoch, dass du auf diese Weise nur unverschlüsselt mit deinem Server kommunizieren kannst. Zur Erhöhung der Sicherheit, empfehlen wir die Einrichtung einer Domain.




Den Server einrichten
----------------------

> TL;DR: bring mich zur [docker-compose File](#erweitertes-setup)

### User und ssh-key einrichten

Bevor du anfangen kannst, adaptor:ex auf deinem Server zu installieren, solltest du zunächst einige Einstellungen vornehmen, die ein komfortables und sicheres Arbeiten mit `ssh` auf dem Server erlauben. 

Der Autor von [nixCraft](https://www.cyberciti.biz/tips/about-us) stellt eine verständliche und leicht zu folgende [Anleitung](https://www.cyberciti.biz/faq/how-to-disable-ssh-password-login-on-linux/) bereit.

Folge den Anweisungen und logge dich auf deinen Server ein, um weiter zu machen.

`ssh deinUsername@deineAddresse`

> Wenn du der oben genannten Anleitung __nicht__ gefolgt bist, so ist dein username `root`


### Docker installieren

Installiere [Docker](https://docs.docker.com/engine/install/) auf deinem Server. Auf der Seite findest du die Installationsanweisungen für unterschiedliche Linux Distributionen. Für Ubunutu oder Debian ist die [apt Installationsmethode](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository) die geläufigste.

Beachte auch die [post-installations Anweisungen](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user).



adaptor:ex installieren
----------------------

Wenn du noch keine oder wenig Erfahrung hast, empfehlen wir dir zuerst das [Minimalsetup](#minimalsetup) zu machen, um sicher zu gehen, dass `adaptor:ex` bei dir grundsätzlich funktioniert.

> __Das Minimalsetup eignet sich noch nicht dazu, in einer produktiven Umgebung genutzt zu werden, da wichtige Sicherheitsmerkmale fehlen.__ 

Wenn du schon Erfahrung hast, dann kannst du direkt zum [erweiterten Setup](#erweitertes-setup) springen.


Minimalsetup
-------------

Logge dich via `ssh` in deinen Server ein und erstelle deinen Projektordner:

`mkdir -p deinProjektordner`

und wechsle in das Verzeichnis:

`cd deinProjektordner`

dann lade die `docker-compose.yml` Datei herunter:

`curl -o docker-compose.yml https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/raw/main/docker-compose.yml`

adaptor:ex besteht aus 3 Komponenten: einer mongo Datenabank, einem Server und einem Client. `mongodb` dient zum Speichern der Daten. `adaptor_server` ist für Verarbeitung der Daten zuständing und auf der Oberfläche des `adaptor_client` manipulierst du die Daten.

Um deine `adaptor:ex` Instanz lauffähig zu machen springe in das Kapitel [Client Config](#client-config) und folge den Anweisungen. Danach kannst du __[adaptor:ex starten](#adaptorex-starten)__

Später kannst du im Minimalsetup noch [berechtigte User hinzufügen](#berechtigte-user-hinzufügen).

Erweitertes Setup
------------------

Im zweiten Teil des Tutorials lernst du, wie du die Nutzer Authentifizierung anschaltest und das HTTPs Protokoll benutzt.

Um dem Tutorial folgen zu können, lade dir die [erweiterte adaptor:ex docker-compose.yml](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_tutorials/-/raw/main/docs/tutorials/server-setup/assets/docker-compose.yml) Datei herunter.

`curl -o docker-compose.yml https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_tutorials/-/raw/main/docs/tutorials/server-setup/assets/docker-compose.yml`

Wir setzen zuerst die [Client Config](#client-config), daraufhin verändern wir die `docker-compose.yml` und schauen auf die [Server Optionen](#server-optionen-setzen). Im letzten Schritt konfigurieren wir noch einen [nginx Server](#sichere-verbindung) zur sicheren Verbindung.

## Client Config

Damit der `adaptor:ex` Client die Server API ausfindig machen kann, brauchst du eine config Datei. Zur Erstellung dieser Datei gehe wie folgt vor:

Lade die Datei mit `curl` direkt auf deinen Server herunter:

`curl -o adaptor_client_config.json https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_tutorials/-/raw/main/docs/tutorials/server-setup/assets/adaptor_client_config.json`

oder lege sie manuell an, indem du folgende Zeilen im text editor [nano](https://wiki.ubuntuusers.de/Nano/) kopierst:


``` title="adaptor_client_config.json"
--8<-- "./docs/tutorials/server-setup/assets/adaptor_client_config.json"
```

``` sh
nano adaptor_client_config.json
```

Danach ersetze die `API_URL` mit der Adresse deines Servers. Gehe dabei wie folgt vor:

- __Minimalsetup__: Setze deine Domain `http://adaptor-server.myurl.org:8081/api` oder IPv4 Addresse `http://123.45.67.89:8081/api` ein. Achte darauf den Standardport des Servers `8081`zusätzlich zu deklarieren. Für das Minimalsetup ist dies der einzige Konfigurationsschritt. Du kannst __[adaptor:ex starten](#adaptorex-starten)__

- __Erweitertes Setup__: `https://adaptor-server.myurl.org/api`. Der Standardport ist bei diesem Setup ausgelassen. Er wird direkt in der `docker-compose.yml` Datei deklariert. Damit die Verbindung über `https` gesichert werden kann,brauchst du eine eigene Domain. Wenn du die erweiterte `docker-compose.yml` Datei schon herunter geladen hast, fahre im nächsten Kapitel fort. Ansonsten lade sie dir im [erweiterten Setup](#erweitertes-setup) herunter.


## Docker Compose File anpassen

Die Server Konfiguration in der neuen `docker-compose.yml` sieht wie folgt aus:

```title="docker-compose.yml | adaptor_server"
--8<-- "./docs/tutorials/server-setup/assets/docker-compose.yml:19:40"
```

Die Client Konfiguration sieht so aus:

```title="docker-compose.yml | adaptor_client"
--8<-- "./docs/tutorials/server-setup/assets/docker-compose.yml:41:52"
```


### URL und E-Mail eintragen 

In beiden Konfigurationen musst du deine eigenen Daten eintragen.

``` sh
nano docker-compose.yml
```

1. Ersetze `adaptor-server.myurl.org` durch deine eigene (sub-) Domain die du für den __Server__ vorgesehen hast an 3 Stellen unter `entrypoint` und `envoirment`.

2. Ersetze `adaptor.myurl.org` durch deine eigene (sub-) Domain die du für den __Client__ vorgesehen hast an 2 Stellen unter `envoirment`.

2. Ersetze die E-mail Adresse `your@email.de` mit deiner eigenen Adresse an 2 Stellen unter `envoirment`.


## Sichere Verbindung

In der neuen `docker-compose.yml` sind 2 neue Services dazu gekommen: `proxy` und `letsencrypt-companion`.

Der `proxy` Service verteilt HTTP (port 80) und HTTPS (port 443) Anfragen an deinen Server auf die verschiedenen Docker Prozesse. Er entscheided anhand der Subdomain an welchen Prozess er die Anfrage weiterleitet.

Das [Lets encrypt Projekt](https://letsencrypt.org/de/about/) stellt kostenlose SSL Zertifikate aus. Sie werden zur sicheren Verbindung zwischen zwei Geräten benutzt. Der `letsencrypt-companion` konfiguriert den `proxy` Server automatisiert. 

Damit die Verteilung reibungslos funktioniert, musst du einige Daten verändern. 

Für den Proxy Docker Container, der das verteilen eingehender Requests auf die anderen Container übernimmt, musst du noch die folgende Docker Datei anlegen.

Erstelle einen Ordner mit dem Namen `proxy`:

``` sh
mkdir proxy
```

und anschließend das Dockerfile für den proxy Container:

``` sh
nano proxy/Dockerfile
```

Füge diesen Inhalt ein:

``` sh
FROM jwilder/nginx-proxy:alpine

COPY uploadsize.conf /etc/nginx/conf.d/uploadsize.conf
```

Zudem müssen wir noch die Datei `uploadsize.conf` erstellen:

``` sh
nano proxy/uploadsize.conf
```

und mit folgendem Inhalt füllen:

``` conf
client_max_body_size 100M;
proxy_request_buffering off;
```

## adaptor:ex starten

Deine Verzeichnisstruktur soll bei dem Start folgendermaßen aussehen:

```bash
╭< ~/deinProjektordner
╰─> pwd                          # print working directory
/home/ada/deinProjektordner

╭─< ~/deinProjektordner
╰─> ls                                                 # list directory
adaptor_client_config.json  docker-compose.yml  proxy  # proxy nur, bei https
```

Um die Docker Container zu starten führe folgende Befehle in deinem Projektverzeichnis aus. 

Lade die aktuellen Docker images herunter. 
``` sh
docker compose pull
```

Komponiere die Docker Container
``` sh
docker compose up -d
```

Zeige anschließend die Container an um zu überprüfen ob alles geklappt hat
``` sh
docker compose ps --services
```

Du solltest die folgende Ausgabe erhalten
``` bash
adaptor_client
mongodb
adaptor_server
proxy                                   # nur bei https
letsencrypt-companion                   # nur bei https
```
## Berechtigte User hinzufügen

> Auch wenn du keine eigene Domain hast, kannst du die Authentifizierung im Minimalsetup einschalten. Wir raten von der produktiven Nutzung ohne verschlüsselte Kommunikation allerdings ab. __[Verbinde mich sicher](#erweitertes-setup)__

Die Server Option `"--auth", "basic"` muss in der *docker-compose.yml* Datei unter `entrypoint` angegeben sein.

> Wenn du auf einen Nutzer Login verzichten willst, entferne ggf. die Optionen `"--auth", "basic"` für den `adaptor-server` in der *docker-compose.yml* Datei unter `entrypoint` und starte adaptor:ex neu mit `docker compose restart`. Du kannst dann direkt direkt auf den [adaptor:ex editor](#den-editor-öffnen) zugreifen.

Um User hinzuzufügen, müssen sie in der `config.json` File des Servers deklariert werden. Wie du unten sehen kannst gehört die Datei `root`, weshalb du ein `sudo` voranstellen musst, um die Datei zu bearbeiten.

```bash
╭─< ~/deinProjektordner
╰─> ls -la adaptor_data/            
insgesamt 8
drwxr-xr-x 2 root     root     4096 Nov 20 12:16 .
drwxrwxr-x 3 ada      ada      4096 Nov 20 12:15 ..
-rw-r--r-- 1 root     root     0 Nov 20 12:16 config.json    #gehört Root!

╭─< ~/deinProjektordner
╰─> sudo nano adaptor_data/config.json 
```

> Der `adaptor_data` Ordner wird erst erstellt, nachdem das Programm [das erste Mal gestartet](#adaptorex-starten) wurde. 


Füge einen `users` Eintrag vor der letzten `}` hinzu:

```title="config.json"
--8<-- "./docs/tutorials/server-setup/assets/config.json:13:"
```

Damit die Änderungen wirksam werden, muss adapptor:ex im Anschluss neu gestartet werden:

``` sh
docker compose restart
```

Wenn du mehr über die Config Optionen des adaptor:ex Server lernen möchtest, schau im Quellcode unter: [https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#configuration](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#configuration)

## Den Editor öffnen

Gib deine URL  in die Address Zeile deines Browsers ein, um auf deinen Client zuzugreifen. Achte dabei auf folgende Besonderheiten:

- __Minimalsetup__: `http://adaptor.my-url.org:8080` oder `http://123.45.67.89:8080`
    - Hier musst du den Standardport des Clients `8080` mit angeben.
- __Erweitertes Setup__: `https://adaptor.my-url.org`
    - Hier hast du die Portweiterleitung im `proxy` deklariert.

Falls die Authentifizierung aktiviert ist logge dich mit deinen Userdaten ein. 

Erstelle ein neues Game und nutze `adaptor:ex`, um [interaktive Theatergames](https://machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/de/getting_started.html) zu gestalten, oder lerne das [erweiterte Setup](#erweitertes-setup) kennen.

Solltest du Verbindungsprobleme zwischen Client und Server haben, oder andere Probleme erfahren, schau im Bereich [Das Setup überwachen](#das-setup-überwachen) nach.

Das Setup überwachen
--------------------

Hier ein paar Befehle die für das Debugging und die Wartung deines Setup von Nutzen sein können.

Generell gilt: Wenn du im Terminal in deinem Projektverzeichnis bist, kannst du deine Container auch über `docker compose […]` überwachen.
Dazu einfach: `cd deinProjektverzeichnis`

### Docker Container auflisten
Um zu überprüfen welche Docker Container gerade laufen nutze den Befehl:

`docker ps` zeigt alle Container, die auf deinem System laufen

oder 

`docker compose ps` zeigt nur die Container, die du über compose gestartet hast.

Hier siehst du auch unter welchem Namen du mit den Containern interagieren kannst.

### Docker Container neu starten

Starte einen Docker Container neu mit `restart`

`docker restart name_of_docker_container`

Starte alle Docker Container aus der Compose Datei neu:

`docker compose restart`

Stoppe alle Container und entferne alle Ressourcen, die durch `docker compose up` initialisiert wurden.

`docker compose down`


### Docker Container Konsole öffnen

Öffne die Console innerhalb eines Docker Containers (z.B. deinem adaptor:ex Server) mit `attach`

`docker attach name_of_docker_container`

> Funktioniert nicht beim Client.


### Browser Konsole öffnen
Bei Verbindungsproblemen drücke `strg + shift + i` (Chrome und Firefox) um die Entwicklerkonsole zu öffnen. 

### IP der Mongodatenbank anzeigen

Zeige mit `inspect` die IP unter der die MongoDB Datenbank per ssh Tunnel erreichbar ist mit folgendem Befehl an:

`docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' machinacompose_mongodb-1`

Passe auch hier den Namen des Containers ggf. an.

### Docker Container Log Datei anzeigen

Zeige mit `logs` die Log Datei eines Docker Containers an

`docker logs name_of_docker_container`

Bei Problemen jeglicher Art, __insbesondere bei Verbindungsproblemen__ ist es ratsam die Logs auf Errors hin zu prüfen:

`docker compose logs | grep -i error`

`-i` bedeutet in diesem Fall `case-insensitive`


### Updates durchführen

Um die docker container images auf dem neuesten Stand zu halten führe den docker pull Befehl aus.

`docker compose pull`

Unter [news](../../blog/index.md) werden aktuelle Updates veröffentlicht

### Hilfe erhalten

Wenn du nicht weiter kommst, kannst du auf unserem [:fontawesome-brands-discord: Discord Server](https://discord.gg/quHbQAMvF6) Hilfe erhalten. 

Ansonsten kannst du auch in der adaptor:ex Server Readme schauen, dort gibt es viele hilfreiche Infos: 
[gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server)