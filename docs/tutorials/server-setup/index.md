Server Setup Guide für adaptor:ex
=================================

This tutorial will help you if you want to use adaptor:ex as a web service that runs 24/7 and can be accessed from anywhere where there is a stable internet connection.

We will create a setup that should work on any common linux server. Even if you have a different setup but still have problems, you might find it here.

The goal is to make the adaptor:ex editor as well as all web enabled APIs accessible over the net on your own domain and secure it with user authentication and let's encrypt certificates.

What you need
----------------

### Web server

You need your own (virtual) web server that you can control via [SSH](https://de.wikipedia.org/wiki/Secure_Shell).

The operating system of the server should be a common Linux distribution (e.g. Ubuntu or Debian). Though most of the instructions in this tutorial should also be applicable to other operating systems.

### Subdomains

You need a domain name for which you can create subdomains and point them to the IP of your server.

How you can create new subdomains for your domain name depends on your domain provider.

We need one subdomain for the adaptor:ex server and one for the adaptor:ex client.

> Of course you can use the main domain name for one of them (I recommend for the client) if you don't have another website or webservice with it yet.

In the following example, wherever you need to specify your own domain, for the adaptor:ex server `adaptor-server.myurl.org` and for the adaptor:ex client `adaptor.myurl.org` is specified.

If you have two domain names set up, point them each to the IP of your server. You can find out the IP on a Linux server with the `ifconfig` command.

The IP we point the domains to can usually be found in the list displayed with `ifconfig` under `eth0`.

Adjust the `A-Record` entry for your domain names by entering the IPv4 IP of your server in both cases.

Set up the server
----------------------

### Docker

Install [Docker](https://docs.docker.com/get-docker/) on your server. If you are using a server with Ubuntu installation, follow the instructions on this page: https://docs.docker.com/engine/install/ubuntu/

### Docker Compose

We will use a Docker Compose file to set up adaptor:ex and all the necessary other services. For this we extend the [adaptor:ex docker-compose.yml](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/docker-compose.yml) file.

Create a new file `docker-compose.yml` by calling the text editor [nano](https://wiki.ubuntuusers.de/Nano/):

`nano docker-compose.yml`

Add the following Docker Compose statements:

``` yml
version: '3'

services:
  mongodb:
    image: mongo:bionic
    expose:
      - 27017
    restart: always
    command: [ "--bind_ip_all", "--replSet", "adaptor-repl" ]
    volumes:
      - mongo_db_data:/data/db
    # See also: https://www.upsync.dev/2021/02/02/run-mongo-replica-set.html
    healthcheck:
      test: test $$(echo "rs.initiate({_id:'adaptor-repl',members:[{_id:0,host:\"mongodb:27017\"}]}).ok || rs.status().ok" | mongo --port 27017 --quiet) -eq 1
      interval: 10s
      start_period: 30s
    networks:
      - mongonetwork

  adaptor_server:
    image: registry.gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server
    # build: adaptor_ex_server/ # use this to build local git tree
    restart: always
    depends_on:
      - mongodb
    tty: true
    stdin_open: true
    #entrypoint: [ "node", "index.js", "--headless"] # use this if you don't need mongodb
    entrypoint: [ "node", "index.js", "--headless", "-d", "mongodb", "-m","mongodb://mongodb:27017/?replicaSet=adaptor-repl", "--url", "https://adaptor-server.myurl.org", "--auth", "basic" ]
    volumes:
      - ./adaptor_data:/usr/src/app/data
    environment:
      - VIRTUAL_HOST=adaptor-server.myurl.org
      - LETSENCRYPT_HOST=adaptor-server.myurl.org
      - LETSENCRYPT_EMAIL=your@email.de
      - VIRTUAL_PORT=8080
    networks:
      - proxy-tier
      - mongonetwork

  adaptor_client:
    image: registry.gitlab.com/machina_ex/adaptor_ex/adaptor_ex_client
    # build: adaptor_ex_client/ # use this to build local git tree
    restart: always
    volumes:
      - ./adaptor_client_config.json:/public/config.json
    environment:
      - VIRTUAL_HOST=adaptor.myurl.org
      - LETSENCRYPT_HOST=adaptor.myurl.org
      - LETSENCRYPT_EMAIL=your@email.de
    networks:
      - proxy-tier
  proxy:
    build: proxy/
    restart: always
    ports:
      - 80:80
      - 443:443
    labels:
      com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy: "true"
    volumes:
      - certs:/etc/nginx/certs:ro
      - vhost.d:/etc/nginx/vhost.d
      - html:/usr/share/nginx/html
      - /var/run/docker.sock:/tmp/docker.sock:ro
    networks:
      - proxy-tier

  letsencrypt-companion:
    image: jrcs/letsencrypt-nginx-proxy-companion
    restart: always
    volumes:
      - certs:/etc/nginx/certs
      - vhost.d:/etc/nginx/vhost.d
      - html:/usr/share/nginx/html
      - /var/run/docker.sock:/var/run/docker.sock:ro
    networks:
      - proxy-tier
    depends_on:
      - proxy

volumes:
  certs:
  vhost.d:
  html:
  mongo_db_data:

networks:
  proxy-tier:
  mongonetwork:
```

You need to make a few changes to the Docker Compose file to make it fit your server.

1. Replace `adaptor-server.myurl.org` with your own (sub-) domain you intended for the server in 3 places under `adaptor_server`:

``` yml
    entrypoint: [ "node", "index.js", "--headless", "-d", "mongodb", "-m","mongodb://mongodb:27017/?replicaSet=adaptor-repl", "--url", "https://adaptor-server.myurl.org" ]
    volumes:
      - ./adaptor_data:/usr/src/app/data
    environment:
      - VIRTUAL_HOST=adaptor-server.myurl.org
      - LETSENCRYPT_HOST=adaptor-server.myurl.org
```

2. Replace `adaptor.myurl.org` with your own (sub-) domain you intended for the adaptor:ex client in 2 places under `adaptor_client`:

``` yml
    environment:
      - VIRTUAL_HOST=adaptor.myurl.org
      - LETSENCRYPT_HOST=adaptor.myurl.org
```

3. Replace the e-mail address `your@email.de` which should be used for the creation of the let's encrypt certificate in `adaptor_server` and `adaptor_client` (2 places):

``` yml
      - LETSENCRYPT_EMAIL=your@email.de
```

### Client Config

To allow the adaptor:ex client to locate the server API, create the config JSON file that is read from the client docker container:

`nano adaptor_client_config.json`

and insert the following configuration:

``` json
{
  "API_URL": "https://adaptor-server.myurl.org/api",
  "LOG_STORE": false,
  "APP_TITLE": "adaptor:ex",
  "LOG_LEVEL": "debug",
  "COMMENT": "LOG_LEVEL is not yet implemented"
}
```

Before saving the file, replace `adaptor-server.myurl.org` with the adaptor server subdomain you already specified in the Docker Compose file.

### Proxy Dockerfile

For the proxy Docker container that handles the distribution of incoming requests to the other containers, you still need to create the following Docker file.

Create a folder with the name `proxy`:

`mkdir proxy`

and then the Dockerfile for the proxy container:

`nano proxy/Dockerfile`

Paste this content:

``` sh
FROM jwilder/nginx-proxy:alpine

COPY uploadsize.conf /etc/nginx/conf.d/uploadsize.conf
```

In addition, we still need to create the `uploadsize.conf` file:

`nano proxy/uploadsize.conf`

and fill it with the following content:

``` conf
client_max_body_size 100M;
proxy_request_buffering off;
```

### Start the setup

Run your docker-compose.yml file with the `pull` command to download all the specified Docker containers:

`docker-compose.yml pull`

You may need to prepend `sudo` to run the command with root user privileges.

Now you can start the Docker composition with `up -d`:

`docker-compose.yml up -d`

### Server Config

Add users to the adaptor:ex server configuration with which you can access the password protected editor.

If the Docker Compose start worked you will find a new folder `adaptor_data` where also the `config.json` configuration file of the server is located.

Open the file:

`nano adaptor_data/config.json`

and add a `users` entry at the end but before the last `}`:

``` json
"users": [
    {
      "login": "ada",
      "password": "secure!?&"
    },
    {
      "login": "grace",
      "password": "secure123"
    }
  ]
```

The file should then look something like this:

``` json
{
  "host": "http://localhost",
  "port": 8081,
  "data": "./data",
  "database": {
    "type": "mongodb",
    "url": "mongodb://mongodb:27017/?replicaSet=adaptor-repl"
  },
  "level": "debug",
  "authentication": "basic",
  "headless": true,
  "url": "https://adaptor-server.myurl.org",
  "users": [
    {
      "login": "ada",
      "password": "secure!?&"
    },
    {
      "login": "grace",
      "password": "secure123"
    }
  ]
}

```

More config options for the adaptor:ex server can be found at: https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#configuration

Restart the adaptor:ex server Docker container to apply the changes:

`docker restart machinacompose-adaptor_server-1`.

The name of your adaptor:ex server Docker container may be different. Check the names of your Docker containers with the command:

`docker ps`

Monitor the setup
--------------------

If everything worked, you should be able to reach the adaptor:ex editor under the domain you set up for the client.

Log in with the user data you entered above and create a new game to check if server and database are reachable.

Here are a few commands that can be useful for debugging and maintaining your setup.

### List Docker Containers
To check which Docker containers are currently running use the command:

`docker ps`

Here you can also see under which name you can interact with the containers.

### Restart Docker Container

Restart a Docker container with `restart`.

`docker restart name_of_docker_container`

### Open Docker Container console

Open the console inside a Docker container (e.g. your adaptor:ex server) with `attach`.

`docker attach name_of_docker_container`

### Display Docker container log file

Show the log file of a Docker container with `logs`

`docker logs name_of_docker_container`

### Show IP of mongo database

Use `inspect` to display the IP at which the MongoDB database is accessible via ssh tunnel with the following command:

`docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' machinacompose_mongodb-1`

Again, adjust the name of the container if necessary.