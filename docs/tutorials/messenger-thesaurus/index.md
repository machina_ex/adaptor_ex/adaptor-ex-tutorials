Create and use a synonym dictionary
==============================================

When we query incoming messages in a messenger, we may often want to respond to similar words or phrases with the same answer.

> This tutorial assumes that you have already created a game, linked the first states in a level and created a session from your level.

Using [Collections and Data Items](../../basics/variables.md#collections-and-data-items) we can create a thesaurus or synonym dictionary that is available in each level and session. Several related terms are then grouped under a collective term and we can respond to several similar incoming messages with a single query.

Create a dictionary
-------------------------

Under `GAME > settings > COLLECTIONS` we find all globally available data of our game. The data that can be seen here is available in all levels and their sessions and can also be created, modified and deleted from any [session](../../basics/live.md#session).

Each game already has a "players" collection in which, for example, new entries (*items*) are created when one of your messenger accounts is contacted by an unknown user.

Create a new collection for the synonym dictionary by clicking on `Create Collection`.

![Screenshot: The collection overview](./assets/create_collection.png)

Name the collection e.g. "thesaurus" and click on `Create`.

![Screenshot: Create a new collection](./assets/collection_name.png)

The "thesaurus" collection is now available.

Each collection can contain multiple data items which in turn can contain multiple values, texts, numbers or lists. For a simple synonym dictionary we need only 1 item.

Click on the `Add Item` button in your "thesaurus" collection.

![Screenshot: The collection overview with thesaurus collection](./assets/add_item.png)

A window with a text editor opens. So far our document is empty and it contains only one opening and one closing curly bracket.

`{}`

adaptor:ex items are described in [JSON](https://www.json.org/json-de.html). Each JSON document in our collection starts and ends with a curly bracket.

Let's start by giving our dictionary a name so that we can easily address it later in our level. We add a variable "name" to the empty document and assign it the value "words".

Copy the following section and replace the content in the `Create Item` text field with it.

```json
{
    "name":"words"
}
```

Then click on `Create`.

![Screenshot: Add a new item](./assets/create_item.png)

If you now click on the title of the "thesaurus" collection, you will see your newly created document. Beside the `name` property some meta information has been added automatically.

Add and edit synonyms
-----------------------------------

We now add a list for each collection of synonymous terms in our "words" item.

Click on the "thesaurus" collection and then on the edit button (pencil icon) of your "words" item.

![Screenshot: Editing a collection item](./assets/edit_item.png)

The text field for editing the item will open as it did when you created the item.

For each synonym collection we now assign a list of terms to a collection term. If we want to collect similar terms for "yes" and for "no" our item will look like this:

```json
{
    "name":"words",
    "ja":["ja","jo","yes","jup","jep","okay","klar","einverstanden"],
    "nein":["nein","nö","no","nicht","nie"]
}
```

Replace the text in the text field with this or write your own synonym lists to add to the "words" item.

![Screenshot: Add synonym lists to a collection item](./assets/edit_item_modal.png)

Note the special JSON syntax. It must be followed accurately, otherwise you will get an error message and cannot update the document until all syntax errors are fixed.

- All words are enclosed in quotation marks (`"`).
- There is a colon (`:`) between the collective term and the list.
- Between all terms in the synonym list is a comma (`,`) as well as between the properties ("name", "yes", "no") of your item.
- A list (*array*) is enclosed in square brackets (`[]`).

Online you can find many tutorials and help on how to create [JSON](https://www.json.org/json-de.html) and what possibilities it offers you to structure data.

Using the synonym dictionary in the Level Editor
----------------------------------------------

When we edit a level we put our synonym lists as [variables](../../basics/variables.md). We can access the synonym dictionary via its `name` property "words".

Address by dot (`.`) notation the collection, the item and finally the collective term:

E.g.: `[[thesaurus.words.yes]]` or `[[thesaurus.words.no]]`

> You can also access the individual words in the list by additionally specifying their numerical position in the list, starting with `0`: `[[thesaurus.words.ja.3]]` results in "jup" in our case.

### Display synonyms in the log console

Try out how to access the synonym list by using the variable in a Control [Log](../../basics/actions/control/log.md) action.

Drag a [Log](../../basics/actions/control/log.md) action from the TOOLBAR to the STAGE and specify your "yes" synonym variable in `message`.

![Screenshot: Synonym variable in log action in LogYes state](./assets/log_yes.png)

Start a session and switch to the "LogYes" state. Take a look at the log console by clicking on the console icon `>_` in the upper right corner. You should see the list of all terms collected under "yes".

![Screenshot: The synonyms for yes in the log console](./assets/log_console_yes.png)

### Query incoming messages for synonyms

We use the synonym dictionary for a simple query in Telegram Messenger.

> If you haven't used the Telegram Plugin before, please have a look at the [Tutorial for setting up the Telegram Plugin](../telegram-settings/index.md).

Let's first send a message with a "yes/no" question to the player using the [Send Message](../../basics/actions/telegram/sendMessage.md) action.

Drag the [Send Message](../../basics/actions/telegram/sendMessage.md) action to the STAGE and specify a question under `message` that can be answered with "yes" or "no".

![Screenshot: Send Message Action with question](./assets/send_message.png)

Now add an [On Incoming Message](../../basics/actions/telegram/onMessage.md) action to the "YesOrNo" state. For our query we need 2 `if` conditions. One that queries the incoming message on our "yes" synonym list and one that queries on our "no" synonym list.

Add the `if` option via `Settings` and click on `Add Condition`.

![Screenshot: Add an if condition to on incoming message](./assets/dialog_if.png)

We make a `contains` query and specify our thesaurus variable in the `contains` field: `[[thesaurus.words.ja]]`

![Screenshot: On Incoming Message with thesaurus in contains query](./assets/dialog_contains.png)

Add another condition with `Add condition` and this time specify `[[thesaurus.words.no]]` for `contains`.

Create a session of this level in [Live Mode](../../basics/live.md) and specify your Telegram user as *Player* argument or link the level as `default level` to the Telegram account you have set up in the telegram plugin.

The corresponding reaction will now be triggered if the incoming message contains one of the terms specified in the thesaurus collection.

![Screenshot: The editor in live mode and the Telegram chat](./assets/example.png)

Since we use a `contains` query, it checks if one of the terms in our synonym list is **contained** in the message.

If we can use `contains`, it is enough to enter the basic forms of the synonyms in the synonym dictionary. The entry "ja" in the word list will match both an incoming message "ja gern" and "jawohl".

> With an `equals` query, we would determine if the entire message **exactly matches** one of the terms in the synonym list. In this case, the synonym list must also contain exact terms.

### Set the synonym dictionary as level argument

If you use synonym dictionaries frequently, it can be helpful to set up the dictionary item as a [*level argument*](../../basics/variables.md#item-container-and-level-arguments) for your level.

This allows you to address the synonyms more easily when editing the level. You can also find your *level arguments* in the *Variables* toolbar in the level editor and drag'n'drop the synonyms into your actions. If you work with multiple dictionaries this way you are also more flexible if you want to swap dictionaries for your levels and can assign dictionaries dynamically e.g. in the [Launch Session](../../basics/actions/control/launch.md).

Change to the 'LEVEL > config'.

![Screenshot: Level Config menu selection](./assets/level_config.png)

Click on `Add Argument` to add a new *level argument*.

![Screenshot: Add Level Argument Button](./assets/add_argument.png)

enter for the new *Level Argument* the following values

![Screenshot: Specifications for the level argument](./assets/thesaurus_argument.png)

#### 1. NAME:  ```Words```

Under this term you can address your synonym dictionary afterwards in the level.

#### 2. COLLECTION: ```thesaurus```

The name of the collection we have created for our dictionaries.

#### 3. DEFAULT QUERY: ```{name:"words"}```

The item to include as `words` in our level when a new [session](../../basics/live.md#session) is started from your level and if no other `words` item should be passed when the session is started.

> A query is a search request in a collection. We specify here that an item in the `thesaurus` collection with `name` `"words"` should be available as `words` in the level.

#### 4.

Confirm the data by clicking on the checkbox

If you now want to use a synonym list in the Level Editor, enter the variable *Level Argument* `Words` and, separated by a dot `.`, the collective term, e.g.:

`[[Words.ja]]`

You will find the synonym lists you created in the "words" thesaurus now also in the VARIABLES TOOLBAR.