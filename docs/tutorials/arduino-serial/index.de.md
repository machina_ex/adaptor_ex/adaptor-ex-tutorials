Arduino Serial in adaptor:ex
=============================

In diesem Tutorial lernst du, wie du aus adaptor:ex Kommandos an deinen Arduino-Microcontroller sendest ([Blink LED](#blink-led) und [Fade LED](#fade-led)) und wie du Daten von deinem Microcontroller in adaptor:ex empfangen und darauf reagieren kannst ([Push Button](#push-button) und [Turn Knob](#analog-read)).

Es gibt mehrere Möglichkeiten um Microcontroller wie die von Arduino in dein Game einzubinden. In diesem Tutorial nutzen wir die serielle Schnittstelle, den Serial Port an deinem Rechner. Dafür musst du adaptor auf einem Gerät mit USB-Anschluss - also einem Laptop, PC, Raspberry Pi o.ä. - installiert haben. Wie das geht, steht hier: [adaptor:ex installieren](../../index.md#installation)

Du brauchst außerdem:

- einen Microcontroller, den du über die [Arduino IDE](https://www.arduino.cc/en/software){target=_blank} programmieren kannst
- eine LED
- einen Widerstand ~ 200 Ohm
- einen Widerstand ~ 10k Ohm
- einen Push Button
- einen verstellbaren Widerstand (Potentiometer)

## Blink LED

Bleiben wir in der Tradition und beginnen damit, über adaptor:ex und unseren Microcontroller eine einzelne LED zum Leuchten zu bringen.

> In unserem Beispiel verwenden wir den legendären Arduino UNO. Für viele andere Microcontroller, die über die Arduino IDE programmiert werden können, kannst du aber genau denselben Schritten folgen.

### Der Arduino-Aufbau

Bevor wir in adaptor:ex ein Device anlegen und ein neues Level erstellen, richten wir unsere Arduino-LED-Schaltung ein und spielen einen einfachen Sketch auf den Arduino auf.

Du hast sicher schon einmal eine LED an deinen Arduino angeschlossen und den ["Blink" example sketch](https://docs.arduino.cc/built-in-examples/basics/Blink){target=_blank} aufgespielt.

Folge dem [Aufbau](https://docs.arduino.cc/built-in-examples/basics/Blink#circuit){target=_blank} in diesem Arduino-Basics-Beispiel oder nutze die Onboard LED des Arduino UNO.

Füge dem [Beispiel-Sketch](https://docs.arduino.cc/built-in-examples/basics/Blink#code){target=_blank} eine Serial-Abfrage hinzu und frage den eingehenden String auf `"led_on"` und `"led_off"` ab:

``` c++
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin 13 as an output.
  pinMode(13, OUTPUT);
  Serial.begin(115200);
  Serial.println("Hello Adaptor!");
}

// the loop function runs over and over again forever
void loop() {
  
  if (Serial.available()) {
    String incoming = Serial.readStringUntil('\n');
    
    if(incoming == "led_on"){
      digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
      Serial.println("Led is ON");
    }
    if(incoming == "led_off"){
      digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
      Serial.println("Led is OFF");
    }
  }
}
```
Nun kannst du den den modifizierten "Blink"-Sketch mit dem Arduino IDE Uploader auf deinen Microcontroller aufspielen.
### Ein adaptor:ex Device erstellen

Erstelle zunächst in adaptor:ex ein neues Game, oder öffne eins, das du schon erstellt hast.

Richte dann unter `Game > Settings` ein neues Serial Device in deinem adaptor:ex Game ein. 

Füge, wenn noch nicht geschehen, das [Devices Plugin](../../basics/plugins/devices.md) zu deinem Game hinzu

![Das Devices Plugin hinzufügen](./assets/add_devices_plugin.png)

und erstelle anschließend ein neues **Serial** Device.

![Ein Serial Device hinzufügen](./assets/add_serial_device.png)

Gib dem Device einen passenden Namen (`name`). Hier nennen wir es "ExampleArduino".

Wenn dein Arduino an den Rechner angeschlossen ist, solltest du unter **settings** nun den `Serial Port` auswählen können. Wenn du den Arduino zwischenzeitlich aus gesteckt hattest, nutze den `reload` Button des Devices Plugins, damit adaptor:ex nach neuen Geräten Ausschau hält.

![Devices und Serial Ports neu laden](./assets/reload_devices.png)

Setze die `Baud Rate` auf dieselbe, die du im Arduino Sketch verwendest. In unserem Beispiel ist die Baud Rate auf `115200` eingestellt.

Klicke auf `SAVE`, um das Serial Device zu erstellen.

![Das Device mit SAVE erstellen](./assets/save_serial_device.png)

> Um deinen Arduino zu bespielen, nutzt die Arduino IDE denselben seriellen Port wie adaptor:ex. Wenn du den Sketch modifizierst und deinen Arduino updatest, denk daran, dein Device vorher in adaptor:ex zu trennen. Verbinde das Device nach dem Upload wieder neu,um es in adaptor:ex zu benutzen.
![Device verbinden und trennen](./assets/device_connect_disconnect.png)

### Die Send Message Action

Erstelle ein neues Level in `Game > overview` mit Klick auf `Add new` und gib ihm einen beliebigen Namen.

Suche in der linken Seitenleiste, der [Toolbar](../../basics/editor.md#toolbar), unter `DEVICES` die Action [Send Message](../../basics/actions/devices/send.md). Ziehe die Action auf die [Stage](../../basics/editor.md#stage).

![Send Message action aus der Toolbar auf die Stage ziehen](./assets/add_send_message.png)
Ein neuer [State](../../basics/editor.md#state) entsteht. Klicke auf die Send Message Action, um sie zu bearbeiten.

Da wir bisher nur ein Device erstellt haben, ist unter `to` bereits unser Serial Device ausgewählt.

Öffne das *Settings* Menü und wähle `message` aus.

![message in der send message action auswählen](./assets/send_choose_message.png)

Mit `message` legen wir fest, was an das Arduino gesendet werden wird. Im Arduino Sketch haben wir festgelegt, dass die LED eingeschaltet werden soll, wenn der Serial Port "led_on" empfängt. Das ist also der text, den wir an dieser Stelle angeben müssen

![led_on in message eingeben](./assets/send_led_on.png)

Dann erstellen wir direkt noch einen zweiten State mit einer Send Message Action, in der wir die `message` "led_off" eingeben. 

Damit du weißt, in welchem State was passieren wird, benenne nun noch die States um, indem du den Namen oben im State doppelt anklickst.

![Einen zweiten state erstellen und umbenennen](./assets/send_on_off.png)

Wir haben unser Level wit genug eingerichtet um es testen zu können. Lass uns schauen, wie wir die neu angelegten Nachrichten versenden können.

### Nachrichten versenden

Um die Nachrichten an den Arduino zu senden, müssen wir eine *Live-Instanz* unseres Levels starten. Wechsle dafür in den [Live Modus](../../basics/live.md), indem du auf den dreieckigen Button oben rechts in adaptor:ex klickst.

![In den Live Modus wechseln](./assets/adaptor_live.png)

Ein neues Menü öffnet sich in der rechten Seitenleiste. Erstelle eine neue Live Session mit `Create Session` und klicke dann auf `Start Session`.

![](./assets/led_on_off_live.png)

Klicke auf den "LED_ON" State, um "led_on", oder auf den "LED_OFF" State, um "led_off" an deinen Arduino zu senden.

Wenn alles richtig eingerichtet ist, sollte die LED an deinem Microcontroller entsprechend aufleuchten oder ausgehen.

### Fehlersuche

Wirf einen Blick auf die Konsole, um zu sehen was passiert und ob Fehlermeldungen auftauchen.

Die Konsole findest du im adaptor:ex Editor oben rechts in der Ecke, links neben dem Live Modus Button.

![Die Log Konsole öffnen](./assets/adaptor_console.png)

In der Konsole kannst du auch beobachten, wenn der Arduino eine Nachricht an adaptor:ex schickt. Wenn du z.B. das Device trennst und wieder verbindest, solltest du in der Konsole die Nachricht "Hello Adaptor!", die dein Arduino im `setup()` sendet, lesen können.

Füge ggf. weitere `Serial.println()` in deinen Arduino Sketch ein, um herauszufinden, was der Arduino sendet.

> Denk dran, dass du das Device in adaptor:ex erst unter `Game > settings > Devices > Serial` trennen musst, wenn du stattdessen den Serial Monitor der Arduino IDE nutzen willst, um zu sehen, was der Arduino sendet.

### Blinken mit Timeouts

Wechsle zurück in den Level Editor Modus, indem du erneut auf das Dreieck-Icon oben rechts in der Ecke klickst.

Damit die LED nun auch ohne unser Zutun blinkt, fügen wir eine [Timeout](../../basics/actions/time/timeout.md) Action zu unseren bestehenden States hinzu. Du findest sie in der Toolbar unter `TIME`.

Ziehe die Timeout Action aus der Toolbar auf den "LED_ON" State. Bearbeiten kannst du sie, indem du darauf klickst. Gib unter `timeout` die Anzahl Sekunden an und wähle unter `next state` deinen "LED_OFF" State aus.

![Einen Timeout hinzufügen](./assets/send_led_on_timeout.png)

Füge einen weiteren Timeout zum "LED_OFF" State hinzu. Wähle diesmal "LED_ON" als `next state` aus.

> Du kannst per Punktnotation auch Bruchteile von Sekunden für den timeout angeben. Z.B. `0.5` für 500 Millisekunden.

Der vollständigkeit halber verbinden wir nun noch "LED_ON" mit dem "START" State, damit unser Level sofort beginnt, wenn wir eine Session erstellen. Öffne die [Next](../../basics/actions/control/next.md) in "START" und wähle "LED_ON" als `next state` aus. 

![Den Blink Kreislauf mit START verbinden](./assets/blink_led_editor.png)

Wechsle in den Live Modus und schließe ggf. deine zuletzt erstellte Session.

Sobald du eine neue Session erstellst, sollte deine LED zu blinken beginnen.

![Blinkende LED im Live Modus](./assets/blink_led_live.png)

Fade LED
-------------

Auch das Dimmen einer Leuchtdiode gehört zu den Basics der Arduino-Beispiele: [Fading a LED](https://docs.arduino.cc/built-in-examples/basics/Fade){target=_blank}

Wie im Kapitel [Blink LED](#blink-led) müssen wir den Arduino-Sketch auch hier durch eine Serial-Abfrage ergänzen.

``` c++
int led = 9;           // the PWM pin the LED is attached to

// the setup routine runs once when you press reset:
void setup() {
  // declare pin 9 to be an output:
  pinMode(led, OUTPUT);
  Serial.begin(115200);
  Serial.println("Hello Adaptor!");
}

// the loop routine runs over and over again forever:
void loop() {
  // set the brightness of pin 9:
  if (Serial.available()) {
    analogWrite(led, Serial.parseInt());
  }
}
```

Erstelle ein neues Level in `Game > overview` und öffne den Editor, oder arbeite im bestehenden Level weiter.

Statt einer Textmessage senden wir nun mit der [Send Message](../../basics/actions/devices/send.md) Action einen Zahlenwert zwischen 0 und 255 an den Arduino. Ziehe die Send Message Action auf die Stage, um sie zu bearbeiten.

Wähle `message` in *Settings* aus. Ändere den Datentyp von `string` auf `integer` (also auf einen Ganzzahlenwert) und gib einen Wert zwischen 0 und 255 ein.

![Einen Zahlenwert per Send Message verschicken](./assets/send_dim_led.png)

Füge weitere States mit Send Message action hinzu die einen Zahlenwert an dein Arduino Device senden. Verbinde die States mit [Timeout]() actions.

Wechsel in den Live Modus und erstelle eine neue Session. Die LED an pin 9 deines Arduino sollte nun unterschiedlich stark leuchten.

![Die LED im Livemodus ansteuern](./assets/dim_led_live.png)

---------------------------
Push Button
-------------

Wir haben bereits Nachrichten von deinem Arduino in adaptor:ex empfangen, konnten damit aber noch nichts anstellen. Das sollten wir ändern.

### Arduino Sketch

Hier ist ein anderes Arduino-Einstiegsbeispiel, das wir gut verwenden können, um Nachrichten vom Arduino an adaptor:ex zu senden: [Digital Read Serial](https://docs.arduino.cc/built-in-examples/basics/DigitalReadSerial){target=_blank}

Folge dem Aufbau, schließe einen Button an deinen Microcontroller an.

Wir nehmen ein paar kleine Änderung am Sketch vor, bevor wir in adaptor:ex loslegen. Die Baud Rate des Serial Ports muss an unser Device angepasst werden. Außerdem wollen wir, dass der Arduino nur eine Nachricht sendet wenn sich tatsächlich etwas ändert. Deshalb fügen wir eine Statusvariable hinzu.

Hier ist der angepasste sketch:

``` c++
// digital pin 2 has a pushbutton attached to it. Give it a name:
int pushButton = 2;
int buttonState = 0;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(115200);
  // make the pushbutton's pin an input:
  pinMode(pushButton, INPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  int buttonValue = digitalRead(pushButton);
  // print out the state of the button if it changed:
  if(buttonValue != buttonState) {
     Serial.println(buttonValue);
     buttonState = buttonValue;
  }
}
```
Kopiere den code in die Arduino IDE und lade ihn auf deinen Microcontroller.

### Die On Event Action

Öffne das adaptor:ex Level, das du im vorigen Kapitel genutzt hast, um eine LED ein- und auszuschalten.

Erstelle zunächst einen neuen State, den wir auslösen wollen, wenn der Button an unserem Arduino gedrückt wird. Dafür eignet sich die [Log Message](../../basics/actions/control/log.md) Action. Du findest sie in der Actions Toolbar unter `CONTROL`.

Ziehe die Log Message Action auf die Stage und benenne den neuen State in "BUTTON_PUSHED" um.

Bearbeite die Log Message Action. Schreibe in `message` "Button gedrückt".

![Die Log Message action](./assets/log_message.png)

Um auf Nachrichten, die von einem Device gesendet werden, zu reagieren, nutzen wir die [On Event](../../basics/actions/logic/onEvent.md) Action. Du findest sie im Level Editor in der Toolbar unter `LOGIC`.

Ziehe die On Event Action an eine leere Stelle auf der Stage und öffne sie, um sie zu bearbeiten.

Aktiviere unter *Settings* die `from` option und wähle dein Device aus.

Gib als Event `name` "incomingMessage" an.

Wir haben zwar nur diesen einen Button, wir müssen aber unterscheiden, ob der Button gedrückt oder losgelassen ist. Lass uns also direkt abfragen, was genau unser Arduino schickt.

Füge die `if` *Settings* option hinzu und erstelle eine Bedingung mit `Add condition`

![Eine Bedingungsabfrage hinzufügen](./assets/button_add_condition.png)

`field` müssen wir zunächst in der if `Settings` Box abwählen, da wir nur einfache Textnachrichten empfangen.

In `equals` setzen wir "1" ein, da wir erwarten, dass der Arduino "1" (bzw. "true", was in diesem Fall dasselbe ist) sendet, wenn der Button gedrückt wird. 

Als `next state` wählen wir unseren "BUTTON_PUSHED" state aus.

![Gedrückten Button abfragen](./assets/button_on_pushed.png)

Als letzten schritt im Editor verbinden wir den "START" State mit "ON_BUTTON". Öffne die *next* action im "START" State und wähle "ON_BUTTON" als `next_state` aus.

![Den Start State verbinden](./assets/start_on_button.png)

Wechsel in den Live Modus und erstelle eine neue Session. Der "START" State hat direkt an "ON_BUTTON" weitergegeben.

![Push Button Live Ansicht](./assets/button_push_live.png)

Wenn du den Button in deinem Arduino-Aufbau drückst, sollte die Session in den "BUTTON_PUSHED" State wechseln.

Öffne die Log Konsole indem du auf den Pfeil Button oben Rechts in der Titelleiste klickst. Deine Log Message sollte dort angezeigt werden.

![Die Log Console zeigt "Button gedrückt"](./assets/on_button_console.png)

Um den Vorgang zu wiederholen klicke auf "ON_BUTTON". Der *On Event* listener wird wieder gestartet und ausgelöst sobald der Button gedrückt wird. 

Turn Knob
-----------

Die [On Event](../../basics/actions/logic/onEvent.md) Action kannst du auch verwenden, um auf analoge Eingaben zu reagieren.

Schließe einen verstellbaren Widerstand - z.B. ein Potentiometer oder lichtempfindlichen Widerstand - an deinen Arduino an.

Hier ist das Beispiel in der Arduino-Dokumentation: [AnalogReadSerial](https://docs.arduino.cc/built-in-examples/basics/AnalogReadSerial){target=_blank}

Auch hier müssen wir die Baud Rate des Serial Ports auf `115200` ändern:

``` c++
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 115200 bits per second:
  Serial.begin(115200);
}
```

In adaptor:ex erstellst du nun einen State mit dem Namen "HIGH", den wir auslösen wollen, wenn der analoge Eingang einen hohen Wert erreicht.

Ziehe eine neue On Event Action auf die Stage und bearbeite sie. Wähle dein Device als `event` aus und erstelle eine neue Condition unter `Switch Event Message`.

Um in den nächsten State zu wechseln, wenn eine der eingehenden analogen Nachrichten einen bestimmten Wert überschreitet, wähle die `Greater Than` Bedingung aus.

![On Event action mit größer als Abfrage](./assets/sensor_greater_than.png)

Aktiviere den "ON_VALUE" State im Live Modus in deiner Session. Überschreitet der eingehende Wert vom Arduino die 200, wechselt die Session in den "HIGH" State.

---------------------------------------------
Nachrichten adressieren
-------------------------

Wenn unser Microcontroller mit mehreren Sensoren, LEDs, Servos usw. ausgestattet ist, brauchen wir eine Möglichkeit, die Nachrichten, die wir versenden, zuzuordnen.

Sowohl in [Send Message](../../basics/actions/devices/send.md) als auch in [On Event](../../basics/actions/logic/onEvent.md) und den anderen Logic Actions können wir Javascript Object Notation (JSON) verwenden. JSON ist eine verbreitete Notationsform, um Daten anzuordnen und zu adressieren.

Für die Arduino IDE empfiehlt es sich, auf die [ArduinoJSON](https://arduinojson.org/)-Bibliothek zurückzugreifen, um JSON-Daten zu interpretieren und zu erstellen.

Versende eine JSON-formatierte `message`, z.B. `{green:"on",red:67}`,

![Json messages versenden](./assets/send_json.png)

und reagiere auf eingehende Nachrichten in adaptor:ex, die JSON als Inhalt haben.

![JSON Nachrichten abfragen mit On Event](./assets/on_event_json.png)

Gib in `field` den Wert innerhalb der empfangenen JSON an, den du überprüfen möchtest, hier `knob`.

> Du kannst auch tiefer verschachtelte JSON-Werte auslesen, indem du sie per Punktnotation adressierst (`tiefer.verschachtelter.wert`).

Hier ist ein erweiterter Arduino-Sketch mit ArduinoJSON, der 2 Sensoren und 2 LEDs verwaltet:

``` c++
#include <ArduinoJson.h>

int green_led_pin = 13;
int red_led_pin = 9;

int button_pin = 2;
int knob_pin = A0;

StaticJsonDocument<300> sensors;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(green_led_pin, OUTPUT);
  pinMode(red_led_pin, OUTPUT);
  pinMode(button_pin, INPUT);
  pinMode(knob_pin, INPUT);
  Serial.begin(115200);
  Serial.println("Hello Adaptor!");
  sensors["button"] = 0;
  sensors["knob"] = 0;
}

// the loop function runs over and over again forever
void loop() {

  // read the input sensors:
  int button = digitalRead(button_pin);
  int knob = analogRead(knob_pin);
  
  // print out the state of the sensors if they changed:
  if(button != sensors["button"] || knob != sensors["knob"]) {
    sensors["button"] = button;
    sensors["knob"] = knob;
    serializeJson(sensors, Serial);
    Serial.println();
    delay(100)
  }
  
  if (Serial.available()) {
    
    StaticJsonDocument<300> incoming;
    deserializeJson(incoming, Serial);

    // send feedback to adaptor:ex
    serializeJson(incoming, Serial);
    Serial.println();
    
    if(incoming["green"]){
      String value = incoming["green"];
      if(value == "on"){
        digitalWrite(green_led_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
      }
      if(value == "off"){
        digitalWrite(green_led_pin, LOW);    // turn the LED off by making the voltage LOW
      }
    }
    if(incoming["red"]){
      int value = incoming["red"];
      analogWrite(red_led_pin, value);   // change the LED pwm value
    }
  }
}
```
Um [ArduinoJSON](https://arduinojson.org/) zu nutzen, musst du die Bibliothek deiner Arduino IDE erweitern. Öffne die Arduino Library Verwaltung mit [Ctrl] + [Shift] + [I] und suche nach "ArduinoJSON".

Nächste Schritte
----------------

### Netzwerk-Devices

Damit du in deinem Game-Theater-Aufbau nicht aufwendig teure USB-Repeater-Kabel verlegen musst, bietet es sich an, mit LAN- und WiFi-fähigen Microcontrollern zu arbeiten.

Wenn du dein Gerät dazu gebracht hast, Nachrichten über ein Netzwerk zu empfangen und zu senden, erstelle ein neues [Device](../../basics/plugins/devices.md) mit dem passenden Netzwerkprotokoll und verwende es in deinem Level in der [Send Message](../../basics/actions/devices/send.md) oder den Logic Actions.

Schau dir unser Tutorial zu [Netzwerk Devices](../networkdevices/index.md) an.

### Mehr Plugins

Binde ein anderes Device oder Plugin in dein Game ein und verbinde es mit deinen Microcontroller-Projekten. Passe die Soundkulisse mit dem [Sound Plugin](../../basics/plugins/sound.md) oder dem [Ableton Live Plugin](../../basics/plugins/ableton.md) an, wenn es dunkel wird, oder starte deine selbstgebaute Arduino-Schneemaschine [per Telegram-Nachricht](../../basics/plugins/telegram.md).
