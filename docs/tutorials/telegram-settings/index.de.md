Telegram Einrichten für adaptor:ex
==================================================

Bevor wir mit adaptor:ex den Messenger Service Telegram benutzen können, müssen wir dafür ein paar Dinge einrichten.

Dieses Tutorial führt dich durch die Einstellung des Telegram Plugins mit einer Telegram Developer ID und hilft dir einen ersten Telegram Bot und einen Account mit adaptor:ex zu verbinden.

Das funktioniert für Telegram sowohl im Server Modus als auch im Lokalen Modus, aber wenn du den Server nicht selbst eingerichtet hast kläre bitte immer vorher ab, ob du die Erlaubnis hast das Telegram Plugin zu benutzen. 

Vorbereitung
------------

> <span style="color:red">:material-alert-outline:</span> Sei dir im Klaren darüber, dass andere mit Zugriff auf den adaptor:ex server, deine Telegram API credentials (dazu kommen wir weiter unten) und ggf. deine Telefonnummer lesen können. Gehe verantwortlich mit diesem Tool um. Telegram blockt User Accounts wenn diese als Spam auffallen oder markiert werden. Außerdem musst du sicherstellen dass alle Daten von Menschen die mit eurer adaptor:ex Instanz via telegram kommunizieren, geschützt sind und alle Datenschutzrichtlinien die für euch zutreffen eingehalten werden. Benutze auf keinen Fall deine private Nummer! Deine Freunde wissen ja nicht dass du mit Telegram Apps spielst und ihre Daten verarbeitet werden!

Wenn du die Erlaubnis hast und Datenschutz mäßig sicher bist, dass alles in Ordnung ist, brauchst du folgendes:

- Zugriff auf adaptor:ex
- ein bereits angelegtes Game in adaptor:ex
- Eine der offiziellen Telegram Apps mit einem Telegram Konto 
- Wenn du einen (echten) Account mit adaptor:ex steuern willst brauchst du auch eine weitere Telefonnummer für die du ebenfalls ein Konto in einer der Offiziellen Apps angelegt hast.

Telegram Einrichten
-------------------

### Developer Account bei Telegramm anlegen

1. Wir öffnen das Game, in dem wir Telegram benutzen wollen und wählen dort in der oberen Leiste `Game > Settings` aus.

2. Dort fügen wir aus der Liste der inaktiven Plugins das Plugin `Telegram` hinzu und öffnen es.

    ![screenshot](./assets/1.png)

    Wir sehen 2 Felder: `api_id` und `api_hash`.

3. Um `api_id` und `api_hash` ausfüllen zu können, müssen wir uns einen developer account bei <a href='https://my.telegram.org'>`https://my.telegram.org`</a> anlegen:

    ![screenshot](./assets/2.png)

4. Dort loggen wir uns dann ein und erstellen unter `API developer tools` die credentials für eine client app. Mehr Infos dazu unter: [https://core.telegram.org/api/obtaining_api_id](https://core.telegram.org/api/obtaining_api_id) 

    ![screenshot](./assets/2a.png)

5. Nachdem wir das getan haben sollten wir unter `API developer tools` unsere persönliche `api_id` und `api_hash` bekommen haben und können diese nun in die entsprechenden Felder in unseren adaptor:ex Game Settings eintragen:

    ![screenshot](./assets/3.png)
    ![screenshot](./assets/4.png)

6. Anschließend klicken wir auf den SAVE button um unser Game als Telegram Client zu aktivieren.

    ![screenshot](./assets/5.png)
    
Ab sofort ist unsere adaptor:ex Instanz ein Telegram Client. Also in etwa so wie die Telegram App auf unserem Smartphone.

Wir können nun einen [Telegram Bot](#einen-telegram-bot-verbinden) oder einen [Account](#einen-telegram-account-verbinden) über das Telegram Plugin mit adaptor:ex verbinden.

### Einen Telegram Bot verbinden

Über ein existierenden Telegram Konto ist es einfach einen neuen Telegram Bot zu erstellen. Kontaktiere den [@BotFather](https://t.me/botfather) Bot account über deine Telegram APP und schreibe die Nachricht `/newbot` in den Chat.

Folge den Anweisungen um deinem Bot einen Anzeige- und einen Nutzernamen zu geben. In unserem Beispiel nennen wir den Bot "Stage_Manager" und geben ihm den Nutzernamen "stage_manager_bot"

![screenshot](./assets/botfather_newbot.png)

In der abschließenden Nachricht erhältst du einen `token`, den du benötigst um den Bot in adaptor:ex zu verbinden.

Öffne in deinem adaptor:ex Game die Settings für dein Telegram Plugin. Füge mit dem blauen Plus Button unten unter `ITEMS`/`BOTS` einen neuen Bot hinzu.

Gib dem Bot einen Namen, hier `StageManager` und kopiere den `token` aus dem telegram Chat mit [@BotFather](https://t.me/botfather) in das Feld `bot api token`.

![screenshot](./assets/bot_add.png)

Klicke anschließend auf SAVE. Du solltest jetzt ein adaptor:ex [LEVEL mit deinem Telegram Bot verbinden](#telegram-account-und-bot-mit-level-verbinden) und Nachrichten empfangen und versenden können.

#### Nachrichten in Gruppenchats

Wenn du mit deinem Bot nicht nur auf Nachrichten in Direkt Chats sondern auch in Gruppenchats auf alle Nachrichten reagieren willst, musst du noch eine weitere Einstellung vornehmen.

Öffne den Chat mit [@BotFather](https://t.me/botfather) in deiner Telegram APP. Schreibe `/setprivacy` in den Chat und gib anschließend den Nutzernamen deines Bots mit vorangehendem `@` Zeichen an (hier `@stage_manager_bot`).

Schreibe dann `Disable` um die Einstellung anzupassen.

![screenshot](./assets/botfather_setprivacy.png)

Du kannst jetzt mit deinem Bot auf alle eingehenden Nachrichten in Gruppenchats in denen sich der Bot befindet reagieren. Andere Nutzer in dem Gruppenchat werden über diese Möglichkeit informiert.

### Einen Telegram Account verbinden

> <span style="color:red">:material-alert-outline:</span> beachte, dass adaptor:ex über das Plugin vollen Zugriff auf den telegram account bekommt

Um einen neuen Account anzulegen, müssen wir unsere Telefonnummer in unserer Telegram Plugin App anmelden. Dazu fügen wir einen Telegram Account im Plugin hinzu (der blaue Plus Button unten unter `ITEMS`/`ACCOUNTS`).

Dem Account geben wir einen guten Namen (bei machina eX Games nehmen wir normalerweise den Namen der Figur, dessen Telegram Account das in unserer Spielwelt sein soll, hier z.B. `Alice`)

![screenshot](./assets/6.png)

Unter `phone number` geben wir die Telefonnummer ein mit der wir uns einloggen wollen. 

> Diese Telefonnummer sollte schon bei Telegram angemeldet sein und wir brauchen Zugriff auf Nachrichten von Telegram im nächsten Schritt. Stelle also sicher, dass du a) angemeldet bist und b) Zugriff mit einer Telegram App (z.b. auf einem Smartphone) hast und Nachrichten von Telegram an diese Nummer empfangen kannst.

Nachdem wir unsere Telefonnummer eingetragen haben, klicken wir auf SAVE.

Das sollte nun unsere Telefonnummer/unseren Telegram Account durch unsere Plugin Telegram App bei Telegram anmelden. Wir sehen daher (2tes Bild) einen Prompt in adaptor:ex der uns nach einem Login Code fragt und (1tes Bild) sollten in Telegram von Telegram eine Nachricht an die gerade registrierte Telefonnummer bekommen die eben diesen Login Code beinhaltet. Das funktioniert genauso als würden wir uns zum ersten Mal mit einer neuen App/einem neuen Gerät in einen Telegram Account anmelden.

![screenshot](./assets/9.png)

![screenshot](./assets/10.png)

Wir tragen also den Login Code in den Prompt unter `code` ein und klicken `send`.

Wenn alles geklappt hat, sollten wir nun eine zweite Nachricht von Telegram bekommen, die ungefähr so aussieht wie in diesem Bild:

![screenshot](./assets/11.png)

Das ist der Telegram Service, der nochmal sicherstellt dass wir auch wissen, dass nun eine neue App Zugriff auf das Telegram Konto mit unserer Telefonnummer hat.

Sollte die LED oben rechts in unserem Telegram Account Settings Feld nicht grün leuchten, überprüfe den Status zunächst indem du auf den Reload Button :material-cached: des Accounts klickst.

![screenshot](./assets/12.png)

adaptor:ex und der Telegram Plugin empfangen jetzt alle Nachrichten für diese Telefonnummer, genauso wie die anderen Telegram Apps mit denen diese Nummer angemeldet ist.

Sollte die Anmeldung fehlgeschlagen sein kannst du mit Klick auf den Connect Button (das Stecker Symbol oben rechts in unserem Telegram Account Settings Feld :material-power-plug-outline:) einen neuen Verbindungsversuch starten.

![screenshot](./assets/8.png)

### Telegram Account und Bot mit LEVEL verbinden

Wir müssen adaptor:ex nun noch sagen, in welches Level es die eingehenden Nachrichten leiten soll: Dazu gehen wir z.B. in unseren Telegram Plugin Account `Alice` und klicken auf den `Settings` Button. Genauso verfährst du, wenn du stattdessen einen Bot angelegt hast.

Dort wählen wir die Option `default level` aus und tragen ein Level ein, in das alle eingehenden Telegram Nachrichten geleitet werden sollen, die von Telefonnummern kommen, die gerade in keinem anderen Level aktiv sind.

![screenshot](./assets/13.png)

In unserem Beispiel ist das hier das Level `Send_Telegram`

![screenshot](./assets/14.png)

Immer wenn nun eine Telegram Nachricht an unseren Account `Alice` geschickt wird, schaut das adaptor:ex Telegram Plugin nach, ob die schreibende Telefonnummer schon in einem Gespräch in einem unserer Levels ist. Wenn dem nicht so ist, erstellt sie automatisch eine `Player Variable` mit den Telegram Daten der schreibenden Person und startet eine neue [Session](../../basics/live.md#session) des `Default Levels`. 

Dort können wir dann auf Nachrichten der `Player` warten, oder ihnen als `Alice` Telegram Nachrichten schicken, oder sie in andere Levels weiterleiten und und und ...

Damit ist das Setup unserer Telegram Plugin App als auch unserer ersten Agents `Alice` und `Stage Manager`abgeschlossen. 

Wir können natürlich mehrere Accounts und Bots innerhalb unserer Plugin App anmelden. Dafür brauchen wir aber weitere bei Telegram bereits registrierte Telefonnummern oder Telegram Bots.

### Loslegen

Wenn alles eingerichtet ist, geht es hier zum ersten Tutorial für ein Telegram-Adventure-Game: [Storytelling mit Telegram und adaptor:ex](../telegram-basics/index.md){target=_blank}