Setup Telegram for adaptor:ex
==================================================

Before we can use the messenger service Telegram with adaptor:ex, we need to go through a few setup steps.

The Tutorial will walk you through setting up the Telegram Plugin with a telegram Developer ID and connecting your first Telegram Bot and Account.

This works for Telegram in server mode as well as in local mode, but on a server that you did not set up yourself, always check if you have the permission to use the Telegram plugin.

Preparation
------------

> <span style="color:red">:material-alert-outline:</span> Be aware that others with access to the adaptor:ex server can read your phone number and Telegram API credentials (we'll get to that below)! Be responsible with this tool. Telegram will block user accounts if they are found to be spam or flagged. Also, you need to make sure that all data of people communicating with your adaptor:ex instance via telegram is protected and all privacy policies that apply to you are followed. If you add an Account, do not use your private number! Your friends don't know that you are playing with Telegram apps and their data is being processed!

If you have permission and are confident that everything is in order privacy-wise, here's what you need:

- access to adaptor:ex
- an already created game in adaptor:ex
- One of the official Telegram apps with a Telegram account
- If you want to control a (real) account with adaptor:ex you also need another phone number for which you have also created an account in one of the Official Apps.

Telegram Setup
-------------------

### Create a developer account with Telegram

1. open the game in which we want to use Telegram and select `Game > Settings` in the upper bar.

2. there we add the plugin `Telegram` from the list of inactive plugins and open it.

    ![screenshot](./assets/1.png)
    
    We see 2 fields: `api_id` and `api_hash`.

3. to fill in `api_id` and `api_hash` we have to create a developer account at <a href='https://my.telegram.org'>`https://my.telegram.org`</a>:

    ![screenshot](./assets/2.png)

4. there we log in and create the credentials for a client app under `API developer tools`. More info on this can be found at: [https://core.telegram.org/api/obtaining_api_id](https://core.telegram.org/api/obtaining_api_id)

    ![screenshot](./assets/2a.png)

5. after we have done that we should have gotten our personal `api_id` and `api_hash` under `API developer tools` and can now enter them into the appropriate fields in our adaptor:ex Game Settings:

    ![screenshot](./assets/3.png)
    ![screenshot](./assets/4.png)

6. Then we click on the SAVE button to activate our Game as a Telegram client.

    ![screenshot](./assets/5.png)
    
As of now, our adaptor:ex instance is a Telegram client. So kind of like the Telegram app on our smartphone.

We can now connect a [telegram bot](#connect-a-telegram-bot) or an [account](#connect-a-telegram-account) to adaptor:ex via the Telegram plugin.

### Connect a Telegram Bot

Using an existing Telegram account, it is easy to create a new Telegram bot. Contact the [@BotFather](https://t.me/botfather) bot account through your Telegram APP and write the message `/newbot` in the chat.

Follow the instructions to give your bot a display name and a username. In our example we name the bot "Stage_Manager" and give it the username "stage_manager_bot".

![screenshot](./assets/botfather_newbot.png)

In the final message you get a `token` which you need to connect the bot in adaptor:ex.

In your adaptor:ex game, open the settings for your Telegram plugin. Add a new bot with the blue plus button at the bottom under `ITEMS`/`BOTS`.

Give the bot a name, here `StageManager` and copy the `token` from the telegram chat with [@BotFather](https://t.me/botfather) into the field `bot api token`.

![screenshot](./assets/bot_add.png)

Then click SAVE. It should now be possible to [connect a LEVEL to your Telegram bot](#telegram-account-and-bot-connect-with-level) and adaptor:ex will be able to receive and send messages.

#### Messages in group chats

If you want your bot to respond not only to messages in direct chats but also to all messages in group chats, you need to make one more setting.

Open the chat with [@BotFather](https://t.me/botfather) in your Telegram APP. Write `/setprivacy` in the chat and then enter the username of your bot preceded by `@` (here `@stage_manager_bot`).

Then write `Disable` to adjust the setting.

![screenshot](./assets/botfather_setprivacy.png)

You can now respond with your bot to all incoming messages in group chats the bot is part of. Other users in the group chat will be informed about this capability.

### Connect a Telegram Account

> <span style="color:red">:material-alert-outline:</span> note that adaptor:ex gets full access to the telegram account via the plugin.

To create a new account, we need to register our phone number in our Telegram plugin app. To do this, we add a Telegram account in the plugin (the blue plus button at the bottom under `ITEMS`/`ACCOUNTS`).

Give the account a good name (in machina eX games we usually use the name of the character whose Telegram account this should be in our game world, here e.g. `Alice`).

![screenshot](./assets/6.png)

Under `phone number` we enter the phone number we want to log in with.

> This phone number should already be logged in to Telegram and we need access to messages from Telegram in the next step. So make sure you a) are logged in and b) have access with a Telegram app (e.g. on a smartphone) and can receive messages from Telegram to this number.

After we have entered our phone number, click SAVE.

This should now register our phone number/our Telegram account with Telegram through our plugin Telegram App. So we see (2nd image) a prompt in adaptor:ex asking us for a login code and (1st image) should get a message in Telegram from Telegram to the just registered phone number containing this login code. This works the same way as if we were logging into a Telegram account for the first time with a new app/device.

![screenshot](./assets/9.png)

![screenshot](./assets/10.png)

So we enter the login code in the prompt under `code` and click `send`.

If everything worked, we should now get a second message from Telegram that looks something like in this picture:

![screenshot](./assets/11.png)

That's the Telegram service, which ensures that we also know that a new app (adaptor:ex) has access to the Telegram account with our phone number.

If the LED in the top right corner of our Telegram Account Settings field is not green, check the status by clicking on the account's reload button :material-cached:.

![screenshot](./assets/12.png)

adaptor:ex and the Telegram plugin now receive all messages for this phone number, as well as the other Telegram apps with which this number is registered.

If the login failed you can start a new connection attempt by clicking on the Connect button (the plug icon in the upper right corner of our Telegram Account Settings field :material-power-plug-outline:).

![screenshot](./assets/8.png)

### Connect Telegram account and bot with LEVEL

We now need to tell adaptor:ex to which level it should route the incoming messages: To do this, we go to our Telegram plugin account `Alice`, for example, and click on the `Settings` button. The same goes if you have created a bot instead.

There we select the option `default level` and enter a level in which all incoming Telegram messages should be routed that come from phone numbers that are not currently active in any other level.
    
![screenshot](./assets/13.png)

In our example this is the level 'Send_Telegram'.

![screenshot](./assets/14.png)

Whenever a Telegram message is sent to our account `Alice`, the adaptor:ex Telegram plugin looks to see if the writing phone number is already in a conversation in one of our levels. If not, it automatically creates a `Player Variable` with the Telegram data of the writing person and starts a new [Session](../../basics/live.md#session) of the `Default Level`.

There we can wait for messages from the `Player`, or send them Telegram messages as `Alice` or `Stage Manager`, or forward them to other levels and and and ...

This completes the setup of our Telegram Plugin App and our first agents `Alice` and `Stage Manager`.

We can of course register several accounts and bots within our plugin app. However, for this we need additional telephone numbers or Telegram bots registered with Telegram.

### Get started

Once everything is set up, here's the first tutorial for a Telegram adventure game: [Storytelling with Telegram and adaptor:ex](../telegram-basics/index.md){target=_blank}