Licht kontrollieren mit adaptor eX und der ENTTEC DMXUSB Pro Box
=================================================================

adaptor:ex ist Teil des Projekts MACHINA COMMONS und wird im Programm "Digitale Entwicklung im Kulturbereich" der Berliner Senatsverwaltung für Kultur und Europa gefördert.

adaptor:ex ist eine open-source Software Engine, die es ermöglicht, interaktive Theatergames und -installationen umzusetzen. Ein Teilbereich davon ist die Kontrolle von Lichtsystemen über das DMX Protokoll.

## Vorab:

Wir gehen davon aus, dass du oder eine Techniker:in bereits eine adaptor:ex server instanz auf deinem Rechner installiert hat und du Zugriff über einen Browser auf die Bedienoberfläche (den sogenannten Client) von adaptor:ex hast.

Wichtig hierbei: Der USB-DMX-PRO-PLUGIN funktioniert nur wenn er auf einem Rechner mit USB Anschlüssen läuft und adaptor:ex Zugriff auf die USB-Schnittstellen des Computers hat (wir empfehlen hier z.B. NICHT die docker installation zu benutzen, sondern z.B. via npm oder ganz manuell zu installieren).

Um dieses Tutorial durchzugehen solltest du dich schon mit den [Basics von adaptor:ex](../../basics/editor.md) auseinandergesetzt haben.

Hilfreich, aber nicht notwendig ist zum Beispiel schon das Tutorial [Storytelling mit Telegram und adaptor eX](../telegram-basics/index.md) durchgearbeitet zu haben.

Du brauchst außerdem:

- eine ENNTEC DMXUSBPRO Box 
- USB-Kabel 
- passendes DMX Kabel
- eine LED PAR die mit 4 DMX Kanälen angesteuert werden kann - wir haben in diesem Tutorial die LedPar36 von Stairville benutzt aber jede andere wird es auch tun. (alternativ kannst du natürlich auch einen Dimmer anschliessen und 3 verschiedene Leuchten auf Kanal 2-4 ansteuern)

<div class="page"/>

## Schritt für Schritt

### Plugin hinzufügen und konfigurieren

1. Lege ein neues Game an. Dann wähle unter GAME > SETTINGS die Settings aus und füge das USB-DMX-PRO Plugin hinzu.

![screenshot](./assets/1.png)

2. Nun verbinde deine USB-DMX-PRO Box mit dem USB Kabel mit dem Computer auf dem der adaptor:ex server läuft.

![screenshot](./assets/dmxbox.jpeg){ style='height: 300 }

3. In den Game Settings, klicke auf den USB-DMX-PRO Plugin um ihn aufzuklappen, klicke auf den SETTINGS Button, und markiere serialPort

![screenshot](./assets/2.png)

4. Anschliessend wähle im Formular unter `serialPort` den korrekten Portnamen aus (auf MacOS z.B. beginnen die ENTTEC DMX Devices meist mit `EN`).

![screenshot](./assets/3.png)

5. Speichere das ganze und klicke anschliessend auf den CONNECT BUTTON (der mit dem Stecker neben dem roten Kreis). Wenn alles klappt, sollte der kreis daneben nun grün werden. Und die grüne LED an deiner DMX Box sollte nun blinken.

![screenshot](./assets/4.png)
![screenshot](./assets/5.png)

    <br>

![screenshot](./assets/dmxboxblink.jpeg){ style='height: 300 }

Damit bist du fertig mit der Konfiguration.

### Leuchten / DMX Empfangsgeräte anschliessen

1. Verbinde nun deine Leuchte mit dem Strom und via DMX Kabel mit der USB-DMX-PRO Box.

![screenshot](./assets/lamp.jpeg){ style='height: 300 }

2. Unter `GAME > OVERVIEW` klicke auf `NEW LEVEL` und kreire ein Level mit Namen deiner Wahl. Wir nennen unseres `Lichterspiel`. Anschliessend öffne das Level im Level Editor.

3. Links in der ACTION BAR scrolle runter. Du findest dort unter USB-DMX-PRO die `setLights` Action. Dragge diese auf die Stage und kreire deinen ersten State: Wir wollen am Anfang die Leuchte aus schalten. Also nennen wir den ersten State `BLACK`. 

![screenshot](./assets/6.png)

4. Verbinde den `START` State mit `BLACK` indem du `BLACK` in der `NEXT STATE` Action in `START` angibst.

5. Nun öffne `setLights` in `BLACK`. Du siehst nun zwei Spalten: Links `Channel`, den DMX Kanal den du ansteuern/ändern möchtest und rechts den Wert (0 zur Zeit) zu dem du den Channel ändern möchtest. 

    Kanal 1 muss bei unserer Leuchte immer auf 0 bleiben, damit sie im DMX Modus bleibt. Wenn du eine andere Leuchte benutzt oder ein anderes DMX Gerät wie einen Dimmer, kann das anders funktionieren. Informiere dich am besten auf welchen Kanälen deine Leuchte angesprochen werden möchte! In unserem Fall ist es relativ simpel: Kanal 1 muss auf 0 sein, damit die Leuchte DMX Signale empfangen kann, Kanal 2 ist rot, Kanal 3 ist grün und Kanal 4 ist blau. Im weiteren Verlauf des Tutorials gehen wir also von diesen Einstellungen aus und du musst die Anweisungen dann ggf. an dein Gerät anpassen.

![screenshot](./assets/7.png)

6. Füge mit dem `ADD ROW` Button drei weitere Kanäle hinzu. setze alle 4 Kanäle (1,2,3,4) alle auf den Wert 0. Damit ist `BLACK` jetzt auch wirklich schwarz und schaltet alle Farben unserer RGB Par aus.

![screenshot](./assets/8.png)

7. Nun füge eine `NEXT` Action in `BLACK` ein und bestimme den State `ROT` als den Folgestate. Der wird also direkt aufgerufen, wenn BLACK alle Lichter ausgeschaltet hat.

8. Nun ziehe eine neue `setLights` Action auf die Bühne und kreiere mit ihr den State `ROT`. Um die Lampe hier auf rotes Licht umzuschalten lege wieder 4 DMX Kanäle an und setze alle auf 0 ausser Kanal 2. Dessen Wert legen wir auf 255 (ganz hell).

![screenshot](./assets/9.png)

9. Jetzt ist es Zeit endlich mal Licht zu machen. Aktiviere den Live Modus (rotes Dreieck oben rechts) und starte eine Session. Wenn alles richtig angeschlossen ist, sollte deine DMX Leuchte nun rot leuchten.

![screenshot](./assets/leveleditor1.png)
![screenshot](./assets/livemode.png)

> Wenn dem nicht so ist, überprüfe nochmal alle Kabelverbindungen, die Kanal Einstellungen der Leuchte und ob alle Schritte wie oben beschrieben geklappt haben. 

Yay!!!! Licht!

### Eine Lichtorgel.

10. Nun füge eine `Timeout` Action in dem State `ROT` hinzu und setze den Timeout auf 2 Sekunden. Als Next State lege `GRÜN` an.

![screenshot](./assets/10.png)

11. Wiederhole 8. - 11. für die States `GRÜN` und `BLAU`, und modifiziere die Werte dementsprechend (`setLigthts` Werte für Grün sind z.B. 0, 0, 255, 0 usw.) und verbinde in 10. mit timeout `GRÜN` zu `BLAU` und schliesslich `BLAU` zu `BLACK`.
    
![screenshot](./assets/11.png)

12. Nun hast du eine zirkuläre Struktur geschaffen, bei der alle 2 sekunden das Licht von ROT zu GRÜN, zu BLAU welchselt. Probier es aus: Starte eine neue Session und bewundere deine Lichtorgel.

![screenshot](./assets/light.png)

### weitermachen

Du weisst jetzt, wie du mit adaptor:ex Licht mit über eine USB-DMX-PRO Box ansteuerst. Damit kannst du natürlich alles mögliche bauen. Zum Beispiel könntest du die die [Telegram Tutorials](../telegram-basics/index.md) anschauen und Lichter via Telegram Messanger ansteuern, oder du kannst dir [anschauen wie du Arduinos oder andere Devices an adaptor:ex anschliesst](../networkdevices/index.md) und dann diese mit einem ganzen Lichtsystem in deiner Bühne vernetzen. Wir sind sicher dir fällt bald noch mehr ein als uns!


