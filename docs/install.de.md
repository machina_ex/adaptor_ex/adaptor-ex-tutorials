---
authors: 
    - Lasse Marburg
date: 2024-07-11
hide:
  - navigation
---

Installation und Updates
==========================

Es stehen drei Installationsmöglichkeiten zur Auswahl: 

1. **Installation per NPM Package Manager [:material-book-open-blank-variant:](#installieren-mit-npm)**

    Mit dem NPM Package Manager kannst du adaptor:ex einfach über die Kommandozeile auf jedem gängigen Betriebssystem installieren und als NPM Anwendung ausführen.

    Wir empfehlen die [Installation per NPM Package Manager](#installieren-mit-npm) wenn du adaptor:ex lokal auf deinem Rechner oder in einem internen Netzwerk nutzen willst.

    Eine lokale Installation erlaubt es dir die Anschlüsse an deinem Rechner zu nutzen. Z.B. den Seriellen Anschluss per USB oder eine Soundkarte.

2. **Installation mit DOCKER [:material-book-open-blank-variant:](#installieren-mit-docker)**

    Im besonderen für eine Server Installation empfehlen wir die [Installation in einem Docker Container](#installieren-mit-docker). 

    Du kannst unser Docker Compose File nutzen um ein umfangreiches und stabiles Setup mit unabhängiger Server und Client Anwendung und MongoDB Integration zu erstellen.

    Das [Server Setup](./tutorials/server-setup/index.md) Tutorial hilft dir dabei adaptor:ex mit Docker und Docker Compose auf einem externen Server zu installieren, zu sichern und für alle, die an deinem Projekt mitarbeiten, zugänglich zu machen.

3. **Installation von SOURCE [:material-book-open-blank-variant:](#installieren-von-source-mit-npm)**

    Lade den adaptor:ex Quellcode herunter und nutze NPM um die Server Anwendung und den Client unabhängig voneinander zu installieren und auszuführen.

    Eine [Installation ausgehend vom Quellcode](#installieren-von-source-mit-npm) erlaubt es dir Änderungen an der Software vorzunehmen.

    Wir empfehlen die Installation vom Quellcode wenn du etwas am Code anpassen oder an der Entwicklung von adaptor:ex mitwirken willst.

Hinweise
--------

### Command Line Terminal

Um adaptor:ex zu installieren musst du derzeit die Kommandozeile deines Betriebssystems nutzen.

=== "macOS"
    Unter macOS suche das Programm "Terminal".
=== "Linux"
    Unter Linux kannst du das terminal mit der Tastenkombination ++ctrl+alt+"T"++ öffnen.
=== "Windows"
    Unter Windows suche die Anwendung "Eingabeaufforderung" bzw. "cmd".

### Konfiguration

Die adaptor:ex Server Anwendung bietet verschiedene Möglichkeiten um deine Installation anzupassen. In der [Readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#configuration) des adaptor:Ex Server Code repositories sind alle Konfigurationsmöglichkeiten beschrieben.

Einige Funktionen des adaptor:ex Server können zudem über die Kommandozeile gesteuert werden: [Commands](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#commands)

### Multi User Server Setup

Wirf einen Blick in  das [Server Setup](./tutorials/server-setup/index.md) Tutorial um adaptor:ex mit Docker und Docker Compose auf einem externen Server zu installieren. Hier findest du auch heraus, wie du den Internetzugriff auf adaptor:ex sichern und für alle, die an deinem Projekt mitarbeiten, zugänglich machen kannst.

### Updates

Wir werden in der Zukunft häufig Fehler beheben, Dinge (hoffentlich) verbessern und neue Features hinzufügen. Führe hin und wieder ein Update via [NPM](#updates-mit-npm) oder [Docker](#updates-mit-docker) durch, um auf dem neuesten Stand zu bleiben.

Neuigkeiten und Updates werden zeitnah im [News Blog](./blog/index.md) und auf unserem [:fontawesome-brands-discord: Discord Kanal](https://discord.gg/quHbQAMvF6) veröffentlicht.

### Hilfe bekommen

Melde dich bei uns, wenn du Schwierigkeiten hast, adaptor:ex zu installieren. Schreib uns an [tech@machinaex.de](mailto:tech@machinaex.de) oder schau auf unserem [Discord Kanal](https://discord.gg/quHbQAMvF6) vorbei.

Installieren mit NPM
--------------------

Bevor Du mit der Installation von adaptor:ex fortfahren kannst, brauchst Du die aktuelle Version von NodeJS. Die nötigen Installationsdateien für dein Betriebssystem findest Du [hier](https://nodejs.org).

Sobald Du NodeJS installiert hast, öffne deine Konsole bzw. das Terminal und führe in der Kommandozeile die Installation mit npm aus

=== "macOS & Linux"
    ```console
    sudo npm install -g adaptorex --unsafe-perm
    ```
=== "Windows"
    ```console
    npm install -g adaptorex --unsafe-perm
    ```

Wenn die Installation erfolgreich war, starte adaptor:ex mit dem Kommando

```console
adaptorex
```

Möglichkeiten adaptor:ex zu Konfigurieren findest du in der [adaptor:ex server readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#install)

Um den adaptor:ex server wieder zu beenden benutze die Tastenkombination 

=== "Windows and Linux"
    ++ctrl+"C"++
=== "macOS"
    ++cmd+"C"++

oder gib `quit` in die Kommandozeile ein und drücke ++enter++.

### Updates mit NPM

Lade dir die neueste Version von adaptor:ex herunter mit:

=== "macOS & Linux"
    ```console
    sudo npm install -g adaptorex --unsafe-perm
    ```
=== "Windows"
    ```console
    npm install -g adaptorex --unsafe-perm
    ```

Mit `npm outdated -g` kannst du überprüfen ob eine neue Version zur Verfügung steht:

=== "macOS & Linux"
    ```console 
    sudo npm outdated -g
    ```
=== "Windows"
    ```console 
    npm outdated -g
    ```

Installieren mit Docker
-----------------------

Lade Docker für dein Betriebssystem herunter und folge den Installationsanweisungen auf [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)

Erstelle einen neuen Ordner in dem du adaptor:ex installieren willst.
 
Öffne deine Kommandozeile, wechsle mit dem `cd` command in das adaptor:ex Verzeichnis und lade die [adaptor:ex docker-compose](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/docker-compose.yml) Datei herunter:

```console
curl -o docker-compose.yml https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/raw/main/docker-compose.yml
```

Lade die config Datei für den adaptor:ex client herunter mit:

```console
curl -o adaptor_client_config.json https://docs.adaptorex.org/assets/adaptor_client_config.json
```

Führe die Docker Compose Datei aus:

```console
docker compose up -d
```

adaptor:ex Server, Client, Datenbank und Plugin Erweiterungen werden heruntergeladen und installiert.

> Wenn du von einem anderen Rechner auf adaptor:ex zugreifen willst, musst du in der `adaptor_client_config.json` die externe URL oder IP des adaptor servers angeben. Ersetze in `http://localhost:8081/api` "localhost" mit der IP des Rechners, auf dem du den docker container aufgesetzt hast, oder mit der URL deines Servers oder Rechners.

Für eine sichere installation auf einem Server wirf einen Blick in das [Server Setup](./tutorials/server-setup/index.md) Tutorial.

In der [adaptor:ex server readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#install) kannst du mehr über die Konfiguration von adaptor:ex erfahren.

### Updates mit Docker

Um die adaptor:ex Docker installation zu aktualisieren wechsle in das adaptor:ex Verzeichnis. Dort führe folgende Kommandos aus:

```console
docker compose pull
```

und anschließend

```console
docker compose up -d
```

Installieren von Source mit NPM
-------------------------------

Bevor Du mit der Installation von adaptor:ex fortfahren kannst, brauchst Du die aktuelle Version von NodeJS. Die nötigen Installatiosdateien für dein Betriebssystem findest Du [hier](https://nodejs.org).

Hole dir adaptor:ex Server und Client:

**Server**

Klone dir das aktuelle GitLab-Repository des adaptor:ex Servers

```console
git clone https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server.git
```

Oder lade es hier herunter: [https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server)

**Client**

Klone dir das aktuelle GitLab-Repository des adaptor:ex Clients: 

```console
git clone https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_client.git
```

oder lade hier herunter: [https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_client](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_client)

Jetzt kannst du den Server und den Client mit [NPM](https://www.npmjs.com/) lokal installieren.

Wechsel im Terminal (der Konsole) in das Verzeichnis in das du den Server oder Client geladen oder geklont hast.

```console
cd adaptor_ex_server/
```

bzw.

```console
cd adaptor_ex_client/
```

Und führe in beiden Verzeichnissen folgenden Befehl aus:

```console
npm install
```

Wenn die Installation erfolgreich war, starte den adaptor:ex Server im entsprechenden Verzeichnis mit dem Kommando

```console
npm run start
```

Bevor du den Client in einer separaten Kommandozeile startest, musst du im Verzeichnis `adaptor_ex_client/public` in der Datei `config.json` unter `API_URL` die Adresse angeben, über die der Server erreichbar ist. Wenn Server und Client mit den default Einstellungen auf dem Selben Rechner laufen ist das `http://localhost:8081/api`.

``` json title="adaptor_ex_client/public/config.json" hl_lines="2"
{
  "API_URL": "http://localhost:8081/api",
  "_COMMENT_LOG_STORE": "Set 'true' to print every store action and mutation",
  "LOG_STORE": false,
  "_COMMENT_LOG_SOCKET": "Set 'true' to print every socket message",
  "LOG_SOCKET": true,
  "LOG_SOCKET": false,
  "_COMMENT_LOG_LEVEL_APP": "Set select wicht toast notifications to show. Can be 'debug', 'info', 'warn', 'error'.",
  "LOG_LEVEL_APP": "debug"
}
```

Starte dann den adaptor:ex Client im entsprechenden Verzeichnis mit dem Kommando

```console
npm run dev
```

### Updates mit git

Lade dir die neueste Version von adaptor:ex server und client herunter indem du im jeweiligen Verzeichnis das Git Repository mit folgendem Befehl updates

```console
git pull
```

Starte adaptor server und client anschließend neu.