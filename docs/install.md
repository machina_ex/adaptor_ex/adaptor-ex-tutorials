---
authors: 
    - Lasse Marburg
date: 2024-07-11
hide:
  - navigation
---

Installation and Updates
==========================

There are three installation options to choose from:

1. **Install via NPM Package Manager [:material-book-open-blank-variant:](#install-with-npm)**

    With the NPM Package Manager you can easily install adaptor:ex from the command line on any common operating system and run it as an NPM application.

    We recommend the [Installation via NPM Package Manager](#install-with-npm) if you want to use adaptor:ex locally on your computer or in an internal network.

    A local installation allows you to use the ports on your computer. E.g. the serial port via USB or a sound card.

2. **Install with DOCKER [:material-book-open-blank-variant:](#installing-with-docker)**

    Especially for a server installation we recommend the [Install in a Docker Container](#installing-with-docker).

    You can use our Docker Compose file to create a comprehensive and stable setup with independent server and client application and MongoDB integration.

    The [Server Setup](./tutorials/server-setup/index.md) tutorial will help you install adaptor:ex with Docker and Docker Compose on an external server, secure it, and make it accessible to everyone working on your project.

3. **Install from SOURCE [:material-book-open-blank-variant:](#installing-from-source-with-npm)**

    Download the adaptor:ex source code and use NPM to install and run the server application and client independently.

    An [install from source](#installing-from-source-with-npm) allows you to make changes to the software. We recommend installing from source if you want to customize some code or contribute to the development of adaptor:ex.

Notes
------

### Command Line Terminal

To install adaptor:ex you currently have to use the command line of your operating system. 

=== "macOS"
    Under macOS search for the program "Terminal".
=== "Linux"
    Under Linux you can open the terminal with the key combination ++ctrl+alt+"T"++
=== "Windows"
    Under windows search for the application "command prompt", "command line" or "cmd".

### Configuration

The adaptor:ex server application offers several ways to customize your installation. The [Readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#configuration) of the adaptor:Ex server code repository describes all configuration options.

Some functions of the adaptor:ex server can also be controlled from the command line: [Commands](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#commands)

### Multi User Server Setup

Take a look at the [Server Setup](./tutorials/server-setup/index.md) tutorial to install adaptor:ex with Docker and Docker Compose on an external server. Here you can also find out how to secure internet access to adaptor:ex and make it accessible to everyone working on your project.

### Updates

We will frequently fix bugs, improve things (hopefully) and add new features in the future. Update via [NPM](#updates-with-npm) or [Docker](#updates-with-docker) every now and then to stay up to date.

Latest updates and changes will be posted in our [News Blog](./blog/index.md) and on our [:fontawesome-brands-discord: Discord Channel](https://discord.gg/quHbQAMvF6).

### Get help

Contact us if you are having trouble installing adaptor:ex. Write us at [tech@machinaex.de](mailto:tech@machinaex.de) or check out our [Discord](https://discord.gg/quHbQAMvF6) channel.

Install with NPM
--------------------

Before you can proceed with the installation of adaptor:ex, you need the latest version of NodeJS. You can find the necessary installation files for your operating system [here](https://nodejs.org).

Once you have installed NodeJS, open your console or terminal and run the installation from the command line with npm

=== "macOS & Linux"
    ```console
    sudo npm install -g adaptorex --unsafe-perm
    ```
=== "Windows"
    ```console
    npm install -g adaptorex --unsafe-perm
    ```

If the installation was successful, start adaptor:ex with the command

```console
adaptorex
```

You can find ways to configure adaptor:ex in the [adaptor:ex server readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#install)

To stop adaptor:ex server use the key combination 

=== "Windows and Linux"
    ++ctrl+"C"++
=== "macOS"
    ++cmd+"C"++
    
or type `quit` and hit ++enter++ in the command line.

### Updates with NPM

Update to the latest version of adaptor:ex with

=== "macOS & Linux"
    ```console
    sudo npm install -g adaptorex --unsafe-perm
    ```
=== "Windows"
    ```console
    npm install -g adaptorex --unsafe-perm
    ```

You can always check wether a new version is available using

=== "macOS & Linux"
    ```console 
    sudo npm outdated -g
    ```
=== "Windows"
    ```console 
    npm outdated -g
    ```

Installing with Docker
-----------------------

Download Docker for your operating system and follow the installation instructions at [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)

Create a new folder where you want to install adaptor:ex.
 
Open your command line, change to the adaptor:ex directory with the `cd` command and download the [adaptor:ex docker-compose](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/docker-compose.yml) file:

```console
curl -o docker-compose.yml https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/raw/main/docker-compose.yml
```

Download the config file for the adaptor:ex client:

```console
curl -o adaptor_client_config.json https://docs.adaptorex.org/assets/adaptor_client_config.json
```

Execute the Docker Compose file:

```console
docker compose up -d
```

adaptor:ex server, client, database and plugin extensions are downloaded and installed.

> If you want to access adaptor:ex from a remote location, you must specify the external URL or IP of the adaptor server application in `adaptor_client_config.json`. In `http://localhost:8081/api`, replace "localhost" with the IP of the machine on which you have set up the docker container, or with the URL of your server or machine.

For a save setup on a remote server we recommend following the [Server Setup](./tutorials/server-setup/index.md) tutorial.

In the [adaptor:ex server readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#install) you can learn more about the configuration of adaptor:ex.

### Updates with Docker

To update the adaptor:ex Docker installation change to the adaptor:ex directory. There execute the following commands:

```console
docker compose pull
```

and

```console
docker compose up -d
```

Installing from Source with NPM
-------------------------------

Before you can proceed with the installation of adaptor:ex, you need the latest version of NodeJS. You can find the necessary installations files for your operating system [here](https://nodejs.org).

Get adaptor:ex server and client:

**server**

Clone  the current GitLab repository of the adaptor:ex server via: 

```console
git clone https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server.git
```

Or download here: [https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server)

**Client**

Clone the current GitLab repository of the adaptor:ex client via:

```console
git clone https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_client.git
```

or download here: [https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_client](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_client)

Now you can install the server and the client locally with [NPM](https://www.npmjs.com/).

In the terminal (console) change to the directory where you have loaded or cloned the server or client.

```console
cd adaptor_ex_server/
```

or

```console
cd adaptor_ex_client/
```

and run the following command in both directories:

```console
npm install
```

If the installation was successful, start the adaptor:ex server in the corresponding directory with the command

```console
npm run start
```

Before you start the client in a separate command line, you must specify the address the server can be reached on. In the `adaptor_ex_client/public` directory open `config.json` and change the value for `API_URL`. If server and client are running on the same computer with default settings, that would be `http://localhost:8081/api`.

``` json title="adaptor_ex_client/public/config.json" hl_lines="2"
{
  "API_URL": "http://localhost:8081/api",
  "_COMMENT_LOG_STORE": "Set 'true' to print every store action and mutation",
  "LOG_STORE": false,
  "_COMMENT_LOG_SOCKET": "Set 'true' to print every socket message",
  "LOG_SOCKET": true,
  "LOG_SOCKET": false,
  "_COMMENT_LOG_LEVEL_APP": "Set select wicht toast notifications to show. Can be 'debug', 'info', 'warn', 'error'.",
  "LOG_LEVEL_APP": "debug"
}
```

Then start the adaptor:ex client in the appropriate directory with

```console
npm run dev
```

### Updates with git

Download the latest version of adaptor:ex server and client by updating the git repository in the respective directory with the following command

```console
git pull
```

Restart the adaptor server and client.