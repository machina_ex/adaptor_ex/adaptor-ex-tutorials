---
authors: 
    - Lasse Marburg
date: 2024-07-11
hide:
  - navigation
---

Getting Startet with adaptor:ex
===============================

Lerne in wenigen Schritten, wie du adaptor:ex installierst und ein erstes Level erstellst und startest.

Installation
------------

Installiere die Software [NodeJS](https://nodejs.org) um adaptor:ex mit dem NodeJS Package Manager NPM zu installieren.

> Schaue im [Installations](./install.md) Kapitel nach, wenn du adaptor:ex stattdessen mit Docker oder vom Source Code installieren willst.

Sobald Du NodeJS installiert hast, öffne deine [Konsole bzw. das Terminal](./install.md#command-line-terminal) und führe in der Kommandozeile die Installation mit npm aus

=== "macOS & Linux"
    ```console
    sudo npm install -g adaptorex --unsafe-perm
    ```
=== "Windows"
    ```console
    npm install -g adaptorex --unsafe-perm
    ```

Wenn die Installation erfolgreich war, starte adaptor:ex indem du das folgende  Kommando in der Konsole eingibst:

```console
adaptorex
```

Wenn alles geklappt hat solltest du in etwa das folgende in deiner Kommandozeile sehen:

![Die Konsolen Ausgabe einer frischen Installation](./assets/getting_startet_console.png)

Behalte die Konsole geöffnet solange du mit adaptor:ex arbeitest. Sobald du die Konsole schließt wird auch adaptor:ex beendet.

Erste Schritte
---------------

Gebe die folgende URL, also die webadresse, deines adaptor:ex setups, im Web Browser ein: [http://localhost:8081](http://localhost:8081)

![Die adaptor:ex Startseite](./assets/no_games.png)

Wir empfehlen als Browser eine aktuelle Version von Google Chrome oder Firefox zu verwenden.

> Die URL kann sich je nachdem mit welchen Einstellungen du adaptor:ex ausführst unterscheiden.
>
> Hast du adaptor:ex auf einem Server oder einem anderen Rechner installiert musst du die URL durch die IP oder den Domainnamen deines Servers ersetzen.

### Ein Game erstellen

Beginne damit, ein neues Game Projekt anzulegen. Klicke auf `New Game` und gib deinem ersten Game einen Namen.

![Ein neues Game erstellen](./assets/new_game.png)

### Level hinzufügen
Wenn du das neu erstellte Game Projekt anklickst, landest du in der Übersicht, die alle existierenden level anzeigt. Jedes Game kann aus mehreren Leveln bestehen, die auf verschiedenste Art miteinander verknüpft sind.

Erstelle dein erstes Level mit `Add new`.

> Games und Level bekommen eine eigene Web Adresse, über die sie im Web Browser erreichbar sind. Darum darf der Name keine Leerzeichen oder Sonderzeichen enthalten.

Gib dem Level einen Namen und klicke auf `Add new Level`.

![Ein Level hinzufügen](./assets/new_level.png)

### Level bearbeiten

Öffne das level anschließend, indem du auf den Namen klickst. Jetzt befindest du dich im level Editor.

Start (*START*) und Endpunkt (*QUIT*) deines Levels existieren bereits.

![Der Level Editor](./assets/empty_level.png)

Suche aus der linken Seitenleiste, den [actions](./basics/editor.md#action), die control action [Log Message](./basics/actions/control/log.md) heraus und ziehe sie per Drag and Drop auf die Mitte der Seite.

Es entsteht ein neues [State](./basics/editor.md#state) Element, dass eine [Log Message](./basics/actions/control/log.md) action enthält (*log_1*). 

![Eine Action hinzufügen](./assets/log_action.png)

Klicke auf die neue action, um sie zu bearbeiten. Schreibe einen kurzen Text in das `message` Eingabefeld und wähle im `level` Dropdown *info* aus.

![Eine Action bearbeiten](./assets/edit_log_action.png)

Um dein Level verständlicher zu gestalten, ist es gut allen States nachvollziehbare Namen zu geben.

> Auch State Namen dürfen keine Leerzeichen beinhalten, da sie in [Variablen](./basics/variables.md) zum Einsatz kommen können

Klicke in die Titelzeile deines Log Message State und benenne ihn um. 

![States umbenennen](./assets/rename_state.png)

Bearbeite auch die [Next](./basics/actions/control/next.md) action *next_1* im *START* State indem du sie anklickst. Wähle dann den neuen State, hier *HelloWorld*, aus dem `next state` Dropdown aus um ihn mit dem *START* State zu verbinden.

![2 States verbinden](./assets/connect_log_action.png)

### Level starten

Wechsel vom Bearbeitungs in den [Live Modus](./basics/live.md), indem du auf den Play button in der oberen rechten Ecke klickst 

![Live Modus Toggle](./assets/live_toggle.png)

Die linke Seitenleiste mit den actions schließt sich und ein Menü öffnet sich an der rechten Seitenleiste.

Klicke in der rechten Seitenleiste auf `Create Session` und dann auf `Start Session` um eine [Session](./basics/live.md#session) von deinem Level zu erstellen.

![Eine Session starten](./assets/start_session.png)

Die neu gestartete Session ist in der rechten Seitenleiste ausgewählt.

![Eine session auswählen](./assets/select_session.png)

Der *HelloWorld* State ist bereits hervorgehoben.

Wenn wir eine neue Session von einem Level erstellen, wird automatisch der *START* State ausgelöst. Die *next_1* action hat direkt danach zu unserem State *HelloWorld* mit der Log Message weitergeführt.

Öffne die Log Konsole, indem du auf den cursor icon klickst.

![Log Konsole anzeigen](./assets/console_toggle.png)

Die neueste Log Nachricht sollte 'Hello World!' sein.

Indem du auf den Play Icon ![Play Icon](./assets/play_icon.png) des *HelloWorld* State  klickst, kannst du ihn manuell erneut auslösen. Beobachte die Log Konsole ob die Nachricht erscheint.

### Level automatisieren

Wechsel zurück in den Bearbeitungsmodus, indem du erneut auf den Live Toggle Button oben rechts in der Menüleiste klickst.

Suche die [Timeout](./basics/actions/time/timeout.md) action in der linken Seitenleiste heraus und ziehe sie auf den *START* State. 

Wenn du die action darüber ziehst wird der State hervorgehoben und zeigt an, dass die action diesem State hinzugefügt wird, anstatt einen neuen State zu erstellen.

> Wenn wir einem State eine action hinzufügen, die auf etwas wartet, wie ein timer der abläuft, ersetzt sie automatisch eine etwaige [Next](./basics/actions/control/next.md) action.

Bearbeite den timeout. Gib eine kleine Zahl Sekunden im `timeout` feld an und wähle den *HelloWorld* State als `next state` aus.

![Einen timeout erstellen](./assets/timeout.png)

Füge auch dem *HelloWorld* State eine Timeout action hinzu. Bearbeite den Timeout und wähle diesmal *Quit* als `next state` aus.

Wenn du nun in den Live Modus wechselst und eine neue Session startest werden die States in festen Zeitabständen nacheinander ausgelöst.

![Mehrere Sessions gleichzeitig](./assets/automated_session.png)

### Level Sessions beenden

Sobald der *QUIT* State erreicht ist, wird die session beendet und verschwindet aus der rechten Seitenleiste.

Deine erste Session wird sich nicht automatisch beenden. Um sie zu beenden kannst du sie in der rechten Seitenleiste schließen (x) oder den *QUIT* State auslösen wenn du die Session ausgewählt hast.

> Jede Session basiert immer auf der aktuellsten Version des Levels. Wenn jedoch der State, in dem sich eine Session befindet, verändert, muss er erneut ausgelöst werden.

Nächste Schritte
----------------

Im [Glossar](./basics/editor.md) findest du mehr über die verschiedenen Grundbausteine, Plugins und Actions von adaptor:ex heraus.

[Tutorials](./tutorials/arduino-serial/index.md) leiten dich Schritt für Schritt durch Projekte die du mit adaptor:ex umsetzen kannst.

Lerne zum Beispiel, wie du das Telegram plugin einsetzt, um ein messenger Adventure zu entwickeln: [Storytelling mit Telegram und adaptor eX](./tutorials/telegram-basics/index.md)

Oder beginne damit, einen Arduino mit adaptor:ex anzusteuern: [Arduino Serial](./tutorials/arduino-serial/index.md)