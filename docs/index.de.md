---
title: adaptor:ex
authors: 
    - Lasse Marburg
date: 2024-07-11
---

![adaptor:ex](./assets/header.png){ style=width:800px align=center }
====================================

!!! info ""
    Click here for an [english version](https://docs.adaptorex.org) of the documentation.

adaptor:ex hilft dir Game Theater, Interaktive Installationen, Chatbot-Adventure, Escape Rooms oder jede andere Art von spielerischem Live Event zu erstellen. Die grafische Programmieroberfläche ist darauf optimiert Dramaturgie und Game Mechanik Zusammenzubringen und übersichtlich anzuordnen.

![Screenshot: Live Games in adaptor:ex erstellen und editieren](./assets/editor_example.png "Live Games in adaptor:ex erstellen und editieren")

Du kannst schnelle Anpassungen während der Proben vornehmen und den Live Ablauf während der Tests und Aufführungen überwachen und steuern.

![Screenshot: Den Ablauf eines adaptor:Ex Games beobachten und steuern](./basics/assets/events_local_path.gif "Den Ablauf eines adaptor:ex Games beobachten und steuern")

Der adaptor:ex Editor wird über den Browser aufgerufen und ihr könnt mit mehreren Leuten das selbe Game bearbeiten.

![Screenshot: Über den Browser mit mehreren Nutzern zusammenarbeiten](./assets/collaboration_example.png "Über den Browser mit mehreren Nutzern zusammenarbeiten")

[Plugins](./basics/plugins/ableton.md) erweitern die Möglichkeiten deines adaptor:ex Setups. So kannst du unterschiedlichste Software Schnittstellen und Geräte miteinander in einem Game oder einer Story verschalten. Neben grundlegenden Schnittstellen wie HTTP, OSC und USB Serial enthält die adaptor:ex Installation Plugins für Telegram, Twilio, MQTT und Ableton Live.

Auf diesen Seiten findest du mehr darüber hinaus, wie du mit adaptor:ex deine Storyline, Game Mechanik, Software und Hardware miteinander verdrahtest und unter Kontrolle bringst.

Folge dem [Getting Startet](./getting_started.md) Guide und probiere adaptor:ex gleich aus.

Beginne mit einem [Tutorial](./tutorials/arduino-serial/index.md), wenn du eine der verschiedenen Möglichkeiten kennenlernen willst auf die du adaptor:ex nutzen kannst.

Im [Glossar](./basics/editor.md) findest du Infos zu allen Grundbausteinen und [Plugins](./basics/plugins/ableton.md). Die Elemente des [Editors](./basics/editor.md) und die Kontrollebene für deine [Live Show](./basics/live.md) werden hier beschrieben.

adaptor:ex ist eine Web Anwendung, die du selbst auf deinem Rechner oder einem Server installierst und hostest. Unter [Installation](./install.md) findest du die verschiedenen Möglichkeiten die Anwendung einzurichten.

Wirf einen Blick in unseren [News Blog](./blog/index.md) wenn du Infos zu aktuellen Updates, Bugfixes und Games die mit adaptor:ex entwickelt wurden erhalten willst.

Wenn du Hilfe brauchst, einen Fehler entdeckt hast oder ein Feature vorschlagen willst erstelle ein [:material-cards-outline: Issue auf Gitlab](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/issues) schreibe uns eine [:fontawesome-solid-envelope: E-mail](mailto:tech@machinaex.de) oder eine Nachricht in unserem [:fontawesome-brands-discord: Discord Channel](https://discord.gg/quHbQAMvF6). Dort kannst du dich auch mit anderen austauschen und erhältst Neuigkeiten zu updates und bugfixes.

-------------------------

Dies ist ein [machina commons](https://commons.machinaex.org) Projekt von 

[![machina eX](./assets/machina-eX-logo-color-small.png){ style=width:400px }](https://machinaex.de)

gefördert durch

![Logo Senatsverwaltung für Kultur und Europa Berlin](./assets/logo_senatsverwaltung_berlin.jpg){ style=width:330px }

Senatsverwaltung für Kultur und Europa Berlin