---
date: 2025-01-03
categories:
  - Showcase
tags:
  - theater
  - game
  - socketio
  - mqtt
  - ableton
  - de
authors:
  - lasse
comments: true
---

Intranet Live und Online
========================

**Intranet** ist die neueste Game Theater Produktion von [machina eX](https://machinaex.de) und hatte am 24.10.24 am HAU3 in Berlin Premiere.

In  **Intranet** spielen jeweils 4 Leute an 6 Tischen ein anderes Redaktionsmitglied einer Lokalzeitung. Die Teams werden durch eine Webapp auf einem Tablet durch das individuelle Spiel geleitet, kommen aber immer wieder in Redaktionskonferenzen zusammen.

Ein Performer treibt die gemeinsame Erzählung voran und begleitet die Teams bei ihren Aufgaben. Die Recherche führt die spielenden auf die Spur eines Cyberangriffs.

<!-- more -->

adaptor:ex steuert die Abläufe und Interaktionen in der Webapp und verschaltet sie mit dem Geschehen auf der Bühne. Eine weitere Webapp erlaubt es dem Performer über adaptor:ex auf den Spielfortschritt der einzelnen Teams zu reagieren und selbst cues auszulösen.

Auf [hau4.de](https://www.hau4.de/machina-ex-intranet-home-office) ist eine online Version des Games mit Materialien zum selbst ausdrucken verfügbar.

Den Source Code für die Software hinter Intranet findet ihr auf unserer gitlab Seite: [gitlab.com/machina_ex/intranet](https://gitlab.com/machina_ex/intranet)

Hier gibt es genauere Infos zu Format und Inhalt und einen Trailer des Games: [machinaex.de/intranet-2024/](https://machinaex.de/intranet-2024/)

![Ein Foto aus der Live Bühnen Version von Intranet. 2 Personen sitzen an einem Büro Schreibtisch und telefonieren. Auf dem Schreibtisch diverse Utensilien, Dokumente und Fotos.](https://machinaex.de/wp-content/uploads/2024/08/241022_MachinaeX_Intranet_233_1.jpg)

![Foto aus der Live Bühnen Version von Intranet. Der Raum in gänze. BesucherInnen sitzen zu viert an 6 Tischen.](https://machinaex.de/wp-content/uploads/2024/08/241022_MachinaeX_Intranet_152_1_1.jpg)