---
date: 2023-03-17
categories:
  - Update
tags:
  - update
  - telegram
  - twilio
  - functions
authors:
  - lasse
comments: true
---

Initial Messages work as you'd expect
=====================================

*:material-creation: Version 2.1*

Did you also wonder why you need to place an [On Incoming Message](../../basics/actions/telegram/onMessage.md) action at the beginning of your telegram chat level? It's because it catches and handles the initial message that started the new session. But why does it still do that if I already responded with a plain and simple Send Message 🤨? Well, it does not anymore ![mexsunglasses](./assets/adaptor-emojis/happy-hearts.png){style=width:20px}

adaptor:ex **Version 2.1.0** comes with some bug fixes and a slight change to the "Default Level" behavior for messenger listeners.

<!-- more -->

Whenever you use a messengers ([Telegram](../../basics/plugins/telegram.md) or [Twilio](../../basics/plugins/twilio.md)) run action before a messengers listen action, the pending initial contact message is discarded. Any following Listener Action in this conversation will wait for the next incoming message like you would probably expect it to.

So you can still react on the initial contact message (or twilio call) but only if the listener is upfront any send type of action of the same conversation.

Confused? Same here. Best just try it out: 

=== "NPM macOS & Linux"
    ```console
    sudo npm update -g adaptorex
    ```
=== "NPM Windows"
    ```console
    npm update -g adaptorex
    ```
=== "Docker Compose"
    ```console
    docker compose pull
    ```

    and

    ```console
    docker compose up -d
    ```

**Version 2.1** In a nutshell

- Added random function to [embedded functions](../../basics/functions.md#embedded-functions)
- Fix: Could not change duration of Telegram "is typing" feature
- Fix: reply option in Telegram [On Message](../..//basics/actions/telegram/onMessage.md) not working anymore since version 2.0.
- A few smaller bug fixes and minor adjustments