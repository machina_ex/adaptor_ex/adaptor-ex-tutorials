---
date: 2023-12-05
categories:
  - Showcase
tags:
  - theater
  - game
  - telegram
  - de
authors:
  - lasse
comments: true
---

Exit Game im Spielzeitheft
===========================

Das Heft Das Digitale Foyer vom FFT Düsseldorf ist der Zugang zu einem Chatbot Adventure von Anton Krause und Robin Hädicke ([machina eX](https://machinaex.de)). Das Projekt nutzt Telegram Bots in adaptor:ex um euch gebührend digital in den Foyers der Oper am Rhein und des FFT Düsseldorf zu begrüßen.

[fft-duesseldorf.de/journal/exit-game](https://www.fft-duesseldorf.de/journal/exit-game )