---
date: 2023-06-19
categories:
  - Update
tags:
  - update
  - mqtt
  - telegram
authors:
  - lasse
---

Added MQTT and Telegram Bots
============================

*:material-creation: Version 2.2*

This piece of software looks promising you might say, but It's worth nothing because it does not support MQTT, meh.

Stop, don't walk away! Make an update instead. **adaptor:ex has MQTT now!** ![mexhappyhearts](./assets/adaptor-emojis/happy-hearts.png){style=width:20px}

<!-- more -->

The plugin is very basic and we will only be using it ourselves for the first time in the upcoming months. If you have any thought on how to integrate MQTT further we are very open to feature requests. Let us know how you would want to use it. Get the details [here](../../basics/plugins/mqtt.md).

In other news the **Telegram Plugin finally supports Telegram Bots**. In like the actual bots that do not try to hide behind a "real" account. Don't need the facade? Just use @botfather to create a new Telegram Bot, connect it to adaptor:ex and you are good to go to create your own chatbot adventure. 🤖 
Here is [how to set up a Telegram Bot](../../tutorials/telegram-settings/index.md#connect-a-telegram-bot) with adaptor:ex.

Version **2.2.0** in a nutshell:

- Fixed: permissions could not be set for telegram group chats using [Edit Chat](../../basics/actions/telegram/editChat.md)
- Added [MQTT Plugin](../../basics/plugins/mqtt.md) that allows to send MQTT messages and to react on messages that have been published to the network.
- In addition to Accounts, [Telegram Bots](../../tutorials/telegram-settings/index.md#connect-a-telegram-bot) can now be connected to adaptor:ex via the Telegram plugin.
- Improved error feedback when setting up plugins and plugin items
- Level names can now be edited
- Some smaller bug fixes and minor adjustments