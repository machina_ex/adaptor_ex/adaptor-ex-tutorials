---
date: 2024-06-18
categories:
  - Update
tags:
  - update
  - telegram
  - dmx
  - control
authors:
  - lasse
comments: true
---

Improved Level Arguments and updated Dependencies
===============================================

*:material-creation: Version 2.7*

Let us envy the brief moment in which none of the many dependencies that carry adaptor:ex shows a known vulnerability 🙏 

And as it goes with updating stuff inconsistencies emerge. Luckily [@taste1](https://gitlab.com/taste1) took a closer look and quite some bugs could be tackled on the go. Did you know  for instance, that adaptor:ex crashed if you tried to quit a session and there is another Telegram [Send Message](../../basics/actions/telegram/sendMessage.md) left to go? We for sure didn't...

But **Version 2.7** not only brings you updated dependencies and bug fixes:

<!-- more -->

Level Arguments now follow a more universal approach and finally allow to also forward strings, numbers, arrays and objects. You can still forward data and plugin items as arguments but the naming shifted slightly. collection is now type and default query is now default value but they serve the same purpose as before extended by the above data types.

Take a look at [level arguments](../../basics/variables.md#level-arguments).

Note that arguments in [Launch Session](../../basics/actions/control/launch.md#arguments) also look slightly different now. 

Your existing levels will still work with the prior naming though. (knocks on wood)

Here comes the wrap up:

- Allow to address other levels as variable
- Allow strings, numbers, arrays and objects as (default) values for [Level Arguments](../../basics/variables.md#level-arguments)
- [Launch Session](../../basics/actions/control/launch.md) recognizes if level config sets a default collection for an argument
- Changed [Launch Session](../../basics/actions/control/launch.md) arguments from Options to List
- Upgrade serialport and axios library to fix vulnerabilities
- Adapt [usb-dmx-pro](../../basics/plugins/usb-dmx-pro.md) plugin to latest serialport library
- Fix error in [Telegram chat](../../basics/actions/telegram/createChat.md) create & update events
- Fix problem with Telegram [On Incoming Message](../../basics/actions/telegram/onMessage.md) not working with initial message
- Several minor bug fixes and improvements