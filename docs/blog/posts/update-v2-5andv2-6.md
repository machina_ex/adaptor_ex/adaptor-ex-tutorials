---
date: 2024-03-21
categories:
  - Update
tags:
  - update
  - logic
  - control
authors:
  - lasse
comments: true
---

Loops and Iterations and Loops and ...
=======================================

*:material-creation: Version 2.5 & 2.6*

Repeating stuff in adaptor:ex was always easy. Still have the moment in my bones when we accidentally built an infinite loop that sent the same sms nonstop via Twilio. Some 1000 messages and merely 50$ later the exciting cycle could be stopped.

From today there is more control over repeating stuff. Proudly presenting the [Iterate Action](../../basics/actions/logic/iterate.md) that allows to easily build for each loops.

![Scheduled Repetitions](./assets/loops_schedule.png)

Of course you can still spend your money uncontrollably. Here is how: [Loops and Iterations](../../basics/loops.md) 💸

And there is more 🌈 

<!-- more -->

**Version 2.5**

- Add [On Error](../../basics/actions/control/onError.md) action to catch and react on errors in States and Sessions
- Add [Keep listening and queue](../../basics/loops.md#keep-listening-and-queue) option for listeners
- Fix Items not shown when collection was deleted and added again
- Allow number type distinction into integer and float for OSC message
- Fix Telegram [On Incoming Message](../../basics/actions/telegram/onMessage.md) gets stuck if respond list is empty
- Fix fatal error in sound play action if there is no audio player available
- Several minor bug fixes

**Version 2.6**

- Add logic [Iterate](../../basics/actions/logic/iterate.md) action
- Add [atIndex()](../../basics/functions.md#atindex) and [length()](../../basics/functions.md#length) functions
- Add options to sort and narrow down item selections. See respective section in [Variables chapter](../../basics/variables.md#sort-and-narrow-down-item-references)
- Add option to create arrays inline inside form fields just like with JS objects
- Allow nested inline queries and inline regex
- Some minor bug fixes

How to update: 

=== "NPM macOS & Linux"
    ```console
    sudo npm update -g adaptorex
    ```
=== "NPM Windows"
    ```console
    npm update -g adaptorex
    ```
=== "Docker Compose"
    ```console
    docker compose pull
    ```

    and

    ```console
    docker compose up -d
    ```

Or, if you want to take things to the next level, check this out : [@taste1](https://gitlab.com/taste1) wrote an exhaustive rework of the [server setup tutorial](https://docs.adaptorex.org/de/tutorials/server-setup) (DE only for now). This time it is based on real life experience 🤩