---
date: 2023-04-11
categories:
  - Showcase
tags:
  - theater
  - game
  - telegram
  - de
authors:
  - taste
comments: false
---

Solingen 1993
==============

Diese Inszenierung, die den rassistischen Brandanschlag von Solingen im Jahr 1993 zum Thema macht, nutzt AdaptorEx und Telegram, um durch die Stadt zu leiten, Medieninhalte zu senden und zur Reflexion anzuregen. Es erwartet euch eine außergewöhnliche und berührende Theatererfahrung. 

Besonderer Dank geht an [@lassemarburg](https://gitlab.com/lassemarburg) und [@gauguerilla](https://gitlab.com/gauguerilla) für ihre tatkräftige Unterstützung.

[dhaus.de/programm/a-z/solingen-1993](https://www.dhaus.de/programm/a-z/solingen-1993/)