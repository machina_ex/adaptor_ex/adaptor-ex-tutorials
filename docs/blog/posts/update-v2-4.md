---
date: 2023-11-09
categories:
  - Update
tags:
  - update
  - telegram
authors:
  - lasse
comments: true
---

Bots with Buttons
=================

*:material-creation: Version 2.4*

adaptor:ex survived it's first Stage Production Telegram Outdoor Adventure Crossover. Phew 😅! Check it out: [machinaex.org/wenn-der-regen-kommt](https://machinaex.org/wenn-der-regen-kommt/)

As custom would have it, machina eX Productions make the best updates. Here comes **Version 2.4.0**:

<!-- more -->

- Add "now" option for [dateTimeFormat](../../basics/functions.md#datetimeformat) and [dateTimeOffset](../../basics/functions.md#datetimeoffset) functions
- Added Button support for Telegram Bots (see Telegram [Buttons](../../basics/actions/telegram/buttons.md) action)
- Added option to sort arrays in action form editor
- Added animation to highlight lines coming from selected states 
- Added text filter for Action, Variables and Media Toolbar
- Fixed and improved [Cancel Session](../../basics/actions/control/cancel.md) action
- Auto select new or latest Sessions in Editor Live Mode
- Convert manually entered dates to improve compatibility with auto generated dates
- Allow regex in database query strings
- Add case sensitive option to regex in all condition capable actions
- Many minor bug fixes and improvements