---
date: 2024-07-08
categories:
  - Bugfix
tags:
  - bug
  - update
  - telegram
authors:
  - lasse
comments: true
---

Fix for Telegram Login Error
=============================

*:material-bug-check: Version 2.7.1*

Sometimes it is not easy to keep up with the constant changes the world holds for us. This time it is!

There was a change in the Telegram API that would make the Telegram plugin throw error 

!!! bug ""
    406: UPDATE_APP_TO_LOGIN 

if you try to connect a new account.

Please update to adaptor:ex version **2.7.1** to fix this issue. To make sure all dependencies are properly updated, run a full install with

=== "NPM macOS & Linux"
    ```console
    sudo npm install -g adaptorex --unsafe-perm
    ```
=== "NPM Windows"
    ```console
    npm install -g adaptorex --unsafe-perm
    ```
=== "Docker Compose"
    ```console
    docker compose pull
    ```

    and

    ```console
    docker compose up -d
    ```