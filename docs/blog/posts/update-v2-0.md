---
date: 2023-02-28
categories:
  - Update
tags:
  - update
  - telegram
  - editor
  - functions
  - events
authors:
  - lasse
comments: true
---

Major Update major excited
===========================

*:material-creation: Version 2.0*

Hello everyone! A new version of adaptor:ex is ready to be installed 💫 💾 

**Version 2.0.0** contains many changes we made during the development of [Life Goes On](https://machinaex.org/life-goes-on/). Some of these things made our own lives much easier already. It seems like a good a idea to use your own software if you want to fix it... 🦥

Also, to make it worth a new major version number, we added several changes that seemed important to us (and that will break most existing games 😬 , sorrrry!! See below!)

<!-- more -->

## So what's new?

We fixed plenty of bugs and did many bigger and smaller improvements. Here are the highlights:

- The editor got some useful new options. You can now copy states and actions, drag and drop actions between states and reorder them. We also added a zoom/scale option.

- Levels are now instantly updated between users working in collaboration (or just between you and yourself in multiple browser tabs). You can also see what user (or tab) is working on what state in the level editor. Conflicts are prevented by locking states you work on for other users (or, tabs of course ☝️)

- If you had problems installing the [Telegram Plugin](../../basics/plugins/telegram.md), we have good news for you. We replaced the (still very good) telegram python module ([telethon](https://docs.telethon.dev)) with an (also very good) native javascript alternative ([gramjs](https://www.npmjs.com/package/telegram)). So no need to install python and its dependencies any more 😅

- The telegram plugin also gained some features like a customizable "typing" delay and the option to create an invite link for groupchats.

- We added a next state "output" option for the function action so that it is much easier to use it to route input.

- Functions in variables (finally!) distinct between string, number and variable types.

- All items, sessions and games can dispatch events now (seems fair 👍)

## Can I use it with my existing games?

We have to admit there are, mostly small though plenty, changes that will break your existing games. Checkout [this list](#migrate-from-previous-version) if you want to migrate your existing levels.

Please reach out if you need help!

## How can I install the update?

If you went with the npm package you need to make a fresh install:

=== "macOS & Linux"
    ```console
    sudo npm install -g adaptorex --unsafe-perm
    ```
=== "Windows"
    ```console
    npm install -g adaptorex --unsafe-perm
    ```

Otherwise take a look into the [Install Documentation](../../install.md).

Besides many major and minor bug fixes and improvements, the update to **Version 2.0.0** includes the following changes:

**Editor**

- Improved line routing
- Duplicate state option
- Drag and Drop action from one state to another
- Copy action option
- Reorder actions inside a state
- Improved multi user and multi browser collaboration
  - Instantly update level changes for all users in all browsers
  - Visualize remote users editing the same level
- Added loading animation
- Fixed: Console log stops updating when there are more than 11000 Lines

**Live Mode**

- Added information about referenced items in level view and overview

**Functions**

- Improved [function](../../basics/actions/logic/function.md) action with `next state` outlets
- Inline functions arguments allow string type, number type and variables (see [functions as variables](../../basics/functions.md#functions-as-variables))
- Add new functions and properties to game and session context

**Events**

- Events are now bound to an event source (see [Events](../../basics/events.md))
- Sessions, Plugins, Plugin Items and Data items can now dispatch events

**Telegram Plugin**

([telethon](https://docs.telethon.dev)) with an (also very good) native javascript alternative ([gramjs](https://www.npmjs.com/package/telegram))

- Replaced python telegram API library [telethon](https://docs.telethon.dev) with native javascript/nodejs module [gramjs](https://www.npmjs.com/package/telegram). Therefor it is no longer required to install python.
- Added typing delay option to [Send Message](../../basics/actions/telegram/sendMessage.md) action
- Improved working with polls
- Added [invite link](../../basics/actions/telegram/createChat.md#create-invite-link) option to [Create Chat](../../basics/actions/telegram/createChat.md) action.

**Setup**

- You can now add plugins from the data or any custom directory

### Documentation for version 1.x

Older versions of the documentation can be found in the [source repository on gitlab](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_tutorials/-/tree/main/docs).

From the branches selection choose the required version under **tags**.

Latest 1.x version can be found [here](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_tutorials/-/tree/v1.2.3-final/docs).

### Migrate from previous version

**Version 2** includes several changes that will break most games that were created in previous versions.

Important changes to the server configuration are:

- The default data directory is now called "adaptorex" instead of "data"
- The "user" option in "config.json" is now called "users" (see [Readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/README.md#secure-with-user-authentication))

If you want to migrate levels created with version 1.x you need to consider the following changes if you use any of the respective elements:

- Data action `load` ("Load Item") is now called `getItem` ("Get Item")
- Data action `create` ("Create Item") is now called `addItem` ("Add Item")
- "name" property in data action `addItem` (fka. `create`) is now called "reference".
- The property `name` in the [function](../../basics/actions/logic/function.md) action is now called `function`
- The syntax for functions that are called inside variables has changed

  Functions that are called inside variables are now referenced by `functions` (plural) instead of `function` and functions that are called inside variables require to define the arguments type. You will need to wrap all arguments in quotes to update your level.
  
  E.g. `[[function.test(value1,value2)]]` should become `[[functions.test("value1","value2")]]` 

- Telegram action `dialog` ("Dialog") is now called `onMessage` ("On Incoming Message")
- Telegram `On Incoming Message` `timeout` structure changed and is now separated into `while idle` and `finally` option.
- Twilio Action `smsout` is now called `sendSms`.
- Twilio Action `smsin` is now called `onSms`.
- Twilio Action `callin` is now called `onCall`.
- Telegram and Twilio `telnumber` property is now called `phone_number`

To make sure you get hold of all changes, download each level and find and replace the above names, before you upload your levels to the updated adaptor:ex editor.

Even though it is possible to adapt all elements of a game to fit Version 2 we recommend to create a new game and recreate your plugins and copy your data collections from the original project.

If you need help migrating your project join our [:fontawesome-brands-discord: Discord Channel](https://discord.gg/quHbQAMvF6) or send us an [:fontawesome-solid-envelope: e-mail](mailto:tech@machinaex.de).