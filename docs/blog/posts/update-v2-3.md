---
date: 2023-08-01
categories:
  - Update
tags:
  - update
  - editor
authors:
  - lasse
comments: true
---

Scroll, Zoom, Multiselect and Multiple Items
============================================

*:material-creation: Version 2.3*

Soon we will dive into the next Game Theater production and begin headlessly hacking into adaptor:ex. That is why we wanted to upload the latest changes so you don't have to wait until November. And there are plenty useful things in **Version 2.3.0** especially when it comes to the level editor 🖱️🌟 

<!-- more -->

Create a next state from inside the [Next State](../../basics/actions/control/next.md) action

![Add State](./assets/add_state.gif)

Hold ++shift++ to select multiple States and press ++ctrl++ with mouse wheel to zoom in or out.

![Multiselect States](./assets/multiselect.gif)

All updates in **Version 2.3**

- Added create next state from action next field
- Added Multiselect States
- Added editor minimap
- Added "lock editor" option
- Added "view all states" option
- Added step by step and scroll zoom/scale
- Added "multiple items" option to data actions (see [Variables Chapter](../../basics/variables.md#referencing-multiple-items))
- Allow directories inside functions folder (see [Functions Chapter](../../basics/functions.md))
- Several bug fixes