---
date: 2023-11-14
categories:
  - Showcase
tags:
  - theater
  - game
  - telegram
  - mqtt
  - ableton
  - de
authors:
  - lasse
comments: true
---

Wenn der Regen kommt feiert Premiere
====================================

*Wenn der Regen kommt* ist gleichzeitig Telegram Adventure im Stadtraum und Indoor Theater Game. 30 spielende werden von Chatbots, Live Performenden und der Theatermaschine durch das Game geführt und vor wichtige Entscheidungen gestellt. Für Wenn der Regen kommt verschaltet adaptor:ex den Telegram Messenger, Licht, Sound und interaktive Bühnendevices.

Das Theater Game von [machina eX](https://machinaex.de) hatte am 4.Oktober 2023 am [HAU](https://www.hebbel-am-ufer.de/) in Berlin Premiere.

[machinaex.org/wenn-der-regen-kommt](https://machinaex.org/wenn-der-regen-kommt/)