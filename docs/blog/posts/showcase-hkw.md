---
date: 2024-07-05
categories:
  - Showcase
tags:
  - theater
  - game
  - telegram
  - de
authors:
  - lasse
comments: true
---

Der Geist des Fußballs ist auf Telegram
======================================

*Die Regeln des Spiels* von Anton Rose führt 15 Spielende durch das [Haus der Kulturen der Welt](https://www.hkw.de) auf der Suche nach dem Geist des Fußballs. Per Telegram Messenger werden die Besuchenden von einer Chat Figur geführt und angeleitet. Ein Indoor und Outdoor Multiplayer Game  mit Live Performance und Gruppenchats.

[hkw.de/programme/ballet-of-the-masses/die-regeln-des-spiels](https://www.hkw.de/programme/ballet-of-the-masses/die-regeln-des-spiels)