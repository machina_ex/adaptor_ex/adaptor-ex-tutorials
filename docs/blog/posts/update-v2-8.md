---
date: 2025-02-12
categories:
  - Update
tags:
  - update
  - socketio
  - editor
authors:
  - lasse
comments: true
---

Introducing Socket.IO
======================

*:material-creation: Version 2.8*

adaptor:ex was always in a close relationship with Socket.IO. It allows the server and editor to talk in both directions in addition to the one way RESTful API. Socket.IO is also a key element in the machina eX Games [Life Goes On](https://machinaex.de/life-goes-on/) and [Intranet](https://machinaex.de/intranet-2024/). For [Intranet](https://machinaex.de/intranet-2024/) we took a more general approach for the implementation of Socket.IO. We polished it a bit, are happy with the result and don't want to keep it for ourselves. 

The new [Socket.IO Plugin](https://docs.adaptorex.org/basics/plugins/socketio.html) allows you to send and receive messages to and from your Website or WebAPP and organize your game using Socket.IO [namespaces](https://socket.io/docs/v4/namespaces/) and [rooms](https://socket.io/docs/v4/rooms/).

We also made some subtle but useful changes to the editor ⬇️

<!-- more -->

* When developing [Intranet](https://machinaex.de/intranet-2024/) it turned out that when you are working with about 1500 Audiofiles it helps to not show them all at once in the MEDIA Toolbar. There is a tree view now that lets you open and close directories 🙌
* You can use right click 🖱️ to open the context menu of States and Actions. And there is a new context menu for the Stage that allows to create an empty State or paste Actions from clipboard.
* Working with (bigger) collections was made more convenient. There is a new "view only" option and *edit*, *delete* and *view* buttons are fixed to the left. Also the max height when inspecting a collection is limited to 5 Elements.

![Right click to open context menu on an action then copy, then right click on the stage and paste](./assets/rightclick_context_menu.gif)

**Version 2.8.** in a tidy list:

* Added new [Socket.IO Plugin](https://docs.adaptorex.org/basics/plugins/socketio.html)
* Made [Iterate](https://docs.adaptorex.org/basics/actions/logic/iterate.html) action reset if it has ended before. Iteration will restart next time the state is triggered.
* "one word" variable content will be replaced if you drop media paths or variables
* Collections max height view is 5 elements
* Collection edit, delete and view buttons are fixed to the left
* Added view whole collection document option
* Added media tree view for folder structures
* Don't empty cache on paste when copying Actions
* Added right click context menu for the stage, states and actions
* Added copy button to read only form fields
* Several minor bug fixes and improvements