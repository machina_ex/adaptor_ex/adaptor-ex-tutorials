---
date: 2023-12-08
categories:
  - Info
tags:
  - helper
  - de
authors:
  - lasse
comments: true
---

Web APP zum verwalten von adaptor:ex Live Shows
================================================

Für einige der letzten Produktionen von [machina eX](https://machinaex.de) mussten wir die Validierung der Tickets in das Game integrieren. Etwa wenn das Publikum wie in [Homecoming](https://machinaex.org/homecoming-2020/) zuhause gespielt hat oder wie bei [Patrol](https://machinaex.org/patrol-2019/) und [Wenn der Regen kommt](https://machinaex.org/wenn-der-regen-kommt/) das Spiel alleine im Telegram Messenger startet ohne zu Begin der Aufführung bei uns vorbeizukommen. 

Dabei ist eine Web APP entstanden die uns hilft die Verwaltung der Veranstaltungstermine und den Ticket und Registrierungsprozess zu organisieren.

<!-- more -->

Die APP greift auf adaptor:ex zu und erlaubt es Veranstalter (venues) und Termine (shows) anzulegen. In Zusammenarbeit mit dem HAU haben wir Sie auch genutzt um von Reservix generierte Ticket Codes, die erst beim Ticketkauf erstellt werden, nach und nach einzupflegen.

Der Source Code ist jetzt öffentlich und MIT lizenziert. Schreibt gern, wenn ihr Interesse habt die Software auszuprobieren, es sind sicher noch einige Fragen offen, wie das ganze genau zum Einsatz kommen kann.

Hier entlang: [gitlab.com/machina_ex/adaptor_ex/helper/scheduling](https://gitlab.com/machina_ex/adaptor_ex/helper/scheduling) 🗓️

![Screenshot Schedule Web APP](./assets/schedule-screenshot_overview.png)