---
date: 2022-11-16
categories:
  - Showcase
tags:
  - theater
  - game
  - socketio
  - de
authors:
  - philip
comments: false
---

Life Goes On on Stage und Online
================================

[machina eX](https://machinaex.org)' neuestes Theater Game (jetzt auch für zuhause) komplett gebaut in adaptor:ex:

[machinaex.de/life-goes-on/](https://machinaex.org/life-goes-on/)