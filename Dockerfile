FROM squidfunk/mkdocs-material
RUN pip install mkdocs-static-i18n
RUN pip install mkdocs-git-revision-date-localized-plugin