adaptor:ex tutorials
====================

<img src="./docs/assets/docs-logo-square-color-large.png" alt="adaptor:ex documentation logo" width="20%"></img>

User tutorials and documentation introducing basic functions of adaptor eX client and server. Find the static website created from this repository here:

[machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/](https://machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/)

[docs.adaptorex.org](https://docs.adaptorex.org) is an alias.

Since the current contributing developers are all native german speakers we will start making those in German language and add translations as needed and possible.

Tutorials and docs are in the [docs](./docs) directory. A pipeline is active that recreates the documentation website on each commit to the main branch.

After commit, check if the pipeline did pass.

> The job will fail if there are broken links in any of the markdown files!

If you add a new page don't forget to add it to the navigation tree. Page Navigation is defined in the [mkdocs.yml](./mkdocs.yml) configuration file.

These documentations are hosted with [gitlab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) using [mkdocs](https://gitlab.com/pages/mkdocs) with [material theme](https://github.com/squidfunk/mkdocs-material). Check the mkdocs [material theme documentation](https://squidfunk.github.io/mkdocs-material/reference/abbreviations/) for details on markdown styling options.

Contributing
------------

We are very much open to contributions. This repository is a great starting place to do just that!

Because here you can learn the basic usage und functions of adaptor:ex and tell us, what you did not understand, would need more explanations on etc.

Feel free to submit issues or (even better): fork this repository, add the changes you think make sense and open a merge request.

If you find typos or translation issues please don't hesitate to contribute a fix!

Development
-----------

Clone this repository and use docker compose to serve a local preview of the documentation.

``` console
docker compose build
docker compose up -d
```

Navigato to [localhost:8000](http://localhost:8080) to view the page with your local changes. The page should update automatically but might take some time to refresh.

Check `docker compose logs` to look out for potential errors if the page does not load properly.

Translation
-----------

As of today we try to provide an english and a german language file for each page in this documentation.

We use the [static i18n](https://github.com/ultrabug/mkdocs-static-i18n) language plugin for mkdocs.

If you want to add a german translation for a file it needs to be in the same directory as the original english language file and have the same name with the extension `de.md` e.g.:

```
index.md
index.de.md
```

If the page navigation title differs, you need to add the title translation in [mkdocs.yml](./mkdocs.yml) to the `nav_translations` property:

```yml
plugins:
  - i18n:
      default_language: de
      languages:
        ...: ...
      nav_translations:
        en:
          Glossary: Glossar
          ...: ...
          'My new Tutorial': 'Mein neues Tutorial' 
```

The pages position in the nav tree does not matter. Nav translations are all on the same level.

If you want to add a new language (nice!!) you need to add the language name and title to the `languages` property in [mkdocs.yml](./mkdocs.yml):

``` yml
plugins:
  - i18n:
      default_language: de
      languages:
        en: 
          name: English
          site_name: adaptor:ex Tutorials and Documentation
        de: 
          name: Deutsch
          site_name: adaptor:ex Tutorials und Dokumentation
        no:
          name: Norsk
          site_name: adaptor:ex Tutorials og Dokumentasjon
```

License
--------

Source code licensed MIT

Authors and acknowledgment
---------------------------

This is a machina commons project by [machina eX](https://machinaex.com).

<br/>

gefördert durch

<img src="./docs/assets/logo_senatsverwaltung_berlin.jpg" alt="Logo Senatsverwaltung für Kultur und Europa Berlin" width="50%"></img>

Senatsverwaltung für Kultur und Europa Berlin

Documentation built with 

[![Material for MkDocs](https://img.shields.io/badge/Material_for_MkDocs-526CFE?style=for-the-badge&logo=MaterialForMkDocs&logoColor=white)](https://squidfunk.github.io/mkdocs-material/)